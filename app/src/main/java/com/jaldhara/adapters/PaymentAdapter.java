package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.PaymentModel;
import com.jaldhara.models.PurchaseModel;

import java.util.List;

public class PaymentAdapter extends RecyclerView.Adapter<PaymentAdapter.MyViewHolder> {

    private Context context;
    private List<PaymentModel> paymentList;
    private onItemClick<PaymentModel> onItemClick;

    public PaymentAdapter(Context context, List<PaymentModel> paymentList) {
        this.context = context;
        this.paymentList = paymentList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_payments, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final PaymentModel paymentModel = paymentList.get(position);

        holder.tvDate.setText(paymentModel.getDate());
        holder.tvReference.setText(paymentModel.getReferenceNo());
        holder.tvAmount.setText(paymentModel.getAmount());
        holder.tvPaidBy.setText(paymentModel.getPaidBy());

        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClick != null){
                    onItemClick.onEditClicked(paymentModel);
                }
            }
        });

        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClick != null){
                    onItemClick.onDeleteClicked(paymentModel);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return paymentList.size();
    }

    public void setOnItemClick(PaymentAdapter.onItemClick<PaymentModel> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public interface onItemClick<T> {
        void onEditClicked(T data);

        void onDeleteClicked(T data);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDate, tvReference, tvAmount, tvPaidBy;
        private ImageButton btnEdit, btnDelete;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.raw_payments_txt_date);
            tvReference = itemView.findViewById(R.id.raw_payments_txt_reference_number);
            tvAmount = itemView.findViewById(R.id.raw_payments_txt_amount);
            tvPaidBy = itemView.findViewById(R.id.raw_purchase_txt_paidby);
            btnEdit = itemView.findViewById(R.id.imgBtnEdit);
            btnDelete = itemView.findViewById(R.id.imgBtnDelete);
        }
    }
}
