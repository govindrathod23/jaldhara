package com.jaldhara.adapters;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Paint;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.AspectRatioImageView;
import com.jaldhara.utills.Util;
import com.jaldhara.view.customer.PromotionOffersActivity;
import com.smarteist.autoimageslider.SliderView;
import com.smarteist.autoimageslider.SliderViewAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromotionOffersAdapter extends SliderViewAdapter<PromotionOffersAdapter.SliderAdapterViewHolder> {

    private ArrayList<SpinnerModel> arryRetuen;
    private Context mContext;
    private RequestOptions requestOptions;


    // Provide a suitable constructor (depends on the kind of dataset)
    public PromotionOffersAdapter(Context context, ArrayList<SpinnerModel> arryRetuen) {
        this.mContext = context;
        this.arryRetuen = arryRetuen;
        requestOptions = new RequestOptions();
        requestOptions.placeholder(R.mipmap.ic_app_logo);
        requestOptions.fitCenter();

    }


     public static class SliderAdapterViewHolder extends SliderViewAdapter.ViewHolder {
        // Adapter class for initializing
        // the views of our slider view.
        // each data item is just a string in this case
        private TextView txtAssetsName;
        private TextView txtPrice;
        private TextView txtPromoPrice;

        private CheckBox chkboxStar;
        private com.jaldhara.utills.AspectRatioImageView img;

        public SliderAdapterViewHolder(View v) {
            super(v);
            txtAssetsName = (TextView) v.findViewById(R.id.raw_add_assets_txt_name);
            txtPrice = (TextView) v.findViewById(R.id.raw_add_assets_txt_price);
            txtPromoPrice = (TextView) v.findViewById(R.id.raw_add_assets_txt_promo_price);
            chkboxStar = (CheckBox) v.findViewById(R.id.raw_promotion_chkbx_star);
            img = (com.jaldhara.utills.AspectRatioImageView)v.findViewById(R.id.img);
        }
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final SliderAdapterViewHolder holder, final int position) {

        final SpinnerModel SpinnerModel = arryRetuen.get(position);

        holder.txtAssetsName.setText(SpinnerModel.getRouteName());
        holder.txtPrice.setText("Rs." + SpinnerModel.getProductPrice());
        holder.txtPrice.setPaintFlags(holder.txtPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
//        if (SpinnerModel.isSelected()) {
//        holder.checkBox.setChecked(SpinnerModel.isSelected());
//        } else {
//        }

        Glide.with(mContext)
                .applyDefaultRequestOptions(requestOptions)
                .load(SpinnerModel.getImage_url())
                .into(holder.img);

        holder.chkboxStar.setChecked(SpinnerModel.isWishItem());

        holder.chkboxStar.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                String APIname = "";
                SpinnerModel.setWishItem(isChecked);

                if (isChecked){
                    APIname = Util.API_ADD_WISHLIST;
                }else{
                    APIname = Util.API_REMOVE_WISHLIST ;
                }
                serviceAddRemoveWishlist(APIname,SpinnerModel.getRouteId());
            }
        });




    }


    @Override
    public SliderAdapterViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_promotion, null);
        return new SliderAdapterViewHolder(inflate);
    }

    @Override
    public int getCount() {
        return arryRetuen.size();
    }

    private void serviceAddRemoveWishlist(String wishAPIName,String productID) {

        final ProgressDialog dialog = ProgressDialog.show(mContext, "Please wait", "Getting Products", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.editWishList(
                wishAPIName,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(mContext.getString(R.string.key_prefrence_company_id)),
                productID
                );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
//                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        //JSONArray jsonArray = jsonResponse.optJSONArray("data");
                    }

                }  catch (Exception e) {
                    Toast.makeText(mContext, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(mContext, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }




}