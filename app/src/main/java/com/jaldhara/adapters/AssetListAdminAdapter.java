package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.AssetsModel;
import com.jaldhara.models.ProductModel;

import java.util.List;

public class AssetListAdminAdapter extends RecyclerView.Adapter<AssetListAdminAdapter.MyViewHolder> {

    private Context context;
    private List<AssetsModel> assetList;
    private onItemClick<AssetsModel> onItemClick;

    public AssetListAdminAdapter(Context context, List<AssetsModel> assetList) {
        this.context = context;
        this.assetList = assetList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_asset_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final AssetsModel productModel = assetList.get(position);

        holder.card_raw.setTag(position);

        holder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(context, holder.btnAction);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.poupup_menu_raw_asset_action, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_delete) {
                            if (onItemClick != null) {
                                onItemClick.onDeleteClicked(productModel);
                            }
                        } else if (item.getItemId() == R.id.action_edit) {
                            if (onItemClick != null) {
                                onItemClick.onEditPurchaseClicked(productModel);
                            }
                        }
                        return true;
                    }
                });
                popup.show();//showing popup menu
            }
        });

        holder.txtCode.setText(productModel.getBiller());

    }

    @Override
    public int getItemCount() {
        return assetList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName, txtCode, txtBrand, txtCategory, txtCost, txtPrice, txtQuantity, txtUnit, txtAlertQuantity;
        private Button btnAction;
        private LinearLayout card_raw;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_ProductName);
            txtCode = itemView.findViewById(R.id.txt_code);
            txtBrand = itemView.findViewById(R.id.txt_Brand);
            txtCategory = itemView.findViewById(R.id.txt_category);
            txtCost = itemView.findViewById(R.id.txt_Cost);
            txtPrice = itemView.findViewById(R.id.txt_Price);
            txtQuantity = itemView.findViewById(R.id.txt_Quantity);
            txtUnit = itemView.findViewById(R.id.txt_Unit);
            txtAlertQuantity = itemView.findViewById(R.id.txt_AlertQuantity);

            btnAction = itemView.findViewById(R.id.raw_purchase_button_action);

            card_raw = itemView.findViewById(R.id.card_raw);
        }
    }

    public interface onItemClick<T> {
        void onDetailClicked(T data);

        void onViewPaymentClicked(T data);

        void onAddPaymentClicked(T data);

        void onDeleteClicked(T data);

        void onEditPurchaseClicked(T data);
    }

    public void setOnItemClick(AssetListAdminAdapter.onItemClick<AssetsModel> onItemClick) {
        this.onItemClick = onItemClick;
    }
}
