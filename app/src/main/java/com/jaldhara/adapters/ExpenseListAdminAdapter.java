package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.ExpensesModel;
import com.jaldhara.utills.Util;

import java.util.List;

public class ExpenseListAdminAdapter extends RecyclerView.Adapter<ExpenseListAdminAdapter.MyViewHolder> {

    private Context mContext;
    private List<ExpensesModel> expenseList;
    private onItemClick<ExpensesModel> onItemClick = null;

    public ExpenseListAdminAdapter(Context mContext, List<ExpensesModel> expenseList) {
        this.mContext = mContext;
        this.expenseList = expenseList;
    }

    public void updateList(List<ExpensesModel> expenseList) {
        this.expenseList = expenseList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_expenses, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final ExpensesModel expensesModel = expenseList.get(position);
        holder.tvCreatedBy.setText(expensesModel.getFirstName() + " " + expensesModel.getLastName());
        holder.tvReferance.setText(expensesModel.getReference());
        holder.tvDate.setText(expensesModel.getDate());
        holder.tvCategory.setText(expensesModel.getCategoryId());
        holder.tvAmount.setText(expensesModel.getAmount());
        holder.tvNote.setText(Util.htmlToText(expensesModel.getNote()));

        holder.imgBtnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClick != null) {
                    onItemClick.onEditClick(expensesModel);
                }
            }
        });

        holder.imgBtnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (onItemClick != null) {
                    onItemClick.onDeleteClick(expensesModel);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return expenseList.size();
    }


    public interface onItemClick<T> {
        void onEditClick(T data);

        void onDeleteClick(T data);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        final private TextView tvCreatedBy, tvDate, tvCategory, tvAmount, tvNote, tvReferance;
        final private ImageButton imgBtnEdit, imgBtnDelete;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvCreatedBy = itemView.findViewById(R.id.raw_expenses_txt_createdBy);
            tvReferance = itemView.findViewById(R.id.raw_expenses_txt_Referance);
            tvDate = itemView.findViewById(R.id.raw_expenses_txt_date);
            tvCategory = itemView.findViewById(R.id.raw_expenses_txt_category);
            tvAmount = itemView.findViewById(R.id.raw_expenses_txt_amount);
            tvNote = itemView.findViewById(R.id.raw_expenses_txt_note);
            imgBtnDelete = itemView.findViewById(R.id.imgBtnDelete);
            imgBtnEdit = itemView.findViewById(R.id.imgBtnEdit);

        }
    }

    public void setOnItemClick(ExpenseListAdminAdapter.onItemClick<ExpensesModel> onItemClick) {
        this.onItemClick = onItemClick;
    }
}