package com.jaldhara.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.utills.LabeledSwitch;
import com.jaldhara.utills.OnToggledListener;
import com.jaldhara.utills.SwitchTrackTextDrawable;
import com.jaldhara.utills.ToggleableView;
import com.jaldhara.utills.onRemoveClick;
import com.jaldhara.view.customer.ViewCustomerReturnActivity;

import java.util.ArrayList;
import java.util.stream.DoubleStream;

public class BottleReturnListAdapter extends RecyclerView.Adapter<BottleReturnListAdapter.MyViewHolder> {

    private ArrayList<DeliveryModel> arryRetuen;
    private Context mContext;
    private onRemoveClick removeClickListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public BottleReturnListAdapter(Context context, ArrayList<DeliveryModel> arryRetuen) {
        this.mContext = context;
        this.arryRetuen = arryRetuen;
//        this.onRecycleViewItemClick = (com.nc4.utills.onRecycleViewItemClick) context;
//        this.onRemoveClick = (onRemoveClick) ((BottleSettlementInfoTab) context);
        setHasStableIds(true);
    }



    public void setRemoveClickListener(onRemoveClick removeClickListener) {
        this.removeClickListener = removeClickListener;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {

        // each data item is just a string in this case
        private TextView txtBottleBrand;
        private EditText edtBottleCount;
        private ImageView imgClear;
        private LinearLayout cardRow;
        private LabeledSwitch switchCompat;
        private LinearLayout card_raw_1;
        private EditText edtBottleAdjCount;


        public MyViewHolder(View v) {
            super(v);
            txtBottleBrand = (TextView) v.findViewById(R.id.raw_add_bottle_txt_bottleBrand);
            edtBottleCount = (EditText) v.findViewById(R.id.raw_add_bottle_edt_count);
            imgClear = (ImageView) v.findViewById(R.id.raw_add_bottle_img_remove);
            cardRow = (LinearLayout) v.findViewById(R.id.card_raw);
            switchCompat = (LabeledSwitch)v.findViewById(R.id.raw_return_bottle_switch);

            card_raw_1 = (LinearLayout) v.findViewById(R.id.card_raw_1);
            edtBottleAdjCount = (EditText) v.findViewById(R.id.raw_add_bottle_edt_adj_count);

        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public BottleReturnListAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_return_bottle, parent, false);
//        v.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                Intent intent = new Intent(mContext, ViewIncidentInfo.class);
////                mContext.startActivity(intent);
// //            }
//        });


        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final DeliveryModel bottleInfoModel = arryRetuen.get(position);


        holder.txtBottleBrand.setText(bottleInfoModel.getProduct_name());
        holder.edtBottleCount.setText(bottleInfoModel.getQuantity());

        holder.edtBottleCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bottleInfoModel.setQuantity(holder.edtBottleCount.getText().toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.edtBottleAdjCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                bottleInfoModel.setAdjustmentQuantity(holder.edtBottleAdjCount.getText().toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

//        holder.switchCompat.setTrackDrawable(new SwitchTrackTextDrawable(mContext,
//                 R.string.lbl_Yes ,  R.string.lbl_No  ));

        if (mContext instanceof ViewCustomerReturnActivity) {
            holder.edtBottleCount.setEnabled(false);
            holder.switchCompat.setVisibility(View.GONE);
        }

//        if (bottleInfoModel.isSwitchOn()){
            holder.card_raw_1.setVisibility(bottleInfoModel.isSwitchOn()?View.VISIBLE:View.GONE);
//        }else{
//            holder.card_raw_1.setVisibility(isOn?View.VISIBLE:View.GONE);
//        }

        holder.switchCompat.setOnToggledListener(new OnToggledListener() {
            @Override
            public void onSwitched(ToggleableView toggleableView, boolean isOn) {

                if (isOn) {
                    AlertDialog.Builder alertDialogBuilder =
                            new AlertDialog.Builder(mContext)
                                    .setTitle("Confirmation!")
                                    .setMessage("Your confirmation will make adjust empty bottle quantity")
                                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            bottleInfoModel.setSwitchOn(isOn);
                                            holder.card_raw_1.setVisibility(isOn?View.VISIBLE:View.GONE);
                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            holder.switchCompat.setOn(false);
                                            dialog.cancel();
                                        }
                                    });

                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                }else{

                    bottleInfoModel.setSwitchOn(isOn);
                    holder.card_raw_1.setVisibility(isOn?View.VISIBLE:View.GONE);
                }


            }
        });

        holder.imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeClickListener.onClick(position);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryRetuen.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}