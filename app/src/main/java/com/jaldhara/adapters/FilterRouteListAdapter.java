package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.view.admin.pendingorder.FilterActivity;
import com.jaldhara.view.admin.pendingorder.FilterDeliveryStatusFragment;
import com.jaldhara.view.admin.pendingorder.FilterDriverFragment;
import com.jaldhara.view.admin.pendingorder.FilterRouteFragment;

import java.util.ArrayList;

public class FilterRouteListAdapter extends RecyclerView.Adapter<FilterRouteListAdapter.MyViewHolder> {

    private ArrayList<SpinnerModel> arryRouteList;
    private Context mContext;
    private int prePosition = -1;
    private String className = "";


    // Provide a suitable constructor (depends on the kind of dataset)
    public FilterRouteListAdapter(Context context, ArrayList<SpinnerModel> arryRouteList, String className) {
        this.mContext = context;
        this.arryRouteList = arryRouteList;
        this.className = className;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txtRouteDriverName;
        private TextView txtAssetsID;
        private LinearLayout cardRow;


        public MyViewHolder(View v) {
            super(v);
            txtRouteDriverName = (TextView) v.findViewById(R.id.raw_bottle_txt_AssetsName);
            txtAssetsID = (TextView) v.findViewById(R.id.raw_bottle_txt_assetsID);
            cardRow = (LinearLayout) v.findViewById(R.id.card_raw);

        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public FilterRouteListAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_assets, parent, false);
//        v.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                Intent intent = new Intent(mContext, ViewIncidentInfo.class);
////                mContext.startActivity(intent);
//
//
//
//
//            }
//        });

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final SpinnerModel bottleInfoModel = arryRouteList.get(position);
        holder.txtRouteDriverName.setText(bottleInfoModel.getRouteName());

        holder.txtAssetsID.setVisibility(View.GONE);

        if (bottleInfoModel.isSelected()) {
            holder.cardRow.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
        } else {
            holder.cardRow.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
        }





        holder.cardRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (prePosition != -1) {
                    arryRouteList.get(prePosition).setSelected(false);
                    holder.cardRow.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));

                }
                arryRouteList.get(position).setSelected(true);
                holder.cardRow.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
                notifyDataSetChanged();
                prePosition = position;

                if (className.equals(FilterRouteFragment.class.getName().toString())) {
                    FilterActivity.ROUTE_ID = bottleInfoModel.getRouteName() + " : "+bottleInfoModel.getRouteId();
                } else if (className.equals(FilterDeliveryStatusFragment.class.getName().toString())) {
                    FilterActivity.SALE_STATUS = bottleInfoModel.getRouteName() ;
                } else if (className.equals(FilterDriverFragment.class.getName().toString())) {
                    FilterActivity.DRIVER_ID = bottleInfoModel.getRouteName()  + " : "+ bottleInfoModel.getRouteId();
                }
            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryRouteList.size();
    }
}