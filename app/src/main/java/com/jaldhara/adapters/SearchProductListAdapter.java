package com.jaldhara.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.utills.onCustomerClick;
import com.jaldhara.utills.onRemoveClick;
import com.jaldhara.view.admin.pendingorder.FilterActivity;

import java.util.ArrayList;

public class SearchProductListAdapter extends RecyclerView.Adapter<SearchProductListAdapter.MyViewHolder> {

    private ArrayList<SpinnerModel> arryCustomer;
    private Context mContext;
    private String customerID = "";

    private onCustomerClick adapterOnclick;

    // Provide a suitable constructor (depends on the kind of dataset)
    public SearchProductListAdapter(Context context, ArrayList<SpinnerModel> arryCustomer, onCustomerClick customerClick) {
        this.mContext = context;

        this.arryCustomer = arryCustomer;
        setHasStableIds(true);
        this.adapterOnclick =  customerClick;

    }

    private onRemoveClick removeClickListener;




    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public  class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txtCustomerName;
        private TextView txtid;
        private LinearLayout cardRow;


        public MyViewHolder(View v) {
            super(v);
            txtCustomerName = (TextView) v.findViewById(R.id.raw_customer_txt_customerName);
            txtid = (TextView) v.findViewById(R.id.raw_customer_txt_customerID);
            cardRow = (LinearLayout) v.findViewById(R.id.card_raw);

        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_search_customer, parent, false);
            return new SearchProductListAdapter.MyViewHolder(v);

    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder  holder, final int position) {

        final SpinnerModel bottleInfoModel = arryCustomer.get(position);
        holder.txtCustomerName.setText(bottleInfoModel.getRouteName());
        holder.txtid.setText( "ID: "+bottleInfoModel.getRouteId());
        holder.cardRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapterOnclick.onCustomerRowClick(position);

                if (!TextUtils.isEmpty( customerID)) {
                    holder.cardRow.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
                }
                holder.cardRow.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
                notifyDataSetChanged();
                customerID = bottleInfoModel.getRouteId();
                FilterActivity.CUSTOMER_ID = customerID;
            }
        });

        if (bottleInfoModel.getRouteId().equals(customerID)){
            holder.cardRow.setBackgroundColor(mContext.getResources().getColor(R.color.colorAccent));
        }else {
            holder.cardRow.setBackgroundColor(mContext.getResources().getColor(android.R.color.transparent));
        }


    }

    @Override
    public int getItemCount() {
        return arryCustomer.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}