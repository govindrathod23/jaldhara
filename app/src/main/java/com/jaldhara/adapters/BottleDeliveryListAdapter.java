package com.jaldhara.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.utills.Util;
import com.jaldhara.utills.onRemoveClick;
import com.jaldhara.view.customer.ViewCustomerDeliveryActivity;

import java.text.ParseException;
import java.util.ArrayList;

public class BottleDeliveryListAdapter extends RecyclerView.Adapter<BottleDeliveryListAdapter.MyViewHolder> {

    private ArrayList<DeliveryModel> arryRetuen;
    private Context mContext;
//    private onRemoveClick onRemoveClick;


    // Provide a suitable constructor (depends on the kind of dataset)
    public BottleDeliveryListAdapter(Context context, ArrayList<DeliveryModel> arryRetuen) {
        this.mContext = context;
        this.arryRetuen = arryRetuen;
//        this.onRecycleViewItemClick = (com.nc4.utills.onRecycleViewItemClick) context;
//        this.onRemoveClick = (onRemoveClick) ((BottleSettlementInfoTab) context);
        setHasStableIds(true);
    }

    private onRemoveClick removeClickListener;

    public void setRemoveClickListener(onRemoveClick removeClickListener) {
        this.removeClickListener = removeClickListener;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txtBottleBrand;
        private EditText edtBottleCount;
        private ImageView imgClear;
        private LinearLayout cardRow;
        private TextView txtOrderID,txtOrderDate;


        public MyViewHolder(View v) {
            super(v);
            txtBottleBrand = (TextView) v.findViewById(R.id.raw_add_bottle_txt_bottleBrand);
            edtBottleCount = (EditText) v.findViewById(R.id.raw_add_bottle_edt_count);
            imgClear = (ImageView) v.findViewById(R.id.raw_add_bottle_img_remove);
            cardRow = (LinearLayout) v.findViewById(R.id.card_raw);
            txtOrderID = (TextView)v.findViewById(R.id.raw_add_bottle_txt_orderID);
            txtOrderDate = (TextView)v.findViewById(R.id.raw_add_bottle_txt_orderDate);

        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public BottleDeliveryListAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_add_bottle, parent, false);
//        v.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                Intent intent = new Intent(mContext, ViewIncidentInfo.class);
////                mContext.startActivity(intent);
//
//            }
//        });


        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final DeliveryModel bottleInfoModel = arryRetuen.get(position);

        holder.txtBottleBrand.setText(bottleInfoModel.getProduct_name());
        holder.edtBottleCount.setText(bottleInfoModel.getPendingQty());

        if (mContext instanceof ViewCustomerDeliveryActivity) {
            holder.edtBottleCount.setEnabled(false);
        }

        holder.txtOrderID.setVisibility((TextUtils.isEmpty(bottleInfoModel.getSale_reference_no()) || bottleInfoModel.getSale_reference_no().equalsIgnoreCase("null")) ? View.GONE : View.VISIBLE );
        holder.txtOrderID.setText(bottleInfoModel.getSale_reference_no());

        holder.txtOrderDate.setVisibility((TextUtils.isEmpty(bottleInfoModel.getOrderDate() )|| bottleInfoModel.getOrderDate().equalsIgnoreCase("null")) ? View.GONE : View.VISIBLE);

        try {
            holder.txtOrderDate.setText(Util.formatDateFromDateString("yyyy-MM-dd HH:mm:ss","dd MMM yyyy / hh:mm:aa",bottleInfoModel.getOrderDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.edtBottleCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                bottleInfoModel.setQuantity(holder.edtBottleCount.getText().toString());
                bottleInfoModel.setDelivered(holder.edtBottleCount.getText().toString());

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.imgClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeClickListener.onClick(position);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryRetuen.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}