package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;

import java.util.ArrayList;

public class BrandCustomerAdapter extends RecyclerView.Adapter<BrandCustomerAdapter.MyViewHolder> {

    private ArrayList<SpinnerModel> arryRetuen;
    private Context mContext;
    private RequestOptions requestOptions;


    // Provide a suitable constructor (depends on the kind of dataset)
    public BrandCustomerAdapter(Context context, ArrayList<SpinnerModel> arryRetuen) {
        this.mContext = context;
        this.arryRetuen = arryRetuen;
        requestOptions = new RequestOptions();
        requestOptions.placeholder(R.mipmap.ic_app_logo);
        requestOptions.fitCenter();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txtAssetsName;
        //        private TextView txtPrice;
//        private EditText edtQuantity;
        private AppCompatImageView img;

        public MyViewHolder(View v) {
            super(v);
            txtAssetsName = (TextView) v.findViewById(R.id.raw_add_assets_txt_name);
//            txtPrice = (TextView) v.findViewById(R.id.raw_add_assets_txt_price);
            //edtQuantity = (EditText) v.findViewById(R.id.raw_add_orders_edt_quantity);
            img = (AppCompatImageView) v.findViewById(R.id.img);

        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public BrandCustomerAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_brand_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final SpinnerModel SpinnerModel = arryRetuen.get(position);

        holder.txtAssetsName.setText(SpinnerModel.getRouteName());
//        holder.txtPrice.setText("Rs." + SpinnerModel.getProductPrice());

//        if (SpinnerModel.isSelected()) {
//        holder.checkBox.setChecked(SpinnerModel.isSelected());
//        } else {
//        }
//        holder.edtQuantity.setText(SpinnerModel.getQuantity());


        if (SpinnerModel.getImage_url() != null && SpinnerModel.getImage_url().length() > 0) {
            Glide.with(mContext)
                    .load(SpinnerModel.getImage_url())
                    .into(holder.img);
        } else {
            Glide.with(mContext)
                    .load(R.mipmap.ic_app_logo)
                    .into(holder.img);
        }

        //By Pratik
//        Glide.with(mContext)
//                .applyDefaultRequestOptions(requestOptions)
//                .load(SpinnerModel.getImage_url())
//                .into(holder.img);

//        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                SpinnerModel.setSelected(isChecked);
//            }
//        });

//        holder.edtQuantity.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                if (s.toString().equals("0")) {
//                    holder.edtQuantity.setText("");
//
//                } else if (!TextUtils.isEmpty(s.toString())) {
//                    SpinnerModel.setQuantity(holder.edtQuantity.getText().toString());
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryRetuen.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}