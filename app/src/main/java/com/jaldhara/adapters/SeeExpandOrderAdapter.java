package com.jaldhara.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.utills.Util;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;

public class SeeExpandOrderAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<DeliveryModel> _listDataHeader; // header titles
    private HashMap<DeliveryModel, List<DeliveryModel>> _listDataChild;

    public SeeExpandOrderAdapter(Context context, List<DeliveryModel> listDataHeader, HashMap<DeliveryModel, List<DeliveryModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final DeliveryModel childText = (DeliveryModel) getChild(groupPosition, childPosition);

//        if (convertView == null) {
        LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.raw_see_order_child, null);
//        }

        final TextView txtBottleBrand = (TextView) convertView.findViewById(R.id.raw_add_bottle_txt_bottleBrand);
        final EditText edtBottleCount = (EditText) convertView.findViewById(R.id.raw_add_bottle_edt_count);

        txtBottleBrand.setText(childText.getProduct_name());

        String btCount = childText.getPendingQty();
        edtBottleCount.setText(btCount);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
//		String headerTitle = (String) getGroup(groupPosition);
        final DeliveryModel headerTitleModel = (DeliveryModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.raw_see_order_parent, null);
        }

        final CardView card = (CardView) convertView.findViewById(R.id.raw_order_parent_card);
        card.setCardBackgroundColor(headerTitleModel.getSaleStatusColor());

        TextView txtCustomerName = (TextView) convertView.findViewById(R.id.raw_bottle_txt_CustomerName);
        txtCustomerName.setText(headerTitleModel.getCustomerName());
        TextView txtDate = (TextView) convertView.findViewById(R.id.raw_bottle_txt_Date);
        txtCustomerName.setCompoundDrawablesWithIntrinsicBounds(isExpanded ? 0 : R.drawable.ic_plus, 0, 0, 0);
        TextView lblListCustomerCount = (TextView) convertView.findViewById(R.id.raw_bottle_txt_BottleCount);
        txtCustomerName.setTypeface(null, Typeface.BOLD);

//        txtOrder.setText(headerTitleModel.getSale_reference_no());

        try {

            txtDate.setText(headerTitleModel.getSale_reference_no() + "\n" + Util.formatDateFromDateString("yyyy-MM-dd HH:mm:ss", "dd MMM yyyy / hh:mm:aa", headerTitleModel.getOrderDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        lblListCustomerCount.setText("(" + headerTitleModel.getBottleCount() + ")");

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
