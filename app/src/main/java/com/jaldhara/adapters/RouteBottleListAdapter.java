package com.jaldhara.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.view.deliveryboy.BottleManageTab;
import com.jaldhara.view.RouteMapActivity;

import java.util.ArrayList;

public class RouteBottleListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static final int LAYOUT_HEADER= 0;
    private static final int LAYOUT_CHILD= 1;

    private ArrayList<BottleInfoModel> arryIncident;
    private Context mContext;
    private LayoutInflater inflater;

    // Provide a suitable constructor (depends on the kind of dataset)
    public RouteBottleListAdapter(Context context, ArrayList<BottleInfoModel> arryIncident) {
        this.mContext = context;
        this.arryIncident = arryIncident;
//        this.onRecycleViewItemClick = (com.nc4.utills.onRecycleViewItemClick) context;
        inflater = LayoutInflater.from(context);
    }



    @Override
    public int getItemViewType(int position)
    {
        if(arryIncident.get(position).isHeader()) {
            return LAYOUT_HEADER;
        }else
            return LAYOUT_CHILD;
    }


    class MyViewHolderHeader extends RecyclerView.ViewHolder{

        private TextView txtRoute;
        private TextView txtCustomerOnRoute;

        public MyViewHolderHeader(View itemView) {
            super(itemView);

            txtRoute = (TextView) itemView.findViewById(R.id.raw_bottle_txt_assetsID);
            txtCustomerOnRoute = (TextView) itemView.findViewById(R.id.raw_bottle_txt_nameCustomerOnRoute);
        }

    }

    class MyViewHolderChild extends RecyclerView.ViewHolder{

        private TextView txtNameOfBottleWith;
        private TextView txtBottleCount;

        private LinearLayout cardRow;

        public MyViewHolderChild(View itemView) {
            super(itemView);

            txtNameOfBottleWith = (TextView) itemView.findViewById(R.id.raw_bottle_txt_AssetsName);
//            txtBottleCount = (TextView) itemView.findViewById(R.id.raw_bottle_txt_BottleCount);

            cardRow = (LinearLayout) itemView.findViewById(R.id.card_raw);
        }

    }




    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {

        RecyclerView.ViewHolder holder;
        if(viewType==LAYOUT_HEADER){
            View view = inflater.inflate(R.layout.raw_bottle_parent, parent, false);
            holder = new MyViewHolderHeader(view);
        }else {
            View view = inflater.inflate(R.layout.raw_bottle_child, parent, false);
            holder = new MyViewHolderChild(view);
        }



        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final BottleInfoModel bottleInfoModel = arryIncident.get(position);



        if(holder.getItemViewType()== LAYOUT_HEADER)
        {
            MyViewHolderHeader vaultItemHolder = (MyViewHolderHeader) holder;
            vaultItemHolder.txtRoute.setText(bottleInfoModel.getRouteName());
            vaultItemHolder.txtCustomerOnRoute.setText(bottleInfoModel.getCustomrsOnRoute());
            vaultItemHolder.txtCustomerOnRoute.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, RouteMapActivity.class);
                    intent.putExtra("size",bottleInfoModel.getCustomrsOnRoute());
                    mContext.startActivity(intent);
                }
            });
        }
        else {

            MyViewHolderChild vaultItemHolder = (MyViewHolderChild) holder;
            vaultItemHolder.txtNameOfBottleWith.setText(bottleInfoModel.getBottleIsWith());
            vaultItemHolder.txtBottleCount.setText("Has   "+bottleInfoModel.getBottleTotalCount()+"   bottle(s)");
            vaultItemHolder.cardRow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Intent intent = new Intent(mContext, BottleManageTab.class);
                    intent.putExtra("bottleInfoModel", bottleInfoModel);
                    mContext.startActivity(intent);
                }
            });
        }








    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryIncident.size();
    }





}