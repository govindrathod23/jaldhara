package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.OrderModel;
import com.jaldhara.models.PaymentInvoiceModel;

import java.util.List;

public class BwtnDateOrderAdapter extends RecyclerView.Adapter<BwtnDateOrderAdapter.MyViewHolder> {

    private Context context;
    private List<OrderModel> orderList;
    private onItemClick<PaymentInvoiceModel> onItemClick;

    public BwtnDateOrderAdapter(Context context, List<OrderModel> orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_bwtn_date_order, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final OrderModel orderModel = orderList.get(position);
//
        holder.tvDate.setText(orderModel.getDate());
        holder.tvReference.setText(orderModel.getReference_no());
        holder.tvBiller.setText(orderModel.getBiller());
        holder.tvCustomer.setText(orderModel.getCustomer());


        holder.tvGrandTotal.setText(orderModel.getGrand_total());
        holder.chkBox.setChecked(orderModel.isChecked());

        holder.chkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderModel.setChecked(holder.chkBox.isChecked());
            }
        });

//        holder.btnEdit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (onItemClick != null){
//                    onItemClick.onEditClicked(paymentModel);
//                }
//            }
//        });
//
//        holder.btnDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                if (onItemClick != null){
//                    onItemClick.onDeleteClicked(paymentModel);
//                }
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }



    public void setOnItemClick(BwtnDateOrderAdapter.onItemClick<PaymentInvoiceModel> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public interface onItemClick<T> {
        void onEditClicked(T data);

        void onDeleteClicked(T data);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDate, tvReference, tvBiller, tvCustomer, tvGrandTotal;
        private CheckBox chkBox;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.raw_payments_txt_date);
            tvReference = itemView.findViewById(R.id.raw_payments_txt_reference_number);
            tvBiller = itemView.findViewById(R.id.raw_payments_txt_Biller);
            tvCustomer = itemView.findViewById(R.id.raw_purchase_txt_customer);
            tvGrandTotal = itemView.findViewById(R.id.raw_purchase_txt_grand_total);
            chkBox = itemView.findViewById(R.id.chkBox);
        }
    }
}
