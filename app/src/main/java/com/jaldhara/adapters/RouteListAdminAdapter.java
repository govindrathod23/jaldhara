package com.jaldhara.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.ExpensesModel;
import com.jaldhara.utills.onRemoveClick;
import com.jaldhara.view.admin.CustomerFromRouteActivity;

import java.util.ArrayList;
import java.util.List;

public class RouteListAdminAdapter extends RecyclerView.Adapter<RouteListAdminAdapter.MyViewHolder> {

    private ArrayList<BottleInfoModel> arryRetuen;
    private Context mContext;
//    private onRemoveClick onRemoveClick;


    // Provide a suitable constructor (depends on the kind of dataset)
    public RouteListAdminAdapter(Context context, ArrayList<BottleInfoModel> arryRetuen) {
        this.mContext = context;
        this.arryRetuen = arryRetuen;
        setHasStableIds(true);
    }

    private onRemoveClick removeClickListener;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txtRouteName;
        private TextView txtRouteCode;
        private TextView txtDriverName;
        private TextView txtLoaderName;

        private LinearLayout cardRow;

        public MyViewHolder(View v) {
            super(v);
            txtRouteName = (TextView) v.findViewById(R.id.raw_add_bottle_txt_routeName);
            txtRouteCode = (TextView) v.findViewById(R.id.raw_add_bottle_txt_routeCode);
            txtDriverName = (TextView) v.findViewById(R.id.raw_add_bottle_txt_driverName);
            txtLoaderName = (TextView) v.findViewById(R.id.raw_add_bottle_txt_loaderName);
            cardRow = (LinearLayout) v.findViewById(R.id.card_raw);

        }
    }

    public void updateList(ArrayList<BottleInfoModel> expenseList) {
        this.arryRetuen = expenseList;
        notifyDataSetChanged();
    }


    // Create new views (invoked by the layout manager)
    @Override
    public RouteListAdminAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_add_route, parent, false);


        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final BottleInfoModel bottleInfoModel = arryRetuen.get(position);

        holder.txtRouteName.setText(bottleInfoModel.getRouteName());
        holder.txtRouteCode.setText(bottleInfoModel.getRouteCode());
        holder.txtDriverName.setText(TextUtils.isEmpty(bottleInfoModel.getDriverName()) ? "Not Assigned" : bottleInfoModel.getDriverName());
        holder.txtLoaderName.setText(TextUtils.isEmpty(bottleInfoModel.getLoaderName()) ? "Not Assigned" : bottleInfoModel.getLoaderName());

        holder.cardRow.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("StringFormatInvalid")
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CustomerFromRouteActivity.class);
                intent.putExtra(mContext.getString(R.string.key_intent_Route_ID), bottleInfoModel.getRouteID());
                intent.putExtra(mContext.getString(R.string.key_intent_Route_Name), bottleInfoModel.getRouteName());
                mContext.startActivity(intent);
            }
        });

    }

    private SpannableStringBuilder getSpannable(String label, String value) {

        SpannableStringBuilder builder = new SpannableStringBuilder();

        SpannableString redSpannable = new SpannableString(label);
        redSpannable.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.color_text_lbl)), 0, label.length(), 0);
        builder.append(redSpannable);

        SpannableString whiteSpannable = new SpannableString(value);
        whiteSpannable.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.color_text)), 0, value.length(), 0);
        builder.append(whiteSpannable);

        return builder;
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryRetuen.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}