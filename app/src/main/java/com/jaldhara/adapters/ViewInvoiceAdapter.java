package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.OrderModel;
import com.jaldhara.models.PaymentInvoiceModel;

import java.util.List;

public class ViewInvoiceAdapter extends RecyclerView.Adapter<ViewInvoiceAdapter.MyViewHolder> {

    private Context context;
    private List<OrderModel> orderList;
    private onItemClick<PaymentInvoiceModel> onItemClick;

    public ViewInvoiceAdapter(Context context, List<OrderModel> orderList) {
        this.context = context;
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_view_invoice, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final OrderModel orderModel = orderList.get(position);

        holder.tvDescription.setText("Description: " + orderModel.getProduct_name());
        holder.tvReference.setText("Reference No: " + orderModel.getReference_no());

        holder.tvHSNCode.setText("HSN Code: " + orderModel.getHsn_code());
        holder.tvQuantity.setText("Quantity: " + orderModel.getUnit_quantity());
        holder.tvUnitPrice.setText("Unit Price: " + orderModel.getUnit_quantity());
        holder.tvSerialNumber.setText("Sr No: " + orderModel.getSerial_no());
        holder.tvPendingQuantity.setText("Pending Qty: " + orderModel.getPending_qty());
        holder.tvSubTotal.setText("Sub Total: " + orderModel.getSubtotal());
    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }


    public void setOnItemClick(ViewInvoiceAdapter.onItemClick<PaymentInvoiceModel> onItemClick) {
        this.onItemClick = onItemClick;
    }

    public interface onItemClick<T> {
        void onEditClicked(T data);

        void onDeleteClicked(T data);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvDescription, tvReference, tvHSNCode, tvSerialNumber, tvQuantity, tvPendingQuantity, tvUnitPrice, tvSubTotal;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDescription = itemView.findViewById(R.id.raw_payments_txt_Description);
            tvReference = itemView.findViewById(R.id.raw_payments_txt_reference_number);
            tvHSNCode = itemView.findViewById(R.id.raw_payments_txt_HSN_code);
            tvSerialNumber = itemView.findViewById(R.id.raw_purchase_txt_SerialNumber);
            tvQuantity = itemView.findViewById(R.id.raw_purchase_txt_Quantity);
            tvPendingQuantity = itemView.findViewById(R.id.raw_purchase_txt_PendingQuantity);

            tvUnitPrice = itemView.findViewById(R.id.raw_purchase_txt_UnitPrice);
            tvSubTotal = itemView.findViewById(R.id.raw_purchase_txt_SubTotal);


        }
    }
}
