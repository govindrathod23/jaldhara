package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.ProductModel;
import com.jaldhara.models.PurchaseModel;
import com.jaldhara.models.SupplierModel;

import java.util.ArrayList;
import java.util.List;

public class ProductListAdminAdapter extends RecyclerView.Adapter<ProductListAdminAdapter.MyViewHolder> {

    private Context context;
    private List<ProductModel> productList;
    private onItemClick<ProductModel> onItemClick;

    public ProductListAdminAdapter(Context context, List<ProductModel> productList) {
        this.context = context;
        this.productList = productList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_product_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    /*Notify Data*/
    public void setRefreshList(List<ProductModel> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final ProductModel productModel = productList.get(position);

        holder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(context, holder.btnAction);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.poupup_menu_raw_product_action, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_delete) {
                            if (onItemClick != null) {
                                onItemClick.onDeleteClicked(productModel);
                            }
                        } else if (item.getItemId() == R.id.action_edit) {
                            if (onItemClick != null) {
                                onItemClick.onEditPurchaseClicked(productModel);
                            }
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }

        });

        holder.txtName.setText(productModel.getName());
        holder.txtCode.setText(productModel.getCode());
        holder.txtBrand.setText("N/A");
        holder.txtCategory.setText("N/A");
        holder.txtCost.setText("N/A");
        holder.txtPrice.setText(productModel.getPrice());
        holder.txtQuantity.setText("N/A");
        if (productModel.getUnit() != null) {
            if (productModel.getUnit().getName() != null && productModel.getUnit().getName().length() > 0) {
                holder.txtUnit.setText(productModel.getUnit().getName());
                holder.layParentUnit.setVisibility(View.VISIBLE);
            } else {
                holder.layParentUnit.setVisibility(View.GONE);
            }
        } else {
            holder.layParentUnit.setVisibility(View.GONE);
        }

        holder.txtAlertQuantity.setText("N/A");

    }

    @Override
    public int getItemCount() {
        return productList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName, txtCode, txtBrand, txtCategory, txtCost, txtPrice, txtQuantity, txtUnit, txtAlertQuantity;
        private Button btnAction;
        private LinearLayout layParentUnit;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_ProductName);
            txtCode = itemView.findViewById(R.id.txt_code);
            txtBrand = itemView.findViewById(R.id.txt_Brand);
            txtCategory = itemView.findViewById(R.id.txt_category);
            txtCost = itemView.findViewById(R.id.txt_Cost);
            txtPrice = itemView.findViewById(R.id.txt_Price);
            txtQuantity = itemView.findViewById(R.id.txt_Quantity);
            txtUnit = itemView.findViewById(R.id.txt_Unit);
            txtAlertQuantity = itemView.findViewById(R.id.txt_AlertQuantity);

            btnAction = itemView.findViewById(R.id.raw_purchase_button_action);
            layParentUnit = itemView.findViewById(R.id.layParentUnit);
        }
    }

    public interface onItemClick<T> {
        void onDetailClicked(T data);

        void onViewPaymentClicked(T data);

        void onAddPaymentClicked(T data);

        void onDeleteClicked(T data);

        void onEditPurchaseClicked(T data);
    }

    public void setOnItemClick(ProductListAdminAdapter.onItemClick<ProductModel> onItemClick) {
        this.onItemClick = onItemClick;
    }
}
