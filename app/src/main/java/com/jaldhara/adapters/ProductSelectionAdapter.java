package com.jaldhara.adapters;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.ProductModel;

import java.util.ArrayList;

public class ProductSelectionAdapter extends RecyclerView.Adapter<ProductSelectionAdapter.MyViewHolder> {

    private ArrayList<ProductModel> arryRetuen;
    private Context mContext;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProductSelectionAdapter(Context context, ArrayList<ProductModel> arryRetuen) {
        this.mContext = context;
        this.arryRetuen = arryRetuen;

    }

    // Create new views (invoked by the layout manager)
    @Override
    public ProductSelectionAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_products, parent, false);


        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final ProductModel productModel = arryRetuen.get(position);

        holder.tvProductName.setText(productModel.getCode() + "-" + productModel.getName());
        holder.tvNetUnitCost.setText("Rs." + productModel.getUnitPrice());

        if (productModel.getQuantity() != null && !productModel.getQuantity().isEmpty()) {
            holder.edtQuantity.setText(productModel.getQuantity());
        } else {
            holder.edtQuantity.setText("0");

        }

        if (productModel.getReturnQuantity() != null && !productModel.getReturnQuantity().isEmpty()) {
            holder.edtReturnQty.setText(productModel.getReturnQuantity());
        } else {
            holder.edtReturnQty.setText("0");
        }

        holder.edtReturnQty.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().equals("0")) {
                    holder.edtReturnQty.setText("");

                } else if (!TextUtils.isEmpty(s.toString())) {
                    productModel.setReturnQuantity(holder.edtReturnQty.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        holder.edtQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.toString().equals("0")) {
                    holder.edtQuantity.setText("");

                } else if (!TextUtils.isEmpty(s.toString())) {
                    productModel.setQuantity(holder.edtQuantity.getText().toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryRetuen.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView tvProductName, tvNetUnitCost;
        private EditText edtQuantity, edtReturnQty;

        public MyViewHolder(View v) {
            super(v);
            tvProductName = (TextView) v.findViewById(R.id.raw_products_tv_name);
            tvNetUnitCost = (TextView) v.findViewById(R.id.raw_products_tv_net_unit_cost);
            edtQuantity = (EditText) v.findViewById(R.id.raw_add_orders_edt_quantity);
            edtReturnQty = v.findViewById(R.id.raw_add_orders_edt_return_quantity);

        }
    }
}