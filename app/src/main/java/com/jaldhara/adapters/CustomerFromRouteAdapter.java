package com.jaldhara.adapters;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.utills.onRemoveClick;
import com.jaldhara.view.RouteMapActivity;
import com.jaldhara.view.admin.order.AdminAddOrdersActivity;

import java.util.ArrayList;

public class CustomerFromRouteAdapter extends RecyclerView.Adapter<CustomerFromRouteAdapter.MyViewHolder> {

    private ArrayList<BottleInfoModel> arryCustomer;
    private Context mContext;
//    private onRemoveClick onRemoveClick;


    // Provide a suitable constructor (depends on the kind of dataset)
    public CustomerFromRouteAdapter(Context context, ArrayList<BottleInfoModel> arryCustomer) {
        this.mContext = context;
        this.arryCustomer = arryCustomer;
        setHasStableIds(true);
    }

    private onRemoveClick removeClickListener;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txtCustomerName;
        private TextView txtAddress;
        private ImageView imgCall;
        private ImageView imgGmail;
        private ImageView imgLocation;
        private ImageView imgAddOrder;


        private LinearLayout cardRow;


        public MyViewHolder(View v) {
            super(v);
            txtCustomerName = (TextView) v.findViewById(R.id.raw_customer_txt_customerName);
            txtAddress = (TextView) v.findViewById(R.id.raw_customer_txt_address);
            imgCall = (ImageView) v.findViewById(R.id.raw_customer_call);
            imgGmail = (ImageView) v.findViewById(R.id.raw_customer_email);
            imgLocation = (ImageView) v.findViewById(R.id.raw_customer_location);
            imgAddOrder = (ImageView) v.findViewById(R.id.raw_customer_addOrder);


            cardRow = (LinearLayout) v.findViewById(R.id.card_raw);

        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public CustomerFromRouteAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_customer, parent, false);


        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final BottleInfoModel bottleInfoModel = arryCustomer.get(position);

        holder.txtCustomerName.setText(bottleInfoModel.getCompany() + " - " + bottleInfoModel.getCustomerName());
        holder.txtAddress.setText(bottleInfoModel.getAddress());

        holder.imgCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent dialIntent = new Intent(Intent.ACTION_DIAL);
                dialIntent.setData(Uri.parse("tel:" + bottleInfoModel.getPhone()));
                mContext.startActivity(dialIntent);

            }
        });
        holder.imgGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mailto = "mailto:govind.rathod23@gmail.com";

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse(mailto));

                try {
                    mContext.startActivity(emailIntent);
                } catch (ActivityNotFoundException e) {
                    //TODO: Handle case where no email app is available
                }

            }
        });
        holder.imgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (bottleInfoModel.getLatitude() != null && !TextUtils.isEmpty(bottleInfoModel.getLatitude()) && !bottleInfoModel.getLatitude().equalsIgnoreCase("null")) {
                    Intent intent = new Intent(mContext, RouteMapActivity.class);
                    intent.putExtra(mContext.getString(R.string.key_intent_Show_On_Map_For), "FromCustomerChild");
                    intent.putExtra("CustomerArray", bottleInfoModel);
                    mContext.startActivity(intent);
                } else {
                    Toast.makeText(mContext, "Location not updated for this customer", Toast.LENGTH_LONG).show();
                }

            }
        });
        holder.imgAddOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(mContext, AdminAddOrdersActivity.class);
                intent.putExtra(mContext.getString(R.string.key_intent_customer_id), bottleInfoModel.getCustomerID());
                intent.putExtra(mContext.getString(R.string.key_intent_customer_Name), bottleInfoModel.getCustomerName());
                mContext.startActivity(intent);

            }
        });

//        holder.cardRow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(mContext, bottleInfoModel.getRouteName(), Toast.LENGTH_LONG).show();
//            }
//        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryCustomer.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}