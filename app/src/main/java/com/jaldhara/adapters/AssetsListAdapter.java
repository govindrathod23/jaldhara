package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.DeliveryModel;

import java.util.ArrayList;

public class AssetsListAdapter extends RecyclerView.Adapter<AssetsListAdapter.MyViewHolder> {

    private ArrayList<DeliveryModel> arryBottleAssets;
    private Context mContext;


    // Provide a suitable constructor (depends on the kind of dataset)
    public AssetsListAdapter(Context context, ArrayList<DeliveryModel> arryBottleAssets) {
        this.mContext = context;
        this.arryBottleAssets = arryBottleAssets;
    }
    // Provide a suitable constructor (depends on the kind of dataset)
    public void updateList(ArrayList<DeliveryModel> arryBottleAssets) {
        this.arryBottleAssets = arryBottleAssets;
        notifyDataSetChanged();
    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txtAssetsName;
        private  TextView txtAssetsID;
        private LinearLayout cardRow;


        public MyViewHolder(View v) {
            super(v);
            txtAssetsName = ( TextView) v.findViewById(R.id.raw_bottle_txt_AssetsName);
            txtAssetsID = ( TextView) v.findViewById(R.id.raw_bottle_txt_assetsID);
            cardRow = (LinearLayout) v.findViewById(R.id.card_raw);

        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public AssetsListAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_assets, parent, false);
//        v.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
////                Intent intent = new Intent(mContext, ViewIncidentInfo.class);
////                mContext.startActivity(intent);
//
//
//
//
//            }
//        });

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final DeliveryModel bottleInfoModel = arryBottleAssets.get(position);

        holder.txtAssetsName.setText(bottleInfoModel.getProduct_name());
        holder.txtAssetsID.setText(bottleInfoModel.getSales_id());

        holder.cardRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryBottleAssets.size();
    }
}