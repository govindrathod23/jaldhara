package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jaldhara.R;
import com.jaldhara.models.CustomerCardModel;
import com.jaldhara.models.ProductModel;

import java.util.List;

public class CustomerCardAdapter extends RecyclerView.Adapter<CustomerCardAdapter.MyViewHolder> {

    private Context context;
    private List<CustomerCardModel> arrycustomerCard;
    private onItemClick<CustomerCardModel> onItemClick;
    private RequestOptions requestOptions;

    public CustomerCardAdapter(Context context, List<CustomerCardModel> arry) {
        this.context = context;
        this.arrycustomerCard = arry;
        requestOptions = new RequestOptions();
        requestOptions.placeholder(R.mipmap.ic_app_logo);
        requestOptions.fitCenter();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_customer_card_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final CustomerCardModel customerCardModel = arrycustomerCard.get(position);



        holder.txt_driverName.setText(customerCardModel.getDriver_name());
        holder.txt_date.setText(customerCardModel.getDate());
        holder.txt_products_name.setText(customerCardModel.getProducts_name());
        holder.txt_return_qty.setText(customerCardModel.getReturn_qty());
        holder.txt_delivered_qty.setText(customerCardModel.getDelivered_qty());
        holder.txt_balance_qty.setText(customerCardModel.getBalance_qty());
        Glide.with(context)
                .applyDefaultRequestOptions(requestOptions)
                .load(customerCardModel.getAttachment())
                .into(holder.img_signature);

    }

    @Override
    public int getItemCount() {
        return arrycustomerCard.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_driverName, txt_date, txt_products_name, txt_return_qty, txt_delivered_qty, txt_balance_qty ;
        private AppCompatImageView img_signature;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_driverName  = itemView.findViewById(R.id.txt_driverName);
            txt_date        = itemView.findViewById(R.id.txt_date);
            txt_products_name = itemView.findViewById(R.id.txt_products_name);
            txt_return_qty = itemView.findViewById(R.id.txt_return_qty);
            txt_delivered_qty = itemView.findViewById(R.id.txt_delivered_qty);
            txt_balance_qty = itemView.findViewById(R.id.txt_balance_qty);

            img_signature = itemView.findViewById(R.id.img_signature);


        }
    }

    public interface onItemClick<T> {
        void onDetailClicked(T data);

        void onViewPaymentClicked(T data);

        void onAddPaymentClicked(T data);

        void onDeleteClicked(T data);

        void onEditPurchaseClicked(T data);
    }

    public void setOnItemClick(CustomerCardAdapter.onItemClick<CustomerCardModel> onItemClick) {
        this.onItemClick = onItemClick;
    }
}
