package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.ProductModel;
import com.jaldhara.models.PurchaseModel;

import java.util.List;

public class PurchaseListAdminAdapter extends RecyclerView.Adapter<PurchaseListAdminAdapter.MyViewHolder> {

    private Context context;
    private List<PurchaseModel> purchaseList;
    private onItemClick<PurchaseModel> onItemClick;

    public PurchaseListAdminAdapter(Context context, List<PurchaseModel> purchaseList) {
        this.context = context;
        this.purchaseList = purchaseList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_purchase, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    /*Notify Data*/
    public void setRefreshList(List<PurchaseModel> purchaseList) {
        this.purchaseList = purchaseList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final PurchaseModel purchaseModel = purchaseList.get(position);

        holder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(context, holder.btnAction);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.poupup_menu_raw_purchase_action, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_purchase_delete_purchase) {
                            if (onItemClick != null) {
                                onItemClick.onDeleteClicked(purchaseModel);
                            }
                        } else if (item.getItemId() == R.id.action_purchase_detail) {
                            if (onItemClick != null) {
                                onItemClick.onDetailClicked(purchaseModel);
                            }
                        } else if (item.getItemId() == R.id.action_purchase_add_payment) {
                            if (onItemClick != null) {
                                onItemClick.onAddPaymentClicked(purchaseModel);
                            }
                        } else if (item.getItemId() == R.id.action_purchase_view_payments) {
                            if (onItemClick != null) {
                                onItemClick.onViewPaymentClicked(purchaseModel);
                            }
                        } else if (item.getItemId() == R.id.action_purchase_edit_purchase) {
                            if (onItemClick != null) {
                                onItemClick.onEditPurchaseClicked(purchaseModel);
                            }
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }

        });

        if (purchaseModel.getReferenceNo() != null && purchaseModel.getReferenceNo().length() > 0) {
            holder.txtReference.setText(purchaseModel.getReferenceNo());
            holder.txtReference.setVisibility(View.VISIBLE);
        } else {
            holder.txtReference.setVisibility(View.GONE);
        }
        if (purchaseModel.getDate() != null && purchaseModel.getDate().length() > 0) {
            holder.txtDate.setText(purchaseModel.getDate());
            holder.txtDate.setVisibility(View.VISIBLE);
        } else {
            holder.txtDate.setVisibility(View.GONE);
        }

        holder.txtSupplier.setText(purchaseModel.getSupplier());
        if (purchaseModel.getStatus() != null && purchaseModel.getStatus().length() > 0) {
            holder.txtPurchaseStatus.setText(purchaseModel.getStatus());
            holder.txtPurchaseStatus.setVisibility(View.VISIBLE);
        } else {
            holder.txtPurchaseStatus.setVisibility(View.GONE);
        }
        holder.txtGrandTotal.setText(purchaseModel.getGrandTotal());
        holder.txtPaid.setText(purchaseModel.getPaid());

        if (purchaseModel.getPaymentStatus() != null && purchaseModel.getPaymentStatus().length() > 0) {
            holder.txtPaymentStatus.setText(purchaseModel.getPaymentStatus());
            holder.txtPaymentStatus.setVisibility(View.VISIBLE);
        } else {
            holder.txtPaymentStatus.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return purchaseList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtReference, txtDate, txtSupplier, txtPurchaseStatus, txtGrandTotal, txtPaid, txtBalance, txtPaymentStatus;
        private Button btnAction;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtReference = itemView.findViewById(R.id.raw_purchase_txt_reference_number);
            txtDate = itemView.findViewById(R.id.raw_purchase_txt_date);
            txtSupplier = itemView.findViewById(R.id.raw_purchase_txt_supplier);
            txtPurchaseStatus = itemView.findViewById(R.id.raw_purchase_txt_purchase_status);
            txtGrandTotal = itemView.findViewById(R.id.raw_purchase_txt_grand_total);
            txtPaid = itemView.findViewById(R.id.raw_purchase_txt_paid);
            txtBalance = itemView.findViewById(R.id.raw_purchase_txt_balance);
            txtPaymentStatus = itemView.findViewById(R.id.raw_purchase_txt_payment_status);
            btnAction = itemView.findViewById(R.id.raw_purchase_button_action);
        }
    }

    public interface onItemClick<T> {
        void onDetailClicked(T data);

        void onViewPaymentClicked(T data);

        void onAddPaymentClicked(T data);

        void onDeleteClicked(T data);

        void onEditPurchaseClicked(T data);
    }

    public void setOnItemClick(PurchaseListAdminAdapter.onItemClick<PurchaseModel> onItemClick) {
        this.onItemClick = onItemClick;
    }
}
