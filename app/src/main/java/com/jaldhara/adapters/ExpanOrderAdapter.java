package com.jaldhara.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.SwitchCompat;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.jaldhara.R;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.utills.Util;
import com.jaldhara.view.RouteMapActivity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpanOrderAdapter extends BaseExpandableListAdapter {

    private Context _context;

    private List<DeliveryModel> _listDataHeader; // header titles
    private HashMap<DeliveryModel, List<DeliveryModel>> _listDataChild;

    public ExpanOrderAdapter(Context context, List<DeliveryModel> listDataHeader, HashMap<DeliveryModel, List<DeliveryModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final DeliveryModel childText = (DeliveryModel) getChild(groupPosition, childPosition);

//        if (convertView == null) {
        LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.raw_order_child, null);
//        }

        final TextView txtBottleBrand = (TextView) convertView.findViewById(R.id.raw_add_bottle_txt_bottleBrand);
        final EditText edtBottleCount = (EditText) convertView.findViewById(R.id.raw_add_bottle_edt_count);

        txtBottleBrand.setText(childText.getProduct_name());

        String btCount = TextUtils.isEmpty(childText.getDelivered()) ? childText.getPendingQty() : childText.getDelivered();
        edtBottleCount.setText(btCount);


        edtBottleCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                childText.setDelivered("" + s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        edtBottleCount.setSelection(edtBottleCount.getText().length());


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
//		String headerTitle = (String) getGroup(groupPosition);
        final DeliveryModel headerTitleModel = (DeliveryModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.raw_order_parent, null);
        }

//      delivering
//      partially
        final CardView card = (CardView) convertView.findViewById(R.id.raw_order_parent_card);
        card.setCardBackgroundColor(headerTitleModel.getSaleStatusColor());

//        if (headerTitleModel.getSale_status().equalsIgnoreCase("delivering")){
//            card.setCardBackgroundColor(ContextCompat.getColor(_context, R.color.colorPrimary));
//        }else if (headerTitleModel.getSale_status().equalsIgnoreCase("partially")){
//            card.setCardBackgroundColor(ContextCompat.getColor(_context, R.color.color_status_partially));
//        }else if (headerTitleModel.getSale_status().equalsIgnoreCase("over")){
//            card.setCardBackgroundColor(ContextCompat.getColor(_context, R.color.color_status_over));
//        }


        final SwitchCompat swOrder = (SwitchCompat) convertView.findViewById(R.id.raw_order_parent_switch_order);
        TextView txtOrder = (TextView) convertView.findViewById(R.id.raw_bottle_txt_OrderId);
        TextView txtDate = (TextView) convertView.findViewById(R.id.raw_bottle_txt_Date);
        txtOrder.setCompoundDrawablesWithIntrinsicBounds(isExpanded ? 0 : R.drawable.ic_plus, 0, 0, 0);
        TextView lblListCustomerCount = (TextView) convertView.findViewById(R.id.raw_bottle_txt_BottleCount);
        txtOrder.setTypeface(null, Typeface.BOLD);


        txtOrder.setText(headerTitleModel.getSale_reference_no());
        try {

            txtDate.setText(Util.formatDateFromDateString("yyyy-MM-dd HH:mm:ss", "dd MMM yyyy / hh:mm:aa", headerTitleModel.getOrderDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        lblListCustomerCount.setText("(" + headerTitleModel.getBottleCount() + ")");

        if (TextUtils.isEmpty(headerTitleModel.getIs_finish()) || headerTitleModel.getIs_finish().equalsIgnoreCase("0")) {
            headerTitleModel.setIs_finish("0");
            swOrder.setChecked(false);
        } else {
            headerTitleModel.setIs_finish("1");
            swOrder.setChecked(true);
        }

        swOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (swOrder.isChecked()) {
                    AlertDialog.Builder alertDialogBuilder =
                            new AlertDialog.Builder(_context)
                                    .setTitle("Confirmation!")
                                    .setMessage("Your confirmation will make this order to be complete")
                                    .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            headerTitleModel.setIs_finish("1");
                                            Log.v("switch", "**********  " + headerTitleModel.getIs_finish());

                                        }
                                    })
                                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            swOrder.setChecked(false);
                                            dialog.cancel();
                                        }
                                    });

                    alertDialogBuilder.setCancelable(false);
                    alertDialogBuilder.show();
                }
            }
        });
        swOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
