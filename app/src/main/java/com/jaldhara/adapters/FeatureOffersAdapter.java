package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.utills.AspectRatioImageView;
import com.smarteist.autoimageslider.SliderViewAdapter;

import java.util.ArrayList;

public class FeatureOffersAdapter extends SliderViewAdapter <FeatureOffersAdapter.SliderAdapterViewHolder> {

    private ArrayList<SpinnerModel> arryRetuen;
    private Context mContext;
    private RequestOptions requestOptions;


    // Provide a suitable constructor (depends on the kind of dataset)
    public FeatureOffersAdapter(Context context, ArrayList<SpinnerModel> arryRetuen) {
        this.mContext = context;
        this.arryRetuen = arryRetuen;
        requestOptions = new RequestOptions();
        requestOptions.placeholder(R.mipmap.ic_app_logo);
        requestOptions.fitCenter();

    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
//    public static class MyViewHolder extends RecyclerView.ViewHolder {
//        // each data item is just a string in this case
//        private TextView txtAssetsName;
//        private TextView txtPrice;
//
//        private com.jaldhara.utills.AspectRatioImageView img;
//
//        public MyViewHolder(View v) {
//            super(v);
//            txtAssetsName = (TextView) v.findViewById(R.id.raw_add_assets_txt_name);
//            txtPrice = (TextView) v.findViewById(R.id.raw_add_assets_txt_price);
//
//            img = (com.jaldhara.utills.AspectRatioImageView)v.findViewById(R.id.img);
//
//        }
//    }

     public static class SliderAdapterViewHolder extends SliderViewAdapter.ViewHolder {
        // Adapter class for initializing
        // the views of our slider view.
        // each data item is just a string in this case
        private TextView txtAssetsName;
        private TextView txtPrice;

        private AppCompatImageView img;

        public SliderAdapterViewHolder(View v) {
            super(v);
            txtAssetsName = (TextView) v.findViewById(R.id.raw_add_assets_txt_name);
            txtPrice = (TextView) v.findViewById(R.id.raw_add_assets_txt_price);

            img = (AppCompatImageView)v.findViewById(R.id.img);
        }
    }


    // Create new views (invoked by the layout manager)
//    @Override
//    public PromotionOffersAdapter.SliderAdapterViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
//        // create a new view
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_promotion, parent, false);
//        SliderAdapterViewHolder vh = new SliderAdapterViewHolder(v);
//        return vh;
//    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final SliderAdapterViewHolder holder, final int position) {

        final SpinnerModel SpinnerModel = arryRetuen.get(position);

        holder.txtAssetsName.setText(SpinnerModel.getRouteName());
        holder.txtPrice.setText("Rs." + SpinnerModel.getProductPrice());

//        if (SpinnerModel.isSelected()) {
//        holder.checkBox.setChecked(SpinnerModel.isSelected());
//        } else {
//        }

        Glide.with(mContext)
                .applyDefaultRequestOptions(requestOptions)
                .load(SpinnerModel.getImage_url())
                .into(holder.img);

//        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                SpinnerModel.setSelected(isChecked);
//            }
//        });




    }

    // Return the size of your dataset (invoked by the layout manager)
//    @Override
//    public int getItemCount() {
//        return arryRetuen.size();
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public int getItemViewType(int position) {
//        return position;
//    }


    // We are inflating the slider_layout
    // inside on Create View Holder method.
    @Override
    public SliderAdapterViewHolder onCreateViewHolder(ViewGroup parent) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_feature, null);
        return new SliderAdapterViewHolder(inflate);
    }

    // Inside on bind view holder we will
    // set data to item of Slider View.
//    @Override
//    public void onBindViewHolder(SliderAdapterViewHolder viewHolder, final int position) {
//
//        final SliderData sliderItem = mSliderItems.get(position);
//
//        // Glide is use to load image
//        // from url in your imageview.
//        Glide.with(viewHolder.itemView)
//                .load(sliderItem.getImgUrl())
//                .fitCenter()
//                .into(viewHolder.imageViewBackground);
//    }

    // this method will return
    // the count of our list.
    @Override
    public int getCount() {
        return arryRetuen.size();
    }

//    static class SliderAdapterViewHolder extends SliderViewAdapter.ViewHolder {
//        // Adapter class for initializing
//        // the views of our slider view.
//        View itemView;
//        ImageView imageViewBackground;
//
//        public SliderAdapterViewHolder(View itemView) {
//            super(itemView);
//            imageViewBackground = itemView.findViewById(R.id.myimage);
//            this.itemView = itemView;
//        }
//    }
//}


}