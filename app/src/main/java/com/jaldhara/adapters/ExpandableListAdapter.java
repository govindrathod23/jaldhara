package com.jaldhara.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.view.RouteMapActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    //	private List<String> _listDataHeader; // header titles
    private List<BottleInfoModel> _listDataHeader; // header titles

    // child data in format of header title, child title
    private HashMap<BottleInfoModel, List<BottleInfoModel>> _listDataChild;

    //	public ExpandableListAdapter(Context context, List<String> listDataHeader,
//			HashMap<String, List<String>> listChildData) {
//		this._context = context;
//		this._listDataHeader = listDataHeader;
//		this._listDataChild = listChildData;
//	}
    public ExpandableListAdapter(Context context, List<BottleInfoModel> listDataHeader,
                                 HashMap<BottleInfoModel, List<BottleInfoModel>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final BottleInfoModel childText = (BottleInfoModel) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//			convertView = infalInflater.inflate(R.layout.list_item, null);
            convertView = infalInflater.inflate(R.layout.raw_bottle_child, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.raw_bottle_txt_AssetsName);

        ImageView imgLocation = (ImageView) convertView.findViewById(R.id.raw_bottle_child_img_locate);
        imgLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (childText.getLatitude() != null && !childText.getLatitude().equalsIgnoreCase("null") && !TextUtils.isEmpty(childText.getLatitude())) {
                    Intent intent = new Intent(_context, RouteMapActivity.class);
                    intent.putExtra(_context.getString(R.string.key_intent_Show_On_Map_For), "FromCustomerChild");
                    intent.putExtra("CustomerArray", childText);
                    _context.startActivity(intent);
                } else {
                    Toast.makeText(_context, "Location not updated for this customer", Toast.LENGTH_LONG).show();
                }
            }
        });

        txtListChild.setText(childText.getCompany());

        LinearLayout llCard = (LinearLayout) convertView.findViewById(R.id.card_raw);
        if (childText.getCustomer_status().equals("1")) {
            llCard.setBackgroundColor(_context.getResources().getColor(R.color.color_bg_bottle_to_deliver));
        } else {
            llCard.setBackgroundColor(_context.getResources().getColor(android.R.color.transparent));

        }


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(final int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
//		String headerTitle = (String) getGroup(groupPosition);
        final BottleInfoModel headerTitle = (BottleInfoModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.raw_bottle_parent, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.raw_bottle_txt_assetsID);
        lblListHeader.setCompoundDrawablesWithIntrinsicBounds(isExpanded ? 0 : R.drawable.ic_plus, 0, 0, 0);

        TextView lblListCustomerCount = (TextView) convertView.findViewById(R.id.raw_bottle_txt_nameCustomerOnRoute);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle.getRouteName());
        lblListCustomerCount.setText("(" + headerTitle.getCustomrsOnRoute() + ")");
        lblListCustomerCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(_context, RouteMapActivity.class);
                intent.putExtra(_context.getString(R.string.key_intent_Show_On_Map_For), "FromCustomer");
                intent.putParcelableArrayListExtra("CustomerArray", (ArrayList<? extends Parcelable>) _listDataChild.get(_listDataHeader.get(groupPosition)));
                _context.startActivity(intent);
            }
        });
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
