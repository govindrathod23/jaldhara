package com.jaldhara.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.models.PurchaseModel;

import java.util.ArrayList;
import java.util.List;

public class InvoiceListAdminAdapter extends RecyclerView.Adapter<InvoiceListAdminAdapter.MyViewHolder> {

    private Context context;
    private List<InvoiceModel> invoiceList;
    private onItemClick<InvoiceModel> onItemClick;

    public InvoiceListAdminAdapter(Context context, List<InvoiceModel> invoiceList) {
        this.context = context;
        this.invoiceList = invoiceList;
    }

    public void setList(List<InvoiceModel> invoiceList) {
        this.invoiceList = invoiceList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_invoice, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    public void updateList(List<InvoiceModel> invoiceList) {
        this.invoiceList = invoiceList;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final InvoiceModel invoiceModel = invoiceList.get(position);

        holder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(context, holder.btnAction);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.poupup_menu_raw_invoice_action, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_invoice_delete_purchase) {
                            if (onItemClick != null) {
                                onItemClick.onDeleteClicked(invoiceModel);
                            }
                        } else if (item.getItemId() == R.id.action_invoice_detail) {
                            if (onItemClick != null) {
                                onItemClick.onDetailClicked(invoiceModel);
                            }
                        } else if (item.getItemId() == R.id.action_invoice_add_payment) {
                            if (onItemClick != null) {
                                onItemClick.onAddPaymentClicked(invoiceModel);
                            }
                        } else if (item.getItemId() == R.id.action_invoice_edit_purchase) {
                            if (onItemClick != null) {
                                onItemClick.onEditClicked(invoiceModel);
                            }
                        } else if (item.getItemId() == R.id.action_invoice_view_payments) {
                            if (onItemClick != null) {
                                onItemClick.onViewPaymentsClicked(invoiceModel);
                            }
                        } else if (item.getItemId() == R.id.action_invoice_email_purchase) {
                            if (onItemClick != null) {
                                onItemClick.onEmailInvoice(invoiceModel);
                            }
                        }

                        return true;
                    }
                });

                popup.show();//showing popup menu
            }

        });

        if (invoiceModel.getInv_reference_no() != null && invoiceModel.getInv_reference_no().length() > 0) {
            holder.txtReference.setText(invoiceModel.getInv_reference_no());
            holder.txtReference.setVisibility(View.VISIBLE);
        } else {
            holder.txtReference.setVisibility(View.GONE);
        }

        if (invoiceModel.getCreated_at() != null && invoiceModel.getCreated_at().length() > 0) {
            holder.txtDate.setText(invoiceModel.getCreated_at());
            holder.txtDate.setVisibility(View.VISIBLE);
        } else {
            holder.txtDate.setVisibility(View.GONE);
        }

        holder.txtBiller.setText(invoiceModel.getBiller());

        if (invoiceModel.getCustomer() != null && invoiceModel.getCustomer().length() > 0) {
            holder.txtCustomer.setText(invoiceModel.getCustomer());
            holder.txtCustomer.setVisibility(View.VISIBLE);
        } else {
            holder.txtCustomer.setVisibility(View.GONE);
        }

        holder.txtGrandTotal.setText(invoiceModel.getTotal_amount());
        holder.txtPaid.setText(invoiceModel.getPaid());

        if (invoiceModel.getPayment_status() != null && invoiceModel.getPayment_status().length() > 0) {
            holder.txtPaymentStatus.setText(invoiceModel.getPayment_status());
            holder.txtPaymentStatus.setVisibility(View.VISIBLE);
        } else {
            holder.txtPaymentStatus.setVisibility(View.GONE);
        }

        holder.txtBalance.setText(invoiceModel.getBalance());

        if (invoiceModel.getRoute_name() != null && invoiceModel.getRoute_name().length() > 0) {
            holder.txtRouteName.setText(invoiceModel.getRoute_name());
            holder.txtRouteName.setVisibility(View.VISIBLE);
        } else {
            holder.txtRouteName.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return invoiceList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtReference, txtDate, txtBiller, txtCustomer, txtGrandTotal, txtPaid, txtBalance, txtPaymentStatus, txtRouteName;
        private Button btnAction;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtReference = itemView.findViewById(R.id.raw_invoice_txt_reference_number);
            txtDate = itemView.findViewById(R.id.raw_invoice_txt_date);
            txtBiller = itemView.findViewById(R.id.raw_invoice_txt_biller);
            txtCustomer = itemView.findViewById(R.id.raw_invoice_txt_customer);
            txtGrandTotal = itemView.findViewById(R.id.raw_invoice_txt_grand_total);
            txtPaid = itemView.findViewById(R.id.raw_invoice_txt_paid);
            txtBalance = itemView.findViewById(R.id.raw_invoice_txt_balance);
            txtPaymentStatus = itemView.findViewById(R.id.raw_invoice_txt_payment_status);
            txtRouteName = itemView.findViewById(R.id.raw_invoice_txt_RouteName);
            btnAction = itemView.findViewById(R.id.raw_invoice_button_action);

        }
    }

    public interface onItemClick<T> {
        void onDetailClicked(T data);

        void onAddPaymentClicked(T data);

        void onDeleteClicked(T data);

        void onEditClicked(T data);

        void onViewPaymentsClicked(T data);

        void onEmailInvoice(T data);

    }

    public void setOnItemClick(InvoiceListAdminAdapter.onItemClick<InvoiceModel> onItemClick) {
        this.onItemClick = onItemClick;
    }
}
