package com.jaldhara.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.view.admin.CustomerFromRouteActivity;

import java.util.ArrayList;

public class AssetsSelectionAdapter extends RecyclerView.Adapter<AssetsSelectionAdapter.MyViewHolder> {

    private ArrayList<SpinnerModel> arryRetuen;
    private Context mContext;


    // Provide a suitable constructor (depends on the kind of dataset)
    public AssetsSelectionAdapter(Context context, ArrayList<SpinnerModel> arryRetuen) {
        this.mContext = context;
        this.arryRetuen = arryRetuen;

    }


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txtAssetsName;
        private CheckBox checkBox;

        private CardView cardRow;


        public MyViewHolder(View v) {
            super(v);
            txtAssetsName = (TextView) v.findViewById(R.id.raw_add_assets_txt_name);
            checkBox = (CheckBox) v.findViewById(R.id.raw_add_assets_chkBox);

            cardRow = (CardView) v.findViewById(R.id.card_raw);


        }
    }


    // Create new views (invoked by the layout manager)
    @Override
    public AssetsSelectionAdapter.MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_add_assets, parent, false);


        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final SpinnerModel SpinnerModel = arryRetuen.get(position);

        holder.txtAssetsName.setText(SpinnerModel.getRouteName());

//        if (SpinnerModel.isSelected()) {
        holder.checkBox.setChecked(SpinnerModel.isSelected());
//        } else {
//        }

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SpinnerModel.setSelected(isChecked);
            }
        });


        holder.txtAssetsName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, CustomerFromRouteActivity.class);
                intent.putExtra(mContext.getString(R.string.key_intent_AssetsID), SpinnerModel.getRouteId());
                intent.putExtra(mContext.getString(R.string.key_intent_AssetsName), SpinnerModel.getRouteName());
                mContext.startActivity(intent);

            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return arryRetuen.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}