package com.jaldhara.services;

import org.json.JSONArray;

import com.google.gson.JsonArray;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiInterface {

    @FormUrlEncoded
    @POST("api/v1/auth")
    Call<ResponseBody> loginAuth(@Field("identity") String identity,
                                 @Field("password") String password,
                                 @Field("api") String api,
                                 @Field("api-key") String api_key);

    @FormUrlEncoded
    @POST("api/v1/auth")
    Call<ResponseBody> forgotPass(@Field("api") String api,
                                  @Field("api-key") String api_key,
                                  @Field("comp_id") String comp_id,
                                  @Field("forgot_email") String forgot_email);

    @FormUrlEncoded
    @POST("api/v1/auth")
    Call<ResponseBody> logoutAuth(@Field("api") String api,
                                  @Field("api-key") String api_key,
                                  @Field("comp_id") String comp_id);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> listOfRoutes(@Field("api") String api,
                                    @Field("api-key") String api_key,
                                    @Field("comp_id") String comp_id);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> listOfCustomersFromRoute(@Field("api") String api,
                                                @Field("api-key") String api_key,
                                                @Field("comp_id") String comp_id,
                                                @Field("route_id") String route_id);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> listOfCustomersFromAssets(@Field("api") String api,
                                                 @Field("api-key") String api_key,
                                                 @Field("comp_id") String comp_id,
                                                 @Field("asset_id") String asset_id);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> listOfDrivers(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> listOfLoaders(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> assignDelivery(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("route_id") String route_id,
                                      @Field("driver_id") String driver_id,
                                      @Field("loader_id") String loader_id,
                                      @Field("driver_commission") String driver_commission,
                                      @Field("loader_commission") String loader_commission);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> listOfCustomer(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("user_id") String user_id);


    @FormUrlEncoded
//    @POST("api/v1/customers")
    @POST("api/v1/customers/index")
    Call<ResponseBody> getAllCustomer(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("group") String group);


    @FormUrlEncoded
//    @POST("api/v1/customers")
    @POST("api/v1/masters/index_post")
    Call<ResponseBody> get_reference_no(@Field("api") String api,
                                        @Field("api-key") String api_key,
                                        @Field("comp_id") String comp_id,
                                        @Field("type") String type);

    @FormUrlEncoded
    @POST("api/v1/customers")
    Call<ResponseBody> getAllCustomerPage(@Field("api") String api,
                                          @Field("api-key") String api_key,
                                          @Field("comp_id") String comp_id,
                                          @Field("group") String group,
                                          @Field("start") int start,
                                          @Field("limit") int limit);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> getSearchTxtCustomer(@Field("api") String api,
                                            @Field("api-key") String api_key,
                                            @Field("comp_id") String comp_id,
                                            @Field("term") String term);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> getAllDriversLatLong(@Field("api") String api,
                                            @Field("api-key") String api_key,
                                            @Field("comp_id") String comp_id);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> getLocationForCustomer(@Field("api") String api,
                                              @Field("api-key") String api_key,
                                              @Field("comp_id") String comp_id,
                                              @Field("customer_id") String customer_id);

    @FormUrlEncoded
    @POST("api/v1/customers")
    Call<ResponseBody> getAllBillers(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id,
                                     @Field("group") String group,
                                     @Field("start") int start,
                                     @Field("limit") int limit);

//    list_of_routes

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> listOfCustomerByID(@Field("api") String api,
                                          @Field("api-key") String api_key,
                                          @Field("comp_id") String comp_id,
                                          @Field("route_id") String route_id);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> listOfAssets(@Field("api") String api,
                                    @Field("api-key") String api_key,
                                    @Field("comp_id") String comp_id);


    @FormUrlEncoded
    @POST("api/v1/products")
    Call<ResponseBody> getBrandsList(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id);

    @FormUrlEncoded
    @POST("api/v1/products")
    Call<ResponseBody> getWishList(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("customer_id") String customer_id);


    @FormUrlEncoded
    @POST("api/v1/products")
    Call<ResponseBody> editWishList(@Field("api") String api,
                                    @Field("api-key") String api_key,
                                    @Field("customer_id") String customer_id,
                                    @Field("product_id") String product_id
    );


    @FormUrlEncoded
    @POST("api/v1/products")
    Call<ResponseBody> getAllProducts(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("start") int start,
                                      @Field("limit") int limit);


    @FormUrlEncoded
    @POST("api/v1/products")
    Call<ResponseBody> getAllPromotions(@Field("api") String api,
                                        @Field("api-key") String api_key,
                                        @Field("comp_id") String comp_id,
                                        @Field("promotion") String promotion,
                                        @Field("start") int start,
                                        @Field("limit") int limit);

    @FormUrlEncoded
    @POST("api/v1/products")
    Call<ResponseBody> getAllFeatures(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("featured") String featured,
                                      @Field("start") int start,
                                      @Field("limit") int limit);


    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> addAssetsToCustomer(@Field("api") String api,
                                           @Field("api-key") String api_key,
                                           @Field("comp_id") String comp_id,
                                           @Field("customer_id") String customer_id,
                                           @Field("data") String data);


    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> addOrderNew(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("comp_id") String comp_id,
                                   @Field("customer_id") String customer_id,
                                   @Field("route") String route,
                                   @Field("delivery_date") String delivery_date,
                                   @Field("products") String products,
                                   @Field("warehouse") String warehouse,
                                   @Field("is_default_invoice") String is_default_invoice,
                                   @Field("sale_status") String sale_status,
                                   @Field("driver") String driver,
                                   @Field("loader") String loader,
                                   @Field("biller") String biller,
                                   @Field("orderType") String orderType
    );

    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> deleteOrder(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("sale_id") String sale_id

    );

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> updateLocation(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("user_id") String user_id,
                                      @Field("latitude") String latitude,
                                      @Field("longitude") String longitude);


    @FormUrlEncoded
    @POST("api/v1/reports")
    Call<ResponseBody> reports(@Field("api") String api,
                               @Field("api-key") String api_key,
                               @Field("comp_id") String comp_id,
                               @Field("start_date") String start_date,
                               @Field("end_date") String end_date);


    @FormUrlEncoded
    @POST("api/v1/customers")
    Call<ResponseBody> addCustomer(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("comp_id") String comp_id,
                                   @Field("name") String name,
                                   @Field("email") String email,
                                   @Field("company") String company,
                                   @Field("address") String address,
                                   @Field("route_id") String route_id,
                                   @Field("latitude") String latitude,
                                   @Field("longitude") String longitude,
                                   @Field("phone") String phone);


    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> getOrder(@Field("api") String api,
                                @Field("api-key") String api_key,
                                @Field("comp_id") String comp_id,
//                                @Field("start_date") String start_date,
//                                @Field("route_id") String "",
                                @Field("end_date") String start_date,
                                @Field("customer_id") String customer_id,
                                @Field("include") String include
//                                ,@Field("status") String status
    );

    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> getOrderByRoute(@Field("api") String api,
                                       @Field("api-key") String api_key,
                                       @Field("comp_id") String comp_id,
                                       @Field("end_date") String start_date,
                                       @Field("customer_id") String customer_id,
                                       @Field("include") String include,
                                       @Field("route") String route,
                                       @Field("driver") String driver);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> updateCusLoc(@Field("api") String api,
                                    @Field("api-key") String api_key,
                                    @Field("comp_id") String comp_id,
                                    @Field("customer_id") String customer_id,
                                    @Field("address") String address,
                                    @Field("latitude") String latitude,
                                    @Field("longitude") String longitude);

    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> SeeOrders(@Field("api") String api,
                                 @Field("api-key") String api_key,
                                 @Field("comp_id") String comp_id,
                                 @Field("customer_id") String customer_id,
                                 @Field("include") String include,
                                 @Field("status") String status,
//                                 @Field("start_date") String start_date,
                                 @Field("end_date") String end_date);


    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> SeeOrdersPagination(@Field("api") String api,
                                           @Field("api-key") String api_key,
                                           @Field("comp_id") String comp_id,
                                           @Field("customer_id") String customer_id,
                                           @Field("include") String include,
                                           @Field("sale_status") String sale_status,
                                           @Field("route") String route,
                                           @Field("driver") String driver,
                                           @Field("start_date") String start_date,
                                           @Field("end_date") String end_date,

                                           @Field("start") int start,
                                           @Field("limit") int limit);


    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> get_all_order(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id,
                                     @Field("customer_id") String customer_id,
                                     @Field("start") int start,
                                     @Field("limit") int limit);


    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> getOrderCustomer(@Field("api") String api,
                                        @Field("api-key") String api_key,
                                        @Field("comp_id") String comp_id,
                                        @Field("customer_id") String customer_id,
                                        @Field("include") String include,
                                        @Field("status") String status,
                                        @Field("start_date") String start_date);


    @FormUrlEncoded
    @POST("api/v1/reports")
    Call<ResponseBody> customerPaymentReport(@Field("api") String api,
                                             @Field("api-key") String api_key,
                                             @Field("comp_id") String comp_id,
                                             @Field("customer_id") String customer_id,
                                             @Field("year") String year);

    @FormUrlEncoded
    @POST("api/v1/reports")
    Call<ResponseBody> getInvoice(@Field("api") String api,
                                  @Field("api-key") String api_key,
                                  @Field("comp_id") String comp_id,
                                  @Field("invoice_id") String invoice_id);

    @FormUrlEncoded
//    @POST("api/v1/reports")
    @POST("api/v1/invoices/index_post")
    Call<ResponseBody> addInvoice(@Field("api") String api,
                                  @Field("api-key") String api_key,
                                  @Field("comp_id") String comp_id,
                                  @Field("customer") String customer_id,
                                  @Field("biller") String biller_id,
                                  @Field("month") String month,
                                  @Field("from_date") String from_date,
                                  @Field("to_date") String to_date,
                                  @Field("note") String note,
                                  @Field("staff_note") String staff_note,
                                  @Field("sales_id") String sales_id
    );

    @FormUrlEncoded
//    @POST("api/v1/reports")
    @POST("api/v1/invoices/index_post")
    Call<ResponseBody> editInvoice(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("customer") String customer_id,
                                   @Field("biller") String biller_id,
                                   @Field("month") String month,
                                   @Field("from_date") String from_date,
                                   @Field("to_date") String to_date,
                                   @Field("note") String note,
                                   @Field("staff_note") String staff_note,
                                   @Field("id") String id,
                                   @Field("sales_id") String sales_id

    );

    @Multipart
    @POST("api/v1/orders")
    Call<ResponseBody> addDelivery(@Part("api") RequestBody api,
                                   @Part("api-key") RequestBody api_key,
                                   @Part("comp_id") RequestBody comp_id,
                                   @Part("customer_id") RequestBody customer_id,
                                   @Part("status") RequestBody status,
                                   @Part("delivered_by") RequestBody delivered_by,
                                   @Part("received_by") RequestBody received_by,
                                   @Part("created_by") RequestBody created_by,
                                   @Part("data") RequestBody data,
                                   @Part MultipartBody.Part document);

    @FormUrlEncoded
    @POST("api/v1/orders")
    Call<ResponseBody> addReturn(@Field("api") String api,
                                 @Field("api-key") String api_key,
                                 @Field("comp_id") String comp_id,
                                 @Field("customer_id") String customer_id,
                                 @Field("data") String data,
                                 @Field("driver_id") String driver_id
    );

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> addRoutes(@Field("api") String api,
                                 @Field("api-key") String api_key,
                                 @Field("comp_id") String comp_id,
                                 @Field("name") String name,
                                 @Field("code") String code);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> showBrands(@Field("api") String api,
                                  @Field("api-key") String api_key,
                                  @Field("comp_id") String comp_id);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> showAllProducts(@Field("api") String api,
                                       @Field("api-key") String api_key,
                                       @Field("comp_id") String comp_id);

    @Multipart
    @POST("api/v1/reports")
    Call<ResponseBody> submitPayment(@Part("api") RequestBody api,
                                     @Part("api-key") RequestBody api_key,
                                     @Part("comp_id") RequestBody comp_id,
                                     @Part("customer_id") RequestBody customer_id,
                                     @Part("amount") RequestBody amount,
                                     @Part("paid_by") RequestBody paid_by,
                                     @Part MultipartBody.Part document

    );

    //
    //region expenses
    //

    @FormUrlEncoded
    @POST("api/v1/expenses/index")
    Call<ResponseBody> getExpenses(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("comp_id") String comp_id);

    @FormUrlEncoded
    @POST("api/v1/expenses/index")
    Call<ResponseBody> deleteExpense(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id,
                                     @Field("id") String id);

    @FormUrlEncoded
    @POST("api/v1/expenses/index")
    Call<ResponseBody> saveExpense(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("comp_id") String comp_id,
                                   @Field("amount") String amount,
                                   @Field("user_id") String user_id,
                                   @Field("note") String note,
                                   @Field("category") String category,
                                   @Field("warehouse") String warehouse,
                                   @Field("route") String route);


    @FormUrlEncoded
    @POST("api/v1/purchases/index")
    Call<ResponseBody> addPayment(@Field("api") String api,
                                  @Field("api-key") String api_key,
                                  @Field("comp_id") String comp_id,
                                  @Field("amount-paid") String amount_paid,
                                  @Field("paid_by") String paid_by,
                                  @Field("purchase_id") String purchase_id,
                                  @Field("reference_no") String reference_no,
                                  @Field("cheque_no") String cheque_no,
                                  @Field("pcc_no") String pcc_no,
                                  @Field("pcc_holder") String pcc_holder,
                                  @Field("pcc_month") String pcc_month,
                                  @Field("pcc_year") String pcc_year,
                                  @Field("pcc_type") String pcc_type,
                                  @Field("note") String note,
                                  @Field("user_id") String user_id,
                                  @Field("userfile") String userfile);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> getAllSupplier(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id);

    @FormUrlEncoded
    @POST("api/v1/expenses/index")
    Call<ResponseBody> updateExpense(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id,
                                     @Field("id") String id,
                                     @Field("amount") String amount,
                                     @Field("user_id") String user_id,
                                     @Field("note") String note,
                                     @Field("category") String category,
                                     @Field("warehouse") String warehouse,
                                     @Field("route") String route);


    //
    //end region
    //

    //
    //region purchase services
    //

    @FormUrlEncoded
    @POST("api/v1/purchases/index")
    Call<ResponseBody> getPurchaseList(@Field("api") String api,
                                       @Field("api-key") String api_key,
                                       @Field("comp_id") String comp_id);

    @FormUrlEncoded
    @POST("api/v1/purchases/index")
    Call<ResponseBody> deletePurchase(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("id") String id);

//    @FormUrlEncoded
//    @POST("api/v1/purchases/index")
//    Call<ResponseBody> addPayment(@Field("api") String api,
//                                  @Field("api-key") String api_key,
//                                  @Field("comp_id") String comp_id,
//                                  @Field("amount-paid") String amount_paid,
//                                  @Field("paid_by") String paid_by,
//                                  @Field("purchase_id") String purchase_id,
//                                  @Field("reference_no") String reference_no,
//                                  @Field("cheque_no") String cheque_no,
//                                  @Field("pcc_no") String pcc_no,
//                                  @Field("pcc_holder") String pcc_holder,
//                                  @Field("pcc_month") String pcc_month,
//                                  @Field("pcc_year") String pcc_year,
//                                  @Field("pcc_type") String pcc_type,
//                                  @Field("note") String note,
//                                  @Field("user_id") String user_id,
//                                  @Field("userfile") String userfile);

    @FormUrlEncoded
    @POST("api/v1/purchases/index")
    Call<ResponseBody> editPayment(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("comp_id") String comp_id,
                                   @Field("amount-paid") String amount_paid,
                                   @Field("paid_by") String paid_by,
                                   @Field("purchase_id") String purchase_id,
                                   @Field("reference_no") String reference_no,
                                   @Field("cheque_no") String cheque_no,
                                   @Field("pcc_no") String pcc_no,
                                   @Field("pcc_holder") String pcc_holder,
                                   @Field("pcc_month") String pcc_month,
                                   @Field("pcc_year") String pcc_year,
                                   @Field("pcc_type") String pcc_type,
                                   @Field("note") String note,
                                   @Field("user_id") String user_id,
                                   @Field("userfile") String userfile,
                                   @Field("id") String paymentId);

    @FormUrlEncoded
    @POST("api/v1/purchases/index")
    Call<ResponseBody> getPaymentList(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("id") String id);

    @FormUrlEncoded
    @POST("api/v1/purchases/index")
    Call<ResponseBody> deletePayment(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id,
                                     @Field("id") String id);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> addPurchase(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("comp_id") String comp_id,
                                   @Field("reference_no") String reference_no,
                                   @Field("warehouse") String warehouse,
                                   @Field("route") String route,
                                   @Field("supplier") String supplier,
                                   @Field("status") String status,
                                   @Field("shipping") String shipping,
                                   @Field("note") String note,
                                   @Field("payment_term") String payment_term,
                                   @Field("discount") String discount,
                                   @Field("order_tax") String order_tax,
                                   @Field("user_id") String user_id,
                                   @Field("Product") JsonArray Product);


//    @FormUrlEncoded
//    @POST("api/v1/masters")
//    Call<ResponseBody> addProduct(@Field("api") String api,
//                                  @Field("api-key") String api_key,
//                                  @Field("comp_id") String comp_id,
//                                  @Field("invoice_id") String invoice_id,
//                                  @Field("warehouse") String warehouse,
//                                  @Field("tax_rate") String tax_rate,
//                                  @Field("type") String type,
//                                  @Field("brand") String brand,
//                                  @Field("category") String category,
//                                  @Field("cost") String cost,
//                                  @Field("price") String price,
//                                  @Field("bottle_cost") String bottle_cost,
//                                  @Field("unit") String unit,
//                                  @Field("default_sale_unit") String default_sale_unit,
//                                  @Field("default_purchase_unit") String default_purchase_unit,
//                                  @Field("tax_method") String tax_method,
//                                  @Field("alert_quantity") String alert_quantity,
//                                  @Field("track_quantity") String track_quantity,
//                                  @Field("details") String details,
//                                  @Field("product_details") String product_details,
//                                  @Field("supplier") String supplier,
//                                  @Field("supplier_price") String supplier_price,
//                                  @Field("barcode_symbology") String barcode_symbology,
//                                  @Field("name") String name);

    //
    //endregion
    //

    @FormUrlEncoded
    @POST("api/v1/invoices/index")
    Call<ResponseBody> getInvoiceList(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("customer_id") String customer_id
    );

    @FormUrlEncoded
    @POST("api/v1/purchases/index")
    Call<ResponseBody> addPurchase(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("comp_id") String comp_id,
                                   @Field("reference_no") String reference_no,
                                   @Field("warehouse") String warehouse,
                                   @Field("route") String route,
                                   @Field("supplier") String supplier,
                                   @Field("status") String status,
                                   @Field("shipping") String shipping,
                                   @Field("note") String note,
                                   @Field("payment_term") String payment_term,
                                   @Field("discount") String discount,
                                   @Field("order_tax") String order_tax,
                                   @Field("user_id") String user_id,
                                   @Field("product") JSONArray Product);

    @FormUrlEncoded
    @POST("api/v1/purchases/index")
    Call<ResponseBody> viewPurchase(@Field("api") String api,
                                    @Field("api-key") String api_key,
                                    @Field("comp_id") String comp_id,
                                    @Field("id") String purchase_id);

    @FormUrlEncoded
    @POST("api/v1/purchases/index")
    Call<ResponseBody> updatePurchase(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("reference_no") String reference_no,
                                      @Field("warehouse") String warehouse,
                                      @Field("route") String route,
                                      @Field("supplier") String supplier,
                                      @Field("status") String status,
                                      @Field("shipping") String shipping,
                                      @Field("note") String note,
                                      @Field("payment_term") String payment_term,
                                      @Field("discount") String discount,
                                      @Field("order_tax") String order_tax,
                                      @Field("user_id") String user_id,
                                      @Field("product") JSONArray Product,
                                      @Field("id") String purchase_id);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> addSupplier(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("comp_id") String comp_id,
                                   @Field("name") String name,
                                   @Field("email") String email,
                                   @Field("company") String company,
                                   @Field("address") String address,
                                   @Field("vat_no") String vat_no,
                                   @Field("city") String city,
                                   @Field("state") String state,
                                   @Field("postal_code") String postal_code,
                                   @Field("country") String country,
                                   @Field("phone") String phone,
                                   @Field("gst_no") String gst_no);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> addProduct(@Field("api") String api,
                                  @Field("api-key") String api_key,
                                  @Field("comp_id") String comp_id,
                                  @Field("warehouse") String warehouse,
                                  @Field("tax_rate") String tax_rate,
                                  @Field("type") String type,
                                  @Field("brand") String brand,
                                  @Field("category") String category,
                                  @Field("cost") String cost,
                                  @Field("price") String price,
                                  @Field("bottle_cost") String bottle_cost,
                                  @Field("unit") String unit,
                                  @Field("default_sale_unit") String default_sale_unit,
                                  @Field("default_purchase_unit") String default_purchase_unit,
                                  @Field("tax_method") String tax_method,
                                  @Field("alert_quantity") String alert_quantity,
                                  @Field("track_quantity") String track_quantity,
                                  @Field("details") String details,
                                  @Field("product_details") String product_details,
                                  @Field("supplier") String supplier,
                                  @Field("supplier_price") String supplier_price,
                                  @Field("barcode_symbology") String barcode_symbology,
                                  @Field("name") String name);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> getProductType(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> getSubCategory(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("category_id") String category_id);

    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> getProductUnit(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id);

    //
    //endregion
    //


    @FormUrlEncoded
    @POST("api/v1/invoices/index_post")
    Call<ResponseBody> delete_invoice(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("id") String id);


    @FormUrlEncoded
    @POST("api/v1/invoices/index")
    Call<ResponseBody> ViewPaymentList(@Field("api") String api,
                                       @Field("api-key") String api_key,
                                       @Field("id") String id);


    @FormUrlEncoded
    @POST("api/v1/invoices/index")
    Call<ResponseBody> getBetweenDatesOrders(@Field("api") String api,
                                             @Field("api-key") String api_key,
                                             @Field("comp_id") String comp_id,
                                             @Field("customer_id") String customer_id,
                                             @Field("to_date") String to_date,
                                             @Field("from_date") String from_date
    );


    @FormUrlEncoded
    @POST("api/v1/invoices/index")
    Call<ResponseBody> invoiceDetails(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("invoice_id") String invoice_id
    );

    @FormUrlEncoded
    @POST("api/v1/invoices/index")
    Call<ResponseBody> delete_payment(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("id") String id);


    @FormUrlEncoded
    @POST("api/v1/invoices/index_post")
    Call<ResponseBody> AddPaymentInvoice(
            @Field("api") String api,
            @Field("api-key") String api_key,
            @Field("comp_id") String comp_id,

            @Field("amount-paid") String amount_paid,
            @Field("paid_by") String paid_by,
            @Field("id") String id,

            @Field("reference_no") String reference_no,
            @Field("cheque_no") String cheque_no,
            @Field("pcc_no") String pcc_no,

            @Field("pcc_holder") String pcc_holder,
            @Field("pcc_month") String pcc_month,
            @Field("pcc_year") String pcc_year,

            @Field("pcc_type") String pcc_type,
            @Field("note") String note,
            @Field("user_id") String user_id,

            @Field("gift_Card_no") String gift_Card_no);


    @FormUrlEncoded
    @POST("api/v1/invoices/index")
    Call<ResponseBody> EditPaymentInvoice(
            @Field("api") String api,
            @Field("api-key") String api_key,
            @Field("amount-paid") String amount_paid,

            @Field("paid_by") String paid_by,
            @Field("invoices_id") String invoices_id,

            @Field("reference_no") String reference_no,
            @Field("cheque_no") String cheque_no,
            @Field("pcc_no") String pcc_no,

            @Field("pcc_holder") String pcc_holder,
            @Field("pcc_month") String pcc_month,
            @Field("pcc_year") String pcc_year,

            @Field("pcc_type") String pcc_type,
            @Field("note") String note,
            @Field("user_id") String user_id,

            @Field("id") String id,
            @Field("gift_Card_no") String gift_Card_no,
            @Field("txn_no") String txn_no);


    @FormUrlEncoded
    @POST("api/v1/masters/index_post")
    Call<ResponseBody> admin_counter(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id);

    @FormUrlEncoded
    @POST("api/v1/masters/index_post")
    Call<ResponseBody> customer_counter(@Field("api") String api,
                                        @Field("api-key") String api_key,
                                        @Field("customer_id") String comp_id);


    @FormUrlEncoded
    @POST("api/v1/drivercommission/index")
    Call<ResponseBody> get_driver_commission(@Field("api") String api,
                                             @Field("api-key") String api_key,
                                             @Field("comp_id") String comp_id,
                                             @Field("driver_id") String driver_id);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> get_version(@Field("api") String api,
                                   @Field("api-key") String api_key,
                                   @Field("comp_id") String comp_id);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> add_attendance(@Field("api") String api,
                                      @Field("api-key") String api_key,
                                      @Field("comp_id") String comp_id,
                                      @Field("type") String type,
                                      @Field("date_time") String date_time,
                                      @Field("user_id") String user_id,
                                      @Field("email") String email);

    @FormUrlEncoded
    @POST("api/v1/Reports")
    Call<ResponseBody> customer_card(@Field("api") String api,
                                     @Field("api-key") String api_key,
                                     @Field("comp_id") String comp_id,
                                     @Field("customer_id") String customer_id,
                                     @Field("start_date") String start_date,
                                     @Field("end_date") String end_date);


    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> get_group_permissions(@Field("api") String api,
                                             @Field("api-key") String api_key,
                                             @Field("comp_id") String comp_id,
                                             @Field("group_id") String group_id);


    // By Pratik
    @FormUrlEncoded
    @POST("api/v1/masters")
    Call<ResponseBody> getAllAssets(@Field("api") String api,
                                    @Field("api-key") String api_key);


}
