package com.jaldhara.services;

import android.content.Context;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {


//    public static final String BASE_URL = "http://replica.jaldhara.rndinfosoft.co.in/";

//    public static final String BASE_URL = "http://myjaldhara.com/";

    private static Retrofit retrofit = null;


    public static Retrofit getClient( ) {
        if (retrofit == null) {



            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.level(HttpLoggingInterceptor.Level.BODY);


            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .connectTimeout(1, TimeUnit.MINUTES)
                    .readTimeout(30, TimeUnit.SECONDS)
                    .writeTimeout(15, TimeUnit.SECONDS)
                    .addInterceptor(logging)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(JalDharaApplication.getInstance().getString(R.string.BASE_URL))
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}