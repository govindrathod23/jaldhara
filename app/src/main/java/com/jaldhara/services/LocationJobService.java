package com.jaldhara.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.util.Log;
import android.widget.Toast;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.SetJobParams;
import com.jaldhara.utills.Util;
import com.jaldhara.view.BestLocationProvider;
import com.jaldhara.view.RouteMapActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LocationJobService extends JobService {
    private static final String TAG = LocationJobService.class.getSimpleName();
    boolean isWorking = false;
    boolean jobCancelled = false;


    // Called by the Android system when it's time to run the job
    @Override
    public boolean onStartJob(JobParameters jobParameters) {

        Log.e(TAG, "onStartJob With job ********************* " + jobParameters.getJobId());
        isWorking = true;

        // We need 'jobParameters' so we can call 'jobFinished'
        if (jobParameters.getJobId() == Util.JOB_ID_UPDATE_DRIVER_LOCATION) {
            startWorkOnNewThread(jobParameters); // Services do NOT run on a separate thread

        }


        return isWorking;
    }


    private void startWorkOnNewThread(final JobParameters jobParameters) {
        new Thread(new Runnable() {
            public void run() {
//               doWork(jobParameters);
                Log.e(TAG, "New Thread has been start ********************* ");
                if (JalDharaApplication.getInstance().getLocation() != null) {
                    if (ConnectivityReceiver.isConnected()) {
                        serviceUpdateDriverLocation(jobParameters);
                    } else {
                //  if we try to print the toast than it will be getting cresh because UI hander cant create inside instantiate thread.
                //  Toast.makeText(JalDharaApplication.getInstance(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                    }
                }
                Log.e(TAG, "New Thread Location Value ********************* " + JalDharaApplication.getInstance().getLocation());
                //job schedular code start
                jobServiceStop(jobParameters);
                //job schedular code start

            }
        }).start();
    }

    private void doWork(JobParameters jobParameters) {
        // 10 seconds of working (1000*10ms)
//       for (int i = 0; i < 1000; i++) {
//           // If the job has been cancelled, stop working; the job will be rescheduled.
//           if (jobCancelled)
//               return;
//
//           try {
//               Thread.sleep(10);
//           } catch (Exception e) {
//
//           }
//       }

//       Log.d(TAG, "Job finished!");
//       isWorking = false;
//       boolean needsReschedule = false;
//       jobFinished(jobParameters, needsReschedule);
    }

    // Called if the job was cancelled before being finished
    @Override
    public boolean onStopJob(JobParameters jobParameters) {

        jobCancelled = true;
        boolean needsReschedule = isWorking;
        jobFinished(jobParameters, needsReschedule);

        Log.e(TAG, "onStopJob ***************** ");
        Log.e(TAG, "onStopJob NeedsReschedule:: ********* " + needsReschedule);
        return needsReschedule;
    }

    /**
     * @param jobParameters
     */
    public void jobServiceStop(JobParameters jobParameters) {
        if (jobParameters != null) {
            isWorking = false;
            boolean needsReschedule = false;
            jobFinished(jobParameters, needsReschedule);
        }
    }

    /***************************************************************************************
     *
     **************************************************************************************
     * @param jobParameters*/
    private void serviceUpdateDriverLocation(final JobParameters jobParameters) {
        ApiInterface apiService = ApiClient.getClient( ).create(ApiInterface.class);


        Call<ResponseBody> call = apiService.updateLocation("update_lat_long", Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID)),
                "" + JalDharaApplication.getInstance().getLocation().getLatitude(),
                "" + JalDharaApplication.getInstance().getLocation().getLongitude());
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    //job schedular code start
                    jobServiceStop(jobParameters);
                    //job schedular code start

                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    Log.e(TAG, "serviceUpdateDriverLocation! " + jsonResponse.toString() + "******************");
//                    if (response.isSuccessful() && jsonResponse.optBoolean("status")) {
//                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
//                    Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("", t.toString());
                Log.e(TAG, "onFailure : Job finished! : " + t.toString() + "******************");
                //job schedular code start
                jobServiceStop(jobParameters);
                //job schedular code start

            }
        });
    }


}