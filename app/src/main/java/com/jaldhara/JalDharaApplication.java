package com.jaldhara;

import android.app.Application;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jaldhara.services.LocationJobService;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;


public class JalDharaApplication extends Application {

    private static JalDharaApplication mInstance;
    //    private DatabaseHelper databaseHelper;
    private static final String TAG = "MainActivity";
    private Location mCurrentLocation;
    private DatabaseReference mDatabase;
    // [START declare_analytics]
    private FirebaseAnalytics mFirebaseAnalytics;
    // [END declare_analytics]
    private FirebaseCrashlytics mCrashlytics;


    @Override
    public void onCreate() {
        super.onCreate();
//        Stetho.initializeWithDefaults(this);
        mInstance = this;

        mDatabase = FirebaseDatabase.getInstance().getReference();

        // [START shared_app_measurement]
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);
        mCrashlytics = FirebaseCrashlytics.getInstance();
        // [END shared_app_measurement]
        // mDatabase.child("users").child(userId).setValue(user);
        //        Stetho.initializeWithDefaults(this);
//        try {
//            databaseHelper = new DatabaseHelper(getApplicationContext());
//            databaseHelper.openDataBase();
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }

    public DatabaseReference getLiveDatabase() {
        return mDatabase;
    }

    public FirebaseAnalytics getFirebaseAnalytics() {
        return mFirebaseAnalytics;
    }

    public FirebaseCrashlytics getFirebaseCrashlytics() {
        return mCrashlytics;
    }


//    public DatabaseHelper getDatabase() {
//        return databaseHelper;
//    }

    public static synchronized JalDharaApplication getInstance() {
        return mInstance;
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public void setSharedPreferance(String key, String value) {
        getInstance().getSharedPreferences(getInstance().getString(R.string.app_name), Context.MODE_PRIVATE).edit().putString(key, value).commit();
    }

    public String getSharedPreferance(String key) {
        return getInstance().getSharedPreferences(getInstance().getString(R.string.app_name), Context.MODE_PRIVATE).getString(key, "");
    }

    public Location getLocation() {
        return mCurrentLocation;
    }

    public void setLocation(Location l) {
        mCurrentLocation = l;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
//        if (databaseHelper != null) {
//            databaseHelper.close();
//        }
    }

    public void createJob() {
        ComponentName componentName = new ComponentName(getPackageName(), LocationJobService.class.getName());
        JobInfo.Builder builder = new JobInfo.Builder(Util.JOB_ID_UPDATE_DRIVER_LOCATION, componentName);
        builder.setPeriodic(Util.JOB_SCHEDULE_TIME);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setRequiresDeviceIdle(false);
        builder.setPersisted(true);

        JobScheduler mJobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        if (mJobScheduler.schedule(builder.build()) <= 0) {
            Log.e(TAG, "onCreate: Some error while scheduling the job");
        }

    }


}
