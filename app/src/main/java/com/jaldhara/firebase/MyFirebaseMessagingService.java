package com.jaldhara.firebase;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.jaldhara.R;
import com.jaldhara.view.HomeForRolesActivity;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;


public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onNewToken(String mToken) {
        super.onNewToken(mToken);

        try {
            // Get updated InstanceID token.
            Log.e("TOKEN :", "TOKEN :" + mToken);
            //PrefHelper.getInstance().setString(PrefHelper.KEY_DEVICE_TOKEN, mToken);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        String message;

        if (remoteMessage.getNotification() != null) {
            if (!TextUtils.isEmpty(remoteMessage.getNotification().getBody())) {
                message = remoteMessage.getNotification().getBody();

                handleNotification(message, "");
            } else {
                getData(remoteMessage);
            }
        } else {
            getData(remoteMessage);
        }
    }

    /*
     * Getting Notification Data
     *
     * @param remoteMessage
     */
    @SuppressLint("NewApi")
    private void getData(RemoteMessage remoteMessage) {
        Map<String, String> messageBody;
        String message = "", title = "";


        if (remoteMessage.getData() != null) {
            messageBody = remoteMessage.getData();
            if (messageBody.containsKey("message") && !TextUtils.isEmpty(messageBody.get("message"))) {
                message = messageBody.get("message");

                handleNotification(message, title);
            }
        } else {
            handleNotification(message, title);
        }
    }

    /**
     * Handling Common Message Notification
     * checking if App is in foreground or background
     */
    /*Chat Notification Type: 7*/
    private void handleNotification(String notificationMessage, String Message) {

        generateNotification(notificationMessage, Message);
    }

    @SuppressLint("InlinedApi")
    private void generateNotification(String message, String title) {
        NotificationCompat.Builder notificationBuilder;
        PendingIntent pendingIntent;
        String channelId = "Jaldhara-01";
        String channelName = "Jaldhara App";
        Intent notificationIntent;


        notificationIntent = new Intent(this, HomeForRolesActivity.class);


        Date now = new Date();
        int id = Integer.parseInt(new SimpleDateFormat("ddHHmmss", Locale.US).format(now));
        pendingIntent = PendingIntent.getActivity(this, id, notificationIntent, PendingIntent.FLAG_ONE_SHOT);


        Uri Sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_app_logo)
                .setColor(ContextCompat.getColor(this, R.color.color_bg_blue))
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(message)
                .setSound(Sound)
                .setAutoCancel(true)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert notificationManager != null;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = new NotificationChannel(channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            stackBuilder.addNextIntent(notificationIntent);
            notificationBuilder.setContentIntent(pendingIntent);
        }

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(notificationIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(id, PendingIntent.FLAG_ONE_SHOT);
        notificationBuilder.setContentIntent(resultPendingIntent);

        Notification notification = notificationBuilder.build();
        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;
        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        //Cancels Notification when Clicked on It
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Sets an ID for the notification
        int mNotificationId = (int) System.currentTimeMillis();

        notificationManager.notify(mNotificationId, notification);
    }
}
