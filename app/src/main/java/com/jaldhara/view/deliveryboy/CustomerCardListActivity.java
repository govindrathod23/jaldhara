package com.jaldhara.view.deliveryboy;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.CustomerCardAdapter;
import com.jaldhara.adapters.ProductListAdminAdapter;
import com.jaldhara.models.CustomerCardModel;
import com.jaldhara.models.DriverCommissionModel;
import com.jaldhara.models.ProductModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.PaginationScrollListener;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.commission.AdminCommissionListActivity;
import com.jaldhara.view.admin.commission.AdminCommissionListAdapter;
import com.jaldhara.view.admin.pendingorder.PendingOrdersActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CustomerCardListActivity extends AppCompatActivity implements View.OnClickListener {


    private Context context;
    ArrayList<CustomerCardModel> arrayCustomerCard;
    private CustomerCardAdapter mAdapter;
    private RecyclerView recyclerViewProducts;

    private static final int PER_PAGE_COUNT = 20;
    private int PAGE_START = 1;
    private int PAGE_LIMIT = 20;
    private int TOTAL_PAGES = 0;
    private int CURRENT_PAGE = 1;
    private boolean isLastPage = false;
    private String fromText = "";
    private String txtTO = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customercardlist);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_purchase_list_activity_imgBack);
        imgBack.setOnClickListener(this);


        recyclerViewProducts = findViewById(R.id.activity_purchase_list_activity_recyclerview);
        arrayCustomerCard = new ArrayList<>();
        mAdapter = new CustomerCardAdapter(context, arrayCustomerCard);
        recyclerViewProducts.setAdapter(mAdapter);


        recyclerViewProducts.addOnScrollListener(new PaginationScrollListener(new LinearLayoutManager(context)) {
            @Override
            protected void loadMoreItems() {
                CURRENT_PAGE += 1;
                PAGE_START += 20;

                if (ConnectivityReceiver.isConnected()) {
                    serviceListOfCustomerCards();
                } else {
                    Snackbar.make(recyclerViewProducts, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        setFromAndToDate();
        if (ConnectivityReceiver.isConnected()) {
            serviceListOfCustomerCards();
        } else {
            Toast.makeText(CustomerCardListActivity.this, getString(R.string.msg_check_internet_connection), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_purchase_list_activity_imgBack) {
            onBackPressed();
        }

    }


    private void serviceListOfCustomerCards() {
        //productList.clear();
        String customerID = getIntent().getStringExtra(getString(R.string.key_intent_customer_id));

        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.customer_card(
                Util.API_CUSTOMER_CARD,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                customerID,
                fromText,
                txtTO);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        Gson gson = new Gson();
                        Type listType = new TypeToken<ArrayList<CustomerCardModel>>() {
                        }.getType();

                        arrayCustomerCard = gson.fromJson(jsonArray.toString(), listType);

//                        adapter = new AdminCommissionListAdapter(AdminCommissionListActivity.this, arryDriverCommission, isFromDriver);
////                        recyclerView.setAdapter(adapter);

                        mAdapter = new CustomerCardAdapter(context, arrayCustomerCard);
                        recyclerViewProducts.setAdapter(mAdapter);
//                        mAdapter.notifyDataSetChanged();

                        if (!(CURRENT_PAGE <= TOTAL_PAGES)) {
                            isLastPage = true;
                        }
                        if (CURRENT_PAGE == TOTAL_PAGES) {
                            isLastPage = true;
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void showAlertDialog(final String purchaseId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Are you sure you want to delete this Product?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        serviceDeletePurchase(purchaseId);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void serviceDeletePurchase(final String purchaseId) {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Removing... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.deletePurchase(Util.API_DELETE_PURCHASE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), purchaseId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                Toast.makeText(CustomerCardListActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
//                                serviceGetPurchases();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }


    private void setFromAndToDate() {


        Calendar calendar = Util.getCalDateFromString(Util.getTodayDate());

        View layoutFrom = (View) findViewById(R.id.ll_Date_from);
        final TextView txtDayFrom = (TextView) layoutFrom.findViewById(R.id.raw_date_day);
        final TextView txtMonthFrom = (TextView) layoutFrom.findViewById(R.id.raw_date_month);
        final TextView txtYearFrom = (TextView) layoutFrom.findViewById(R.id.raw_date_year);

        String strMonth = String.valueOf(calendar.get(Calendar.MONTH) + 1).length() == 2 ? String.valueOf(calendar.get(Calendar.MONTH) + 1) : ("0" + (calendar.get(Calendar.MONTH) + 1));
        String strDay = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)).length() == 2 ? String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)) : ("0" + calendar.get(Calendar.DAY_OF_MONTH));


        txtDayFrom.setText(strDay);
        txtMonthFrom.setText("" + Util.getMonth().get(calendar.get(Calendar.MONTH)));
        txtYearFrom.setText("" + calendar.get(Calendar.YEAR));

        fromText = txtTO = txtYearFrom.getText().toString() + "-" + strMonth + "-" + strDay;

        View layoutTo = (View) findViewById(R.id.ll_Date_to);
        final TextView txtDayTo = (TextView) layoutTo.findViewById(R.id.raw_date_day);
        final TextView txtMonthTo = (TextView) layoutTo.findViewById(R.id.raw_date_month);
        final TextView txtYearTo = (TextView) layoutTo.findViewById(R.id.raw_date_year);

        txtDayTo.setText(strDay);
        txtMonthTo.setText("" + Util.getMonth().get(calendar.get(Calendar.MONTH)));
        txtYearTo.setText("" + calendar.get(Calendar.YEAR));


        layoutFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CustomerCardListActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day) {
                        String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                        String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);

                        fromText = year + "-" + strMonth + "-" + strDay;

                        txtDayFrom.setText(strDay);
                        txtMonthFrom.setText("" + Util.getMonth().get(month));
                        txtYearFrom.setText("" + year);

                        serviceListOfCustomerCards();

                    }
                }, Integer.valueOf(txtYearFrom.getText().toString()), Util.getMonthFromName().get(txtMonthFrom.getText().toString()), Integer.valueOf(txtDayFrom.getText().toString()));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        layoutTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(CustomerCardListActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day) {

                        String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                        String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);

                        txtTO = year + "-" + strMonth + "-" + strDay;

                        txtDayTo.setText(strDay);
                        txtMonthTo.setText("" + Util.getMonth().get(month));
                        txtYearTo.setText("" + year);

                        serviceListOfCustomerCards();

                    }
                }, Integer.valueOf(txtYearTo.getText().toString()), Util.getMonthFromName().get(txtMonthTo.getText().toString()), Integer.valueOf(txtDayTo.getText().toString()));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
    }


}