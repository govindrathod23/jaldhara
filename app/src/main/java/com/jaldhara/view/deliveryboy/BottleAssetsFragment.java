package com.jaldhara.view.deliveryboy;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.adapters.AssetsListAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BottleAssetsFragment extends Fragment {

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    public static String KEY_CUSTOMER_INFO = "CUSTOMER_INFO";
    private ArrayList<DeliveryModel> arryBottleAssets;
    private TextView txtLbl;

    public BottleAssetsFragment() {
    }

    public static BottleAssetsFragment newInstance(BottleInfoModel infoModel) {
        BottleAssetsFragment fragment = new BottleAssetsFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_CUSTOMER_INFO, infoModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vLayout = inflater.inflate(R.layout.fragment_assets, container, false);

        mRecyclerView = (RecyclerView) vLayout.findViewById(R.id.my_recycler_view);
        txtLbl = (TextView) vLayout.findViewById(R.id.lbl_nodelivery);

        return vLayout;
    }





    /**
     * @param arryBottleAssets
     */
    public void setServiceData(ArrayList<DeliveryModel> arryBottleAssets) {
        if (arryBottleAssets.isEmpty()) {
            txtLbl.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            mAdapter = new AssetsListAdapter(getActivity(), arryBottleAssets);
            mRecyclerView.setAdapter(mAdapter);
        }

    }
}
