package com.jaldhara.view.deliveryboy.invoice;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.models.PaymentInvoiceModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DAddInvoicePaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private LinearLayout linearDate, linearCreditCard, linearChequeNumber, linerGiftCard;
    private TextView txtDate,txtlblGiftCard;
    private EditText edtReference, edtAmount, edtCreditCardNumber, edtHolderName, edtCVV, edtChequeNumber, edtNote, edtGiftCardNo;
    private Spinner spinnerPaymentMethod, spinnerCreditCard, spnCCMonth, spnCCYear;
    private String[] paymentMethodList = {"Cash", "Gift Card", "Credit Card", "Cheque/Online",   "Other"};
    private String[] creditCardList = {"Visa", "Master Card", "Amex", "Discover"};
    private String[] arryCCMonth = {"01 (Jan)", "02 (Feb)", "03 (Mar)", "04 (Apr)", "05 (May)", "06 (Jun)", "07 (Jul)", "08 (Aug)", "09 (Sep)", "10 (Oct)", "11 (Nov)", "12 (Dec)"};
    private String[] arryCCYear = {"2020", "2021", "2022", "2023", "2024", "2025", "2026", "2027", "2028", "2029", "2030", "2031", "2032"};
    private InvoiceModel invoiceModel;
    private PaymentInvoiceModel paymentModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add__invoice_payment);

        ImageView imgBack = findViewById(R.id.activity_add_payment_imgBack);
        Button btnAddPayment = findViewById(R.id.activity_add_payment_button_add);
        linearDate = findViewById(R.id.activity_add_payment_linear_date);
        txtDate = findViewById(R.id.activity_add_payment_tv_date);
        edtReference = findViewById(R.id.activity_add_payment_tv_reference);
        edtAmount = findViewById(R.id.activity_add_payment_edt_amount);
        spinnerPaymentMethod = findViewById(R.id.activity_add_payment_spinner_paying_by);
        linearCreditCard = findViewById(R.id.activity_add_payment_linear_credit_card);
        edtCreditCardNumber = findViewById(R.id.activity_add_payment_tv_credit_card_number);
        edtHolderName = findViewById(R.id.activity_add_payment_tv_holder_name);
        spinnerCreditCard = findViewById(R.id.activity_add_payment_spinner_credit_card);
        spnCCMonth = findViewById(R.id.activity_add_payment_edt_month);
        spnCCYear = findViewById(R.id.activity_add_payment_edt_year);
        edtCVV = findViewById(R.id.activity_add_payment_edt_cvv);
        linearChequeNumber = findViewById(R.id.activity_add_payment_linear_cheque_no);
        edtChequeNumber = findViewById(R.id.activity_add_payment_edt_cheque_no);
        edtNote = findViewById(R.id.activity_add_payment_edt_note);
        linerGiftCard = findViewById(R.id.activity_add_payment_linear_giftCard);
        edtGiftCardNo = findViewById(R.id.activity_add_payment_edt_giftCard_no);
        txtlblGiftCard = findViewById(R.id.activity_add_payment_txt_giftCard_no);

        setSpinners();

        imgBack.setOnClickListener(this);
        btnAddPayment.setOnClickListener(this);


        invoiceModel = getIntent().getParcelableExtra(getString(R.string.key_intent_invoice_detail));
        paymentModel = getIntent().getParcelableExtra(getString(R.string.key_intent_invoice_payment));

        if (invoiceModel != null) {
            edtReference.setText(invoiceModel.getInv_reference_no());
            edtReference.setEnabled(false);
            edtAmount.setText(invoiceModel.getBalance());
        }

        if (paymentModel != null) {

            edtReference.setText(paymentModel.getReferenceNo());
            edtAmount.setText(paymentModel.getAmount());
            btnAddPayment.setText("Edit Payment");
            edtNote.setText(paymentModel.getNote());
            spinnerPaymentMethod.setSelection(Arrays.asList(paymentMethodList).indexOf(paymentModel.getPaidBy()));

        }


    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_add_payment_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_add_payment_button_add) {


            serviceAddInvoice();
        }
    }

    private void setSpinners() {

        ArrayAdapter ccMonthAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, arryCCMonth);
        ccMonthAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCCMonth.setAdapter(ccMonthAdapter);

        ArrayAdapter ccYearAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, arryCCYear);
        ccYearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnCCYear.setAdapter(ccYearAdapter);


        ArrayAdapter paymentAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, paymentMethodList);
        paymentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPaymentMethod.setAdapter(paymentAdapter);

        spinnerPaymentMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 1 || position == 4 ) {
                    linerGiftCard.setVisibility(View.VISIBLE);
                    txtlblGiftCard.setText(position == 1 ? "Gift card No" : "Transaction Number");

                    linearCreditCard.setVisibility(View.GONE);
                    linearChequeNumber.setVisibility(View.GONE);


                } else if (position == 2) {
                    linearCreditCard.setVisibility(View.VISIBLE);
                    linearChequeNumber.setVisibility(View.GONE);
                    linerGiftCard.setVisibility(View.GONE);
                } else if (position == 3) {
                    linearCreditCard.setVisibility(View.GONE);
                    linearChequeNumber.setVisibility(View.VISIBLE);
                    linerGiftCard.setVisibility(View.GONE);
                } else {
                    linearCreditCard.setVisibility(View.GONE);
                    linearChequeNumber.setVisibility(View.GONE);
                    linerGiftCard.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter creditCardAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, creditCardList);
        creditCardAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCreditCard.setAdapter(creditCardAdapter);
    }

    /**
     * =============================================================================================
     *
     * @param =============================================================================================
     * @param
     * @param
     */
    private void serviceAddInvoice() {

        final ProgressDialog dialog = ProgressDialog.show(DAddInvoicePaymentActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        String CustomerID = "";
        //3 group id is for customer
        if (JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID)).equalsIgnoreCase("3")){
            CustomerID = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id));
        }else{
            CustomerID = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID));
        }

        Call<ResponseBody> call = null;
        if(paymentModel==null){
              call = apiService.AddPaymentInvoice(
                    Util.API_ADD_PAYMENT,
                    Util.API_KEY,
                    JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                    edtAmount.getText().toString(),
                    paymentMethodList[spinnerPaymentMethod.getSelectedItemPosition()],
                    invoiceModel.getId(),
                    edtReference.getText().toString(),
                    edtChequeNumber.getText().toString(),
                    edtCreditCardNumber.getText().toString(),
                    edtHolderName.getText().toString(),
                    String.valueOf(spnCCMonth.getSelectedItemPosition() + 1),
                    arryCCYear[spnCCYear.getSelectedItemPosition()],
                    creditCardList[spinnerCreditCard.getSelectedItemPosition()],
                    edtNote.getText().toString(),
                      CustomerID,
                    edtGiftCardNo.getText().toString()

            );
        }else{
             call = apiService.EditPaymentInvoice(
                    Util.API_EDIT_PAYMENT,
                    Util.API_KEY,
                    edtAmount.getText().toString(),
                    paymentMethodList[spinnerPaymentMethod.getSelectedItemPosition()],
                    paymentModel.getCustomerInvoicesId(),
                    edtReference.getText().toString(),
                    edtChequeNumber.getText().toString(),
                    edtCreditCardNumber.getText().toString(),
                    edtHolderName.getText().toString(),
                    String.valueOf(spnCCMonth.getSelectedItemPosition() + 1),
                    arryCCYear[spnCCYear.getSelectedItemPosition()],
                    creditCardList[spinnerCreditCard.getSelectedItemPosition()],
                    edtNote.getText().toString(),
                     CustomerID,
                    paymentModel.getId(),
                    edtGiftCardNo.getText().toString(),
                    edtGiftCardNo.getText().toString()
            );
        }



        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
//                        {"message":"add purchases payment","status":true}
                        final String strResponse = response.body().string();
                        JSONObject jsonResponse = new JSONObject(strResponse);

                        Toast.makeText(DAddInvoicePaymentActivity.this, jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                        if (jsonResponse.has("status") && jsonResponse.optBoolean("status")) {
                            finish();
                        }
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(DAddInvoicePaymentActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(DAddInvoicePaymentActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });

    }

}