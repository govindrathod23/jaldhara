package com.jaldhara.view.deliveryboy.invoice;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.R;
import com.jaldhara.adapters.ViewPaymentAdapter;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.models.PaymentInvoiceModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DViewPaymentsActivity extends AppCompatActivity implements View.OnClickListener {

    private String invoiceID = "";
    private RecyclerView recyclerView;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_payments);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_payments_imgBack);
        imgBack.setOnClickListener(this);

        recyclerView = findViewById(R.id.activity_payments_recyclerView);

        try {
            InvoiceModel invoiceModel =  getIntent().getParcelableExtra(getString(R.string.key_intent_invoice_detail));
            if (invoiceModel != null) {
                invoiceID = String.valueOf(invoiceModel.getId());
                serviceGetViewPayments();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        if (id == R.id.activity_payments_imgBack) {
            onBackPressed();
        }

    }





    /**
     *
     */
    private void serviceGetViewPayments() {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.ViewPaymentList(
                Util.API_LIST_OF_PAYMENT,
                Util.API_KEY,
                invoiceID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    List<PaymentInvoiceModel> paymentList = new ArrayList<>();

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("payments")){
                            JSONArray jsonArray = jsonResponse.getJSONArray("payments");
                            for (int i=0;i<jsonArray.length();i++){
                                JSONObject paymentObj = jsonArray.getJSONObject(i);

                                PaymentInvoiceModel paymentModel = new PaymentInvoiceModel();

                                paymentModel.setId(paymentObj.getString("id"));
                                paymentModel.setCompId(paymentObj.getString("comp_id"));
                                paymentModel.setDate(paymentObj.getString("date"));
                                paymentModel.setSaleId(paymentObj.getString("sale_id"));
                                paymentModel.setReturnId(paymentObj.getString("return_id"));
                                paymentModel.setPurchaseId(paymentObj.getString("purchase_id"));
                                paymentModel.setDeliveriesReportId(paymentObj.getString("deliveries_report_id"));
                                paymentModel.setCustomerInvoicesId(paymentObj.getString("customer_invoices_id"));
                                paymentModel.setReferenceNo(paymentObj.getString("reference_no"));
                                paymentModel.setTransactionId(paymentObj.getString("transaction_id"));
                                paymentModel.setPaidBy(paymentObj.getString("paid_by"));
                                paymentModel.setChequeNo(paymentObj.getString("cheque_no"));
                                paymentModel.setCcNo(paymentObj.getString("cc_no"));
                                paymentModel.setCcHolder(paymentObj.getString("cc_holder"));
                                paymentModel.setCcMonth(paymentObj.getString("cc_month"));
                                paymentModel.setCcYear(paymentObj.getString("cc_year"));
                                paymentModel.setCcType(paymentObj.getString("cc_type"));
                                paymentModel.setAmount(paymentObj.getString("amount"));
                                paymentModel.setCurrency(paymentObj.getString("currency"));
                                paymentModel.setCreatedBy(paymentObj.getString("created_by"));
                                paymentModel.setAttachment(paymentObj.getString("attachment"));
                                paymentModel.setCcType(paymentObj.getString("type"));
                                paymentModel.setNote(paymentObj.getString("note"));
                                paymentModel.setPosPaid(paymentObj.getString("pos_paid"));
                                paymentModel.setPosBalance(paymentObj.getString("pos_balance"));
                                paymentModel.setApprovalCode(paymentObj.getString("approval_code"));

                                paymentList.add(paymentModel);
                            }

                            ViewPaymentAdapter adapter = new ViewPaymentAdapter(context, paymentList);
                            recyclerView.setAdapter(adapter);
                            adapter.setOnItemClick(new ViewPaymentAdapter.onItemClick<PaymentInvoiceModel>() {
                                @Override
                                public void onEditClicked(PaymentInvoiceModel data) {
                                    Intent intent = new Intent(DViewPaymentsActivity.this, DAddInvoicePaymentActivity.class);
                                    intent.putExtra(getString(R.string.key_intent_invoice_payment), data);
                                    startActivity(intent);


                                }

                                @Override
                                public void onDeleteClicked(PaymentInvoiceModel data) {
                                    serviceDeletePayments(data.getId());
                                }
                            });
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }


    /**
     *
     * @param id
     */
    private void serviceDeletePayments(String id) {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.delete_payment(
                Util.API_DELETE_PAYMENT,
                Util.API_KEY,
                id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {


                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
//                        {"message":"add purchases payment","status":true}
                            final String strResponse = response.body().string();
                            Toast.makeText(DViewPaymentsActivity.this, jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                            if (ConnectivityReceiver.isConnected()) {
                                serviceGetViewPayments();
                            } else {
                                Snackbar.make(recyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                            }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

}