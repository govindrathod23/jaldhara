package com.jaldhara.view.deliveryboy;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.SignatureView;
import com.jaldhara.utills.Util;
import com.jaldhara.view.customer.ViewCustomerDeliveryActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BottlPaymentFragment extends Fragment implements View.OnClickListener {

    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 1;
    private String[] storage_permissions =
            {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };
    private Button btnCheque;
    private Button btnCash;
    private LinearLayout llContainer;
    private TextView txtDueAmount;
    private String paymentTag = "";

    public BottlPaymentFragment() {
    }

    public static BottlPaymentFragment newInstance(BottleInfoModel infoModel) {
        BottlPaymentFragment fragment = new BottlPaymentFragment();
        Bundle args = new Bundle();
        args.putParcelable(Util.KEY_INTENT_CUSTOMER_MODEL, infoModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vLayout = inflater.inflate(R.layout.fragment_payment, container, false);
        btnCheque = (Button) vLayout.findViewById(R.id.fragment_payment_btn_Cheque);
        btnCash = (Button) vLayout.findViewById(R.id.fragment_payment_btn_Cash);
        llContainer = (LinearLayout) vLayout.findViewById(R.id.fragment_payment_ll_container);
        btnCheque.setOnClickListener((View.OnClickListener) this);
        btnCash.setOnClickListener((View.OnClickListener) this);


        txtDueAmount = (TextView) vLayout.findViewById(R.id.fragment_payment_txt_dueAmount);
        txtDueAmount.setText("Due Amount : Rs.0");
        if (ConnectivityReceiver.isConnected()) {
            getListOfInvoices();
        } else {
            Snackbar.make(vLayout, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        return vLayout;


    }


    @Override
    public void onClick(View v) {

        paymentTag = ((TextView) v).getText().toString();

        if ((int) Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                        builder.setMessage("To get storage access you have to allow us access to your sd card content.");
                        builder.setTitle("Storage");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(), storage_permissions, 0);
                                openPaymentDialog();
                            }
                        });

                        builder.show();
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), storage_permissions, 0);
                        openPaymentDialog();
                    }
                } else {
                    ActivityCompat.requestPermissions(getActivity(),
                            storage_permissions,
                            MY_PERMISSIONS_REQUEST_STORAGE);
                    openPaymentDialog();
                }

            } else {
                openPaymentDialog();
            }
        }

    }

    private void openPaymentDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_payment);


        Button btnCancel = (Button) dialog.findViewById(R.id.dialog_payment_btn_cancel);
        Button btnClear = (Button) dialog.findViewById(R.id.dialog_payment_btn_Clear);
        Button btnSubmit = (Button) dialog.findViewById(R.id.dialog_payment_btn_Submit);
        final EditText edtAmount = (EditText) dialog.findViewById(R.id.dialog_payment_edt_amount);
        final SignatureView signatureView = (SignatureView) dialog.findViewById(R.id.dialog_payment_signattureView);


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clearSignature();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!signatureView.isSignatureOk()) {
                    Toast.makeText(getActivity(), "Please do signature.", Toast.LENGTH_LONG).show();
                    return;

                } else if (TextUtils.isEmpty(edtAmount.getText().toString().trim())) {
                    Toast.makeText(getActivity(), "Please enter amount!", Toast.LENGTH_LONG).show();
                    return;
                }

                if (ConnectivityReceiver.isConnected()) {
                    signatureView.saveSignature(getActivity());
                    dialog.dismiss();
                    submitPayment(edtAmount.getText().toString());
                } else {
                    Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                }

            }
        });

        dialog.show();

    }


    /*****************************************************************************************************
     *
     *****************************************************************************************************
     */
    private void getListOfInvoices() {
        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Loading... ", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        BottleInfoModel bottleInfoModel = getArguments().getParcelable(Util.KEY_INTENT_CUSTOMER_MODEL);
        //Call<ResponseBody> call = apiService.getOrder("get_order", Util.API_KEY, dateToStr, bottleInfoModel.getCustomerID());

        Call<ResponseBody> call = apiService.customerPaymentReport("customer_wise_monthly_reports",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                bottleInfoModel.getCustomerID(),
                "2020");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("status") && !jsonResponse.optBoolean("status")) {
                            Toast.makeText(getActivity(), jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                            return;
                        }


                        JSONArray jArray = jsonResponse.optJSONArray("data");

                        Gson gson = new Gson();
                        String jsonOutput = jArray.toString();
                        Type listType = new TypeToken<List<DeliveryModel>>() {
                        }.getType();
                        final List<DeliveryModel> posts = gson.fromJson(jsonOutput, listType);

                        double amount = 0;
                        for (int i = 0; i < posts.size(); i++) {
                            final View child = getLayoutInflater().inflate(R.layout.raw_payment, null);


                            LinearLayout ll_main = (LinearLayout) child.findViewById(R.id.ll_main);
                            TextView txtInvoice = (TextView) child.findViewById(R.id.raw_payment_invoice);
                            final TextView txtDate = (TextView) child.findViewById(R.id.raw_payment_date);
                            TextView txtStatus = (TextView) child.findViewById(R.id.raw_payment_Status);
                            TextView txtMode = (TextView) child.findViewById(R.id.raw_payment_Mode);
                            TextView txtAmount = (TextView) child.findViewById(R.id.raw_payment_Amount);
                            TextView txtRemaining = (TextView) child.findViewById(R.id.raw_payment_remaining);


                            txtInvoice.setText("-");
                            txtDate.setText(posts.get(i).getMonth());
                            txtStatus.setText(posts.get(i).getBalance().toString().contains("0.") ? "Paid" : "Unpaid");
                            txtMode.setText("-");
                            txtAmount.setText(posts.get(i).getTotal());
                            txtRemaining.setText(posts.get(i).getBalance());

                            try {

                                if (posts.get(i).getBalance() != null && (!posts.get(i).getBalance().equalsIgnoreCase("null"))) {
                                    amount = amount + (Double.parseDouble(posts.get(i).getBalance()));
                                }
                                txtDueAmount.setText("Due Amount : Rs." + amount);
                            } catch (Exception e) {

                            }

                            if (i % 2 == 0) {
                                ll_main.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_list_item));
                            } else {
                                ll_main.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.color_bg_cards));
                            }


                            final int finalItemp = i;
                            ll_main.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (ConnectivityReceiver.isConnected()) {
                                        serviceOpenPDFInvoice(posts.get(finalItemp).getInvoice_id());
                                    } else {
                                        Snackbar.make(txtDate, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                                    }
                                }
                            });


                            llContainer.addView(child);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceOpenPDFInvoice(String invoceID) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        BottleInfoModel bottleInfoModel = getArguments().getParcelable(Util.KEY_INTENT_CUSTOMER_MODEL);

        Call<ResponseBody> call = apiService.getInvoice(
                "get_invoice",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                invoceID);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {

                        final String strResponse = response.body().string();
                        JSONObject jsonResponse = new JSONObject(strResponse);


                        Uri uri = Uri.parse(jsonResponse.optString("invoice"));
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(uri, "application/pdf");
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                        try {
                            startActivity(intent);
                        } catch (ActivityNotFoundException e) {
                            // Instruct the user to install a PDF reader here, or something
                            e.printStackTrace();

                            Intent intentWebView = new Intent(getActivity(), ViewInvoiceActivity.class);
                            intentWebView.putExtra(getActivity().getString(R.string.key_intent_invoice_content), jsonResponse.optString("invoice"));
                            getActivity().startActivity(intentWebView);
                        }


//                        if (jsonResponse.has("status") && !jsonResponse.optBoolean("status")) {
//                            Toast.makeText(getActivity(), jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
//                            return;
//                        }
//
//
//                        JSONArray jArray = jsonResponse.optJSONArray("data");


                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void submitPayment(String amount) {


        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        BottleInfoModel bottleInfoModel = getArguments().getParcelable(Util.KEY_INTENT_CUSTOMER_MODEL);

        File file = new File(Util.IMAGE_PATH);

        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("document", file.getName(), fileReqBody);


        RequestBody requestAPI = RequestBody.create(MediaType.parse("text/plain"), "pay_invoice_by_customer");
        RequestBody requestAPIKey = RequestBody.create(MediaType.parse("text/plain"), Util.API_KEY);
        RequestBody requestCompId = RequestBody.create(MediaType.parse("text/plain"), JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        RequestBody requestCustomer = RequestBody.create(MediaType.parse("text/plain"), bottleInfoModel.getCustomerID());
        RequestBody requestAmount = RequestBody.create(MediaType.parse("text/plain"), amount);
        RequestBody requestPaymentTag = RequestBody.create(MediaType.parse("text/plain"), paymentTag);


        Call<ResponseBody> call = apiService.submitPayment(
                requestAPI,
                requestAPIKey,
                requestCompId,
                requestCustomer,
                requestAmount,
                requestPaymentTag,
                part
        );


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.optBoolean("status")) {

                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                            builder.setMessage(jsonResponse.optString("data")
                                    + "\nYour Remaining payment is " + jsonResponse.optJSONObject("array").optString("balance"));

                            builder.setTitle("Payment");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getActivity().finish();
                                }
                            });
                            builder.setCancelable(false);
                            builder.show();


                        }
//                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
