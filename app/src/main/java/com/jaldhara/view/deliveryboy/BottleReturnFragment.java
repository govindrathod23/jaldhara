package com.jaldhara.view.deliveryboy;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.BottleReturnListAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.utills.onRemoveClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BottleReturnFragment extends Fragment implements onRemoveClick {


    //    private FloatingActionButton btnAddBottle;
    private RecyclerView mRecyclerView;
    private BottleReturnListAdapter mAdapter;
    private onRemoveClick removeClickListener;
    private ArrayList<DeliveryModel> arryBottleReturns;
    public static String KEY_CUSTOMER_INFO = "CUSTOMER_INFO";
    private TextView txtLbl;


    public BottleReturnFragment() {
    }

    public static BottleReturnFragment newInstance(BottleInfoModel infoModel) {
        BottleReturnFragment fragment = new BottleReturnFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_CUSTOMER_INFO, infoModel);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vLayout = inflater.inflate(R.layout.fragment_bottles_return, container, false);


//        btnAddBottle = (FloatingActionButton) vLayout.findViewById(R.id.fragment_bottles_return_btn_payment);
//        btnAddBottle.setVisibility(View.GONE);
        mRecyclerView = (RecyclerView) vLayout.findViewById(R.id.fragment_bottles_return_recycler_view);

        txtLbl = (TextView) vLayout.findViewById(R.id.lbl_nodelivery);


        arryBottleReturns = new ArrayList<DeliveryModel>();
        mAdapter = new BottleReturnListAdapter(getActivity(), arryBottleReturns);
        mAdapter.setRemoveClickListener(this);
        mRecyclerView.setAdapter(mAdapter);

//        btnAddBottle.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                openDialogForAddBottles();
//            }
//        });

//        serviceReturnOrder();

        return vLayout;
    }


    @Override
    public void onClick(int position) {
        Log.v("TAG", "click working on : " + position);
//        arrayListBottleReturn.remove(position);
//        arrayListBottleReturn.trimToSize();
//        mAdapter = new BottleReturnListAdapter(getActivity(), arrayListBottleReturn);
//        mAdapter.setRemoveClickListener(this);
//        mRecyclerView.setAdapter(mAdapter);

    }


    /*******************************************************
     *
     *******************************************************/
    public void onDoneClick() {

        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arryBottleReturns.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("product_id", arryBottleReturns.get(i).getSales_id());
                jsonObject.put("quantity", arryBottleReturns.get(i).getQuantity());
                jsonObject.put("adjustment_quantity", arryBottleReturns.get(i).isSwitchOn() ? arryBottleReturns.get(i).getAdjustmentQuantity() : "0");
                jsonArray.put(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (ConnectivityReceiver.isConnected()) {
            serviceAddReturnOrder(jsonArray.toString());
        } else {
            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
    }


    private void serviceAddReturnOrder(String jsonArray) {
        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        BottleInfoModel bottleInfoModel = getArguments().getParcelable(KEY_CUSTOMER_INFO);

        Call<ResponseBody> call = apiService.addReturn(
                Util.API_ADD_RETURN,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                bottleInfoModel.getCustomerID(),
                jsonArray,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID))
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.optBoolean("status")) {

                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                            builder.setMessage(jsonResponse.optString("message"));
                            builder.setTitle("Return");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getActivity().finish();
                                }
                            });
                            builder.setCancelable(false);
                            builder.show();


                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Please try after sometime!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(getActivity(), "Please try after sometime!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void setServiceData(ArrayList<DeliveryModel> arryBottleReturns) {
        if (arryBottleReturns.isEmpty()) {
            txtLbl.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
        } else {
            this.arryBottleReturns = arryBottleReturns;
            mAdapter = new BottleReturnListAdapter(getActivity(), arryBottleReturns);
            mRecyclerView.setAdapter(mAdapter);
        }
    }
}
