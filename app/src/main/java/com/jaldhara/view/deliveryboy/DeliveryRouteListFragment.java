package com.jaldhara.view.deliveryboy;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.ExpandableListAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.Locations;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeliveryRouteListFragment extends Fragment {

    private View vLayout;
    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private TextView txtLbl;
    private List<BottleInfoModel> listDataHeader;
    private HashMap<BottleInfoModel, List<BottleInfoModel>> listDataChild;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        vLayout = inflater.inflate(R.layout.content_main, container, false);

        init();


        return vLayout;
    }

    @Override
    public void onStart() {
        super.onStart();
        // JalDharaApplication.getInstance().createJob();

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfCustomer();
        } else {
            Snackbar.make(getActivity().getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
            txtLbl.setText("Please check your internet connection!");
            txtLbl.setVisibility(View.VISIBLE);
            expListView.setVisibility(View.GONE);
        }

    }


    @Override
    public void onDetach() {
        super.onDetach();
//        JalDharaApplication.getInstance().();
    }

    private void init() {

        expListView = (ExpandableListView) vLayout.findViewById(R.id.lvExp);
        txtLbl = (TextView) vLayout.findViewById(R.id.content_main_txt_lbl);
        listDataHeader = new ArrayList<BottleInfoModel>();
        listDataChild = new HashMap<BottleInfoModel, List<BottleInfoModel>>();

        // Listview Group click listener
        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
//                Toast.makeText(getActivity(),
//                        "Group Clicked " + listDataHeader.get(groupPosition),
//                        Toast.LENGTH_SHORT).show();
                return false;
            }
        });

        // Listview Group expanded listener
        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
//                Toast.makeText(getActivity(),
//                        listDataHeader.get(groupPosition) + " Expanded",
//                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview Group collasped listener
        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getActivity(),
//                        listDataHeader.get(groupPosition) + " Collapsed",
//                        Toast.LENGTH_SHORT).show();
            }
        });

        // Listview on child click listener
        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                // TODO Auto-generated method stub
//                Toast.makeText(getActivity(), listDataHeader.get(groupPosition) + " : " + listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition), Toast.LENGTH_SHORT).show();


                Intent intent = new Intent(getActivity(), BottleManageTab.class);
                intent.putExtra(BottleDeliveryFragment.KEY_CUSTOMER_INFO, listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
                getActivity().startActivity(intent);

//                FragmentManager fragmentManager = getChildFragmentManager();

//                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                BottleManageTab bottleManageTab = BottleManageTab.newInstance(listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition));
//                fragmentManager.beginTransaction().add(R.id.container_fragment, bottleManageTab).addToBackStack("BOTTLE_MANAGE_TAB").commit();

                return false;
            }
        });
    }


    private void serviceListOfCustomer() {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "Please wait", "Getting Customer", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ResponseBody> call = apiService.listOfCustomer(
                "routes_wise_customer",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID)));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    listDataHeader.clear();
                    listDataChild.clear();
                    int headerCountOnlyCust = 0;

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int header = 0; header < jsonArray.length(); header++) {
                            BottleInfoModel modelHeader = new BottleInfoModel();
                            modelHeader.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                            String routeID = jsonArray.optJSONObject(header).optString("id");

                            JSONArray jsonArrayChild = jsonArray.optJSONObject(header).optJSONArray("customer");
                            if (jsonArrayChild != null) {
                                int cusArrLen = jsonArrayChild == null ? 0 : jsonArrayChild.length();
                                modelHeader.setCustomrsOnRoute(cusArrLen + "");

                                listDataHeader.add(modelHeader);

                                final ArrayList<BottleInfoModel> array = new ArrayList<>();
                                for (int child = 0; child < jsonArrayChild.length(); child++) {
                                    BottleInfoModel modelChild = new BottleInfoModel();
                                    JSONObject jsonObjectChild = jsonArrayChild.optJSONObject(child);
                                    modelChild.setCompany(jsonObjectChild.optString("company"));
                                    modelChild.setCustomerID(jsonObjectChild.optString("c_id"));
                                    modelChild.setAlias_name(jsonObjectChild.optString("alias_name"));
                                    modelChild.setLatitude(jsonObjectChild.optString("latitude"));
                                    modelChild.setLongitude(jsonObjectChild.optString("longitude"));
                                    modelChild.setCustomer_status(jsonObjectChild.optString("customer_status"));
                                    modelChild.setRouteID(routeID);

                                    array.add(modelChild);
                                }
                                Collections.sort(array, new Comparator<BottleInfoModel>() {
                                    @Override
                                    public int compare(BottleInfoModel lhs, BottleInfoModel rhs) {
                                        return rhs.getCustomer_status().compareTo(lhs.getCustomer_status());
                                    }
                                });
                                listDataChild.put(listDataHeader.get(headerCountOnlyCust), array);
                                headerCountOnlyCust++;
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                if (listDataHeader.isEmpty()) {
                    txtLbl.setText("No Data Found!");
                    txtLbl.setVisibility(View.VISIBLE);
                    expListView.setVisibility(View.GONE);
                } else {
                    listAdapter = new ExpandableListAdapter(getActivity(), listDataHeader, listDataChild);
                    expListView.setAdapter(listAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
