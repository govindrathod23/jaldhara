package com.jaldhara.view.deliveryboy;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.jaldhara.R;

public class ViewInvoiceActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoice_pdf);

        WebView webView = (WebView) findViewById(R.id.activity_admin_report_webview);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
//        webView.loadData(getIntent().getStringExtra(getString(R.string.key_intent_invoice_content)), "text/html", "UTF-8");
//        webView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + getIntent().getStringExtra(getString(R.string.key_intent_invoice_content)));
        webView.loadUrl("https://drive.google.com/viewerng/viewer?embedded=true&url=" + getIntent().getStringExtra(getString(R.string.key_intent_invoice_content)));

        final ProgressDialog pDialog = new ProgressDialog(this);
        pDialog.setMessage("Loading...");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pDialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pDialog.dismiss();
            }
        });


    }
}
