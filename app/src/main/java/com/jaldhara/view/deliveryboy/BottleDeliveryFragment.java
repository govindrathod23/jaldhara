package com.jaldhara.view.deliveryboy;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;

import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.BottleDeliveryListAdapter;
import com.jaldhara.adapters.ExpanOrderAdapter;
import com.jaldhara.adapters.ExpandableListAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.SignatureView;
import com.jaldhara.utills.Util;
import com.jaldhara.utills.onRemoveClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BottleDeliveryFragment extends Fragment implements onRemoveClick {

    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 1;
    private String[] storage_permissions =
            {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };


    private ExpanOrderAdapter listAdapter;
    private ExpandableListView expListView;
    private HashMap<DeliveryModel, List<DeliveryModel>> listDataChild;
    private List<DeliveryModel> listDataHeader;
    public static String KEY_CUSTOMER_INFO = "CUSTOMER_INFO";


    private TextView txtLbl;

    public BottleDeliveryFragment() {
    }

    public static BottleDeliveryFragment newInstance(BottleInfoModel infoModel) {
        BottleDeliveryFragment fragment = new BottleDeliveryFragment();
        Bundle args = new Bundle();
        args.putParcelable(KEY_CUSTOMER_INFO, infoModel);
        fragment.setArguments(args);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vLayout = inflater.inflate(R.layout.fragment_bottles_delivery, container, false);

        expListView = (ExpandableListView) vLayout.findViewById(R.id.fragment_bottles_return_recycler_view);
        txtLbl = (TextView) vLayout.findViewById(R.id.lbl_nodelivery);

        listDataHeader = new ArrayList<DeliveryModel>();
        listDataChild = new HashMap<DeliveryModel, List<DeliveryModel>>();

        Util.hideSoftKeyboardFragment(BottleDeliveryFragment.this);

        return vLayout;
    }

    @Override
    public void onClick(int position) {
        Log.v("TAG", "click working on : " + position);
    }


    public void onDoneClick() {
        if ((int) Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) &&
                        ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) &&
                            ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                        builder.setMessage("To get storage access you have to allow us access to your sd card content.");
                        builder.setTitle("Storage");
                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(getActivity(), storage_permissions, 0);
                                openDeliveryDialog();
                            }
                        });
                        builder.show();
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), storage_permissions, 0);
                        openDeliveryDialog();
                    }
                } else {
                    ActivityCompat.requestPermissions(getActivity(), storage_permissions, MY_PERMISSIONS_REQUEST_STORAGE);
                    openDeliveryDialog();
                }

            } else {
                openDeliveryDialog();
            }
        }else {
            openDeliveryDialog();
        }


    }


    private void openDeliveryDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_delivery);


        Button btnCancel = (Button) dialog.findViewById(R.id.dialog_payment_btn_cancel);
        Button btnClear = (Button) dialog.findViewById(R.id.dialog_payment_btn_Clear);
        final Button btnSubmit = (Button) dialog.findViewById(R.id.dialog_payment_btn_Submit);
        final EditText edtDeliverBy = (EditText) dialog.findViewById(R.id.dialog_payment_edt_deliverdBy);
        final EditText edtReceiveBy = (EditText) dialog.findViewById(R.id.dialog_payment_edt_receivedBy);

        final SignatureView signatureView = (SignatureView) dialog.findViewById(R.id.dialog_payment_signattureView);

        edtDeliverBy.setText(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_user_name)));
        edtDeliverBy.setEnabled(false);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clearSignature();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                signatureView.saveSignature(getActivity());
                dialog.dismiss();

                try {

                    JSONArray jarrayData = new JSONArray();

                    for (int i = 0; i < listDataHeader.size(); i++) {
                        DeliveryModel deliveryModel = listDataHeader.get(i);

                        JSONObject jObRoot = new JSONObject();
                        jObRoot.put("sales_id", deliveryModel.getSales_id().toString());
                        jObRoot.put("sale_reference_no", deliveryModel.getSale_reference_no().toString());
                        jObRoot.put("is_finish", deliveryModel.getIs_finish());

                        jObRoot.put("order_tax_id", deliveryModel.getOrder_tax_id().toString());
                        jObRoot.put("warehouse_id", deliveryModel.getWarehouse_id().toString());
                        jObRoot.put("biller_id", deliveryModel.getBiller_id().toString());
                        jObRoot.put("sale_status", deliveryModel.getSale_status().toString());

                        JSONArray arrayItem = new JSONArray();

                        for (int j = 0; j < listDataChild.get(listDataHeader.get(i)).size(); j++) {

                            DeliveryModel deliveryModelchild = listDataChild.get(listDataHeader.get(i)).get(j);

                            JSONObject jobjItem = new JSONObject();
                            jobjItem.put("product_code", deliveryModelchild.getProduct_code());
                            jobjItem.put("product_id", deliveryModelchild.getProduct_id());
                            jobjItem.put("quantity", deliveryModelchild.getQuantity());
                            jobjItem.put("pending_qty", deliveryModelchild.getPendingQty());
                            jobjItem.put("delivered", deliveryModelchild.getDelivered());


                            jobjItem.put("unit_price", deliveryModelchild.getUnit_price());
                            jobjItem.put("product_name", deliveryModelchild.getProduct_name());

                            jobjItem.put("cgst", deliveryModelchild.getCgst());
                            jobjItem.put("comment", deliveryModelchild.getComment());
                            jobjItem.put("discount", deliveryModelchild.getDiscount());
                            jobjItem.put("gst", deliveryModelchild.getGst());
                            jobjItem.put("igst", deliveryModelchild.getIgst());

                            jobjItem.put("item_discount", deliveryModelchild.getItem_discount());
                            jobjItem.put("item_tax", deliveryModelchild.getItem_tax());
                            jobjItem.put("net_unit_price", deliveryModelchild.getNet_unit_price());
                            jobjItem.put("product_type", deliveryModelchild.getProduct_type());
                            jobjItem.put("product_unit_code", deliveryModelchild.getProduct_unit_code());

                            jobjItem.put("product_unit_id", deliveryModelchild.getProduct_unit_id());
                            jobjItem.put("product_unit_quantity", deliveryModelchild.getProduct_unit_quantity());
                            jobjItem.put("serial_no", deliveryModelchild.getSerial_no());
                            jobjItem.put("sgst", deliveryModelchild.getSgst());
                            jobjItem.put("subtotal", deliveryModelchild.getSubtotal());

                            jobjItem.put("tax", deliveryModelchild.getTax());
                            jobjItem.put("tax_rate_id", deliveryModelchild.getTax_rate_id());

                            arrayItem.put(jobjItem);
                        }
                        jObRoot.put("item", arrayItem);
                        jarrayData.put(jObRoot);

                    }
                    Log.v("Data jarrayData: ", "jarrayData : " + jarrayData.toString());


                    if (ConnectivityReceiver.isConnected()) {
                        submitDeliveryItems(edtDeliverBy.getText().toString(), edtReceiveBy.getText().toString(), jarrayData.toString());
                    } else {
                        Snackbar.make(btnSubmit, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    Util.showDialog(getActivity(), "Bottle deliver error", "err msg : " + e.getMessage() + "\n\n\n****only e" + e);
                }


            }
        });

        dialog.show();
    }

    private void submitDeliveryItems(String deliveredBy, String receivedBy, String jsonArray) {


        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        BottleInfoModel bottleInfoModel = getArguments().getParcelable(KEY_CUSTOMER_INFO);



            File file = SignatureView.getSignatireFile(getActivity());


        //File file = new File("/storage/emulated/0/Pictures/JalDhara_Signature.png");

        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);
        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("document", file.getName(), fileReqBody);


        RequestBody requestAPI = RequestBody.create(MediaType.parse("text/plain"), "add_delivery");
        RequestBody requestAPIKey = RequestBody.create(MediaType.parse("text/plain"), Util.API_KEY);
        RequestBody requestCompID = RequestBody.create(MediaType.parse("text/plain"), JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        RequestBody requestCustomerID = RequestBody.create(MediaType.parse("text/plain"), bottleInfoModel.getCustomerID());
        RequestBody requestStatus = RequestBody.create(MediaType.parse("text/plain"), "delivered");
        RequestBody requestDeliverBy = RequestBody.create(MediaType.parse("text/plain"), deliveredBy);
        RequestBody requestReceivedBy = RequestBody.create(MediaType.parse("text/plain"), receivedBy);
        RequestBody requestCreatedBy = RequestBody.create(MediaType.parse("text/plain"), JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID)));
        RequestBody requestData = RequestBody.create(MediaType.parse("text/plain"), jsonArray);


        Call<ResponseBody> call = apiService.addDelivery(
                requestAPI,
                requestAPIKey,
                requestCompID,
                requestCustomerID,
                requestStatus,
                requestDeliverBy,
                requestReceivedBy,
                requestCreatedBy,
                requestData,
                part
        );


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        Toast.makeText(getActivity(), jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                        if (jsonResponse.optBoolean("status")) {

                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                            builder.setMessage(jsonResponse.optString("message"));
                            builder.setTitle("Delivery");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getActivity().finish();
                                }
                            });
                            builder.setCancelable(false);
                            builder.show();


                        }
//                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                    }

                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                    e.getMessage();
                    Util.showDialog(getActivity(), "Driver error", "error msg : " + e.getMessage() + "\n\n\n****only e" + e);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
                Util.showDialog(getActivity(), "Driver error", "onFailure msg : " + t.toString());

            }
        });
    }

    public void setServiceData(List<DeliveryModel> listDataHeader, HashMap<DeliveryModel, List<DeliveryModel>> listDataChild) {

        if (listDataHeader.isEmpty()) {
            txtLbl.setVisibility(View.VISIBLE);
            expListView.setVisibility(View.GONE);
        } else {
            this.listDataHeader = listDataHeader;
            this.listDataChild = listDataChild;

            listAdapter = new ExpanOrderAdapter(getActivity(), listDataHeader, listDataChild);
            expListView.setAdapter(listAdapter);
        }
    }

}
