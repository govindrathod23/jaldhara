package com.jaldhara.view.deliveryboy.invoice;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.InvoiceListAdminAdapter;
import com.jaldhara.adapters.InvoiceListDriverAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.invoice.InvoicesActivity;
import com.jaldhara.view.admin.invoice.ViewInvoiceListActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DInvoicesFragmnet extends Fragment {

    private RecyclerView mRecyclerView;
    private ArrayList<InvoiceModel> arryInvoice;
    private InvoiceListDriverAdapter mAdapter;
    private ImageView btnCreateInvoice;
    public static String KEY_CUSTOMER_INFO = "CUSTOMER_INFO";

    public static DInvoicesFragmnet newInstance(BottleInfoModel infoModel) {
        DInvoicesFragmnet fragment = new DInvoicesFragmnet();
        Bundle args = new Bundle();
        args.putParcelable(KEY_CUSTOMER_INFO, infoModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View vLayout = inflater.inflate(R.layout.activity_invoices, container, false);

        ((Toolbar) vLayout.findViewById(R.id.toolbar)).setVisibility(View.GONE);

        mRecyclerView = (RecyclerView) vLayout.findViewById(R.id.activity_invoice_recyclerview);
        arryInvoice = new ArrayList<InvoiceModel>();

        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mAdapter = new InvoiceListDriverAdapter(getActivity(), arryInvoice);
        //mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        btnCreateInvoice = vLayout.findViewById(R.id.activity_invoice_btn_create);
        btnCreateInvoice.setVisibility(View.GONE);


        if (ConnectivityReceiver.isConnected()) {
            serviceListOfInvoice();
        } else {
            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }


        ((TextView)vLayout.findViewById(R.id.lbl_customer)).setVisibility(View.GONE);
        ((TextView)vLayout.findViewById(R.id.activity_invoice_SpnCustomer)).setVisibility(View.GONE);
        ((TextView)vLayout.findViewById(R.id.lblRoute)).setVisibility(View.GONE);
        ((Spinner)vLayout.findViewById(R.id.activity_admin_invoice_SpnRoutes)).setVisibility(View.GONE);


        return vLayout;


    }


    private void serviceListOfInvoice() {
        arryInvoice.clear();

        BottleInfoModel bottleInfoModel = getArguments().getParcelable(KEY_CUSTOMER_INFO);


        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getInvoiceList(
                Util.API_LIST_OF_INVOICE,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                bottleInfoModel.getCustomerID());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());


                        if (jsonResponse.has("invoice")) {
                            JSONArray jsonArrayInvoice = jsonResponse.optJSONArray("invoice");

//                            Gson gson = new Gson();
//                            Type listType = new TypeToken<ArrayList<InvoiceModel>>() {
//                            }.getType();
//
//                            arryInvoice = gson.fromJson(jsonArrayInvoice.toString(), listType);

                            for (int i = 0; i < jsonArrayInvoice.length(); i++) {
                                JSONObject jobj = jsonArrayInvoice.getJSONObject(i);


                                InvoiceModel model = new InvoiceModel();
                                model.setInv_reference_no(jobj.optString("inv_reference_no"));
                                model.setId(jobj.optString("id"));
                                model.setComp_id(jobj.optString("comp_id"));
                                model.setBiller_id(jobj.optString("biller_id"));
                                model.setBiller(jobj.optString("biller"));
                                model.setCustomer_id(jobj.optString("customer_id"));
                                model.setCustomer(jobj.optString("customer"));
                                model.setTotal_item(jobj.optString("total_item"));
                                model.setTotal_amount(jobj.optString("total_amount"));
                                model.setPaid(jobj.optString("paid"));
                                model.setBalance(jobj.optString("balance"));
                                model.setTax(jobj.optString("tax"));
                                model.setPayment_status(jobj.optString("payment_status"));
                                model.setPaid_by(jobj.optString("paid_by"));
                                model.setInv_month(jobj.optString("inv_month"));
                                model.setFrom_date(jobj.optString("from_date"));
                                model.setTo_date(jobj.optString("to_date"));
                                model.setNote(jobj.optString("note"));
                                model.setStaff_note(jobj.optString("staff_note"));
                                model.setCreated_at(jobj.optString("created_at"));
                                model.setUpdated_at(jobj.optString("updated_at"));
                                model.setName(jobj.optString("name"));
                                model.setRoute_name(jobj.optString("route_name"));
                                model.setRoute_id(jobj.optString("route_id"));

                                arryInvoice.add(model);
                            }

                            mAdapter = new InvoiceListDriverAdapter(getActivity(), arryInvoice);
                            mRecyclerView.setAdapter(mAdapter);

                            mAdapter.setOnItemClick(new InvoiceListDriverAdapter.onItemClick<InvoiceModel>() {
                                @Override
                                public void onDetailClicked(InvoiceModel data) {
                                    Intent intent = new Intent(getActivity(), ViewInvoiceListActivity.class);
                                    intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
                                    startActivity(intent);

                                }

                                @Override
                                public void onAddPaymentClicked(InvoiceModel data) {
                                    Intent intent = new Intent(getActivity(), DAddInvoicePaymentActivity.class);
                                    intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
                                    startActivity(intent);
                                }

                                @Override
                                public void onDeleteClicked(final InvoiceModel data) {
                                    Util.showDialog(getActivity(), "Delete Invoice", "Please contact your admin.\nOnly admin can delete the invoice.");
                                }

                                @Override
                                public void onEditClicked(InvoiceModel data) {
                                    Util.showDialog(getActivity(), "Edit Invoice", "Please contact your admin.\nOnly admin can edit the invoice.");
                                }

                                @Override
                                public void onViewPaymentsClicked(InvoiceModel data) {
                                    Intent intent = new Intent(getActivity(), DViewPaymentsActivity.class);
                                    intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
                                    startActivity(intent);
                                }

                                @Override
                                public void onEmailInvoice(InvoiceModel data) {

                                    Intent intent = new Intent(Intent.ACTION_SEND);
                                    intent.setType("plain/text");
                                    intent.putExtra(Intent.EXTRA_SUBJECT, "Your Invoice");
                                    intent.putExtra(Intent.EXTRA_TEXT, "invoice text HTML ma banava padse body mate");
                                    startActivity(Intent.createChooser(intent, "Mail invoice"));

                                }

                            });
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void serviceDeleteInvoice(String invoiceID) {

        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.delete_invoice(
                Util.API_DELETE_INVOICE,
                Util.API_KEY,
                invoiceID
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {
                    if (response.isSuccessful()) {
//                        {"message":"add purchases payment","status":true}
                        final String strResponse = response.body().string();
                        JSONObject jsonResponse = new JSONObject(strResponse);

                        Toast.makeText(getActivity(), jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                        if (ConnectivityReceiver.isConnected()) {
                            serviceListOfInvoice();
                        } else {
                            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }


}