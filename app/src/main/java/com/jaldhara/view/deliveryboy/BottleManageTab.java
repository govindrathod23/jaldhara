package com.jaldhara.view.deliveryboy;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.view.menu.MenuBuilder;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.BestLocationProvider;
import com.jaldhara.view.deliveryboy.invoice.DInvoicesFragmnet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BottleManageTab extends BestLocationProvider implements ViewPager.OnPageChangeListener {


    private SectionsPagerAdapter mSectionsPagerAdapter;


    private ViewPager mViewPager;
    private TextView txtTitle;
    private ImageView imgDone;
    BottleInfoModel bottleInfoModel;
    BottleDeliveryFragment deliveryFragment;
    BottleReturnFragment bottleReturnFragment;
    BottleAssetsFragment bottleAssetsFragment;
    //    BottlPaymentFragment bottlPaymentFragment;
    List<DeliveryModel> listDataHeader;
    DInvoicesFragmnet dInvoicesFragmnet;

    private ArrayList<DeliveryModel> arryBottleReturns;
    private ArrayList<DeliveryModel> arryBottleAssets;

    public BottleManageTab() {

    }

//    public BottleManageTab getCurrentInstance() {
//        return BottleManageTab.this;
//    }

//    public static BottleManageTab newInstance(BottleInfoModel infoModel) {
//        BottleManageTab fragment = new BottleManageTab();
//        Bundle args = new Bundle();
//        args.putParcelable(BottleDeliveryFragment.KEY_CUSTOMER_INFO, infoModel);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottle_settlement_tab);


//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View vLayout = inflater.inflate(R.layout.activity_bottle_settlement_tab, container, false);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        txtTitle = (TextView) findViewById(R.id.activity_bottle_settlement_tab_txtTitle);
        imgDone = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgDone);

//        setActionBar(toolbar);
//        getActionBar().setDisplayHomeAsUpEnabled(true);
//        getActionBar().setDisplayShowHomeEnabled(true);
        toolbar.inflateMenu(R.menu.menu_delivery);

//        bottleInfoModel = getArguments().getParcelable(BottleDeliveryFragment.KEY_CUSTOMER_INFO);
        bottleInfoModel = getIntent().getParcelableExtra(BottleDeliveryFragment.KEY_CUSTOMER_INFO);
        txtTitle.setText(bottleInfoModel.getCompany() + "   " + bottleInfoModel.getRouteName());


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.)
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));
        mViewPager.setOffscreenPageLimit(4);
        mViewPager.addOnPageChangeListener(this);


        //-----------------------------------------------------------------------

        imgDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //item 0 = delivery Fragment tab
                //item 1 = return Fragment tab
                if (mViewPager.getCurrentItem() == 0) {
                    deliveryFragment.onDoneClick();

                } else if (mViewPager.getCurrentItem() == 1) {
                    bottleReturnFragment.onDoneClick();
                }
            }
        });

        //-----------------------------------------------------------------------
        if (ConnectivityReceiver.isConnected()) {
            serviceGetOrder();
        } else {
            Snackbar.make(txtTitle, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        txtTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.action_updateLoc) {
//                    Toast.makeText(BottleManageTab.this, "menupress", Toast.LENGTH_LONG).show();
                    String methodMessage = isGPSsettingDone();
                    if (!methodMessage.equalsIgnoreCase("AllDone")) {
                        showAlertdialog("Alert!", methodMessage);
                    } else {
                        serviceUpdtCusLoc(
                                "" + mCurrentLocation.getLatitude(),
                                "" + mCurrentLocation.getLongitude());
                    }
                } else if (item.getItemId() == R.id.action_customerCard) {
                    Intent intent = new Intent(BottleManageTab.this, CustomerCardListActivity.class);
                    intent.putExtra(getString(R.string.key_intent_customer_id), bottleInfoModel.getCustomerID());
                    startActivity(intent);
                }
                return false;
            }
        });
    }


    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {

            int visibility = (listDataHeader == null || listDataHeader.isEmpty()) ? View.GONE : View.VISIBLE;
            imgDone.setVisibility(visibility);

        } else if (position == 1) {

            int visibility = (arryBottleReturns == null || arryBottleReturns.isEmpty()) ? View.GONE : View.VISIBLE;
            imgDone.setVisibility(visibility);

        } else if (position == 2) {
            imgDone.setVisibility(View.GONE);

        } else if (position == 3) {
            imgDone.setVisibility(View.GONE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }


    /**
     * A {@link  } that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            if (position == 0) {
                deliveryFragment = BottleDeliveryFragment.newInstance(bottleInfoModel);
                return deliveryFragment;

            } else if (position == 1) {

                bottleReturnFragment = BottleReturnFragment.newInstance(bottleInfoModel);
                return bottleReturnFragment;

            } else if (position == 2) {
                bottleAssetsFragment = BottleAssetsFragment.newInstance(bottleInfoModel);
                return bottleAssetsFragment;

            } else if (position == 3) {
//                bottlPaymentFragment = BottlPaymentFragment.newInstance(bottleInfoModel);
//                return bottlPaymentFragment;
                dInvoicesFragmnet = DInvoicesFragmnet.newInstance(bottleInfoModel);
                return dInvoicesFragmnet;
            }

            return null;
        }


        @Override
        public int getCount() {
            // Show 4 total pages.
            return 4;
        }
    }

    /*****************************************************************************************************
     *
     *****************************************************************************************************
     */
    private void serviceGetOrder() {
        final ProgressDialog dialog = ProgressDialog.show(BottleManageTab.this, "", "Loading... ", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        //BottleInfoModel bottleInfoModel = getArguments().getParcelable(KEY_CUSTOMER_INFO);
        //Call<ResponseBody> call = apiService.getOrder("get_order", Util.API_KEY, dateToStr, bottleInfoModel.getCustomerID());

        Call<ResponseBody> call = apiService.getOrderByRoute(
                Util.API_GET_ORDER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                Util.getTodayDate(),
                bottleInfoModel.getCustomerID(),
                "items",
                bottleInfoModel.getRouteID(),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID))
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArrayDelivery = jsonResponse.optJSONArray("data");
                        fillDeliveryItems(jsonArrayDelivery);

                        JSONArray jsonArrayReturns = jsonResponse.optJSONArray("returns");
                        fillReturnItems(jsonArrayReturns);

                        JSONArray jsonArrayAssets = jsonResponse.optJSONArray("assets");
                        fillAssetsItems(jsonArrayAssets);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(BottleManageTab.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(BottleManageTab.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fillAssetsItems(JSONArray jsonArray) {

        arryBottleAssets = new ArrayList<DeliveryModel>();
        if (jsonArray == null || jsonArray.length() == 0) {
            bottleAssetsFragment.setServiceData(arryBottleAssets);
            return;
        }
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.optJSONObject(i);

            DeliveryModel deliveryModel = new DeliveryModel();
            deliveryModel.setSales_id(jsonObject.optString("id"));
            deliveryModel.setProduct_name(jsonObject.optString("name"));

            arryBottleAssets.add(deliveryModel);
        }

        bottleAssetsFragment.setServiceData(arryBottleAssets);

    }


    private void fillReturnItems(JSONArray jsonArray) {
        arryBottleReturns = new ArrayList<DeliveryModel>();

        if (jsonArray == null || jsonArray.length() == 0) {
//            imgDone.setVisibility(View.GONE);
            bottleReturnFragment.setServiceData(arryBottleReturns);
            return;
        }
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.optJSONObject(i);

            DeliveryModel deliveryModel = new DeliveryModel();
            deliveryModel.setSales_id(jsonObject.optString("id"));
            deliveryModel.setProduct_name(jsonObject.optString("name"));
            deliveryModel.setQuantity(jsonObject.optString("quantity"));

            arryBottleReturns.add(deliveryModel);
        }
        bottleReturnFragment.setServiceData(arryBottleReturns);
    }

    /**
     * set only delivery Fragment data data
     */
    private void fillDeliveryItems(JSONArray jsonArray) {


        listDataHeader = new ArrayList<DeliveryModel>();
        HashMap<DeliveryModel, List<DeliveryModel>> listDataChild = new HashMap<DeliveryModel, List<DeliveryModel>>();

        if (jsonArray == null || jsonArray.length() == 0) {
//            Toast.makeText(BottleManageTab.this, "No Delivery Items found!", Toast.LENGTH_SHORT).show();
            imgDone.setVisibility(View.GONE);
            deliveryFragment.setServiceData(listDataHeader, listDataChild);
            return;
        }


        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.optJSONObject(i);

            String orderID = jsonObject.optString("id");
            String orderDate = jsonObject.optString("delivery_date");

            JSONArray arrayItems = jsonObject.optJSONArray("items");
            ////////////////////////////////////////////////

            DeliveryModel deliveryModelHeader = new DeliveryModel();
            deliveryModelHeader.setOrderDate(orderDate);
            deliveryModelHeader.setOrderID(orderID);
            deliveryModelHeader.setIs_finish(jsonObject.optString("is_finish"));
            deliveryModelHeader.setSale_reference_no(jsonObject.optString("reference_no"));
            deliveryModelHeader.setBottleCount(arrayItems.length() + "");
            deliveryModelHeader.setSales_id(jsonObject.optString("id"));
            deliveryModelHeader.setOrder_tax_id(jsonObject.optString("order_tax_id"));
            deliveryModelHeader.setWarehouse_id(jsonObject.optString("warehouse_id"));
            deliveryModelHeader.setBiller_id(jsonObject.optString("biller_id"));
            deliveryModelHeader.setSale_status(jsonObject.optString("sale_status"));

            if (jsonObject.optString("sale_status").equalsIgnoreCase("delivering")) {
                deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_delivering));
            } else if (jsonObject.optString("sale_status").equalsIgnoreCase("partially")) {
                deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_partially));
            } else if (jsonObject.optString("sale_status").equalsIgnoreCase("over")) {
                deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_over));
            } else if (jsonObject.optString("sale_status").equalsIgnoreCase("completed")) {
                deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_completed));
            }

            listDataHeader.add(deliveryModelHeader);


            final ArrayList<DeliveryModel> array = new ArrayList<>();
            for (int j = 0; j < arrayItems.length(); j++) {

                DeliveryModel deliveryModel = new DeliveryModel();

                JSONObject jobjItems = arrayItems.optJSONObject(j);
                deliveryModel.setProduct_code(jobjItems.optString("product_code"));
                deliveryModel.setProduct_id(jobjItems.optString("product_id"));
                deliveryModel.setQuantity(jobjItems.optString("quantity"));
                deliveryModel.setPendingQty(jobjItems.optString("pending_qty"));
                deliveryModel.setUnit_price(jobjItems.optString("unit_price"));
                deliveryModel.setProduct_name(jobjItems.optString("product_name"));


                deliveryModel.setCgst(jobjItems.optString("cgst"));
                deliveryModel.setComment(jobjItems.optString("comment"));
                deliveryModel.setDiscount(jobjItems.optString("discount"));
                deliveryModel.setGst(jobjItems.optString("gst"));
                deliveryModel.setIgst(jobjItems.optString("igst"));

                deliveryModel.setItem_discount(jobjItems.optString("item_discount"));
                deliveryModel.setItem_tax(jobjItems.optString("item_tax"));
                deliveryModel.setNet_unit_price(jobjItems.optString("net_unit_price"));
                deliveryModel.setProduct_type(jobjItems.optString("product_type"));
                deliveryModel.setProduct_unit_code(jobjItems.optString("product_unit_code"));

                deliveryModel.setProduct_unit_id(jobjItems.optString("product_unit_id"));
                deliveryModel.setProduct_unit_quantity(jobjItems.optString("product_unit_quantity"));
                deliveryModel.setSerial_no(jobjItems.optString("serial_no"));
                deliveryModel.setSgst(jobjItems.optString("sgst"));
                deliveryModel.setSubtotal(jobjItems.optString("subtotal"));

                deliveryModel.setTax(jobjItems.optString("tax"));
                deliveryModel.setTax_rate_id(jobjItems.optString("tax_rate_id"));

                try {

                    double pendingQty = Double.valueOf(deliveryModel.getPendingQty());
                    int pendingQtyint = ((int) pendingQty);
                    deliveryModel.setPendingQty("" + pendingQtyint);
                    deliveryModel.setDelivered("" + pendingQtyint);

                } catch (NumberFormatException NumExe) {
                    deliveryModel.setPendingQty("0");
                    deliveryModel.setDelivered("0");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                array.add(deliveryModel);
            }

            listDataChild.put(listDataHeader.get(i), array);

        }
        deliveryFragment.setServiceData(listDataHeader, listDataChild);
    }

    public interface OnTabChangeFromBottleManageTab {
        public void OnTabChange(int position);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_delivery, menu);
        if (menu instanceof MenuBuilder) {
            MenuBuilder m = (MenuBuilder) menu;
            m.setOptionalIconsVisible(true);
        }
        return true;
    }

    /*****************************************************************************************************
     *
     *****************************************************************************************************
     */

    private void serviceUpdtCusLoc(String latitude, String longitude) {
        final ProgressDialog dialog = ProgressDialog.show(BottleManageTab.this, "", "Updating... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.updateCusLoc(
                "update_customer_data",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                bottleInfoModel.getCustomerID(),
                "",
                latitude,
                longitude);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.optString("status").equalsIgnoreCase("true")) {
                            showAlertdialog("Update", bottleInfoModel.getCompany() + "`s location updated successfully.");
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(BottleManageTab.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(BottleManageTab.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
