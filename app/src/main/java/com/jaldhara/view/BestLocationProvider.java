package com.jaldhara.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.Locations;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class BestLocationProvider extends AppCompatActivity
        implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        ResultCallback,
        com.google.android.gms.location.LocationListener {


    private static final long INTERVAL = 1000 * 15;
    private static final long FASTEST_INTERVAL = 1000 * 15;
    private int REQUEST_CHECK_SETTINGS = 100;
    private LocationRequest mLocationRequest;
    private GoogleApiClient mGoogleApiClient;
    private boolean isFirstTimeLocation = true;
    public Location mCurrentLocation;
    protected LocationManager myLocationManager = null;
    private boolean isGPSEnabled = false;
    private static final String GROUP_ID_DRIVER = "5";
    private static final String GROUP_ID_PAYMENT_COLLECTOR = "35";
    public static final String ROLE_DRIVER = "DRIVER";
    public static final String ROLE_PAYMENT_COLLECTOR = "PAYMENT_COLLECTOR";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        myLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        String groupID = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID));

        if (groupID.equals(GROUP_ID_DRIVER) ||
                groupID.equals(GROUP_ID_PAYMENT_COLLECTOR) ||
                BestLocationProvider.this instanceof RouteMapActivity) {

            //Toast.makeText(BestLocationProvider.this, "Google Play Service Error ", Toast.LENGTH_LONG).show();

            if (!isGooglePlayServicesAvailable()) {
                Toast.makeText(BestLocationProvider.this, "Google Play Service Error ", Toast.LENGTH_LONG).show();
            } else {
                //code for dialog of google play service forcefully updated
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                isLocationPermissionChecked();
            } else {
                connectGoogleClient();
            }


        }


    }
    //Location work start


    public String isGPSsettingDone() {
//        boolean isGPSEnabled = myLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
//        if (!isGPSEnabled) {
//            return "Please enable GPS for better accuracy.\nDo you want to enable GPS?";
//        }

        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnected() && mCurrentLocation != null) {
//                    Log.v("SiteAssetActivity", "Last Known Location : lat :" + latitude + ",\n Longitude" + longitude);
                return "AllDone";
            } else {
                mCurrentLocation = createLocationRequest();
                if (mCurrentLocation != null) {
                    return "AllDone";
                } else {
                    return "Getting location! Please wait or try again";
                }
            }
        } else {
            //print toast or mclient not connected.
            return "Google Client connecting, Please wait or try again";
        }


//

    }


    @Override
    public void onLocationChanged(Location location) {
        Log.e(this.getClass().getName().toString(), "Firing onLocationChanged..............................................");
        if (location != null) {
            mCurrentLocation = location;
            JalDharaApplication.getInstance().setLocation(location);
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy / hh:mm:aa");
            String getCurrentDateTime = sdf.format(Calendar.getInstance().getTime());

            //Group id = 5 is for sales person /Driver

            final String groupID = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID));
            if (groupID.equals(GROUP_ID_DRIVER) || groupID.equals(GROUP_ID_PAYMENT_COLLECTOR)) {

                String driverIDKey = "DriverID:" + JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID));
                String companyIDKey = "CompID:" + JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id));

                JalDharaApplication.getInstance().getLiveDatabase().
                        child("Locations").
                        child(companyIDKey).
                        child(driverIDKey).push();

                Locations locations = new Locations(
                        "" + location.getLatitude(),
                        "" + location.getLongitude(),
                        JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID)),
                        JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserName)),
                        getCurrentDateTime,
                        groupID.equals(GROUP_ID_DRIVER) ? ROLE_DRIVER : ROLE_PAYMENT_COLLECTOR);


                JalDharaApplication.getInstance().getLiveDatabase().
                        child("Locations").
                        child(companyIDKey).
                        child(driverIDKey).setValue(locations);
//
            }


//            Toast.makeText(SiteAssetActivity.this, "Change lat : " + latitude + ", \n change Long : " + longitude, Toast.LENGTH_SHORT).show();
//            Toast.makeText(SiteAssetActivity.this, "Change lat : " + latitude + ", \n change Long : " + longitude, Toast.LENGTH_SHORT).show();

        }

    }


    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int code = api.isGooglePlayServicesAvailable(this);
        if (code == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(code) && api.showErrorDialogFragment(this, code, 1234)) {
            Toast.makeText(this, api.getErrorString(code), Toast.LENGTH_LONG).show();
            return false;
        } else {
            Toast.makeText(this, api.getErrorString(code), Toast.LENGTH_LONG).show();
            return false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case 0: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    connectGoogleClient();
                } else {
                    Toast.makeText(BestLocationProvider.this, "Please allow Location permission", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    protected void connectGoogleClient() {
        if (mGoogleApiClient == null || !mGoogleApiClient.isConnected()) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addApi(LocationServices.API).addConnectionCallbacks(this).addOnConnectionFailedListener(this).build();
            mGoogleApiClient.connect();
        }
    }

    protected Location createLocationRequest() {

        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(INTERVAL); // Update location every second
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
        builder.setAlwaysShow(true);
        // this code will show the dialog for location require and start GPS from device.
        PendingResult result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(this);

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            if (ActivityCompat.checkSelfPermission(BestLocationProvider.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(BestLocationProvider.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }

//            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
//            mFusedLocationClient.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            return mCurrentLocation;
        }

        Criteria criteria = new Criteria();
        String best = myLocationManager.getBestProvider(criteria, true);
        //since you are using true as the second parameter, you will only get the best of providers which are enabled.
        if (myLocationManager != null && best != null) {
            mCurrentLocation = myLocationManager.getLastKnownLocation(best);
        }

        return mCurrentLocation;
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d(this.getClass().toString(), "onStart fired ..............");

        final String groupID = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID));

        if (groupID.equals(GROUP_ID_DRIVER) ||
                groupID.equals(GROUP_ID_PAYMENT_COLLECTOR) ||
                BestLocationProvider.this instanceof RouteMapActivity) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                isLocationPermissionChecked();
            } else {
                connectGoogleClient();
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();

        final String groupID = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID));
        Log.d(this.getClass().getName().toString(), "Location update resumed ....................." + groupID);
        if (groupID.equals(GROUP_ID_DRIVER) ||
                groupID.equals(GROUP_ID_PAYMENT_COLLECTOR) ||
                BestLocationProvider.this instanceof RouteMapActivity) {

            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                startLocationUpdates();
                Log.d("SiteAssetActivity", "Location update resumed .....................");
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (isLocationPermissionChecked()) {
                    connectGoogleClient();
                }
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(this.getClass().getName().toString(), "BEST Location onStop  fired ..............");

        if (mGoogleApiClient != null && BestLocationProvider.this instanceof RouteMapActivity) {
            mGoogleApiClient.disconnect();
            Log.d("SiteAssetActivity", "disconnect ...............: " + mGoogleApiClient.isConnected());
        }

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected() && BestLocationProvider.this instanceof RouteMapActivity) {
            stopLocationUpdates();
        }
    }

    protected void stopLocationUpdates() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
//            FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(BestLocationProvider.this);
//            fusedLocationClient.removeLocationUpdates( );
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.e(this.getClass().getName().toString(), "Location update stopped .......................");
        }
    }

    @Override
    public void onConnected(Bundle arg0) {
        Log.e(this.getClass().getName().toString(), "onConnected - isConnected ...............: " + mGoogleApiClient.isConnected());
        createLocationRequest();
    }

    protected void startLocationUpdates() {

        if (ActivityCompat.checkSelfPermission(BestLocationProvider.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(BestLocationProvider.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            return;
        }
        PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, BestLocationProvider.this);
        Log.e(this.getClass().getName().toString(), "Location update started ..............: ");
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(this.getClass().getName().toString(), "Connection failed: " + connectionResult.toString());

    }


    /**
     *
     */
    protected boolean isLocationPermissionChecked() {

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) &&
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                new AlertDialog.Builder(this).setTitle("Location Permission Needed!")
                        .setMessage("This app needs the Location permission, Please accept to use location functionality")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(BestLocationProvider.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
                            }
                        }).create().show();


                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        0);
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
            }
            return false;
        } else {
            return true;
        }
    }

    protected void showAlertdialog(String title, String message) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        alertDialog.create();
        alertDialog.show();

    }


    @Override
    public void onResult(@NonNull Result locationSettingsResult) {

        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // NO need to show the dialog;
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location settings are not satisfied. Show the user a dialog
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().

                    status.startResolutionForResult(BestLocationProvider.this, REQUEST_CHECK_SETTINGS);

                } catch (IntentSender.SendIntentException e) {
                    //failed to show
                }
                break;

            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(JalDharaApplication.getInstance(), "GPS enabled", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(JalDharaApplication.getInstance(), "GPS is not enabled", Toast.LENGTH_LONG).show();
//                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest);
//                builder.setAlwaysShow(true);
//                PendingResult result = LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
//                result.setResultCallback(this);
            }
//

        }
    }


}
