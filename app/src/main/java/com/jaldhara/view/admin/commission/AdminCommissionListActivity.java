package com.jaldhara.view.admin.commission;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.DriverCommissionModel;
import com.jaldhara.models.ProductModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONObject;
import org.xmlpull.v1.sax2.Driver;

import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminCommissionListActivity extends AppCompatActivity {

    private TextView txtLbl;
    private AdminCommissionListAdapter adapter;
    private RecyclerView recyclerView;
    private TextView lbl_nodelivery;
    private FloatingActionButton btnAddOrders;
    private boolean isFromDriver = false;

    private EditText searchText;
    private TextView txtClear;

    ArrayList<DriverCommissionModel> arryDriverCommission = new ArrayList<>();

    @SuppressLint("RestrictedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_order_list);

        isFromDriver = getIntent().getBooleanExtra(getString(R.string.key_intent_is_from_driver), false);

        txtLbl = (TextView) findViewById(R.id.activity_addRoute_txtTitle);
        txtLbl.setText(isFromDriver ? "My Commission" : "Driver Commission");

        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);
        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);

        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        lbl_nodelivery = findViewById(R.id.lbl_nodelivery);
        ImageView imgBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (ConnectivityReceiver.isConnected()) {

            //pass From date and to date same for first time.
            serviceGetDriverCommission();
        } else {
            Snackbar.make(recyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ((ImageView) findViewById(R.id.img_info)).setVisibility(View.GONE);
        btnAddOrders = findViewById(R.id.btn_Add);
//        btnAddOrders.setVisibility(isFromDriver ? View.GONE : View.VISIBLE);
        btnAddOrders.setVisibility(View.GONE);
        btnAddOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminCommissionListActivity.this, AdminAddCommissionActivity.class);
                startActivity(intent);
            }
        });
    }

    void filter(String text) {
        ArrayList<DriverCommissionModel> temp = new ArrayList();

        if (arryDriverCommission != null && arryDriverCommission.size() > 0) {
            for (DriverCommissionModel d : arryDriverCommission) {
                if (d.getPayment_status().toLowerCase().contains(text) ||
                        d.getFirst_name().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }
            if (temp != null && temp.size() > 0) {
                //update recyclerview
                lbl_nodelivery.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                adapter.setRefreshList(temp);
            } else {
                lbl_nodelivery.setText("No record found.");
                lbl_nodelivery.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }

    private void serviceGetDriverCommission() {
        final ProgressDialog dialog = ProgressDialog.show(AdminCommissionListActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.get_driver_commission(
                "get",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                isFromDriver ? JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID)) : ""
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();

                if (!response.isSuccessful()) {
                    lbl_nodelivery.setVisibility(View.VISIBLE);
                    lbl_nodelivery.setText("No record found.");
                    recyclerView.setVisibility(View.GONE);
                    Toast.makeText(AdminCommissionListActivity.this, "Error in getting commission list", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {


                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    JSONArray jsonArrayDelivery = jsonResponse.optJSONArray("driver_commission");
                    if (jsonArrayDelivery != null && jsonArrayDelivery.length() > 0) {
                        lbl_nodelivery.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);

                        Gson gson = new Gson();
                        Type listType = new TypeToken<ArrayList<DriverCommissionModel>>() {
                        }.getType();

                        arryDriverCommission = gson.fromJson(jsonArrayDelivery.toString(), listType);

                        adapter = new AdminCommissionListAdapter(AdminCommissionListActivity.this, arryDriverCommission, isFromDriver);
                        recyclerView.setAdapter(adapter);

                    } else {
                        lbl_nodelivery.setText("No record found.");
                        lbl_nodelivery.setVisibility(View.VISIBLE);
                        recyclerView.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    lbl_nodelivery.setText("No record found.");
                    lbl_nodelivery.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                lbl_nodelivery.setText("No record found.");
                lbl_nodelivery.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminCommissionListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void onClickRefresh(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
        serviceGetDriverCommission();
    }
}
