package com.jaldhara.view.admin.purchase;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.PaymentAdapter;
import com.jaldhara.models.PaymentModel;
import com.jaldhara.models.PurchaseModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentsActivity extends AppCompatActivity implements View.OnClickListener {

    private String purchaseId = "";
    private RecyclerView recyclerView;
    private Context context;
    private static final int INTENT_EDIT_PAYMENT = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payments);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_payments_imgBack);
        imgBack.setOnClickListener(this);

        recyclerView = findViewById(R.id.activity_payments_recyclerView);

        try {
            PurchaseModel purchaseModel = (PurchaseModel) getIntent().getSerializableExtra(getString(R.string.key_intent_purchase_detail));
            if (purchaseModel != null) {
                purchaseId = String.valueOf(purchaseModel.getId());
                serviceGetPayments();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {

        int id = v.getId();

        if (id == R.id.activity_payments_imgBack) {
            onBackPressed();
        }

    }

    private void serviceGetPayments() {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getPaymentList(Util.API_LIST_OF_PAYMENT, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), purchaseId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("payments")) {
                            List<PaymentModel> paymentList = new ArrayList<>();
                            PaymentAdapter paymentAdapter = new PaymentAdapter(context, paymentList);
                            recyclerView.setAdapter(paymentAdapter);

                            JSONArray jsonArray = jsonResponse.getJSONArray("payments");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject paymentObj = jsonArray.getJSONObject(i);

                                PaymentModel paymentModel = new PaymentModel();
                                paymentModel.setId(paymentObj.getString("id"));
                                paymentModel.setCompId(paymentObj.getString("comp_id"));
                                paymentModel.setDate(paymentObj.getString("date"));
                                paymentModel.setSaleId(paymentObj.getString("sale_id"));
                                paymentModel.setReturnId(paymentObj.getString("return_id"));
                                paymentModel.setPurchaseId(paymentObj.getString("purchase_id"));
                                paymentModel.setDeliveriesReportId(paymentObj.getString("deliveries_report_id"));
                                paymentModel.setCustomerInvoicesId(paymentObj.getString("customer_invoices_id"));
                                paymentModel.setReferenceNo(paymentObj.getString("reference_no"));
                                paymentModel.setTransactionId(paymentObj.getString("transaction_id"));
                                paymentModel.setPaidBy(paymentObj.getString("paid_by"));
                                paymentModel.setChequeNo(paymentObj.getString("cheque_no"));
                                paymentModel.setCcNo(paymentObj.getString("cc_no"));
                                paymentModel.setCcHolder(paymentObj.getString("cc_holder"));
                                paymentModel.setCcMonth(paymentObj.getString("cc_month"));
                                paymentModel.setCcYear(paymentObj.getString("cc_year"));
                                paymentModel.setCcType(paymentObj.getString("cc_type"));
                                paymentModel.setAmount(paymentObj.getString("amount"));
                                paymentModel.setCurrency(paymentObj.getString("currency"));
                                paymentModel.setCreatedBy(paymentObj.getString("created_by"));
                                paymentModel.setAttachment(paymentObj.getString("attachment"));
                                paymentModel.setCcType(paymentObj.getString("type"));
                                paymentModel.setNote(paymentObj.getString("note"));
                                paymentModel.setPosPaid(paymentObj.getString("pos_paid"));
                                paymentModel.setPosBalance(paymentObj.getString("pos_balance"));
                                paymentModel.setApprovalCode(paymentObj.getString("approval_code"));

                                paymentList.add(paymentModel);
                            }

                            paymentAdapter.notifyDataSetChanged();
                            paymentAdapter.setOnItemClick(new PaymentAdapter.onItemClick<PaymentModel>() {
                                @Override
                                public void onEditClicked(PaymentModel data) {
                                    Intent intent = new Intent(context, AddPaymentActivity.class);
                                    intent.putExtra(getString(R.string.key_intent_payment_detail), data);
                                    startActivityForResult(intent, INTENT_EDIT_PAYMENT);
                                }

                                @Override
                                public void onDeleteClicked(final PaymentModel data) {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                                    builder.setTitle("Are you sure you want to delete this expense?")
                                            .setCancelable(false)
                                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.dismiss();
                                                    serviceDeletePayment(data.getId());
                                                }
                                            })
                                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                    AlertDialog alert = builder.create();
                                    alert.show();
                                }
                            });
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void serviceDeletePayment(String paymentId) {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.deletePayment(Util.API_DELETE_PAYMENT, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), paymentId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("message")) {
                            Toast.makeText(PaymentsActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                serviceGetPayments();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_EDIT_PAYMENT && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra("reload")) {
                if (data.getBooleanExtra("reload", false)) {
                    serviceGetPayments();
                }
            }
        }
    }
}