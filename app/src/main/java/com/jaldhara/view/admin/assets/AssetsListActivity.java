package com.jaldhara.view.admin.assets;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.AssetListAdminAdapter;
import com.jaldhara.adapters.ProductListAdminAdapter;
import com.jaldhara.models.AssetsModel;
import com.jaldhara.models.ProductModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.PaginationScrollListener;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.products.AdminAddProductActivity;
import com.jaldhara.view.deliveryboy.BottleManageTab;
import com.jaldhara.view.deliveryboy.CustomerCardListActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AssetsListActivity extends AppCompatActivity implements View.OnClickListener {


    private Context context;
    private List<AssetsModel> assetList = new ArrayList<>();
    private AssetListAdminAdapter mAdapter;
    private RecyclerView recyclerviewAssets;
    private TextView txtNoAsset;

    private static final int PER_PAGE_COUNT = 20;
    private int PAGE_START = 1;
    private int PAGE_LIMIT = 20;
    private int TOTAL_PAGES = 0;
    private int CURRENT_PAGE = 1;
    private boolean isLastPage = false;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assetslist);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_purchase_list_activity_imgBack);
        imgBack.setOnClickListener(this);

        FloatingActionButton btnAdd = findViewById(R.id.activity_purchase_list_button_add_purchase);
        btnAdd.setOnClickListener(this);

        recyclerviewAssets = findViewById(R.id.recyclerviewAssets);
        txtNoAsset = findViewById(R.id.txtNoAsset);

        mAdapter = new AssetListAdminAdapter(context, assetList);
        recyclerviewAssets.setAdapter(mAdapter);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.inflateMenu(R.menu.menu_admin_asset);

        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.action_barcode) {
                   // Toast.makeText(AssetsListActivity.this, "menupress", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(AssetsListActivity.this, AssetsBarCodeScannerActivity.class);
                    startActivity(intent);
                }
                return false;
            }
        });

        recyclerviewAssets.addOnScrollListener(new PaginationScrollListener(new LinearLayoutManager(context)) {
            @Override
            protected void loadMoreItems() {
                CURRENT_PAGE += 1;
                PAGE_START += 20;

                if (ConnectivityReceiver.isConnected()) {
                    serviceListOfAssets();
                } else {
                    Snackbar.make(recyclerviewAssets, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        serviceListOfAssets();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_purchase_list_activity_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_purchase_list_button_add_purchase) {
            startActivity(new Intent(AssetsListActivity.this, AdminAddAssetActivity.class));
        } else if (id == R.id.card_raw) {
            int position = (int) v.getTag();
            AssetsModel model = assetList.get(position);
            Intent intent = new Intent(AssetsListActivity.this, AssetsDetailsActivity.class);
            intent.putExtra("assetModel", model);
            startActivity(intent);
        }
    }

    private void serviceListOfAssets() {
//        productList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Assets", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllAssets(
                Util.API_GET_ASSET,
                Util.API_KEY);
        call.enqueue(new Callback<ResponseBody>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();

                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                AssetsModel productModel = new AssetsModel();

                                productModel.setAssets_paid_status(jsonArray.optJSONObject(i).optString("assets_paid_status"));
                                productModel.setAssets_status(jsonArray.optJSONObject(i).optString("assets_status"));
                                productModel.setBiller(jsonArray.optJSONObject(i).optString("biller"));
                                productModel.setBiller_id(jsonArray.optJSONObject(i).optInt("biller_id"));
                                productModel.setCustomer_id(jsonArray.optJSONObject(i).optInt("customer_id"));
                                productModel.setDate(jsonArray.optJSONObject(i).optString("date"));
                                productModel.setDelivery_date(jsonArray.optJSONObject(i).optString("delivery_date"));
                                productModel.setId(jsonArray.optJSONObject(i).optInt("id"));
                                productModel.setReference_no(jsonArray.optJSONObject(i).optString("reference_no"));
                                productModel.setPayment_status(jsonArray.optJSONObject(i).optString("payment_status"));

                                assetList.add(productModel);
                            }
                        }

                        if (assetList != null && assetList.size() > 0) {
                            mAdapter.notifyDataSetChanged();
                            recyclerviewAssets.setVisibility(View.VISIBLE);
                            txtNoAsset.setVisibility(View.GONE);
                        } else {
                            recyclerviewAssets.setVisibility(View.GONE);
                            txtNoAsset.setVisibility(View.VISIBLE);
                        }

                        mAdapter.setOnItemClick(new AssetListAdminAdapter.onItemClick<AssetsModel>() {
                            @Override
                            public void onDetailClicked(AssetsModel data) {
                                //  Toast.makeText(AssetsListActivity.this, "onDetailClicked", Toast.LENGTH_SHORT).show();
//                                    Intent intent = new Intent(ProductListActivity.this, PurchaseDetailActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
//                                    startActivity(intent);
                            }

                            @Override
                            public void onViewPaymentClicked(AssetsModel data) {
                                //   Toast.makeText(AssetsListActivity.this, "onViewPaymentClicked", Toast.LENGTH_SHORT).show();
//                                    Intent intent = new Intent(ProductListActivity.this, PaymentsActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
//                                    startActivity(intent);
                            }

                            @Override
                            public void onAddPaymentClicked(AssetsModel data) {
                                //   Toast.makeText(AssetsListActivity.this, "onAddPaymentClicked", Toast.LENGTH_SHORT).show();
//                                    Intent intent = new Intent(ProductListActivity.this, AddPaymentActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
//                                    startActivity(intent);
                            }

                            @Override
                            public void onDeleteClicked(AssetsModel data) {
                                //   Toast.makeText(AssetsListActivity.this, "onDeleteClicked", Toast.LENGTH_SHORT).show();
//                                    showAlertDialog(data.getId());
                            }

                            @Override
                            public void onEditPurchaseClicked(AssetsModel data) {
                                //  Toast.makeText(AssetsListActivity.this, "onEditPurchaseClicked", Toast.LENGTH_SHORT).show();
//                                    Intent intent = new Intent(ProductListActivity.this, AddPurchaseActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
//                                    intent.putExtra(getString(R.string.key_intent_edit_purchase), true);
//                                    startActivityForResult(intent, INTENT_ADD_PURCHASE);
                            }
                        });
                    } else {
                        recyclerviewAssets.setVisibility(View.GONE);
                        txtNoAsset.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    dialog.dismiss();
                    recyclerviewAssets.setVisibility(View.GONE);
                    txtNoAsset.setVisibility(View.VISIBLE);
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                recyclerviewAssets.setVisibility(View.GONE);
                txtNoAsset.setVisibility(View.VISIBLE);
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showAlertDialog(final String purchaseId) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Are you sure you want to delete this assets?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        serviceDeletePurchase(purchaseId);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void serviceDeletePurchase(final String purchaseId) {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Removing... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.deletePurchase(Util.API_DELETE_PURCHASE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), purchaseId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                Toast.makeText(AssetsListActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
//                                serviceGetPurchases();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();
            }
        });
    }
}