package com.jaldhara.view.admin.routes;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.RouteListAdminAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.ExpensesModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddRouteActivity extends Activity {

    private FloatingActionButton btnAddBottle;
    private RecyclerView mRecyclerView;
    private RouteListAdminAdapter bottleReturnListAdapter;
    private ArrayList<BottleInfoModel> arrayListRouteList;

    private TextView lbl_nodeliver;

    private EditText searchText;
    private TextView txtClear;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_route);

        btnAddBottle = (FloatingActionButton) findViewById(R.id.fragment_bottles_return_btn_payment);
        mRecyclerView = (RecyclerView) findViewById(R.id.activity_add_route_recycleView);

        ImageView imgBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        lbl_nodeliver = (TextView) findViewById(R.id.lbl_nodelivery);

        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);
        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);


        arrayListRouteList = new ArrayList<>();
        bottleReturnListAdapter = new RouteListAdminAdapter(AddRouteActivity.this, arrayListRouteList);
        mRecyclerView.setAdapter(bottleReturnListAdapter);


        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btnAddBottle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogForAdd();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ConnectivityReceiver.isConnected()) {
            serviceListOfRoutes();
        } else {
            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
    }

    void filter(String text) {
        ArrayList<BottleInfoModel> temp = new ArrayList();
        if (arrayListRouteList != null && arrayListRouteList.size() > 0) {
            for (BottleInfoModel d : arrayListRouteList) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getRouteName().toLowerCase().contains(text) ||
                        d.getLoaderName().toLowerCase().contains(text) ||
                        d.getDriverName().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }

            if (temp != null && temp.size() > 0) {
                mRecyclerView.setVisibility(View.VISIBLE);
                lbl_nodeliver.setVisibility(View.GONE);
                //update recyclerview
                bottleReturnListAdapter.updateList(temp);
            } else {
                mRecyclerView.setVisibility(View.GONE);
                lbl_nodeliver.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }

    /**
     *
     */
    private void openDialogForAdd() {

        final Dialog dialog = new Dialog(AddRouteActivity.this);
        dialog.setContentView(R.layout.dialog_add_route);

        final EditText edtRouteName = (EditText) dialog.findViewById(R.id.dialog_add_route_edit_addRoute);
        final EditText edtRouteCode = (EditText) dialog.findViewById(R.id.dialog_add_route_edit_addRouteCode);

        Button btnAdd = (Button) dialog.findViewById(R.id.dialog_add_route_btnAdd);
        Button btnCancel = (Button) dialog.findViewById(R.id.dialog_add_route_btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        edtRouteName.setError(null);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(edtRouteName.getText().toString().trim())) {
                    edtRouteName.setFocusable(true);
                    edtRouteName.setError("Please Enter Route Name");

                } else if (TextUtils.isEmpty(edtRouteCode.getText().toString().trim())) {
                    edtRouteCode.setFocusable(true);
                    edtRouteCode.setError("Please Enter Route Code");

                } else {

                    if (ConnectivityReceiver.isConnected()) {
                        serviceAddRoute(edtRouteName.getText().toString().trim(), edtRouteCode.getText().toString().trim(), dialog);
                    } else {
                        Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
        dialog.show();

    }


    private void serviceListOfRoutes() {

        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Routes", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_ROUTES, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        arrayListRouteList.clear();

                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int header = 0; header < jsonArray.length(); header++) {
                                BottleInfoModel modelHeader = new BottleInfoModel();
                                modelHeader.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                                modelHeader.setRouteCode(jsonArray.optJSONObject(header).optString("code"));
                                modelHeader.setRouteID(jsonArray.optJSONObject(header).optString("id"));

                                JSONObject jobjDriver = jsonArray.optJSONObject(header).optJSONObject("driver");

                                if (jobjDriver != null) {
                                    modelHeader.setDriverName(jobjDriver.optString("first_name") + " " + jobjDriver.optString("last_name"));
                                }
                                JSONObject jobjLoader = jsonArray.optJSONObject(header).optJSONObject("loader");
                                if (jobjLoader != null) {
                                    modelHeader.setLoaderName(jobjLoader.optString("first_name") + " " + jobjLoader.optString("last_name"));
                                }

                                modelHeader.setHeader(true);
                                arrayListRouteList.add(modelHeader);
                            }

                            mRecyclerView.setVisibility(View.VISIBLE);
                            lbl_nodeliver.setVisibility(View.GONE);
                            bottleReturnListAdapter = new RouteListAdminAdapter(AddRouteActivity.this, arrayListRouteList);
                            mRecyclerView.setAdapter(bottleReturnListAdapter);
                        } else {
                            mRecyclerView.setVisibility(View.GONE);
                            lbl_nodeliver.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mRecyclerView.setVisibility(View.GONE);
                        lbl_nodeliver.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    mRecyclerView.setVisibility(View.GONE);
                    lbl_nodeliver.setVisibility(View.VISIBLE);
                    Toast.makeText(AddRouteActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                mRecyclerView.setVisibility(View.GONE);
                lbl_nodeliver.setVisibility(View.VISIBLE);
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AddRouteActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceAddRoute(String routeName, String routeCode, final Dialog dialogAddRoute) {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Adding Routes", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.addRoutes("add_route", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), routeName, routeCode);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {


                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

//                        for (int header = 0; header < jsonArray.length(); header++) {
//                            BottleInfoModel modelHeader = new BottleInfoModel();
//                            modelHeader.setRouteName(jsonArray.optJSONObject(header).optString("name"));
////                             listDataHeader.add(modelHeader);
//                            modelHeader.setHeader(true);
//
//
//                            JSONArray jsonArrayChild = jsonArray.optJSONObject(header).optJSONArray("customer");
//                            modelHeader.setCustomrsOnRoute(jsonArrayChild.length() + "");
//                            arrayListRouteList.add(modelHeader);
//
//                        }

//                        bottleReturnListAdapter = new RouteListAdminAdapter(AddRouteActivity.this, arrayListRouteList);
//                        mRecyclerView.setAdapter(bottleReturnListAdapter);
                        dialogAddRoute.dismiss();
                        arrayListRouteList.clear();
//                        serviceListOfRoutes();
                        onResume();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AddRouteActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AddRouteActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
