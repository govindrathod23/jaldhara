package com.jaldhara.view.admin.supplier;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.SupplierModel;

import java.util.ArrayList;
import java.util.List;

public class AdminSupplierListAdapter extends RecyclerView.Adapter<AdminSupplierListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<SupplierModel> arrySupplier;
    private AdminSupplierListAdapter.onItemClick<SupplierModel> onItemClick;

    public AdminSupplierListAdapter(Context context, ArrayList<SupplierModel> arrySupplier) {
        this.context = context;
        this.arrySupplier = arrySupplier;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_supplier_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    /*Notify Data*/
    public void setRefreshList(ArrayList<SupplierModel> notificationlist) {
        this.arrySupplier = notificationlist;
        notifyDataSetChanged();
    }


    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final SupplierModel model = arrySupplier.get(position);

        holder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(context, holder.btnAction);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.poupup_menu_supplier_action, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_delete) {
                            if (onItemClick != null) {
                                onItemClick.onDeleteClicked(model);
                            }
                        } else if (item.getItemId() == R.id.action_edit) {
                            if (onItemClick != null) {
                                onItemClick.onEditPurchaseClicked(model);
                            }
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }

        });

        holder.txtName.setText(model.getName());
        holder.txtCompany.setText(model.getCompany());
        holder.txtEmail.setText(model.getEmail());
        holder.txtPhone.setText(model.getPhone());
        holder.txtCity.setText(model.getCity());
        holder.txtCountry.setText(model.getCountry());
        holder.txtVAT.setText(model.getVatNo());
        holder.txtGST.setText(model.getGstNo());

    }

    @Override
    public int getItemCount() {
        return arrySupplier.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName, txtCompany, txtEmail, txtPhone, txtCity, txtCountry, txtVAT, txtGST;
        private Button btnAction;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txtName = itemView.findViewById(R.id.txt_name);
            txtCompany = itemView.findViewById(R.id.txt_company);
            txtEmail = itemView.findViewById(R.id.txt_email);
            txtPhone = itemView.findViewById(R.id.txt_phone);
            txtCity = itemView.findViewById(R.id.txt_city);
            txtCountry = itemView.findViewById(R.id.txt_country);
            txtVAT = itemView.findViewById(R.id.txt_vat);
            txtGST = itemView.findViewById(R.id.txt_gstno);
            btnAction = itemView.findViewById(R.id.raw_button_action);
        }
    }

    public interface onItemClick<T> {
        void onDetailClicked(T data);

        void onViewPaymentClicked(T data);

        void onAddPaymentClicked(T data);

        void onDeleteClicked(T data);

        void onEditPurchaseClicked(T data);
    }

    public void setOnItemClick(AdminSupplierListAdapter.onItemClick<SupplierModel> onItemClick) {
        this.onItemClick = onItemClick;
    }
}