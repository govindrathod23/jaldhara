package com.jaldhara.view.admin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.AssetsSelectionAdapter;
import com.jaldhara.adapters.SeeExpandOrderAdapter;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import android.support.annotation.Nullable;

public class AdminAddAssetsActivity extends Activity {


    ArrayList<SpinnerModel> arryCustomer;
    ArrayList<SpinnerModel> arryAssets;
    Spinner spnCustomer;
    private RecyclerView mRecyclerView;

    private RecyclerView.LayoutManager mLayoutManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_assets);

        spnCustomer = (Spinner) findViewById(R.id.activity_admin_add_assets_SpnCustomer);

        mRecyclerView = (RecyclerView) findViewById(R.id.fragment_bottles_return_recycler_view);

        arryCustomer = new ArrayList<SpinnerModel>();
        arryAssets = new ArrayList<SpinnerModel>();

        Button btnAdd = (Button) findViewById(R.id.activity_admin_add_assets_btnAdd);
        Button btnCancle = (Button) findViewById(R.id.activity_admin_add_assets_btnCancel);
        ImageView imgBtnBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < arryAssets.size(); i++) {
                        if (arryAssets.get(i).isSelected()) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("asset_id", arryAssets.get(i).getRouteId());
                            jsonArray.put(jsonObject);
                        }
                    }

                    if (ConnectivityReceiver.isConnected()) {
                        serviceAddAssetsToCustomer(jsonArray.toString());
                    } else {
                        Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfCustomer();
            serviceListOfAssets();
        } else {
            Snackbar.make(btnAdd, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
    }

    /***************************************************************************************
     *
     ***************************************************************************************/
    private void serviceListOfCustomer() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllCustomer("person", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), "customer");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int route = 0; route < jsonArray.length(); route++) {
                            JSONObject jsonCust = jsonArray.optJSONObject(route);
                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonCust.optString("company"));
                            spinnerModel.setRouteId(jsonCust.optString("id"));
                            arryCustomer.add(spinnerModel);
                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddAssetsActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryCustomer);
                        spnCustomer.setAdapter(arrayAdapter);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddAssetsActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddAssetsActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /***************************************************************************************
     *
     ***************************************************************************************/
    private void serviceListOfAssets() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Assets", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfAssets("get_all_assets", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("asset_name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryAssets.add(spinnerModel);

                        }

//                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddAssetsActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryOrders);
//                        spnAssets.setAdapter(arrayAdapter);
                        AssetsSelectionAdapter selectionAdapter = new AssetsSelectionAdapter(AdminAddAssetsActivity.this, arryAssets);
                        mRecyclerView.setAdapter(selectionAdapter);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddAssetsActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddAssetsActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /***************************************************************************************
     *
     ***************************************************************************************/
    private void serviceAddAssetsToCustomer(String jsonData) {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Adding Assets", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.addAssetsToCustomer("add_assets_to_customer", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), arryCustomer.get(spnCustomer.getSelectedItemPosition()).getRouteId().toString(), jsonData);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    if (response.isSuccessful() && jsonResponse.optBoolean("status")) {

//                        {"message":"Assets update sucessfully","status":true}

                        AlertDialog.Builder builder = new AlertDialog.Builder(AdminAddAssetsActivity.this);
                        builder.setTitle("Assets");
                        builder.setMessage("Assets added successfully!");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();

                            }
                        });

                        builder.show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddAssetsActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddAssetsActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
