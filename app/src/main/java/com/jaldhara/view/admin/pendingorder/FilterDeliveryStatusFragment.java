package com.jaldhara.view.admin.pendingorder;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.adapters.FilterRouteListAdapter;
import com.jaldhara.models.SpinnerModel;

import java.util.ArrayList;

public class FilterDeliveryStatusFragment extends Fragment {

    private View vLayout;
    private RecyclerView recyclerView;
    private ArrayList<SpinnerModel> arryRouteList;
    FilterRouteListAdapter filterRouteListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        vLayout = inflater.inflate(R.layout.fragment_filter_route, container, false);

        recyclerView = (RecyclerView) vLayout.findViewById(R.id.fragment_filter_route_recycleView);

        arryRouteList = new ArrayList<>();


        SpinnerModel s1 = new SpinnerModel();
        s1.setRouteName("Completed");
        arryRouteList.add(s1);

        SpinnerModel s2 = new SpinnerModel();
        s2.setRouteName("Partially");
        arryRouteList.add(s2);

        SpinnerModel s3 = new SpinnerModel();
        s3.setRouteName("delivering");
        arryRouteList.add(s3);
        //spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));



        filterRouteListAdapter = new FilterRouteListAdapter(getActivity(), arryRouteList,FilterDeliveryStatusFragment.class.getName().toString());
        recyclerView.setAdapter(filterRouteListAdapter);

        return vLayout;
    }


}
