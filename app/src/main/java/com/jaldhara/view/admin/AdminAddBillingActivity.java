package com.jaldhara.view.admin;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.SignatureView;
import com.jaldhara.utills.Util;
import com.jaldhara.view.customer.CustomerBottleBillingActivity;
import com.jaldhara.view.deliveryboy.ViewInvoiceActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAddBillingActivity extends Activity
//        implements View.OnClickListener
{


    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 1;
    private String[] storage_permissions =
            {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };

    ArrayList<SpinnerModel> arryCustomer;
    ArrayList<SpinnerModel> arryBiller;
    Spinner spnCustomer;
    private TextView txtDueAmount;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_billing);


        spnCustomer = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnCustomer);


        ImageView imgBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtDueAmount = (TextView) findViewById(R.id.activity_Admin_add_billig_txtDueAmount);
        txtDueAmount.setText("Due Amount : Rs.0");


        arryCustomer = new ArrayList<SpinnerModel>();
        arryBiller = new ArrayList<SpinnerModel>();


        spnCustomer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (ConnectivityReceiver.isConnected()) {
                    servicePayment();
                } else {
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        if (ConnectivityReceiver.isConnected()) {
            serviceListOfCustomer();
            serviceListOfBiller();
        } else {
            Snackbar.make(spnCustomer, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
    }

//    @Override
//    public void onClick(View v) {
//
//
//        if ((int) Build.VERSION.SDK_INT >= 23) {
//            if (ActivityCompat.checkSelfPermission(AdminAddBillingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
//                    ActivityCompat.checkSelfPermission(AdminAddBillingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
//
//                if (ActivityCompat.shouldShowRequestPermissionRationale(AdminAddBillingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(AdminAddBillingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                    if (ActivityCompat.shouldShowRequestPermissionRationale(AdminAddBillingActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.shouldShowRequestPermissionRationale(AdminAddBillingActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(AdminAddBillingActivity.this);
//                        builder.setMessage("To get storage access you have to allow us access to your sd card content.");
//                        builder.setTitle("Storage");
//                        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                ActivityCompat.requestPermissions(AdminAddBillingActivity.this, storage_permissions, 0);
//                                openPaymentDialog();
//                            }
//                        });
//
//                        builder.show();
//                    } else {
//                        ActivityCompat.requestPermissions(AdminAddBillingActivity.this, storage_permissions, 0);
//                        openPaymentDialog();
//                    }
//                } else {
//                    ActivityCompat.requestPermissions(AdminAddBillingActivity.this,
//                            storage_permissions,
//                            MY_PERMISSIONS_REQUEST_STORAGE);
//                    openPaymentDialog();
//                }
//
//            } else {
//                openPaymentDialog();
//            }
//        }
//
//    }

    private void openPaymentDialog() {
        final Dialog dialog = new Dialog(AdminAddBillingActivity.this);
        dialog.setContentView(R.layout.dialog_payment);


        Button btnCancel = (Button) dialog.findViewById(R.id.dialog_payment_btn_cancel);
        Button btnClear = (Button) dialog.findViewById(R.id.dialog_payment_btn_Clear);
        Button btnSubmit = (Button) dialog.findViewById(R.id.dialog_payment_btn_Submit);
        final EditText edtAmount = (EditText) dialog.findViewById(R.id.dialog_payment_edt_amount);
        final SignatureView signatureView = (SignatureView) dialog.findViewById(R.id.dialog_payment_signattureView);


        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signatureView.clearSignature();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(AdminAddBillingActivity.this, "Amount collected :" + edtAmount.getText().toString(), Toast.LENGTH_LONG).show();

                dialog.dismiss();
            }
        });

        dialog.show();

    }


    /*****************************************************************************************************
     *
     *****************************************************************************************************
     */
    private void servicePayment() {
        final ProgressDialog dialog = ProgressDialog.show(AdminAddBillingActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


//        BottleInfoModel bottleInfoModel = getArguments().getParcelable(Util.KEY_INTENT_CUSTOMER_MODEL);
        //Call<ResponseBody> call = apiService.getOrder("get_order", Util.API_KEY, dateToStr, bottleInfoModel.getCustomerID());

        Call<ResponseBody> call = apiService.customerPaymentReport("customer_wise_monthly_reports",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                arryCustomer.get(spnCustomer.getSelectedItemPosition()).getRouteId(),
                "2019");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("status") && !jsonResponse.optBoolean("status")) {
                            Toast.makeText(AdminAddBillingActivity.this, jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        LinearLayout llContainer = (LinearLayout) findViewById(R.id.fragment_payment_ll_container);
                        llContainer.removeAllViews();

                        JSONArray jArray = jsonResponse.optJSONArray("data");

                        Gson gson = new Gson();
                        String jsonOutput = jArray.toString();
                        Type listType = new TypeToken<List<DeliveryModel>>() {
                        }.getType();
                        final List<DeliveryModel> posts = gson.fromJson(jsonOutput, listType);

                        double amount = 0;
                        for (int i = 0; i < posts.size(); i++) {
                            final View child = getLayoutInflater().inflate(R.layout.raw_payment, null);

                            LinearLayout ll_main = (LinearLayout) child.findViewById(R.id.ll_main);
                            TextView txtInvoice = (TextView) child.findViewById(R.id.raw_payment_invoice);
                            final TextView txtDate = (TextView) child.findViewById(R.id.raw_payment_date);
                            TextView txtStatus = (TextView) child.findViewById(R.id.raw_payment_Status);
                            TextView txtMode = (TextView) child.findViewById(R.id.raw_payment_Mode);
                            TextView txtAmount = (TextView) child.findViewById(R.id.raw_payment_Amount);
                            TextView txtRemaining = (TextView) child.findViewById(R.id.raw_payment_remaining);
                            ImageView imgViewInvoice = (ImageView) child.findViewById(R.id.raw_payment_viewInvoice);


                            txtInvoice.setText("-");
                            txtDate.setText(posts.get(i).getMonth());
                            final String paidStatus = posts.get(i).getBalance().toString().contains("0.") ? "Paid" : "Unpaid";
                            txtStatus.setText(paidStatus);
                            txtMode.setText("-");
                            txtAmount.setText(posts.get(i).getTotal());
                            txtRemaining.setText(posts.get(i).getBalance());

                            try {

                                if (posts.get(i).getBalance() != null && (!posts.get(i).getBalance().equalsIgnoreCase("null"))) {
                                    amount = amount + (Double.parseDouble(posts.get(i).getBalance()));
                                }
                                txtDueAmount.setText("Due Amount : Rs." + amount);
                            } catch (Exception e) {

                            }


//                            if (paidStatus.equalsIgnoreCase("Paid")) {
//                                imgViewInvoice.setVisibility(View.INVISIBLE);
//                            } else {
//                                imgViewInvoice.setVisibility(View.VISIBLE);
//                            }

                            if (i % 2 == 0) {
                                ll_main.setBackgroundColor(ContextCompat.getColor(AdminAddBillingActivity.this, R.color.color_list_item));
                            } else {
                                ll_main.setBackgroundColor(ContextCompat.getColor(AdminAddBillingActivity.this, R.color.color_bg_cards));
                            }

                            final int finalI = i;
                            ll_main.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (paidStatus.equalsIgnoreCase("Paid")) {
                                        Snackbar.make(txtDate, "You can't generate paid amount Invoice!", Snackbar.LENGTH_LONG).show();
                                        return;
                                    }

                                    openDialogForAddInvoice(posts.get(finalI).getInvoice_id(), posts.get(finalI).getMonth());
                                }
                            });


                            llContainer.addView(child);
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddBillingActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddBillingActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void openDialogForAddInvoice(final String invoice_id, final String month) {

        final Dialog dialog = new Dialog(AdminAddBillingActivity.this);
        dialog.setContentView(R.layout.dialog_add_invoice);

        final Spinner spnBiller = (Spinner) dialog.findViewById(R.id.activity_admin_add_invoice_spnBiller);
        Button btnAddInvoice = (Button) dialog.findViewById(R.id.dialog_add_invoice_btn_AddInvoice);
        Button btnCancel = (Button) dialog.findViewById(R.id.dialog_add_invoice_btn_Cancel);

        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddBillingActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryBiller);
        spnBiller.setAdapter(arrayAdapter);


        btnAddInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {
                    serviceAddInvoice(month, arryBiller.get(spnBiller.getSelectedItemPosition()).getRouteId());
                    dialog.dismiss();
                } else {
                    Snackbar.make(spnCustomer, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }

            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();


    }

    /**
     * =============================================================================================
     *
     * @param =============================================================================================
     * @param month
     * @param
     */
    private void serviceAddInvoice(String month, String billerID) {

        final ProgressDialog dialog = ProgressDialog.show(AdminAddBillingActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        Call<ResponseBody> call = apiService.addInvoice(
                "add_invoice",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                arryCustomer.get(spnCustomer.getSelectedItemPosition()).getRouteId(),
                billerID,
                month, "", "", "", "" ,"");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {

                        final String strResponse = response.body().string();
                        JSONObject jsonResponse = new JSONObject(strResponse);

                        Toast.makeText(AdminAddBillingActivity.this, jsonResponse.optString("data"), Toast.LENGTH_SHORT).show();

//                        Intent intent = new Intent(AdminAddBillingActivity.this, ViewInvoiceActivity.class);
//                        intent.putExtra(AdminAddBillingActivity.this.getString(R.string.key_intent_invoice_content), jsonResponse.optString("invoice"));
//                        AdminAddBillingActivity.this.startActivity(intent);
//                        if (jsonResponse.has("status") && !jsonResponse.optBoolean("status")) {
//
//                            return;
//                        }
//
//
//                        JSONArray jArray = jsonResponse.optJSONArray("data");


                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddBillingActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddBillingActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });

    }


    /***************************************************************************************
     * Get Customer -- Get Customer -- Get Customer -- Get Customer
     ***************************************************************************************/
    private void serviceListOfCustomer() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllCustomer("person", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), "customer");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int route = 0; route < jsonArray.length(); route++) {
                            JSONObject jsonCust = jsonArray.optJSONObject(route);
                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonCust.optString("company"));
                            spinnerModel.setRouteId(jsonCust.optString("id"));
                            arryCustomer.add(spinnerModel);
                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddBillingActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryCustomer);
                        spnCustomer.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddBillingActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddBillingActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfBiller() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Billers", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllBillers("person", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), "biller", 1, 100);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int route = 0; route < jsonArray.length(); route++) {
                            JSONObject jsonCust = jsonArray.optJSONObject(route);
                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonCust.optString("company"));
                            spinnerModel.setRouteId(jsonCust.optString("id"));
                            arryBiller.add(spinnerModel);
                        }

//                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddBillingActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryBiller);
//                        spnBiller.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddBillingActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddBillingActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
