package com.jaldhara.view.admin;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminReportsActivity extends Activity {

//    ZoomableRelativeLayout zoomableRelativeLayout;

    private TextView txtTO;
    private TextView txtFrom;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_report);

        txtTO = (TextView) findViewById(R.id.activity_admin_report_txtTO);
        txtFrom = (TextView) findViewById(R.id.activity_admin_report_txtFROM);

        if (ConnectivityReceiver.isConnected()) {
            serviceReports();
        } else {
            Snackbar.make(txtTO, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

//        zoomableRelativeLayout = findViewById(R.id.zoomable);
//        final ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(this, new OnPinchListener());

//        zoomableRelativeLayout.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                // TODO Auto-generated method stub
//                scaleGestureDetector.onTouchEvent(event);
//                return true;
//            }
//        });

         ImageView imgBack = ( ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Calendar calendar = Calendar.getInstance();

        txtTO.setText(calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH));
        txtFrom.setText(calendar.get(Calendar.YEAR) + "/" + (calendar.get(Calendar.MONTH) + 1) + "/" + calendar.get(Calendar.DAY_OF_MONTH));

        txtTO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogForDate(txtTO);
            }
        });
        txtFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialogForDate(txtFrom);
            }
        });

    }

    private void openDialogForDate(final TextView txt) {
        Calendar calendar = Calendar.getInstance();

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                txt.setText(year + "/" + (month + 1) + "/" + day);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//        datePickerDialog.findViewById(Resources.getSystem().getIdentifier("day", "id", "android")).setVisibility(View.GONE);
        datePickerDialog.show();

    }


    private void serviceReports() {

        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Reports", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        //Call<ResponseBody> call = apiService.reports("profit_loss", Util.API_KEY, "", "");
        Call<ResponseBody> call = apiService.reports(
                Util.METHOD_PROFIT_LOSS,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                txtTO.getText().toString(),
                txtFrom.getText().toString());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string()).optJSONObject("data");
                        generateReportTable(jsonResponse);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminReportsActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminReportsActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void generateReportTable(JSONObject jReport) {


        JSONObject jobjWareHouseMain = jReport.optJSONArray("warehouses_report").optJSONObject(0);

        JSONObject jobjExpense = jobjWareHouseMain.optJSONObject("total_expenses");
        JSONObject jobjPurchase = jobjWareHouseMain.optJSONObject("total_purchases");
        JSONObject jobjReturns = jobjWareHouseMain.optJSONObject("total_returns");
        JSONObject jobjSales = jobjWareHouseMain.optJSONObject("total_sales");
        JSONObject jobjWareHoues = jobjWareHouseMain.optJSONObject("warehouse");

//        jobjExpense.optString("total");
//        jobjExpense.optString("total_amount");
//        jobjExpense.optString("paid");
//        jobjExpense.optString("tax");
//
//        jobjPurchase.optString("total");
//        jobjPurchase.optString("total_amount");
//        jobjPurchase.optString("paid");
//        jobjPurchase.optString("tax");
//
//        jobjReturns.optString("total");
//        jobjReturns.optString("total_amount");
//        jobjReturns.optString("paid");
//        jobjReturns.optString("tax");
//
//        jobjSales.optString("total");
//        jobjSales.optString("total_amount");
//        jobjSales.optString("paid");
//        jobjSales.optString("tax");
//
//        jobjWareHoues.optString("name");
//        jobjWareHoues.optString("address");
//        jobjWareHoues.optString("phone");
//        jobjWareHoues.optString("email");


        //https://www.tablesgenerator.com/html_tables
        String withStyle = "<style type=\"text/css\">\n" +
                ".tg  {border-collapse:collapse;border-spacing:0;}\n" +
                ".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}\n" +
                ".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}\n" +
                ".tg .tg-05px{font-weight:bold;font-family:Georgia, serif !important;;border-color:inherit;text-align:left}\n" +
                ".tg .tg-kt03{font-family:Georgia, serif !important;;border-color:inherit;text-align:center;vertical-align:top}\n" +
                ".tg .tg-s1k4{font-family:Georgia, serif !important;;text-align:left;vertical-align:top}\n" +
                ".tg .tg-95lf{font-weight:bold;font-family:Georgia, serif !important;;text-align:left;vertical-align:top}\n" +
                ".tg .tg-161g{font-family:Georgia, serif !important;;border-color:inherit;text-align:center}\n" +
                ".tg .tg-kftd{background-color:#efefef;text-align:left;vertical-align:top}\n" +
                ".tg .tg-ylso{background-color:#efefef;color:#c6c6e7;border-color:#000000;text-align:left;vertical-align:top}\n" +
                ".tg .tg-y698{background-color:#efefef;border-color:inherit;text-align:left;vertical-align:top}\n" +
                ".tg .tg-xldj{border-color:inherit;text-align:left}\n" +
                ".tg .tg-j8cd{font-weight:bold;font-family:Georgia, serif !important;;border-color:inherit;text-align:center}\n" +
                ".tg .tg-g4e4{font-weight:bold;font-family:Georgia, serif !important;;border-color:inherit;text-align:center;vertical-align:top}\n" +
                "</style>\n" +
                "<table class=\"tg\">\n" +
                "  <tr>\n" +
                "    <th class=\"tg-ylso\" rowspan=\"10\"></th>\n" +
                "    <th class=\"tg-95lf\">Name</th>\n" +
                "    <th class=\"tg-s1k4\" colspan=\"4\">" + jobjWareHoues.optString("name") + "</th>\n" +
                "    <th class=\"tg-kftd\" rowspan=\"10\"></th>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td class=\"tg-95lf\">Address</td>\n" +
                "    <td class=\"tg-s1k4\" colspan=\"4\">" + jobjWareHoues.optString("address") + "</td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td class=\"tg-95lf\">Phone</td>\n" +
                "    <td class=\"tg-s1k4\" colspan=\"4\">" + jobjWareHoues.optString("phone") + "</td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td class=\"tg-95lf\">Email</td>\n" +
                "    <td class=\"tg-s1k4\" colspan=\"4\">" + jobjWareHoues.optString("email") + "</td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td class=\"tg-y698\" colspan=\"5\"></td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td class=\"tg-xldj\"></td>\n" +
                "    <td class=\"tg-j8cd\">Amount</td>\n" +
                "    <td class=\"tg-g4e4\">Paid</td>\n" +
                "    <td class=\"tg-g4e4\">Tax</td>\n" +
                "    <td class=\"tg-g4e4\">Total</td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td class=\"tg-05px\">Purchase</td>\n" +
                "    <td class=\"tg-161g\">" + jobjPurchase.optString("total_amount") + "</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjPurchase.optString("paid") + "</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjPurchase.optString("tax") + "</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjPurchase.optString("total") + "</td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td class=\"tg-05px\">Sales</td>\n" +
                "    <td class=\"tg-161g\">" + jobjSales.optString("total_amount") + "</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjSales.optString("paid") + "</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjSales.optString("tax") + "</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjSales.optString("total") + "</td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td class=\"tg-05px\">Return</td>\n" +
                "    <td class=\"tg-161g\">" + jobjReturns.optString("total_amount") + "</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjReturns.optString("paid") + "</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjReturns.optString("tax") + "</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjReturns.optString("total") + "</td>\n" +
                "  </tr>\n" +
                "  <tr>\n" +
                "    <td class=\"tg-05px\">Expense</td>\n" +
                "    <td class=\"tg-161g\">" + jobjExpense.optString("total_amount") + "</td>\n" +
                "    <td class=\"tg-kt03\">-</td>\n" +
                "    <td class=\"tg-kt03\">-</td>\n" +
                "    <td class=\"tg-kt03\">" + jobjExpense.optString("total") + "</td>\n" +
                "  </tr>\n" +
                "</table>";


        WebView webView = (WebView) findViewById(R.id.activity_admin_report_webview);
        webView.getSettings().setJavaScriptEnabled(true);
//        webView.getSettings().setDisplayZoomControls(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.loadData(withStyle, "text/html", "UTF-8");

    }

}
