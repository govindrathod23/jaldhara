package com.jaldhara.view.admin.order;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.InvoiceListAdminAdapter;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.EndlessScrollListener;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.invoice.AddInvoicePaymentActivity;
import com.jaldhara.view.admin.invoice.CreateInvoiceActivity;
import com.jaldhara.view.admin.invoice.InvoicesActivity;
import com.jaldhara.view.admin.invoice.ViewInvoiceListActivity;
import com.jaldhara.view.admin.invoice.ViewPaymentsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class AdminOrdersListActivity extends AppCompatActivity {


    //    private ArrayList arryOrders;
    private TextView txtLbl;
    //    private ArrayList<DeliveryModel> arryDeliveryItems;
    private ArrayList<SpinnerModel> arryCustomer;
    //    private ArrayList<DeliveryModel> arryBottleReturns;
    private List<DeliveryModel> tempOrdersList = new ArrayList<>();

    private List<DeliveryModel> orderList;

    private String customrID = "";
    private String RouteID = "";
    private String Status = "";
    private String DriverID = "";

    private TextView txtHeader;
    private static final int REQUEST_CODE_CUSTOMER = 2078;
    private String fromText = "";
    private String txtTO = "";

    private AdminOrdersListAdapter adapter;
    private RecyclerView recyclerView;
    private FloatingActionButton btnAddOrders;
    private boolean isFromParent = false;

    private EditText searchText;
    private TextView txtClear;
    private int pageNo = 1;
    private int pageSize = 20;
    private boolean isLoading = false;

    private LinearLayoutManager linearLayoutManager;
    private int totalRecord = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_order_list);


        txtHeader = (TextView) findViewById(R.id.activity_addRoute_txtTitle);
        txtLbl = (TextView) findViewById(R.id.lbl_nodelivery);
        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);

        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);

        arryCustomer = new ArrayList<SpinnerModel>();
        SpinnerModel spinnerModel = new SpinnerModel();
        spinnerModel.setRouteName("None");
        spinnerModel.setRouteId("0");
        arryCustomer.add(spinnerModel);

        linearLayoutManager = new LinearLayoutManager(AdminOrdersListActivity.this);
        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);


        ImageView imgBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (getIntent().getBooleanExtra(getString(R.string.key_intent_isFromParent),false)){
//                    Intent intent = new Intent(AdminDashboardCountersActivity.this, AdminOrdersListActivity.class);
//                    startActivity(intent);
//                }
                finish();
            }
        });

        if (ConnectivityReceiver.isConnected()) {
            //pass From date and to date same for first time.
            serviceGetOrder();
        } else {
            Snackbar.make(recyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        scrollListener();

        btnAddOrders = findViewById(R.id.btn_Add);
        btnAddOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AdminOrdersListActivity.this, AdminAddOrdersActivity.class);
                startActivity(intent);
            }
        });


        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    /*Scroll Listener for RecyclerView*/
    private void scrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                int visibleItemCount = linearLayoutManager.getChildCount();
                int totalItemCount = linearLayoutManager.getItemCount();
                int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                if (!isLoading) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0 && totalRecord > orderList.size()) {
                        isLoading = true;
                        pageNo = pageNo + 1;
                        serviceGetOrder();
                    }
                }
            }
        });
    }

    void filter(String text) {
        ArrayList<DeliveryModel> temp = new ArrayList();
        if (orderList != null && orderList.size() > 0) {
            for (DeliveryModel d : orderList) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getCustomerName().toLowerCase().startsWith(text) ||
                        d.getSale_reference_no().toLowerCase().contains(text) ||
                        d.getInvoices_payment_status().toLowerCase().startsWith(text) ||
                        d.getInv_reference_no().toLowerCase().startsWith(text) ||
                        d.getSale_status().toLowerCase().startsWith(text) ||
                        d.getOrderDate().toLowerCase().startsWith(text)) {
                    temp.add(d);
                }
            }

            if (temp != null && temp.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                txtLbl.setVisibility(View.GONE);
                //update recyclerview
                adapter.updateList(temp);
            } else {
                recyclerView.setVisibility(View.GONE);
                txtLbl.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }

//    @Override
//    public void onBackPressed() {
////        NavUtils.navigateUpFromSameTask(this);
//    }

    private void serviceGetOrder() {

        final ProgressDialog dialog = ProgressDialog.show(AdminOrdersListActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.get_all_order(
                "get_all_order",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                "",// here we need all customer records so passing blank customer id
                pageNo,
                pageSize);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        isLoading = false;
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        totalRecord = jsonResponse.getInt("total");
                        Log.d("totalRecord :","totalRecord :"+totalRecord);
                        JSONArray jsonArrayDelivery = jsonResponse.optJSONArray("data");
                        fillDeliveryItems(jsonArrayDelivery);
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    isLoading = false;
                    Toast.makeText(AdminOrdersListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                isLoading = false;
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminOrdersListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * @param v
     */
    public void onClickLegends(View v) {
        final Dialog dialog = new Dialog(AdminOrdersListActivity.this);
        dialog.setContentView(R.layout.dialog_legends);
        dialog.show();
    }

    public void onClickRefresh(View v) {
        pageNo = 1;
        isLoading = false;
        if (orderList != null && orderList.size() > 0) {
            orderList.clear();
        }
        if (tempOrdersList != null && tempOrdersList.size() > 0) {
            tempOrdersList.clear();
        }
        serviceGetOrder();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_CUSTOMER) {
            customrID = data.getStringExtra(getString(R.string.key_intent_customer_id));
            RouteID = data.getStringExtra(getString(R.string.key_intent_Route_ID));
            Status = data.getStringExtra(getString(R.string.key_intent_status));
            DriverID = data.getStringExtra(getString(R.string.key_intent_Driver_ID));

            serviceGetOrder();
        }
    }

    private void bindOrderList(List<DeliveryModel> list) {
        if (list != null && list.size() > 0) {
            if (orderList != null && orderList.size() > 0) {
                orderList.addAll(list);
            } else {
                orderList = new ArrayList<>();
                orderList.addAll(list);
            }
            Log.e("orderList :", "orderList :" + orderList.size());
            if (orderList != null && orderList.size() > 0) {
                recyclerView.setVisibility(View.VISIBLE);
                txtLbl.setVisibility(View.GONE);

                adapter = (AdminOrdersListAdapter) recyclerView.getAdapter();
                if (adapter != null && adapter.getItemCount() > 0) {
                    adapter.setRefreshList(orderList);
                } else {
                    adapter = new AdminOrdersListAdapter(AdminOrdersListActivity.this, orderList);
                    recyclerView.setAdapter(adapter);
                }

                adapter.setOnItemClick(new AdminOrdersListAdapter.onItemClick<DeliveryModel>() {
                    @Override
                    public void onDetailClicked(DeliveryModel data) {
                    }

                    @Override
                    public void onViewPaymentClicked(DeliveryModel data) {

                    }

                    @Override
                    public void onAddPaymentClicked(DeliveryModel data) {

                    }

                    @Override
                    public void onDeleteClicked(DeliveryModel data) {

                    }

                    @Override
                    public void onEditPurchaseClicked(DeliveryModel data) {

                    }
                });
            } else {
                recyclerView.setVisibility(View.GONE);
                txtLbl.setVisibility(View.VISIBLE);
            }
        } else {
            recyclerView.setVisibility(View.GONE);
            txtLbl.setVisibility(View.VISIBLE);
        }
    }

    /**
     * set only delivery Fragment data data
     */
    private void fillDeliveryItems(JSONArray jsonArray) {
        if (jsonArray == null || jsonArray.length() == 0) {
            txtLbl.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            Toast.makeText(AdminOrdersListActivity.this, "No Delivery Items found!", Toast.LENGTH_SHORT).show();
            return;
        }
        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.optJSONObject(i);
            DeliveryModel deliveryModelHeader = new DeliveryModel();

            deliveryModelHeader.setOrderID(jsonObject.optString("id"));
            deliveryModelHeader.setOrderDate(jsonObject.optString("date"));
            deliveryModelHeader.setSale_reference_no(jsonObject.optString("reference_no"));
            deliveryModelHeader.setBillerName(jsonObject.optString("biller"));
            deliveryModelHeader.setCustomerName(jsonObject.optString("customer"));
            deliveryModelHeader.setSale_status(jsonObject.optString("sale_status"));
            deliveryModelHeader.setGrandTotal(jsonObject.optString("grand_total"));
            deliveryModelHeader.setInv_reference_no(jsonObject.optString("inv_reference_no"));
            deliveryModelHeader.setInvoices_total_amount(jsonObject.optString("invoices_total_amount"));
            deliveryModelHeader.setInvoices_payment_status(jsonObject.optString("invoices_payment_status"));

            if (jsonObject.optString("sale_status").equalsIgnoreCase("delivering")) {
                deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_delivering));
            } else if (jsonObject.optString("sale_status").equalsIgnoreCase("partially")) {
                deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_partially));
            } else if (jsonObject.optString("sale_status").equalsIgnoreCase("over")) {
                deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_over));
            } else if (jsonObject.optString("sale_status").equalsIgnoreCase("completed")) {
                deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_completed));
            } else if (jsonObject.optString("sale_status").equalsIgnoreCase("Ordered")) {
                deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_ordered));
            }
            tempOrdersList.add(deliveryModelHeader);

        }

        bindOrderList(tempOrdersList);
    }
}
