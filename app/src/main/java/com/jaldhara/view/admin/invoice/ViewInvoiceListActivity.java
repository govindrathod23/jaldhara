package com.jaldhara.view.admin.invoice;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.adapters.ViewInvoiceAdapter;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.models.OrderModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewInvoiceListActivity extends AppCompatActivity implements View.OnClickListener {


    private RecyclerView recyclerView;
    private ImageView imgBack;
    private ArrayList<OrderModel> arryOrderList;
    private TextView txtCustomerName, txtCustomerAddress, txtCustomerTel, txtCustomerEmail;
    private TextView txtBillerName, txtBillerAddress, txtBillerTel, txtBillerEmail;
    private TextView txtTotalAmount, txtPaid, txtBalance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_invoice);


        imgBack = findViewById(R.id.activity_payments_imgBack);
        imgBack.setOnClickListener(this);

        txtCustomerName = findViewById(R.id.txtCustomerName);
        txtCustomerAddress = findViewById(R.id.txtCustomerAddress);
        txtCustomerTel = findViewById(R.id.txtCustomerTel);
        txtCustomerEmail = findViewById(R.id.txtCustomerEmail);

        txtBillerName = findViewById(R.id.txtBillerName);
        txtBillerAddress = findViewById(R.id.txtBillerAddress);
        txtBillerTel = findViewById(R.id.txtBillerTel);
        txtBillerEmail = findViewById(R.id.txtBillerEmail);

        txtTotalAmount = findViewById(R.id.txtTotalAmount);
        txtPaid = findViewById(R.id.txtPaid);
        txtBalance = findViewById(R.id.txtBalance);

        arryOrderList = new ArrayList<>();

        recyclerView = findViewById(R.id.activity_payments_recyclerView);

        try {
            InvoiceModel invoiceModel = getIntent().getParcelableExtra(getString(R.string.key_intent_invoice_detail));
            if (ConnectivityReceiver.isConnected()) {
                callViewInvoice(invoiceModel.getId());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {


        if (v == imgBack) {
            onBackPressed();
        }


    }


    /**
     * @param id
     */
    private void callViewInvoice(String id) {


        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.invoiceDetails
                (Util.API_INVOICE_DETAILS,
                        Util.API_KEY,
                        id
                );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {


                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());


                        JSONObject jObjCustomer = jsonResponse.optJSONObject("customer");
                        JSONObject jObjBiller = jsonResponse.optJSONObject("biller");
                        JSONObject jObjInv = jsonResponse.optJSONObject("inv");

                        try {
                            String paid = jObjInv.optString("paid");
                            String balance = jObjInv.optString("balance");
                            Double totalAmount = Double.valueOf(paid) + Double.valueOf(balance);

                            txtPaid.setText("Paid: " + paid);
                            txtBalance.setText("Balance: " + balance);
                            txtTotalAmount.setText("" + totalAmount);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        txtCustomerName.setText(jObjCustomer.optString("name"));
                        txtCustomerAddress.setText(
                                jObjCustomer.optString("address") + " " +
                                        jObjCustomer.optString("city") + " " +
                                        jObjCustomer.optString("state") + " " +
                                        jObjCustomer.optString("country") + " " +
                                        jObjCustomer.optString("postal_code"));
                        txtCustomerTel.setText(jObjCustomer.optString("phone"));
                        txtCustomerEmail.setText(jObjCustomer.optString("email"));

                        txtBillerName.setText(jObjBiller.optString("company"));
                        txtBillerAddress.setText(
                                jObjBiller.optString("address") + " " +
                                        jObjBiller.optString("city") + " " +
                                        jObjBiller.optString("state") + " " +
                                        jObjBiller.optString("country") + " " +
                                        jObjBiller.optString("postal_code"));
                        txtBillerTel.setText(jObjBiller.optString("phone"));
                        txtBillerEmail.setText(jObjBiller.optString("email"));


                        if (jsonResponse.has("rows")) {
                            JSONArray jsonArray = jsonResponse.getJSONArray("rows");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject orderObj = jsonArray.getJSONObject(i);
                                OrderModel orderModel = new OrderModel();


                                orderModel.setProduct_name(
                                        orderObj.optString("product_code") + " - " +
                                                orderObj.optString("product_name")
//                                                        + " " +
//                                                orderObj.optString("second_name")
                                );

                                orderModel.setReference_no(orderObj.optJSONObject("sale_array").optString("reference_no"));
                                orderModel.setHsn_code(orderObj.optString("hsn_code"));
                                orderModel.setUnit_quantity(orderObj.optString("unit_quantity"));
                                orderModel.setPending_qty(orderObj.optString("pending_qty"));
                                orderModel.setSerial_no(orderObj.optString("serial_no"));
                                orderModel.setUnit_price(orderObj.optString("unit_price"));
                                orderModel.setSubtotal(orderObj.optString("subtotal"));


                                arryOrderList.add(orderModel);
                            }
//
                            ViewInvoiceAdapter adapter = new ViewInvoiceAdapter(ViewInvoiceListActivity.this, arryOrderList);
                            recyclerView.setAdapter(adapter);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }



}