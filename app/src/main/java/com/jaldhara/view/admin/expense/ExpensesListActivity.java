package com.jaldhara.view.admin.expense;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.ExpenseListAdminAdapter;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.ExpensesModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExpensesListActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgBack;
    private FloatingActionButton btnAddExpenses;
    private Context context;
    private List<ExpensesModel> expenseList = new ArrayList<>();
    private RecyclerView recyclerViewExpense;
    private ExpenseListAdminAdapter expenseAdapter;
    private TextView lbl_nodeliver;


    private EditText searchText;
    private TextView txtClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_expenses);
        context = this;

        imgBack = findViewById(R.id.activity_expenses_list_tab_imgBack);
        btnAddExpenses = findViewById(R.id.activity_expenses_list_tab_add_expenses);
        recyclerViewExpense = findViewById(R.id.activity_expenses_list_recyclerview);
        lbl_nodeliver = findViewById(R.id.lbl_nodelivery);

        imgBack.setOnClickListener(this);
        btnAddExpenses.setOnClickListener(this);

        lbl_nodeliver = (TextView) findViewById(R.id.lbl_nodelivery);

        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);
        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        serviceGetExpenses();
    }


    void filter(String text) {
        ArrayList<ExpensesModel> temp = new ArrayList();
        if (expenseList != null && expenseList.size() > 0) {
            for (ExpensesModel d : expenseList) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getFirstName().toLowerCase().contains(text) ||
                        d.getLastName().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }

            if (temp != null && temp.size() > 0) {
                recyclerViewExpense.setVisibility(View.VISIBLE);
                lbl_nodeliver.setVisibility(View.GONE);
                //update recyclerview
                expenseAdapter.updateList(temp);
            } else {
                recyclerViewExpense.setVisibility(View.GONE);
                lbl_nodeliver.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_expenses_list_tab_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_expenses_list_tab_add_expenses) {
            Intent intent = new Intent(context, AddOrUpdateExpensesActivity.class);
            intent.putExtra("type", "create");
            startActivity(intent);
        }

    }

    private void showAlertDialog(final String expenseId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Are you sure you want to delete this expense?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        serviceDeleteExpense(expenseId);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void serviceGetExpenses() {
        expenseList.clear();
        final ProgressDialog dialog = ProgressDialog.show(ExpensesListActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getExpenses(Util.API_VIEW_EXPENSES, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("expenses")) {
                            JSONArray jsonExpenseArray = jsonResponse.getJSONArray("expenses");
                            if (jsonExpenseArray != null && jsonExpenseArray.length() > 0) {
                                for (int i = 0; i < jsonExpenseArray.length(); i++) {
                                    JSONObject jsonObject = jsonExpenseArray.getJSONObject(i);
                                    ExpensesModel expensesModel = new ExpensesModel();
                                    expensesModel.setId(jsonObject.getString("id"));
                                    expensesModel.setCompId(jsonObject.getString("comp_id"));
                                    expensesModel.setDate(jsonObject.getString("date"));
                                    expensesModel.setReference(jsonObject.getString("reference"));
                                    expensesModel.setAmount(jsonObject.getString("amount"));
                                    expensesModel.setNote(jsonObject.getString("note"));
                                    expensesModel.setCreatedBy(jsonObject.getString("created_by"));
                                    expensesModel.setCategoryId(jsonObject.getString("category_id"));
                                    expensesModel.setWarehouseId(jsonObject.getString("warehouse_id"));
                                    expensesModel.setRoute(jsonObject.getString("route"));
                                    expensesModel.setFirstName(jsonObject.getString("first_name"));
                                    expensesModel.setLastName(jsonObject.getString("last_name"));
                                    expensesModel.setExpenseCategoriesName(jsonObject.getString("expense_categories_name"));

                                    expenseList.add(expensesModel);
                                }

                                recyclerViewExpense.setVisibility(View.VISIBLE);
                                lbl_nodeliver.setVisibility(View.GONE);
                                expenseAdapter = new ExpenseListAdminAdapter(context, expenseList);
                                recyclerViewExpense.setAdapter(expenseAdapter);
                                expenseAdapter.setOnItemClick(new ExpenseListAdminAdapter.onItemClick<ExpensesModel>() {
                                    @Override
                                    public void onEditClick(ExpensesModel data) {
                                        Intent intent = new Intent(context, AddOrUpdateExpensesActivity.class);
                                        intent.putExtra("type", "edit");
                                        intent.putExtra("expenseDetail", data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onDeleteClick(ExpensesModel data) {
                                        showAlertDialog(data.getId());
                                    }
                                });
                            } else {
                                recyclerViewExpense.setVisibility(View.GONE);
                                lbl_nodeliver.setVisibility(View.VISIBLE);
                            }
                        } else {
                            recyclerViewExpense.setVisibility(View.GONE);
                            lbl_nodeliver.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recyclerViewExpense.setVisibility(View.GONE);
                        lbl_nodeliver.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();
                recyclerViewExpense.setVisibility(View.GONE);
                lbl_nodeliver.setVisibility(View.VISIBLE);
            }
        });

    }

    private void serviceDeleteExpense(String id) {
        final ProgressDialog dialog = ProgressDialog.show(ExpensesListActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.deleteExpense(Util.API_DELETE_EXPENSE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                serviceGetExpenses();
                            } else {
                                Toast.makeText(ExpensesListActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });

    }
}