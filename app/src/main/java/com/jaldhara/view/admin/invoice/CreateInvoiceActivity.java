package com.jaldhara.view.admin.invoice;


import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;


import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.SignatureView;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateInvoiceActivity extends Activity {

    private ArrayList<SpinnerModel> arryCustomer;
    private ArrayList<SpinnerModel> arryBiller;
    private Spinner spnCustomer;
    private Spinner spnBiller;

    private TextView txtFromDate;
    private TextView txtToDate;
    private TextView txtCustomer;
    private TextInputEditText edtInvoiceReferenceNo;
    private TextInputEditText edtInvoiceNote;
    private TextInputEditText edtStaffNote;
    private Button btnAddInvoice;
    private InvoiceModel invoiceModel;
    private static int REQUEST_CODE_ORDER = 887;
    private String selectedIDs = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_create_invoice);


        spnCustomer = (Spinner) findViewById(R.id.activity_admin_add_invoice_SpnCustomer);
        spnBiller = (Spinner) findViewById(R.id.activity_admin_add_invoice_SpnBiller);
        //txtDate = (TextView) findViewById(R.id.activity_admin_add_invoice_date);
        txtFromDate = (TextView) findViewById(R.id.activity_admin_add_invoice_FromDate);
        txtToDate = (TextView) findViewById(R.id.activity_admin_add_invoice_ToDate);
        edtInvoiceReferenceNo = (TextInputEditText) findViewById(R.id.activity_admin_add_invoice_edt_InvoiceReferenceNo);
        edtInvoiceNote = (TextInputEditText) findViewById(R.id.activity_admin_add_invoice_edt_InvoiceNote);
        edtStaffNote = (TextInputEditText) findViewById(R.id.activity_admin_add_invoice_edt_StaffNote);
        txtCustomer = (TextView) findViewById(R.id.activity_admin_add_invoice_txtCustomer);
        btnAddInvoice = (Button) findViewById(R.id.activity_admin_add_invoice_btnCreate);
        //txtDate.setText(Util.getTodayDate());
        txtFromDate.setText(Util.getTodayDate());
        txtToDate.setText(Util.getTodayDate());


        arryCustomer = new ArrayList<SpinnerModel>();
        arryBiller = new ArrayList<SpinnerModel>();

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfCustomer();
            serviceListOfBiller();

        } else {
            Snackbar.make(spnCustomer, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }


        invoiceModel = getIntent().getParcelableExtra(getString(R.string.key_intent_invoice_detail));
        if (invoiceModel != null) {
            spnCustomer.setVisibility(View.GONE);
            txtCustomer.setText(invoiceModel.getCustomer());
            txtCustomer.setVisibility(View.VISIBLE);

            edtInvoiceReferenceNo.setText(invoiceModel.getInv_reference_no());
            txtFromDate.setText(invoiceModel.getFrom_date());
            txtToDate.setText(invoiceModel.getTo_date());
            edtStaffNote.setText(invoiceModel.getStaff_note());
            edtInvoiceNote.setText(invoiceModel.getNote());


            btnAddInvoice.setText("Edit Invoice");

        } else {

            if (ConnectivityReceiver.isConnected()) {
                serviceGetReferanceNo();
            } else {
                Snackbar.make(spnCustomer, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
            }
        }


    }


    public void onClick(View v) {


        if (v.getId() == R.id.activity_admin_add_invoice_btnCreate) {
            if (ConnectivityReceiver.isConnected()) {
                if (TextUtils.isEmpty(selectedIDs)) {

                    Toast.makeText(this, "Please select order between dates, press on To or From date!", Toast.LENGTH_LONG).show();

                    return;
                }
                serviceAddInvoice();
            } else {
                Snackbar.make(spnCustomer, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
            }

        } else if (v.getId() == R.id.activity_admin_add_invoice_btnCancel || v.getId() == R.id.activity_admin_add_invoice_imgBack) {
//            finish();
            onBackPressed();
        }
//        else if (v == txtDate) {
//            Calendar calendar = Util.getCalDateFromString(txtDate.getText().toString());
//            setDate(calendar, txtDate);
//        }
        else if (v == txtFromDate) {
            Calendar calendar = Util.getCalDateFromString(txtFromDate.getText().toString());
            setDate(calendar, txtFromDate);
        } else if (v == txtToDate) {
            Calendar calendar = Util.getCalDateFromString(txtToDate.getText().toString());
            setDate(calendar, txtToDate);
        }


    }

    /**
     * @param calendar
     * @param txtDate
     */
    private void setDate(Calendar calendar, final TextView txtDate) {

        DatePickerDialog datePickerDialog = new DatePickerDialog(CreateInvoiceActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);

                txtDate.setText(year + "-" + strMonth + "-" + strDay);

                if (!TextUtils.isEmpty(txtFromDate.getText().toString()) && !TextUtils.isEmpty(txtToDate.getText().toString())) {
                    Intent intent = new Intent(CreateInvoiceActivity.this, BtwnDateOrdersListActivity.class);
                    intent.putExtra("from_date", txtFromDate.getText().toString());
                    intent.putExtra("to_date", txtToDate.getText().toString());
                    intent.putExtra("customer_id", invoiceModel != null ? invoiceModel.getCustomer_id() : arryCustomer.get(spnCustomer.getSelectedItemPosition()).getRouteId().toString());
                    startActivityForResult(intent, REQUEST_CODE_ORDER);
                }

            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
        //datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_ORDER) {

            selectedIDs = data.getStringExtra("selected_id").substring(0, data.getStringExtra("selected_id").lastIndexOf(","));

        }

    }

    /**
     * =============================================================================================
     *
     * @param
     * @param
     * @param =============================================================================================
     */
    private void serviceAddInvoice() {

        final ProgressDialog dialog = ProgressDialog.show(CreateInvoiceActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        int currMonth = Util.getCalDateFromString(Util.getTodayDate()).get(Calendar.MONTH);

        Call<ResponseBody> call = null;

        if (invoiceModel == null && TextUtils.isEmpty(invoiceModel.getId())) {
            call = apiService.addInvoice(
                    Util.API_ADD_INVOICE,
                    Util.API_KEY,
                    JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                    arryCustomer.get(spnCustomer.getSelectedItemPosition()).getRouteId(),
                    arryBiller.get(spnBiller.getSelectedItemPosition()).getRouteId(),
                    "" + (currMonth + 1),
                    txtFromDate.getText().toString(),
                    txtToDate.getText().toString(),
                    edtInvoiceNote.getText().toString(),
                    edtStaffNote.getText().toString(),
                    selectedIDs
            );
        } else {
            call = apiService.editInvoice(
                    Util.API_EDIT_INVOICE,
                    Util.API_KEY,
                    arryCustomer.get(spnCustomer.getSelectedItemPosition()).getRouteId(),
                    arryBiller.get(spnBiller.getSelectedItemPosition()).getRouteId(),
                    "" + (currMonth + 1),
                    txtFromDate.getText().toString(),
                    txtToDate.getText().toString(),
                    edtInvoiceNote.getText().toString(),
                    edtStaffNote.getText().toString(),
                    invoiceModel.getId(),
                    selectedIDs
            );
        }


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {

                        final String strResponse = response.body().string();
                        JSONObject jsonResponse = new JSONObject(strResponse);

                        Toast.makeText(CreateInvoiceActivity.this, jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                        if (jsonResponse.has("status") && jsonResponse.optBoolean("status")) {
                            finish();
                        }
                    }


                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CreateInvoiceActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CreateInvoiceActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });

    }


    /***************************************************************************************
     * Get Customer -- Get Customer -- Get Customer -- Get Customer
     ***************************************************************************************/
    private void serviceListOfCustomer() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllCustomer("person", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), "customer");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int route = 0; route < jsonArray.length(); route++) {
                            JSONObject jsonCust = jsonArray.optJSONObject(route);
                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonCust.optString("company"));
                            spinnerModel.setRouteId(jsonCust.optString("id"));
                            arryCustomer.add(spinnerModel);
                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(CreateInvoiceActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryCustomer);
                        spnCustomer.setAdapter(arrayAdapter);

//                        if (invoiceModel!=null) {
//                            spnCustomer.setSelection(Arrays.asList(arryCustomer).indexOf(invoiceModel.getCustomer_id()));
//                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CreateInvoiceActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CreateInvoiceActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfBiller() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Billers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllBillers("person", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), "biller", 1, 100);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                int currentPosition = 0;
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int route = 0; route < jsonArray.length(); route++) {
                            JSONObject jsonCust = jsonArray.optJSONObject(route);
                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonCust.optString("company"));
                            spinnerModel.setRouteId(jsonCust.optString("id"));
                            arryBiller.add(spinnerModel);

                            if (invoiceModel != null && (jsonCust.optString("id").equals(invoiceModel.getBiller_id()))) {
                                currentPosition = route;
                            }
                        }


                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(CreateInvoiceActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryBiller);
                        spnBiller.setAdapter(arrayAdapter);

                        spnBiller.setSelection(currentPosition);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CreateInvoiceActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CreateInvoiceActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void serviceGetReferanceNo() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.get_reference_no(
                "get_reference_no",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                "inv");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.optBoolean("status")) {
                            edtInvoiceReferenceNo.setText(jsonResponse.optJSONObject("data").optString("reference_no"));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CreateInvoiceActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CreateInvoiceActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
