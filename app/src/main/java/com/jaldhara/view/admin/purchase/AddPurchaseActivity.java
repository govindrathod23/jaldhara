package com.jaldhara.view.admin.purchase;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.android.material.textfield.TextInputEditText;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.ProductModel;
import com.jaldhara.models.PurchaseModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.supplier.AdminAddSupplierActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPurchaseActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int INTENT_PRODUCTS_LIST = 101;
    String[] statusList = {"Received", "Pending", "Ordered"};
    String[] taxList = {"No Tax", "GST @18%", "IGST", "CGST", "SGST"};
    private Context context;
    private Spinner spinnerSupplier, spinnerStatus, spinnerTax, spinnerWareHouse, spinnerRoutes;
    private TextInputEditText edtDate, edtReference, edtDiscount, edtShipping, edtPaymentTerm, edtNote;
    private View viewDate;
    private List<SpinnerModel> arrayWarehouseList = new ArrayList<>();
    private List<SpinnerModel> arrayRouteList = new ArrayList<>();
    private List<SpinnerModel> supplierList = new ArrayList<>();
    private ArrayList<ProductModel> selectedProductList = new ArrayList<>();
    private boolean isEdit = false;
    private String warehouse = null, routes = null, supplier = null, purchaseId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_purchase);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_add_purchase_imgBack);
        imgBack.setOnClickListener(this);

        LinearLayout linearSupplier = findViewById(R.id.activity_add_purchase_linear_add_supplier);
        linearSupplier.setOnClickListener(this);

        edtDate = findViewById(R.id.activity_add_expense_edt_date);
        edtDate.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US).format(Calendar.getInstance().getTimeInMillis()));
        viewDate = findViewById(R.id.activity_add_purchase_view_date);
        viewDate.setOnClickListener(this);

        edtReference = findViewById(R.id.activity_add_purchase_edt_reference);
        edtDiscount = findViewById(R.id.activity_add_purchase_edt_discount);
        edtShipping = findViewById(R.id.activity_add_purchase_edt_shipping);
        edtPaymentTerm = findViewById(R.id.activity_add_purchase_edt_payment_term);
        edtNote = findViewById(R.id.activity_add_purchase_edt_note);

        spinnerSupplier = findViewById(R.id.activity_add_purchase_spinner_supplier);
        spinnerStatus = findViewById(R.id.activity_add_purchase_spinner_status);
        spinnerTax = findViewById(R.id.activity_add_purchase_spinner_order_tax);
        spinnerWareHouse = findViewById(R.id.activity_add_purchase_spinner_warehouse);
        spinnerRoutes = findViewById(R.id.activity_add_purchase_spinner_routes);

        Button btnAddPurchase = findViewById(R.id.activity_add_purchase_button_add);
        btnAddPurchase.setOnClickListener(this);

        final LinearLayout linearMoreOption = findViewById(R.id.activity_add_purchase_linear_more_option);

        Button btnOrderItems = findViewById(R.id.activity_add_purchase_button_order_item);
        btnOrderItems.setOnClickListener(this);

        CheckBox checkBoxMoreOption = findViewById(R.id.activity_add_purchase_checkbox_more_option);
        checkBoxMoreOption.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    linearMoreOption.setVisibility(View.VISIBLE);
                } else {
                    linearMoreOption.setVisibility(View.GONE);
                }
            }
        });

        ArrayAdapter adapterStatus = new ArrayAdapter(this, android.R.layout.simple_spinner_item, statusList);
        adapterStatus.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatus.setAdapter(adapterStatus);

        ArrayAdapter adapterTax = new ArrayAdapter(this, android.R.layout.simple_spinner_item, taxList);
        adapterTax.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTax.setAdapter(adapterTax);

        serviceGetSupplier();
        serviceListOfRoutes();
        serviceListOfWarehouses();

        try {
            if (getIntent().hasExtra(getString(R.string.key_intent_edit_purchase))) {
                PurchaseModel purchaseModel = (PurchaseModel) getIntent().getSerializableExtra(getString(R.string.key_intent_purchase_detail));
                btnAddPurchase.setText("Update Purchase");
                isEdit = true;

                if (purchaseModel != null) {
                    setPurchaseData(purchaseModel);
                }
                serviceViewPurchase();

            } else {

                serviceGetReferanceNo();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_add_purchase_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_add_purchase_linear_add_supplier) {
            startActivity(new Intent(context, AdminAddSupplierActivity.class));
        } else if (id == R.id.activity_add_purchase_button_order_item) {
            startActivityForResult(new Intent(context, ProductsActivity.class).putExtra(getString(R.string.key_intent_product_list), selectedProductList), INTENT_PRODUCTS_LIST);
        } else if (id == R.id.activity_add_purchase_view_date) {
            showDatePickerDialog();
        } else if (id == R.id.activity_add_purchase_button_add) {
            if (isEdit) {
                serviceUpdatePurchase();
            } else {
                serviceAddPurchase();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_PRODUCTS_LIST && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra("products")) {
                Log.e("ADDPurchase", " data " + data.getSerializableExtra("products"));
           /*     ArrayList<ProductModel> selectedProducts = (ArrayList<ProductModel>) data.getSerializableExtra("products");
                selectedProductList.addAll(selectedProducts);*/
                selectedProductList.clear();
                selectedProductList.addAll((ArrayList<ProductModel>) data.getSerializableExtra("products"));
            }
        }
    }

    private void setPurchaseData(PurchaseModel purchaseData) {
        edtDate.setText(purchaseData.getDate());
        edtDiscount.setText(purchaseData.getOrderDiscount());
        edtShipping.setText(purchaseData.getShipping());
        edtPaymentTerm.setText(purchaseData.getPaymentTerm());
        edtReference.setText(purchaseData.getReferenceNo());
        edtNote.setText(purchaseData.getNote());

        warehouse = purchaseData.getWarehouseId();
        routes = purchaseData.getRoute();
        supplier = purchaseData.getSupplierId();
        purchaseId = purchaseData.getId();

        if (purchaseData.getStatus() != null) {

            int index = 0;
            for (int i = 0; i < statusList.length; i++) {
                if (statusList[i].toLowerCase().equals(purchaseData.getStatus().toLowerCase())) {
                    index = i;
                    break;
                }
            }

            spinnerStatus.setSelection(index);
        }

        if (purchaseData.getOrderTax() != null) {

            int index = 0;
            for (int i = 0; i < taxList.length; i++) {
                if (taxList[i].equals(purchaseData.getOrderTax())) {
                    index = i;
                    break;
                }
            }

            spinnerTax.setSelection(index);
        }

    }

    private void showDatePickerDialog() {

        new SingleDateAndTimePickerDialog.Builder(context)
                .bottomSheet()
                .setTimeZone(TimeZone.getDefault())
                .curved()
                .defaultDate(Calendar.getInstance().getTime())
                .mainColor(Color.BLACK)
                .titleTextColor(Color.BLACK)
                .displayAmPm(true)
                .title("Select date and time")
                .minutesStep(1)
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        edtDate.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US).format(date));
                    }
                }).display();
    }

    private void serviceGetSupplier() {
        supplierList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getAllSupplier(Util.API_LIST_OF_SUPPLIER, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("data")) {
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject paymentObj = jsonArray.getJSONObject(i);

                                SpinnerModel spinnerModel = new SpinnerModel();
                                spinnerModel.setRouteId(paymentObj.getString("id"));
                                spinnerModel.setRouteName(paymentObj.getString("name"));

                                supplierList.add(spinnerModel);
                            }

                            ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, supplierList);
                            spinnerSupplier.setAdapter(arrayAdapter);

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void serviceListOfRoutes() {
        arrayRouteList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Routes", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_ROUTES, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            arrayRouteList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, arrayRouteList);
                        spinnerRoutes.setAdapter(arrayAdapter);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfWarehouses() {
        arrayWarehouseList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Warehouses", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_WAREHOUSE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            arrayWarehouseList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, arrayWarehouseList);
                        spinnerWareHouse.setAdapter(arrayAdapter);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceAddPurchase() {

        String comp_id = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id));
        String user_id = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID));
        String date = edtDate.getText().toString().trim();
        String reference = edtReference.getText().toString().trim();
        String warehouse = "";
        if (spinnerWareHouse.getSelectedItemPosition() != -1) {
            warehouse = arrayWarehouseList.get(spinnerWareHouse.getSelectedItemPosition()).getRouteId();
        }
        String status = spinnerStatus.getSelectedItem().toString();
        String route = "";
        if (spinnerRoutes.getSelectedItemPosition() != -1) {
            route = arrayRouteList.get(spinnerRoutes.getSelectedItemPosition()).getRouteId();
        }
        String supplier = "";
        if (spinnerSupplier.getSelectedItemPosition() != -1) {
            supplier = supplierList.get(spinnerSupplier.getSelectedItemPosition()).getRouteId();
        }
        String orderTax = spinnerTax.getSelectedItem().toString();
        String discount = edtDiscount.getText().toString().trim();
        String shipping = edtShipping.getText().toString().trim();
        String paymentTerm = edtPaymentTerm.getText().toString().trim();
        String note = edtNote.getText().toString().trim();

        if (warehouse.isEmpty()) {
            Toast.makeText(context, "Please fill all mandatory detail.", Toast.LENGTH_SHORT).show();
            return;
        }

        JSONArray productArray = new JSONArray();
        for (ProductModel product : selectedProductList) {
            try {
                JSONObject productObj = new JSONObject();
                productObj.put("net_cost", product.getNetPrice());
                productObj.put("unit_cost", product.getUnitPrice());
                productObj.put("real_unit_cost", product.getPrice());
                productObj.put("quantity", product.getQuantity());
                productObj.put("return_quantity", product.getReturnQuantity());
                productObj.put("product_option", "");
                productObj.put("product_tax", "");
                productObj.put("product_discount", "");
                productObj.put("expiry", "");
                productObj.put("part_no", "");
                productObj.put("product_unit", product.getUnit().getId());
                productObj.put("product_base_quantity", "");
                productObj.put("product", "");
                productObj.put("item_code", product.getCode());
                productObj.put("product_id", product.getId());

                productArray.put(productObj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Log.e("addpurchase ", " productArray " + productArray);

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.addPurchase(
                Util.API_ADD_PURCHASE,
                Util.API_KEY,
                comp_id,
                reference,
                warehouse,
                route,
                supplier,
                status,
                shipping,
                note,
                paymentTerm,
                discount,
                orderTax,
                user_id,
                productArray);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("message")) {
                            Toast.makeText(AddPurchaseActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                Intent intent = new Intent();
                                intent.putExtra("reload", true);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void serviceUpdatePurchase() {

        String comp_id = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id));
        String user_id = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID));
        String date = edtDate.getText().toString().trim();
        String reference = edtReference.getText().toString().trim();
        String warehouse = "";
        if (spinnerWareHouse.getSelectedItemPosition() != -1) {
            warehouse = arrayWarehouseList.get(spinnerWareHouse.getSelectedItemPosition()).getRouteId();
        }
        String status = spinnerStatus.getSelectedItem().toString();
        String route = "";
        if (spinnerRoutes.getSelectedItemPosition() != -1) {
            route = arrayRouteList.get(spinnerRoutes.getSelectedItemPosition()).getRouteId();
        }
        String supplier = "";
        if (spinnerSupplier.getSelectedItemPosition() != -1) {
            supplier = supplierList.get(spinnerSupplier.getSelectedItemPosition()).getRouteId();
        }
        String orderTax = spinnerTax.getSelectedItem().toString();
        String discount = edtDiscount.getText().toString().trim();
        String shipping = edtShipping.getText().toString().trim();
        String paymentTerm = edtPaymentTerm.getText().toString().trim();
        String note = edtNote.getText().toString().trim();

        if (warehouse.isEmpty()) {
            Toast.makeText(context, "Please fill all mandatory detail.", Toast.LENGTH_SHORT).show();
            return;
        }

        JSONArray productArray = new JSONArray();
        for (ProductModel product : selectedProductList) {
            try {
                JSONObject productObj = new JSONObject();
                productObj.put("net_cost", product.getNetPrice());
                productObj.put("unit_cost", product.getUnitPrice());
                productObj.put("real_unit_cost", product.getPrice());
                productObj.put("quantity", product.getQuantity());
                productObj.put("return_quantity", product.getReturnQuantity());
                productObj.put("product_option", "");
                productObj.put("product_tax", "");
                productObj.put("product_discount", "");
                productObj.put("expiry", "");
                productObj.put("part_no", "");
                productObj.put("product_unit", product.getUnit().getId());
                productObj.put("product_base_quantity", "");
                productObj.put("product", "");
                productObj.put("item_code", product.getCode());
                productObj.put("product_id", product.getId());

                productArray.put(productObj);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Log.e("addpurchase ", " productArray " + productArray);

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.updatePurchase(Util.API_ADD_PURCHASE, Util.API_KEY,
                comp_id,
                reference, warehouse, route,
                supplier, status, shipping,
                note, paymentTerm,
                discount, orderTax,
                user_id, productArray, purchaseId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("message")) {
                            Toast.makeText(AddPurchaseActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                Intent intent = new Intent();
                                intent.putExtra("reload", true);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void serviceViewPurchase() {
        String comp_id = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id));

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.viewPurchase(Util.API_VIEW_PURCHASE, Util.API_KEY,
                comp_id, purchaseId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {

                                if (jsonResponse.has("rows")) {
                                    JSONArray rowArray = jsonResponse.getJSONArray("rows");

                                    for (int i = 0; i < rowArray.length(); i++) {

                                        JSONObject productObj = rowArray.getJSONObject(i);

                                        ProductModel productModel = new ProductModel();
                                        productModel.setId(productObj.getString("product_id"));
                                        productModel.setCode(productObj.getString("product_code"));
                                        productModel.setName(productObj.getString("product_name"));
                                        productModel.setNetPrice(productObj.getString("net_unit_cost"));
                                        productModel.setPrice(productObj.getString("real_unit_cost"));
                                        productModel.setUnitPrice(productObj.getString("unit_cost"));
                                        productModel.setQuantity(productObj.getString("quantity"));
                                        productModel.setReturnQuantity(productObj.getString("return_quantity"));

                                        ProductModel.Unit unit = new ProductModel.Unit();
                                        unit.setId(productObj.getString("product_unit_id"));
                                        unit.setCode(productObj.getString("product_unit_code"));
                                        productModel.setUnit(unit);

                                        selectedProductList.add(productModel);

                                    }

                                }

                                try {
                                    if (warehouse != null && !warehouse.isEmpty()) {
                                        int index = 0;
                                        for (int i = 0; i < arrayWarehouseList.size(); i++) {
                                            if (arrayWarehouseList.get(i).getRouteId().equals(warehouse)) {
                                                index = i;
                                                break;
                                            }
                                        }

                                        spinnerWareHouse.setSelection(index);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    if (routes != null && !routes.isEmpty()) {
                                        int index = 0;
                                        for (int i = 0; i < arrayRouteList.size(); i++) {
                                            if (arrayRouteList.get(i).getRouteId().equals(routes)) {
                                                index = i;
                                                break;
                                            }
                                        }

                                        spinnerRoutes.setSelection(index);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                try {
                                    if (supplier != null && !supplier.isEmpty()) {
                                        int index = 0;
                                        for (int i = 0; i < supplierList.size(); i++) {
                                            if (supplierList.get(i).getRouteId().equals(supplier)) {
                                                index = i;
                                                break;
                                            }
                                        }

                                        spinnerSupplier.setSelection(index);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
                        } else {
                            if (jsonResponse.has("message")) {
                                Toast.makeText(AddPurchaseActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void serviceGetReferanceNo() {

        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.get_reference_no(
                "get_reference_no",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                "purchase");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.optBoolean("status")) {
                            edtReference.setText(jsonResponse.optJSONObject("data").optString("reference_no"));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AddPurchaseActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AddPurchaseActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

}