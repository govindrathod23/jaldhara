package com.jaldhara.view.admin.supplier;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.InvoiceListDriverAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.DriverCommissionModel;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.models.SupplierModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.commission.AdminAddCommissionActivity;
import com.jaldhara.view.admin.commission.AdminCommissionListAdapter;
import com.jaldhara.view.admin.invoice.ViewInvoiceListActivity;
import com.jaldhara.view.admin.order.AdminOrdersListAdapter;
import com.jaldhara.view.deliveryboy.invoice.DAddInvoicePaymentActivity;
import com.jaldhara.view.deliveryboy.invoice.DViewPaymentsActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminSupplierListActivity extends AppCompatActivity {


    private ArrayList arryOrders;
    private TextView txtLbl;
    private ArrayList<DeliveryModel> arryDeliveryItems;
    private ArrayList<SpinnerModel> arryCustomer;
    private ArrayList<DeliveryModel> arryBottleReturns;
    private String customrID = "";
    private String RouteID = "";
    private String Status = "";
    private String DriverID = "";

    private TextView txtHeader;
    private static final int REQUEST_CODE_CUSTOMER = 2078;
    private String fromText = "";
    private String txtTO = "";


    private RecyclerView recyclerView;
    private FloatingActionButton btnAddOrders;

    private EditText searchText;
    private TextView txtClear;

    ArrayList<SupplierModel> arrySupplier;

    AdminSupplierListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplier_list);


        txtHeader = (TextView) findViewById(R.id.activity_addRoute_txtTitle);
        txtLbl = (TextView) findViewById(R.id.lbl_nodelivery);

        arryCustomer = new ArrayList<SpinnerModel>();
        SpinnerModel spinnerModel = new SpinnerModel();
        spinnerModel.setRouteName("None");
        spinnerModel.setRouteId("0");
        arryCustomer.add(spinnerModel);

        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);
        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);

        arryDeliveryItems = new ArrayList<DeliveryModel>();
        arryOrders = new ArrayList<DeliveryModel>();

        arrySupplier = new ArrayList<>();

        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        ImageView imgBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfSupplier();
        } else {
            Snackbar.make(recyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btnAddOrders = findViewById(R.id.btn_Add);
        btnAddOrders.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(AdminSupplierListActivity.this, AdminAddCommissionActivity.class);
//                startActivity(intent);
            }
        });
    }

    void filter(String text) {
        ArrayList<SupplierModel> temp = new ArrayList();

        if (arrySupplier != null && arrySupplier.size() > 0) {
            for (SupplierModel d : arrySupplier) {
                if (d.getCompany().toLowerCase().contains(text) ||
                        d.getName().toLowerCase().contains(text) ||
                        d.getEmail().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }
            if (temp != null && temp.size() > 0) {
                //update recyclerview
                recyclerView.setVisibility(View.VISIBLE);
                txtLbl.setVisibility(View.GONE);
                mAdapter.setRefreshList(temp);
            } else {
                recyclerView.setVisibility(View.GONE);
                txtLbl.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }

    private void serviceListOfSupplier() {

        final ProgressDialog dialog = ProgressDialog.show(AdminSupplierListActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllSupplier(
                Util.API_LIST_OF_SUPPLIER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());


                        if (jsonResponse.has("data")) {
                            JSONArray jsonArraySupplier = jsonResponse.optJSONArray("data");
                            if (jsonArraySupplier != null && jsonArraySupplier.length() > 0) {
                                recyclerView.setVisibility(View.VISIBLE);
                                txtLbl.setVisibility(View.GONE);

                                Gson gson = new Gson();
                                Type listType = new TypeToken<ArrayList<SupplierModel>>() {
                                }.getType();

                                arrySupplier = gson.fromJson(jsonArraySupplier.toString(), listType);

                                mAdapter = new AdminSupplierListAdapter(AdminSupplierListActivity.this, arrySupplier);
                                recyclerView.setAdapter(mAdapter);
                            } else {
                                recyclerView.setVisibility(View.GONE);
                                txtLbl.setVisibility(View.VISIBLE);
                            }

//
//                            mAdapter.setOnItemClick(new InvoiceListDriverAdapter.onItemClick<InvoiceModel>() {
//                                @Override
//                                public void onDetailClicked(InvoiceModel data) {
//                                    Intent intent = new Intent(getActivity(), ViewInvoiceListActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
//                                    startActivity(intent);
//
//                                }
//
//                                @Override
//                                public void onAddPaymentClicked(InvoiceModel data) {
//                                    Intent intent = new Intent(getActivity(), DAddInvoicePaymentActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
//                                    startActivity(intent);
//                                }
//
//                                @Override
//                                public void onDeleteClicked(final InvoiceModel data) {
//                                    Util.showDialog(getActivity(), "Delete Invoice", "Please contact your admin.\nOnly admin can delete the invoice.");
//                                }
//
//                                @Override
//                                public void onEditClicked(InvoiceModel data) {
//                                    Util.showDialog(getActivity(), "Edit Invoice", "Please contact your admin.\nOnly admin can edit the invoice.");
//                                }
//
//                                @Override
//                                public void onViewPaymentsClicked(InvoiceModel data) {
//                                    Intent intent = new Intent(getActivity(), DViewPaymentsActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
//                                    startActivity(intent);
//                                }
//
//                                @Override
//                                public void onEmailInvoice(InvoiceModel data) {
//
//                                    Intent intent = new Intent(Intent.ACTION_SEND);
//                                    intent.setType("plain/text");
//                                    intent.putExtra(Intent.EXTRA_SUBJECT, "Your Invoice");
//                                    intent.putExtra(Intent.EXTRA_TEXT, "invoice text HTML ma banava padse body mate");
//                                    startActivity(Intent.createChooser(intent, "Mail invoice"));
//
//                                }
//
//                            });
                        }
                    } else {
                        recyclerView.setVisibility(View.GONE);
                        txtLbl.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();
                recyclerView.setVisibility(View.GONE);
                txtLbl.setVisibility(View.VISIBLE);
            }
        });
    }
}
