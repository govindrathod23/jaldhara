package com.jaldhara.view.admin.pendingorder;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.FilterRouteListAdapter;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterDriverFragment extends Fragment {


    private View vLayout;
    private RecyclerView recyclerView;
    private ArrayList<SpinnerModel> arryDriverList;
    FilterRouteListAdapter filterRouteListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        vLayout = inflater.inflate(R.layout.fragment_filter_route, container, false);
        recyclerView = (RecyclerView) vLayout.findViewById(R.id.fragment_filter_route_recycleView);

        arryDriverList = new ArrayList<>();


        if (ConnectivityReceiver.isConnected()) {
            serviceListOfDriver();
        } else {
            Snackbar.make(recyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        return vLayout;
    }

    private void serviceListOfDriver() {
        final ProgressDialog dialog = ProgressDialog.show(getActivity(), "Please wait", "Getting Driver data", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfDrivers("list_of_drivers", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("first_name") + " " + jsonArray.optJSONObject(header).optString("last_name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryDriverList.add(spinnerModel);

                        }

                        filterRouteListAdapter = new FilterRouteListAdapter(getActivity(), arryDriverList,FilterDriverFragment.class.getName().toString());
                        recyclerView.setAdapter(filterRouteListAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}


