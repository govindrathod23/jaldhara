package com.jaldhara.view.admin.products;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAddProductActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner spinnerProductType, spinnerWarehouse, spinnerBarcodeSymbology, spinnerBrand, spinnerCategory, spinnerSubCategory, spinnerProductUnit, spinnerDefaultSaleUnit, spinnerDefaultPurchaseUnit, spinnerProductTax, spinnerTaxMethod, spinnerSupplier;
    private TextInputEditText edtProductName, edtProductCode, edtSlug, edtSecondaryName, edtWeight, edtProductCost, edtProductPrice, edtBottleCost, edtHSNCode, edtAlertQuantity, edtProductDetails, edtSupplierCost, edtProductDetailsForInvoice;
    private String[] barcodeList = {"Code25", "Code39", "Code128", "EAN8", "EAN13", "UPC-A", "UPC-E"};
    private String[] taxList = {"No Tax", "GST @18%", "IGST", "CGST", "SGST"};
    private String[] taxMethodList = {"Exclusive", "Inclusive"};
    private List<SpinnerModel> productTypeList = new ArrayList<>();
    private List<SpinnerModel> arrayWarehouseList = new ArrayList<>();
    private List<SpinnerModel> brandList = new ArrayList<>();
    private List<SpinnerModel> categoryList = new ArrayList<>();
    private List<SpinnerModel> subCategoryList = new ArrayList<>();
    private List<SpinnerModel> unitList = new ArrayList<>();
    private List<SpinnerModel> supplierList = new ArrayList<>();
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_add_product_imgBack);
        imgBack.setOnClickListener(this);

        spinnerProductType = findViewById(R.id.activity_add_product_spinner_product_type);
        spinnerWarehouse = findViewById(R.id.activity_add_product_spinner_warehouse);
        spinnerBarcodeSymbology = findViewById(R.id.activity_add_product_spinner_barcode_symbology);
        spinnerBrand = findViewById(R.id.activity_add_product_spinner_brand);
        spinnerCategory = findViewById(R.id.activity_add_product_spinner_category);
        spinnerSubCategory = findViewById(R.id.activity_add_product_spinner_sub_category);
        spinnerProductUnit = findViewById(R.id.activity_add_product_spinner_product_unit);
        spinnerDefaultSaleUnit = findViewById(R.id.activity_add_product_spinner_default_sale_unit);
        spinnerDefaultPurchaseUnit = findViewById(R.id.activity_add_product_spinner_default_purchase_unit);
        spinnerProductTax = findViewById(R.id.activity_add_product_spinner_product_tax);
        spinnerTaxMethod = findViewById(R.id.activity_add_product_spinner_tax_method);
        spinnerSupplier = findViewById(R.id.activity_add_product_spinner_supplier);

        edtProductName = findViewById(R.id.activity_add_product_edt_name);
        edtProductCode = findViewById(R.id.activity_add_product_edt_code);
        edtSlug = findViewById(R.id.activity_add_product_edt_slug);
        edtSecondaryName = findViewById(R.id.activity_add_product_edt_secondary_name);
        edtWeight = findViewById(R.id.activity_add_product_edt_weight);
        edtProductCost = findViewById(R.id.activity_add_product_edt_product_cost);
        edtProductPrice = findViewById(R.id.activity_add_product_edt_product_price);
        edtBottleCost = findViewById(R.id.activity_add_product_edt_bottle_cost);
        edtHSNCode = findViewById(R.id.activity_add_product_edt_hsn_code);
        edtAlertQuantity = findViewById(R.id.activity_add_product_edt_alert_quantity);
        edtSupplierCost = findViewById(R.id.activity_add_product_edt_supplier_cost);
        edtProductDetails = findViewById(R.id.activity_add_product_edt_product_detail);
        edtProductDetailsForInvoice = findViewById(R.id.activity_add_product_edt_product_detail_invoices);

        Button btnAddProduct = findViewById(R.id.activity_add_product_button_add);
        btnAddProduct.setOnClickListener(this);

        ArrayAdapter adapterBarcode = new ArrayAdapter(this, android.R.layout.simple_spinner_item, barcodeList);
        adapterBarcode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerBarcodeSymbology.setAdapter(adapterBarcode);

        ArrayAdapter adapterProductTax = new ArrayAdapter(this, android.R.layout.simple_spinner_item, taxList);
        adapterProductTax.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerProductTax.setAdapter(adapterProductTax);

        ArrayAdapter adapterTaxMethod = new ArrayAdapter(this, android.R.layout.simple_spinner_item, taxMethodList);
        adapterTaxMethod.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTaxMethod.setAdapter(adapterTaxMethod);

        serviceListOfProductType();
        serviceListOfWarehouses();
        serviceListOfBrands();
        serviceListOfCategory();
        serviceListOfUnit();
        serviceGetSupplier();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_add_product_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_add_product_button_add) {
            addProduct();
        }
    }

    private void serviceListOfProductType() {
        productTypeList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getProductType(Util.API_LIST_PRODUCT_TYPE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            productTypeList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, productTypeList);
                        spinnerProductType.setAdapter(arrayAdapter);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfWarehouses() {
        arrayWarehouseList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Warehouses", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_WAREHOUSE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            arrayWarehouseList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, arrayWarehouseList);
                        spinnerWarehouse.setAdapter(arrayAdapter);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfBrands() {
        brandList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.showBrands(Util.API_LIST_OF_BRANDS, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            brandList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, brandList);
                        spinnerBrand.setAdapter(arrayAdapter);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfCategory() {
        categoryList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Categories", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_CATEGORY, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            categoryList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, categoryList);
                        spinnerCategory.setAdapter(arrayAdapter);

                        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                serviceListOfSubCategory(categoryList.get(position).getRouteId());
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfSubCategory(String categoryId) {
        subCategoryList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Categories", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getSubCategory(Util.API_LIST_OF_SUBCATEGORY, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), categoryId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            subCategoryList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, subCategoryList);
                        spinnerSubCategory.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfUnit() {
        unitList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Categories", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getProductUnit(Util.API_LIST_OF_SUBCATEGORY, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            unitList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> productUnitAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, unitList);
                        spinnerProductUnit.setAdapter(productUnitAdapter);

                        ArrayAdapter<SpinnerModel> saleUnitAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, unitList);
                        spinnerDefaultSaleUnit.setAdapter(saleUnitAdapter);

                        ArrayAdapter<SpinnerModel> purchaseUnitAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, unitList);
                        spinnerDefaultPurchaseUnit.setAdapter(purchaseUnitAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceGetSupplier() {
        supplierList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllSupplier(
                Util.API_LIST_OF_SUPPLIER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("data")) {
                            JSONArray jsonArray = jsonResponse.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject paymentObj = jsonArray.getJSONObject(i);

                                SpinnerModel spinnerModel = new SpinnerModel();
                                spinnerModel.setRouteId(paymentObj.getString("id"));
                                spinnerModel.setRouteName(paymentObj.getString("name"));

                                supplierList.add(spinnerModel);
                            }

                            ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, supplierList);
                            spinnerSupplier.setAdapter(arrayAdapter);

                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void addProduct() {

        String pCost = edtProductCost.getText().toString().trim();
        String pPrice = edtProductPrice.getText().toString().trim();
        String bottleCost = edtBottleCost.getText().toString().trim();
        String alertQuantity = edtAlertQuantity.getText().toString().trim();
        String details = edtProductDetails.getText().toString().trim();
        String supplierCost = edtSupplierCost.getText().toString().trim();
        String pDetailsForInvoice = edtProductDetailsForInvoice.getText().toString().trim();
        String pName = edtProductName.getText().toString().trim();

        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Categories", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.addProduct(
                Util.API_ADD_PRODUCT,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                arrayWarehouseList.get(spinnerWarehouse.getSelectedItemPosition()).getRouteId(),
                "",
                productTypeList.get(spinnerProductType.getSelectedItemPosition()).getRouteId(),
                brandList.get(spinnerBrand.getSelectedItemPosition()).getRouteId(),
                categoryList.get(spinnerCategory.getSelectedItemPosition()).getRouteId(),
                pCost,
                pPrice,
                bottleCost,
                unitList.get(spinnerProductUnit.getSelectedItemPosition()).getRouteId(),
                unitList.get(spinnerDefaultSaleUnit.getSelectedItemPosition()).getRouteId(),
                unitList.get(spinnerDefaultPurchaseUnit.getSelectedItemPosition()).getRouteId(),
                spinnerTaxMethod.getSelectedItem().toString().trim(),
                alertQuantity,
                "",
                pDetailsForInvoice,
                details,
                supplierList.get(spinnerSupplier.getSelectedItemPosition()).getRouteId(),
                supplierCost,
                spinnerBarcodeSymbology.getSelectedItem().toString().trim(),
                pName);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("message")) {
                            onBackPressed();
                            Toast.makeText(AdminAddProductActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                onBackPressed();
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}