package com.jaldhara.view.admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.view.RouteMapActivity;
import com.jaldhara.view.admin.assets.AssetsListActivity;
import com.jaldhara.view.admin.commission.AdminCommissionListActivity;
import com.jaldhara.view.admin.customer.CreateCustomerActivity;
import com.jaldhara.view.admin.expense.ExpensesListActivity;
import com.jaldhara.view.admin.invoice.InvoicesActivity;
import com.jaldhara.view.admin.order.AdminOrdersListActivity;
import com.jaldhara.view.admin.pendingorder.PendingOrdersActivity;
import com.jaldhara.view.admin.products.ProductListActivity;
import com.jaldhara.view.admin.purchase.PurchasesActivity;
import com.jaldhara.view.admin.routes.AddRouteActivity;
import com.jaldhara.view.admin.supplier.AdminSupplierListActivity;

public class AdminDashBoardFragment extends Fragment implements View.OnClickListener {

    private View vLayout;
    private CardView cardAddRoute;
    private CardView cardAddCustomer;
    private CardView cardAdminReport;
    private CardView cardAddAssets;
    private CardView cardAddOrders;

    private CardView cardAssigndelivery;
    private CardView cardTrackRoutes;
    //    private CardView cardAddBilling;
    private CardView cardCheckOrders;
    private CardView cardExpenses;
    private CardView cardPurchase;
    private CardView cardInvoices;
    private CardView cardDriverCommission;

    private CardView cardProductList;
    private CardView cardSupplierList;
    private CardView cardDashboard;
    private ImageView imgDashBoard;
    //    private GroupPermissionModel model;
    private String groupID = "";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        vLayout = inflater.inflate(R.layout.activity_admin_dashboard, container, false);

        String permission = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_group_permission));
        Log.v("permission ", "permission : " + permission);
//        model = new Gson().fromJson(permission, GroupPermissionModel.class);
        groupID = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID));


        cardAddRoute = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_addRoute);
        cardAddRoute.setOnClickListener(this);

        cardAddCustomer = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_Customer);
        cardAddCustomer.setOnClickListener(this);

        cardAdminReport = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_ViewReports);
        cardAdminReport.setOnClickListener(this);

        cardAddAssets = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_addAssets);
        cardAddAssets.setOnClickListener(this);

        cardAddOrders = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_AddOrders);
        cardAddOrders.setOnClickListener(this);

        cardAssigndelivery = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_AssignDelivery);
        cardAssigndelivery.setOnClickListener(this);

        cardTrackRoutes = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_trackRoutes);
        cardTrackRoutes.setOnClickListener(this);

//        cardAddBilling = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_addBilling);
//        cardAddBilling.setOnClickListener(this);

        cardCheckOrders = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_Check_orders);
        cardCheckOrders.setOnClickListener(this);

        cardExpenses = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_expenses);
        cardExpenses.setOnClickListener(this);

        cardPurchase = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_purchase);
        cardPurchase.setOnClickListener(this);

        cardInvoices = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_invoices);
        cardInvoices.setOnClickListener(this);

        cardDriverCommission = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_driverCommision);
        cardDriverCommission.setOnClickListener(this);


        cardProductList = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_products);
        cardProductList.setOnClickListener(this);

        cardSupplierList = (CardView) vLayout.findViewById(R.id.activity_admin_dashboard_Supplier);
        cardSupplierList.setOnClickListener(this);

        imgDashBoard = (ImageView) vLayout.findViewById(R.id.activity_img_dashboard);
        imgDashBoard.setOnClickListener(this);

        ((CardView) vLayout.findViewById(R.id.activity_admin_dashboard_Counters)).setOnClickListener(this);


        return vLayout;
    }


    @Override
    public void onClick(View v) {

//        Log.v("modelvalue ","model :"+model.getGroup_id());

        if (v.getId() == R.id.activity_admin_dashboard_addRoute) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(getActivity(), AddRouteActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_admin_dashboard_Customer) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(getActivity(), CreateCustomerActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_admin_dashboard_ViewReports) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(getActivity(), AdminReportsActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_admin_dashboard_addAssets) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(getActivity(), AssetsListActivity.class);
            startActivity(intent);
//            Intent intent = new Intent(getActivity(), AdminAddAssetsActivity.class);
//            startActivity(intent);

        } else if (v.getId() == R.id.activity_admin_dashboard_AddOrders) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(getActivity(), AdminOrdersListActivity.class);
            intent.putExtra(getString(R.string.key_intent_Driver_ID), true);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_admin_dashboard_AssignDelivery) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(getActivity(), AdminAssignActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_admin_dashboard_trackRoutes) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(getActivity(), RouteMapActivity.class);
            intent.putExtra(getString(R.string.key_intent_Show_On_Map_For), "Admin");
            startActivity(intent);

//

        } else if (v.getId() == R.id.activity_admin_dashboard_Check_orders) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(getActivity(), PendingOrdersActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_admin_dashboard_expenses) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            Intent intent = new Intent(getActivity(), ExpensesListActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_admin_dashboard_purchase) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            startActivity(new Intent(getActivity(), PurchasesActivity.class));

        } else if (v.getId() == R.id.activity_admin_dashboard_invoices) {
            startActivity(new Intent(getActivity(), InvoicesActivity.class));

        } else if (v.getId() == R.id.activity_admin_dashboard_driverCommision) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            startActivity(new Intent(getActivity(), AdminCommissionListActivity.class));
        } else if (v.getId() == R.id.activity_admin_dashboard_products) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            startActivity(new Intent(getActivity(), ProductListActivity.class));
        } else if (v.getId() == R.id.activity_admin_dashboard_Supplier) {
            if (!groupID.equals("1")) {
                Toast.makeText(getActivity(), "Your don't have to permission to access this module", Toast.LENGTH_LONG).show();
                return;
            }
            startActivity(new Intent(getActivity(), AdminSupplierListActivity.class));

        } else if (v.getId() == R.id.activity_img_dashboard || v.getId() == R.id.activity_admin_dashboard_Counters) {
            Intent intent = new Intent(getActivity(), AdminCustomerCountersActivity.class);
            intent.putExtra(getString(R.string.key_intent_comes_from), "admin");
            startActivity(intent);
            getActivity().finish();

        }
    }
}
