package com.jaldhara.view.admin.invoice;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.BwtnDateOrderAdapter;
import com.jaldhara.models.OrderModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BtwnDateOrdersListActivity extends AppCompatActivity implements View.OnClickListener {

    private String invoiceID = "";
    private RecyclerView recyclerView;

    private ImageView imgBack, imgDone;
    private ArrayList<OrderModel> arryOrderList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_payments);


        imgBack = findViewById(R.id.activity_payments_imgBack);
        imgBack.setOnClickListener(this);

        imgDone = findViewById(R.id.activity_payments_imgDone);
        imgDone.setOnClickListener(this);
        imgDone.setVisibility(View.VISIBLE);
        arryOrderList = new ArrayList<>();

        recyclerView = findViewById(R.id.activity_payments_recyclerView);

        try {
            if (ConnectivityReceiver.isConnected()) {
                getBetweenDateOrders();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {


        if (v == imgBack) {
            onBackPressed();
        } else if (v == imgDone) {

            StringBuffer sb = new StringBuffer();
            for (int i = 0; arryOrderList.size() > i; i++) {
                if (!arryOrderList.get(i).isChecked()) continue;
                sb.append(arryOrderList.get(i).getId()).append(",");

                Log.v("selected id ", "is Checked " + arryOrderList.get(i).isChecked());
                Log.v("selected id ", "is id " + arryOrderList.get(i).getId());
                Log.v("selected id ", "is reference no " + arryOrderList.get(i).getReference_no());
            }

            Intent intent = new Intent();
            intent.putExtra("selected_id", sb.toString());
            setResult(RESULT_OK, intent);
            finish();

        }

    }


    /**
     *
     */
    private void getBetweenDateOrders() {


        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getBetweenDatesOrders
                (Util.API_GET_SALES_BY_ID,
                        Util.API_KEY,
                        JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                        getIntent().getStringExtra("customer_id"),
                        getIntent().getStringExtra("to_date"),
                        getIntent().getStringExtra("from_date")
                );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {


                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("sale")) {
                            JSONArray jsonArray = jsonResponse.getJSONArray("sale");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject orderObj = jsonArray.getJSONObject(i);
                                OrderModel orderModel = new OrderModel();
                                orderModel.setId(orderObj.optString("id"));
                                orderModel.setDate(orderObj.optString("date"));
                                orderModel.setReference_no(orderObj.optString("reference_no"));
                                orderModel.setCustomer_id(orderObj.optString("customer_id"));
                                orderModel.setCustomer(orderObj.optString("customer"));
                                orderModel.setBiller_id(orderObj.optString("biller_id"));
                                orderModel.setBiller(orderObj.optString("biller"));
                                orderModel.setGrand_total(orderObj.optString("grand_total"));

                                arryOrderList.add(orderModel);
                            }
//
                            BwtnDateOrderAdapter adapter = new BwtnDateOrderAdapter(BtwnDateOrdersListActivity.this, arryOrderList);
                            recyclerView.setAdapter(adapter);
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }


}