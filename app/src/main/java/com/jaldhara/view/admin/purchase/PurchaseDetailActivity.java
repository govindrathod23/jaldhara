package com.jaldhara.view.admin.purchase;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.jaldhara.R;
import com.jaldhara.models.PurchaseModel;

public class PurchaseDetailActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvTitle,tvReferenceNo,tvDate,tvSupplier,tvPurchaseStatus,tvGrandTotal,tvPaid,tvBalance,tvPaymentStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_detail);

        ImageView imgBack = findViewById(R.id.activity_purchase_detail_activity_imgBack);
        tvTitle = findViewById(R.id.activity_purchase_detail_txtTitle);
        tvReferenceNo = findViewById(R.id.activity_purchase_detail_txt_reference_number);
        tvDate = findViewById(R.id.activity_purchase_detail_txt_date);
        tvSupplier = findViewById(R.id.activity_purchase_detail_txt_supplier);
        tvPurchaseStatus = findViewById(R.id.activity_purchase_detail_txt_purchase_status);
        tvGrandTotal = findViewById(R.id.activity_purchase_detail_txt_grand_total);
        tvPaid = findViewById(R.id.activity_purchase_detail_txt_paid);
        tvBalance = findViewById(R.id.activity_purchase_detail_txt_balance);
        tvPaymentStatus = findViewById(R.id.activity_purchase_detail_txt_payment_status);

        try {
            PurchaseModel purchaseModel = (PurchaseModel) getIntent().getSerializableExtra(getString(R.string.key_intent_purchase_detail));
            if (purchaseModel != null) {
                setPurchaseDetail(purchaseModel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        imgBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_purchase_detail_activity_imgBack) {
            onBackPressed();
        }
    }

    private void setPurchaseDetail(PurchaseModel purchaseModel) {
        tvTitle.setText("Purchase Number "+purchaseModel.getId());
        tvReferenceNo.setText(purchaseModel.getReferenceNo());
        tvDate.setText(purchaseModel.getDate());
        tvSupplier.setText(purchaseModel.getSupplier());
        tvPurchaseStatus.setText(purchaseModel.getStatus());
        tvGrandTotal.setText(purchaseModel.getGrandTotal());
        tvPaid.setText(purchaseModel.getPaid());
        tvPaymentStatus.setText(purchaseModel.getPaymentStatus());
    }
}