package com.jaldhara.view.admin.products;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.ProductListAdminAdapter;
import com.jaldhara.models.ProductModel;
import com.jaldhara.models.SupplierModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.PaginationScrollListener;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ProductListActivity extends AppCompatActivity implements View.OnClickListener {


    private Context context;
    private List<ProductModel> productList = new ArrayList<>();
    private ProductListAdminAdapter mAdapter;
    private RecyclerView recyclerViewProducts;

    private static final int PER_PAGE_COUNT = 20;
    private int PAGE_START = 1;
    private int PAGE_LIMIT = 20;
    private int TOTAL_PAGES = 0;
    private int CURRENT_PAGE = 1;
    private boolean isLastPage = false;
    private TextView lbl_nodelivery;

    private EditText searchText;
    private TextView txtClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_productlist);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_purchase_list_activity_imgBack);
        imgBack.setOnClickListener(this);

        FloatingActionButton btnAdd = findViewById(R.id.activity_purchase_list_button_add_purchase);
        btnAdd.setOnClickListener(this);

        recyclerViewProducts = findViewById(R.id.activity_purchase_list_activity_recyclerview);

        mAdapter = new ProductListAdminAdapter(context, productList);
        recyclerViewProducts.setAdapter(mAdapter);

        lbl_nodelivery = findViewById(R.id.lbl_nodelivery);

        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);
        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);

        recyclerViewProducts.addOnScrollListener(new PaginationScrollListener(new LinearLayoutManager(context)) {
            @Override
            protected void loadMoreItems() {
                CURRENT_PAGE += 1;
                PAGE_START += 20;

                if (ConnectivityReceiver.isConnected()) {
                    serviceListOfProducts();
                } else {
                    Snackbar.make(recyclerViewProducts, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        serviceListOfProducts();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_purchase_list_activity_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_purchase_list_button_add_purchase) {
            startActivity(new Intent(ProductListActivity.this, AdminAddProductActivity.class));
        }
    }


    private void serviceListOfProducts() {
//        productList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllProducts(
                Util.API_GET_PRODUCT,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                PAGE_START,
                PAGE_LIMIT);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            try {
                                int totalRecord = Integer.parseInt(jsonResponse.optString("total"));
                                TOTAL_PAGES = (int) Math.ceil(totalRecord / (float) PER_PAGE_COUNT);
                            } catch (Exception e) {

                            }

                            for (int header = 0; header < jsonArray.length(); header++) {

                                ProductModel productModel = new ProductModel();
                                productModel.setCode(jsonArray.optJSONObject(header).optString("code"));
                                productModel.setId(jsonArray.optJSONObject(header).optString("id"));
                                productModel.setImageUrl(jsonArray.optJSONObject(header).optString("image_url"));
                                productModel.setName(jsonArray.optJSONObject(header).optString("name"));
                                productModel.setNetPrice(jsonArray.optJSONObject(header).optString("net_price"));
                                productModel.setPrice(jsonArray.optJSONObject(header).optString("price"));
                                productModel.setSlug(jsonArray.optJSONObject(header).optString("slug"));
                                productModel.setTaxMethod(jsonArray.optJSONObject(header).optString("tax_method"));
                                productModel.setTaxRate(jsonArray.optJSONObject(header).optString("tax_rate"));
                                productModel.setType(jsonArray.optJSONObject(header).optString("type"));
                                productModel.setUnitPrice(jsonArray.optJSONObject(header).optString("unit_price"));


                                ProductModel.Unit unit = new ProductModel.Unit();
                                if (jsonArray.optJSONObject(header).has("unit") && jsonArray.optJSONObject(header).optJSONObject("unit") != null) {
                                    unit.setId(jsonArray.optJSONObject(header).optJSONObject("unit").optString("id"));
                                    unit.setCompId(jsonArray.optJSONObject(header).optJSONObject("unit").optString("comp_id"));
                                    unit.setCode(jsonArray.optJSONObject(header).optJSONObject("unit").optString("code"));
                                    unit.setName(jsonArray.optJSONObject(header).optJSONObject("unit").optString("name"));
                                    unit.setBaseUnit(jsonArray.optJSONObject(header).optJSONObject("unit").optString("base_unit"));
                                    unit.setOperator(jsonArray.optJSONObject(header).optJSONObject("unit").optString("operator"));
                                    unit.setUnitValue(jsonArray.optJSONObject(header).optJSONObject("unit").optString("unit_value"));
                                    unit.setOperationValue(jsonArray.optJSONObject(header).optJSONObject("unit").optString("operation_value"));
                                }
                                productModel.setUnit(unit);
                                productList.add(productModel);
                            }


                            recyclerViewProducts.setVisibility(View.VISIBLE);
                            lbl_nodelivery.setVisibility(View.GONE);
                            mAdapter.notifyDataSetChanged();

                            if (!(CURRENT_PAGE <= TOTAL_PAGES)) {
                                isLastPage = true;
                            }
                            if (CURRENT_PAGE == TOTAL_PAGES) {
                                isLastPage = true;
                            }


//                        productListAdminAdapter = new ProductListAdminAdapter(context, purchaseList);
//                        recyclerViewProducts.setAdapter(productListAdminAdapter);

                            mAdapter.setOnItemClick(new ProductListAdminAdapter.onItemClick<ProductModel>() {
                                @Override
                                public void onDetailClicked(ProductModel data) {
//                                    Intent intent = new Intent(ProductListActivity.this, PurchaseDetailActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
//                                    startActivity(intent);
                                }

                                @Override
                                public void onViewPaymentClicked(ProductModel data) {
//                                    Intent intent = new Intent(ProductListActivity.this, PaymentsActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
//                                    startActivity(intent);
                                }

                                @Override
                                public void onAddPaymentClicked(ProductModel data) {
//                                    Intent intent = new Intent(ProductListActivity.this, AddPaymentActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
//                                    startActivity(intent);
                                }

                                @Override
                                public void onDeleteClicked(ProductModel data) {
//                                    showAlertDialog(data.getId());
                                }

                                @Override
                                public void onEditPurchaseClicked(ProductModel data) {
//                                    Intent intent = new Intent(ProductListActivity.this, AddPurchaseActivity.class);
//                                    intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
//                                    intent.putExtra(getString(R.string.key_intent_edit_purchase), true);
//                                    startActivityForResult(intent, INTENT_ADD_PURCHASE);
                                }
                            });
                        } else {
                            recyclerViewProducts.setVisibility(View.GONE);
                            lbl_nodelivery.setVisibility(View.VISIBLE);
                        }
                    } else {
                        recyclerViewProducts.setVisibility(View.GONE);
                        lbl_nodelivery.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                recyclerViewProducts.setVisibility(View.GONE);
                lbl_nodelivery.setVisibility(View.VISIBLE);
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    void filter(String text) {
        ArrayList<ProductModel> temp = new ArrayList();

        if (productList != null && productList.size() > 0) {
            for (ProductModel d : productList) {
                if (d.getName().toLowerCase().contains(text) ||
                        d.getCode().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }
            if (temp != null && temp.size() > 0) {
                //update recyclerview
                recyclerViewProducts.setVisibility(View.VISIBLE);
                lbl_nodelivery.setVisibility(View.GONE);
                mAdapter.setRefreshList(temp);
            } else {
                recyclerViewProducts.setVisibility(View.GONE);
                lbl_nodelivery.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }

    private void showAlertDialog(final String purchaseId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Are you sure you want to delete this Product?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        serviceDeletePurchase(purchaseId);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void serviceDeletePurchase(final String purchaseId) {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Removing... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.deletePurchase(Util.API_DELETE_PURCHASE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), purchaseId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                Toast.makeText(ProductListActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
//                                serviceGetPurchases();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }
}