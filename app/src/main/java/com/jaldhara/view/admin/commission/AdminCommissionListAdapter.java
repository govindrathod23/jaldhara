package com.jaldhara.view.admin.commission;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.DriverCommissionModel;
import com.jaldhara.models.ProductModel;

import org.jsoup.internal.StringUtil;

import java.util.List;

public class AdminCommissionListAdapter extends RecyclerView.Adapter<AdminCommissionListAdapter.MyViewHolder> {

    private Context context;
    private List<DriverCommissionModel> productList;
    private AdminCommissionListAdapter.onItemClick<DriverCommissionModel> onItemClick;
    private boolean isFromDriver = false;

    public AdminCommissionListAdapter(Context context, List<DriverCommissionModel> productList, boolean isFrmDriver) {
        this.context = context;
        this.productList = productList;
        this.isFromDriver = isFrmDriver;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_commission_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final DriverCommissionModel commissionModel = productList.get(position);

        holder.btnAction.setVisibility(isFromDriver ? View.INVISIBLE : View.VISIBLE);
        holder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(context, holder.btnAction);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.poupup_menu_raw_commission_action, popup.getMenu());

//                    MenuItem itemEdit = popup.getMenu().findItem(R.id.action_edit);
//                    itemEdit.setTitle("Edit Commission");
//
//                    MenuItem itemdelete = popup.getMenu().findItem(R.id.action_delete);
//                    itemdelete.setVisible(false);

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_delete) {
                            if (onItemClick != null) {
                                onItemClick.onDeleteClicked(commissionModel);
                            }
                        } else if (item.getItemId() == R.id.action_edit) {
                            if (onItemClick != null) {
                                onItemClick.onEditPurchaseClicked(commissionModel);
                            }
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }

        });

        holder.txt_user_name.setText(commissionModel.getFirst_name());
        holder.txt_user_type.setText(commissionModel.getUser_type());
        holder.txt_start_date.setText(commissionModel.getStart_date());
        holder.txt_end_date.setText(commissionModel.getEnd_date());
        holder.txt_grand_total.setText(commissionModel.getGrand_total());
        holder.txt_paid.setText(commissionModel.getPaid());
        holder.txt_payment_status.setText(commissionModel.getPayment_status());

        try {
            int grandTotal = Integer.parseInt(TextUtils.isEmpty(commissionModel.getGrand_total()) ? "0" : commissionModel.getGrand_total());
            int paid = Integer.parseInt(TextUtils.isEmpty(commissionModel.getPaid()) ? "0" : commissionModel.getPaid());
            holder.txt_balance.setText("" + (grandTotal - paid));
        } catch (Exception e) {
        }
    }

    /*Notify Data*/
    public void setRefreshList(List<DriverCommissionModel> productList) {
        this.productList = productList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_user_name, txt_user_type, txt_start_date, txt_end_date, txt_grand_total, txt_paid, txt_balance, txt_payment_status;
        private Button btnAction;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            txt_user_name = itemView.findViewById(R.id.txt_user_name);
            txt_user_type = itemView.findViewById(R.id.txt_user_type);
            txt_start_date = itemView.findViewById(R.id.txt_start_date);
            txt_end_date = itemView.findViewById(R.id.txt_end_date);
            txt_grand_total = itemView.findViewById(R.id.txt_grand_total);
            txt_paid = itemView.findViewById(R.id.txt_paid);
            txt_balance = itemView.findViewById(R.id.txt_balance);
            txt_payment_status = itemView.findViewById(R.id.txt_payment_status);

            btnAction = itemView.findViewById(R.id.raw_purchase_button_action);
        }
    }

    public interface onItemClick<T> {
        void onDetailClicked(T data);

        void onViewPaymentClicked(T data);

        void onAddPaymentClicked(T data);

        void onDeleteClicked(T data);

        void onEditPurchaseClicked(T data);
    }

    public void setOnItemClick(AdminCommissionListAdapter.onItemClick<DriverCommissionModel> onItemClick) {
        this.onItemClick = onItemClick;
    }
}