package com.jaldhara.view.admin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;
import com.jaldhara.view.HomeForRolesActivity;
import com.jaldhara.view.admin.order.AdminOrdersListActivity;
import com.jaldhara.view.customer.order.CustomerOrdersListActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.text.DecimalFormat;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;

public class AdminCustomerCountersActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgHome;

    private TextView txt_user;
    private TextView txt_customer;
    private TextView txt_driver;
    private TextView txt_order;
    private TextView txt_purchaces;
    private TextView txt_todays_payments;
    private TextView txt_deliverys;
    private TextView txt_payments;
    private TextView txt_today_sales;
    private TextView txt_today_pending_deliveries;
    private TextView txt_daily_deliveries;

    private TextView txt_routes;
    private TextView txt_warehouses;
    private TextView txt_products;
    private TextView txt_sales;
    private TextView txt_owner_count;
    private TextView txt_SalesValue;
    private TextView txt_today_return_bottle;
    private TextView txt_total_return_bot;
    private TextView txt_total_pending_empty_bott;
    private TextView txt_total_expenses;
    private TextView txt_today_expenses;
    private TextView txt_total_purchases;
    private TextView txt_total_sales;
    private TextView txt_invoices_amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        if (getIntent().getStringExtra("comes_from").equals("admin")){
            setContentView(R.layout.activity_admin_dashboard_counter);
//        }else  {
//            setContentView(R.layout.activity_customer_dashboard_counter);
//        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorAccent));

//            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN); //show the activity in full screen
        }


        TextView txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_user_name.setText(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserName)));

        imgHome = (ImageView) findViewById(R.id.activity_img_dashboard);
        imgHome.setOnClickListener(this);

        if (getIntent().getStringExtra("comes_from").equals("admin")) {
            txt_customer = (TextView) findViewById(R.id.txt_customer);

            txt_user = (TextView) findViewById(R.id.txt_user);
            txt_driver = (TextView) findViewById(R.id.txt_driver);

            txt_purchaces = (TextView) findViewById(R.id.txt_purchaces);
            txt_todays_payments = (TextView) findViewById(R.id.txt_todays_payments);



            txt_payments = (TextView) findViewById(R.id.txt_payments);
            txt_today_sales = (TextView) findViewById(R.id.txt_today_sale);
            txt_today_pending_deliveries = (TextView) findViewById(R.id.txt_todays_pending_deliverys);
            txt_daily_deliveries = (TextView) findViewById(R.id.txt_daily_deliverys);

            txt_routes = (TextView) findViewById(R.id.txt_routes);
            txt_warehouses = (TextView) findViewById(R.id.txt_warehoues);
            txt_products = (TextView) findViewById(R.id.txt_products);
            txt_sales = (TextView) findViewById(R.id.txt_sales);
            txt_owner_count = (TextView) findViewById(R.id.txt_owner_count);
            txt_SalesValue = (TextView) findViewById(R.id.txt_SalesValue);
            txt_today_return_bottle = (TextView) findViewById(R.id.txt_today_return_bottle);
            txt_total_return_bot = (TextView) findViewById(R.id.txt_total_return_bottle);
            txt_total_pending_empty_bott = (TextView) findViewById(R.id.txt_total_pending_empty_bottle);
            txt_total_expenses = (TextView) findViewById(R.id.txt_total_expenses);
            txt_today_expenses = (TextView) findViewById(R.id.txt_total_expenses);
            txt_total_purchases = (TextView) findViewById(R.id.txt_total_purchases);
            txt_total_sales = (TextView) findViewById(R.id.txt_total_sales);

        }

        txt_order = (TextView) findViewById(R.id.txt_orders);
        txt_deliverys = (TextView) findViewById(R.id.txt_deliverys);
        txt_invoices_amount = (TextView) findViewById(R.id.txt_invoices_amount);

        if (getIntent().getStringExtra("comes_from").equals("admin")) {
            serviceGetAdminCounters();
        }else{
            serviceGetCustomerCounters();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        if (v == imgHome) {

            if (getIntent().getStringExtra("comes_from").equals("admin")){
                Intent intent = new Intent(AdminCustomerCountersActivity.this, HomeForRolesActivity.class);
                intent.putExtra(getString(R.string.key_intent_comes_from), "admin");
                startActivity(intent);
                finish();
            }else if (getIntent().getStringExtra("comes_from").equals("customer")){
                Intent intent = new Intent(AdminCustomerCountersActivity.this, HomeForRolesActivity.class);
                intent.putExtra(getString(R.string.key_intent_comes_from), "customer");
                startActivity(intent);
                finish();
            }


        }else if(v==txt_order){
            if (getIntent().getStringExtra("comes_from").equals("admin")) {
                Intent intent = new Intent(AdminCustomerCountersActivity.this, AdminOrdersListActivity.class);
                startActivity(intent);
//            finish();
            }else{
                Intent intent = new Intent(AdminCustomerCountersActivity.this, CustomerOrdersListActivity.class);
                startActivity(intent);
//            finish();
            }

        }
    }

    private void serviceGetAdminCounters() {

        final ProgressDialog dialog = ProgressDialog.show(AdminCustomerCountersActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.admin_counter(
                Util.API_ADMIN_COUNTER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id))
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.optJSONObject("data");

                        txt_customer.setText(jsonData.optString("customer"));


                        convertValue(jsonData.optString("users"), txt_user);
                        convertValue(jsonData.optString("driver"), txt_driver);
                        convertValue(jsonData.optString("orders"), txt_order);
                        convertValue(jsonData.optString("purchases"), txt_purchaces);

                        convertValue(jsonData.optString("today_payments"), txt_todays_payments);
                        convertValue(jsonData.optString("deliveries"), txt_deliverys);

                        convertValue(jsonData.optString("payments"), txt_payments);
                        convertValue(jsonData.optString("today_sales"), txt_today_sales);

                        convertValue(jsonData.optString("today_pending_deliveries"), txt_today_pending_deliveries);

                        convertValue(jsonData.optString("daily_deliveries"), txt_daily_deliveries);
                        convertValue(jsonData.optString("sales"), txt_sales);
                        convertValue(jsonData.optString("today_return_bottle"), txt_today_return_bottle);
                        convertValue(jsonData.optString("total_return_bottle"), txt_total_return_bot);
                        convertValue(jsonData.optString("total_pending_empty_bottle"), txt_total_pending_empty_bott);

                        convertValue(jsonData.optString("total_purchases"), txt_total_purchases);
                        convertValue(jsonData.optString("total_sales"), txt_total_sales);
                        convertValue(jsonData.optString("invoices_amount"), txt_invoices_amount);


                        convertValue(jsonData.optString("routes"), txt_routes);
                        convertValue(jsonData.optString("warehouses"), txt_warehouses);
                        convertValue(jsonData.optString("products"), txt_products);

                        convertValue(jsonData.optString("owner_count"), txt_owner_count);
                        convertValue(jsonData.optString("SalesValue"), txt_SalesValue);
                        convertValue(jsonData.optString("total_expenses"), txt_total_expenses);

                        convertValue(jsonData.optString("today_expenses"), txt_today_expenses);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void serviceGetCustomerCounters() {

        final ProgressDialog dialog = ProgressDialog.show(AdminCustomerCountersActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.customer_counter(
                Util.API_CUSTOMER_COUNTER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id))
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.optJSONObject("data");

                        //txt_customer.setText(jsonData.optString("customer"));

                        // {"data":{"orders":5,"deliveries":5,"invoices_amount_paid":0,"invoices_amount_pending":0},"status":true}
// convertValue(jsonData.optString("users"), txt_user);
//                        convertValue(jsonData.optString("driver"), txt_driver);
                        convertValue(jsonData.optString("orders"), txt_order);
//                        convertValue(jsonData.optString("purchases"), txt_purchaces);
//
//                        convertValue(jsonData.optString("today_payments"), txt_todays_payments);
                        convertValue(jsonData.optString("deliveries"), txt_deliverys);
//
//                        convertValue(jsonData.optString("payments"), txt_payments);
//                        convertValue(jsonData.optString("today_sales"), txt_today_sales);
//
//                        convertValue(jsonData.optString("today_pending_deliveries"), txt_today_pending_deliveries);
//
//                        convertValue(jsonData.optString("daily_deliveries"), txt_daily_deliveries);
//                        convertValue(jsonData.optString("sales"), txt_sales);
//                        convertValue(jsonData.optString("today_return_bottle"), txt_today_return_bottle);
//                        convertValue(jsonData.optString("total_return_bottle"), txt_total_return_bot);
//                        convertValue(jsonData.optString("total_pending_empty_bottle"), txt_total_pending_empty_bott);
//
//                        convertValue(jsonData.optString("total_purchases"), txt_total_purchases);
//                        convertValue(jsonData.optString("total_sales"), txt_total_sales);
                        convertValue(jsonData.optString("invoices_amount_pending"), txt_invoices_amount);
//
//
//                        convertValue(jsonData.optString("routes"), txt_routes);
//                        convertValue(jsonData.optString("warehouses"), txt_warehouses);
//                        convertValue(jsonData.optString("products"), txt_products);
//
//                        convertValue(jsonData.optString("owner_count"), txt_owner_count);
//                        convertValue(jsonData.optString("SalesValue"), txt_SalesValue);
//                        convertValue(jsonData.optString("total_expenses"), txt_total_expenses);
//
//                        convertValue(jsonData.optString("today_expenses"), txt_today_expenses);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void convertValue(String value, TextView txtView) {

        int text_size_counter = getResources().getDimensionPixelSize(R.dimen.text_size_counter);

        if (value.contains(",")) {
            value = value.replaceAll(",", "");
        }
        if (value.contains(".")) {
            value = value.substring(0, value.lastIndexOf("."));
        }

        if (value.length() <= 3) {
            SpannableString span1 = new SpannableString(value);
            span1.setSpan(new AbsoluteSizeSpan(text_size_counter), 0, value.length(), SPAN_INCLUSIVE_INCLUSIVE);
            txtView.setText(span1);
            return;
        }

        long valL = Long.valueOf(value);
        final String[] units = new String[]{"", "K", "M", "B", "P"};
        int digitGroups = (int) (Math.log10(valL) / Math.log10(1000));

        //------------------------------------------------------------------------------------------------------------

        String digitFormat = new DecimalFormat("#,##0.#").format(valL / Math.pow(1000, digitGroups));
        String unitFormat = units[digitGroups];


        int text_size_counterK = getResources().getDimensionPixelSize(R.dimen.text_size_counterK);

        SpannableString span1 = new SpannableString(digitFormat);
        span1.setSpan(new AbsoluteSizeSpan(text_size_counter), 0, digitFormat.length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString span2 = new SpannableString(unitFormat);
        span2.setSpan(new AbsoluteSizeSpan(text_size_counterK), 0, unitFormat.length(), SPAN_INCLUSIVE_INCLUSIVE);

        // let's put both spans together with a separator and all
        CharSequence finalText = TextUtils.concat(span1, span2);

        txtView.setText(finalText);

    }


}