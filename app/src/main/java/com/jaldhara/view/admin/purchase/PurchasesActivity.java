package com.jaldhara.view.admin.purchase;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.PurchaseListAdminAdapter;
import com.jaldhara.models.ProductModel;
import com.jaldhara.models.PurchaseModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchasesActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int INTENT_ADD_PURCHASE = 101;
    private Context context;
    private List<PurchaseModel> purchaseList = new ArrayList<>();
    private PurchaseListAdminAdapter purchaseAdapter;
    private RecyclerView recyclerViewPurchase;

    private EditText searchText;
    private TextView txtClear;

    private TextView lbl_nodelivery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchaselist);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_purchase_list_activity_imgBack);
        imgBack.setOnClickListener(this);

        FloatingActionButton btnAdd = findViewById(R.id.activity_purchase_list_button_add_purchase);
        btnAdd.setOnClickListener(this);

        recyclerViewPurchase = findViewById(R.id.activity_purchase_list_activity_recyclerview);
        serviceGetPurchases();

        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);
        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);
        lbl_nodelivery = findViewById(R.id.lbl_nodelivery);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_purchase_list_activity_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_purchase_list_button_add_purchase) {
            startActivityForResult(new Intent(PurchasesActivity.this, AddPurchaseActivity.class), INTENT_ADD_PURCHASE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == INTENT_ADD_PURCHASE && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra("reload")) {
                if (data.getBooleanExtra("reload", false)) {
                    serviceGetPurchases();
                }
            }
        }
    }

    private void serviceGetPurchases() {
        purchaseList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getPurchaseList(Util.API_LIST_OF_PURCHASES, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("purchases")) {
                            JSONArray jsonExpenseArray = jsonResponse.getJSONArray("purchases");
                            if (jsonExpenseArray != null && jsonExpenseArray.length() > 0) {
                                for (int i = 0; i < jsonExpenseArray.length(); i++) {
                                    JSONObject jsonObject = jsonExpenseArray.getJSONObject(i);
                                    PurchaseModel purchaseModel = new PurchaseModel();
                                    purchaseModel.setId(jsonObject.getString("id"));
                                    purchaseModel.setCompId(jsonObject.getString("comp_id"));
                                    purchaseModel.setReferenceNo(jsonObject.getString("reference_no"));
                                    purchaseModel.setDate(jsonObject.getString("date"));
                                    purchaseModel.setSupplierId(jsonObject.getString("supplier_id"));
                                    purchaseModel.setSupplier(jsonObject.getString("supplier"));
                                    purchaseModel.setWarehouseId(jsonObject.getString("warehouse_id"));
                                    purchaseModel.setRoute(jsonObject.getString("route"));
                                    purchaseModel.setNote(jsonObject.getString("note"));
                                    purchaseModel.setTotal(jsonObject.getString("total"));
                                    purchaseModel.setProductDiscount(jsonObject.getString("product_discount"));
                                    purchaseModel.setOrderDiscountId(jsonObject.getString("order_discount_id"));
                                    purchaseModel.setOrderDiscount(jsonObject.getString("order_discount"));
                                    purchaseModel.setTotalDiscount(jsonObject.getString("total_discount"));
                                    purchaseModel.setProductTax(jsonObject.getString("product_tax"));
                                    purchaseModel.setOrderTaxId(jsonObject.getString("order_tax_id"));
                                    purchaseModel.setOrderTax(jsonObject.getString("order_tax"));
                                    purchaseModel.setTotalTax(jsonObject.getString("total_tax"));
                                    purchaseModel.setShipping(jsonObject.getString("shipping"));
                                    purchaseModel.setGrandTotal(jsonObject.getString("grand_total"));
                                    purchaseModel.setPaid(jsonObject.getString("paid"));
                                    purchaseModel.setStatus(jsonObject.getString("status"));
                                    purchaseModel.setPaymentStatus(jsonObject.getString("payment_status"));
                                    purchaseModel.setCreatedBy(jsonObject.getString("created_by"));
                                    purchaseModel.setUpdatedBy(jsonObject.getString("updated_by"));
                                    purchaseModel.setUpdatedAt(jsonObject.getString("updated_at"));
                                    purchaseModel.setAttachment(jsonObject.getString("attachment"));
                                    purchaseModel.setPaymentTerm(jsonObject.getString("payment_term"));
                                    purchaseModel.setDueDate(jsonObject.getString("due_date"));
                                    purchaseModel.setReturnId(jsonObject.getString("return_id"));
                                    purchaseModel.setSurcharge(jsonObject.getString("surcharge"));
                                    purchaseModel.setReturnPurchaseRef(jsonObject.getString("return_purchase_ref"));
                                    purchaseModel.setPurchaseId(jsonObject.getString("purchase_id"));
                                    purchaseModel.setReturnPurchaseRef(jsonObject.getString("return_purchase_total"));
                                    purchaseModel.setCgst(jsonObject.getString("cgst"));
                                    purchaseModel.setSgst(jsonObject.getString("sgst"));
                                    purchaseModel.setIgst(jsonObject.getString("igst"));

                                    purchaseList.add(purchaseModel);
                                }

                                recyclerViewPurchase.setVisibility(View.VISIBLE);
                                lbl_nodelivery.setVisibility(View.GONE);
                                purchaseAdapter = new PurchaseListAdminAdapter(context, purchaseList);
                                recyclerViewPurchase.setAdapter(purchaseAdapter);

                                purchaseAdapter.setOnItemClick(new PurchaseListAdminAdapter.onItemClick<PurchaseModel>() {
                                    @Override
                                    public void onDetailClicked(PurchaseModel data) {
                                        Intent intent = new Intent(PurchasesActivity.this, PurchaseDetailActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onViewPaymentClicked(PurchaseModel data) {
                                        Intent intent = new Intent(PurchasesActivity.this, PaymentsActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onAddPaymentClicked(PurchaseModel data) {
                                        Intent intent = new Intent(PurchasesActivity.this, AddPaymentActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onDeleteClicked(PurchaseModel data) {
                                        showAlertDialog(data.getId());
                                    }

                                    @Override
                                    public void onEditPurchaseClicked(PurchaseModel data) {
                                        Intent intent = new Intent(PurchasesActivity.this, AddPurchaseActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
                                        intent.putExtra(getString(R.string.key_intent_edit_purchase), true);
                                        startActivityForResult(intent, INTENT_ADD_PURCHASE);
                                    }
                                });
                            } else {
                                recyclerViewPurchase.setVisibility(View.GONE);
                                lbl_nodelivery.setVisibility(View.VISIBLE);
                            }
                        }
                    } else {
                        recyclerViewPurchase.setVisibility(View.GONE);
                        lbl_nodelivery.setVisibility(View.VISIBLE);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();
                recyclerViewPurchase.setVisibility(View.GONE);
                lbl_nodelivery.setVisibility(View.VISIBLE);

            }
        });
    }

    void filter(String text) {
        ArrayList<PurchaseModel> temp = new ArrayList();

        if (purchaseList != null && purchaseList.size() > 0) {
            for (PurchaseModel d : purchaseList) {
                if (d.getReferenceNo().toLowerCase().contains(text) ||
                        d.getSupplier().toLowerCase().contains(text) ||
                        d.getPaymentStatus().toLowerCase().contains(text) ||
                        d.getStatus().toLowerCase().contains(text) ||
                        d.getDate().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }
            if (temp != null && temp.size() > 0) {
                //update recyclerview
                recyclerViewPurchase.setVisibility(View.VISIBLE);
                lbl_nodelivery.setVisibility(View.GONE);
                purchaseAdapter.setRefreshList(temp);
            } else {
                recyclerViewPurchase.setVisibility(View.GONE);
                lbl_nodelivery.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }


    private void showAlertDialog(final String purchaseId) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Are you sure you want to delete this Purchase?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        serviceDeletePurchase(purchaseId);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void serviceDeletePurchase(final String purchaseId) {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Removing... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.deletePurchase(Util.API_DELETE_PURCHASE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), purchaseId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                Toast.makeText(PurchasesActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                                serviceGetPurchases();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }
}