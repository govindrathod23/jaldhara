package com.jaldhara.view.admin.assets;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.BeepManager;
import com.jaldhara.R;
import com.jaldhara.models.AssetsModel;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import java.util.List;


public class AssetsBarCodeScannerActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;

    private ImageView imgBack;

    private CaptureManager capture;
    private boolean apiCalled = false;
    private DecoratedBarcodeView zxing_barcode_scanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barcode_scanner);

        context = this;
        initComponent(savedInstanceState);

    }

    private void initComponent(Bundle savedInstanceState) {
        imgBack = findViewById(R.id.activity_purchase_list_activity_imgBack);
        imgBack.setOnClickListener(this);

        zxing_barcode_scanner = findViewById(R.id.zxing_barcode_scanner);

        capture = new CaptureManager(this, zxing_barcode_scanner);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        zxing_barcode_scanner.setStatusText("");
        zxing_barcode_scanner.decodeContinuous(new BarcodeCallback() {
            @Override
            public void barcodeResult(BarcodeResult result) {
                if (result.getText() != null && !apiCalled) {
                    BeepManager beepManager = new BeepManager(AssetsBarCodeScannerActivity.this);
                    beepManager.playBeepSoundAndVibrate();

                    if (!apiCalled) {
                        apiCalled = true;
                        Toast.makeText(AssetsBarCodeScannerActivity.this, "result :" + result.toString(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void possibleResultPoints(List<ResultPoint> resultPoints) {

            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_purchase_list_activity_imgBack) {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}