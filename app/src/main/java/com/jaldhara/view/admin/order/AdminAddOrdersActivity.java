package com.jaldhara.view.admin.order;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.AdminAssignActivity;
import com.jaldhara.view.admin.SearchCustomerActivity;
import com.jaldhara.view.admin.adapteradmin.SelectAdminProductExpandAdapter;


import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAddOrdersActivity extends Activity implements View.OnClickListener {


    private static final int REQUEST_CODE_CUSTOMER = 2078;
    private static final int INTENT_PRODUCTS_LIST = 101;

    private ArrayList<SpinnerModel> arryRoutes;
    private ArrayList<SpinnerModel> arryBiller;
    private ArrayList<SpinnerModel> arryDriver;
    private ArrayList<SpinnerModel> arryLoaderList;
    private ArrayList<SpinnerModel> arryOrders;
    private ArrayList<SpinnerModel> arrayWarehouseList = new ArrayList<>();
    private TextView txtShowProductCount;
    private TextView txtDeliveryDate;
    private TextView txtCustomer;
    private Spinner spnWarehouse;
    private Spinner spnRoutes;
    private Spinner spnBillers;
    private Spinner spnSaleStatus;
    private Spinner spnPaymentStatus;
    private Spinner spnDriver;
    private Spinner spnLoader;
    private String customerName = "";
    private String customerId = "";
    private RadioButton rbPeriodic;
    private EditText edtReference;

    private Button btnExpand, btnColspan;
    private RecyclerView recycleSelectProduct;
    private LinearLayoutManager linearLayoutManager;
    private SelectAdminProductExpandAdapter selectAdminProductExpandAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_add_orders);


        spnWarehouse = findViewById(R.id.activity_admin_add_orders_SpnWarehouse);
        spnRoutes = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnRoutes);
        spnBillers = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnBiller);
        spnDriver = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnDriver);
        spnLoader = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnLoader);

        spnSaleStatus = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnSaleStatus);
        spnPaymentStatus = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnPaymentStatus);
        txtCustomer = (TextView) findViewById(R.id.activity_admin_add_orders_SpnCustomer);
        txtShowProductCount = (TextView) findViewById(R.id.txt_showProductCount);
        rbPeriodic = (RadioButton) findViewById(R.id.rb_Periodic);
        txtDeliveryDate = (TextView) findViewById(R.id.activity_admin_add_orders_deliverydate);
        edtReference = (EditText) findViewById(R.id.activity_admin_add_orders_edtReferance);

        txtDeliveryDate.setText(Util.getTodayDate());
        txtDeliveryDate.setOnClickListener(this);

        btnExpand = findViewById(R.id.btnExpand);
        btnColspan = findViewById(R.id.btnColspan);
        recycleSelectProduct = findViewById(R.id.recycleSelectProduct);
        linearLayoutManager = new LinearLayoutManager(AdminAddOrdersActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recycleSelectProduct.setLayoutManager(linearLayoutManager);

        arryOrders = new ArrayList<SpinnerModel>();
        arryRoutes = new ArrayList<SpinnerModel>();
        arryBiller = new ArrayList<SpinnerModel>();
        arryDriver = new ArrayList<SpinnerModel>();
        arryLoaderList = new ArrayList<>();

        selectAdminProductExpandAdapter = new SelectAdminProductExpandAdapter(AdminAddOrdersActivity.this, arryOrders);
        recycleSelectProduct.setAdapter(selectAdminProductExpandAdapter);

        Button btnAdd = (Button) findViewById(R.id.activity_admin_add_orders_btnAdd);
        btnAdd.setOnClickListener(this);

        Button btnCancle = (Button) findViewById(R.id.activity_admin_add_orders_btnCancel);
        btnCancle.setOnClickListener(this);
        ImageView imgBtnBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBtnBack.setOnClickListener(this);

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfRoutes();
            serviceListOfBiller();
            serviceListOfDriver();
            serviceListOfLoader();
            serviceListOfWarehouses();
            serviceGetReferanceNo();
        } else {
            Snackbar.make(getCurrentFocus(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }


    }


    /***************************************************************************************
     * Get Customer -- Get Customer -- Get Customer -- Get Customer
     ***************************************************************************************/
    private void serviceListOfRoutes() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Routes", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

//        Call<ResponseBody> call = apiService.getAllBillers( "person", Util.API_KEY, "biller",1,100);
        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_ROUTES, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryRoutes.add(spinnerModel);

                        }
                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddOrdersActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryRoutes);
                        spnRoutes.setAdapter(arrayAdapter);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void serviceListOfBiller() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Billers", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllBillers("person", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), "biller", 1, 100);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int route = 0; route < jsonArray.length(); route++) {
                            JSONObject jsonCust = jsonArray.optJSONObject(route);
                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonCust.optString("company"));
                            spinnerModel.setRouteId(jsonCust.optString("id"));
                            arryBiller.add(spinnerModel);
                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddOrdersActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryBiller);
                        spnBillers.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void serviceListOfDriver() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Driver data", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfDrivers(
                "list_of_drivers",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("first_name") + " " + jsonArray.optJSONObject(header).optString("last_name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryDriver.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddOrdersActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryDriver);
                        spnDriver.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfLoader() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Loader data", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfLoaders("list_of_loaders", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("first_name") + " " + jsonArray.optJSONObject(header).optString("last_name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryLoaderList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddOrdersActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryLoaderList);
                        spnLoader.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void serviceListOfWarehouses() {
        arrayWarehouseList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Warehouses", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_WAREHOUSE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            arrayWarehouseList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddOrdersActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arrayWarehouseList);
                        spnWarehouse.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    /***************************************************************************************
     *
     *
     **/
    private void serviceAddOrder(String jsonData) {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Adding Assets", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.addOrderNew(
                Util.API_ADD_ORDER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                customerId,

                arryRoutes.get(spnRoutes.getSelectedItemPosition()).getRouteId().toString(),
                txtDeliveryDate.getText().toString(),
                jsonData,
                arrayWarehouseList.get(spnWarehouse.getSelectedItemPosition()).getRouteId().toString(),
                rbPeriodic.isChecked() ? "0" : "1",
                spnSaleStatus.getSelectedItem().toString(),
                arryDriver.get(spnDriver.getSelectedItemPosition()).getRouteId().toString(),
                arryLoaderList.get(spnLoader.getSelectedItemPosition()).getRouteId().toString(),
                arryBiller.get(spnBillers.getSelectedItemPosition()).getRouteId().toString(),
                "admin"
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    if (response.isSuccessful() && jsonResponse.optBoolean("status")) {

//                        {"message":"Assets update sucessfully","status":true}
                        AlertDialog.Builder builder = new AlertDialog.Builder(AdminAddOrdersActivity.this);
                        builder.setTitle("Order added");
                        builder.setMessage("Order added successfully!");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_CUSTOMER) {
            customerName = data.getStringExtra(getString(R.string.key_intent_customer_Name));
            customerId = data.getStringExtra(getString(R.string.key_intent_customer_id));
            txtCustomer.setText(customerName);
//            Snackbar.make(mRecyclerView, "-Customer Name - "+data.getStringExtra(getString(R.string.key_intent_customer_Name)), Snackbar.LENGTH_LONG).show();

        } else if (requestCode == INTENT_PRODUCTS_LIST && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra("products")) {
                Log.e("ADDPurchase", " data " + data.getSerializableExtra("products"));
           /*     ArrayList<ProductModel> selectedProducts = (ArrayList<ProductModel>) data.getSerializableExtra("products");
                selectedProductList.addAll(selectedProducts);*/
                arryOrders.clear();
                arryOrders.addAll(data.getParcelableArrayListExtra("products"));
                txtShowProductCount.setText(arryOrders.size() + " Product(s) selected");
                selectAdminProductExpandAdapter.notifyDataSetChanged();
            }
        }
    }

    public void onClickFilterApply(View v) {
        Intent intent = new Intent(AdminAddOrdersActivity.this, SearchCustomerActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CUSTOMER);
    }

    public void onClickSelectProduct(View v) {
        startActivityForResult(new Intent(AdminAddOrdersActivity.this, ProductsListForOrderActivity.class).putExtra(getString(R.string.key_intent_product_list), arryOrders), INTENT_PRODUCTS_LIST);
    }

    @Override
    public void onClick(View v) {

        int position = 0;
        SpinnerModel spinnerModel;
        int totalQty = 1;

        if (v.getId() == R.id.activity_admin_add_orders_btnAdd) {
            try {

                if (arryOrders.isEmpty()) {
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Please add quantity to add order", Snackbar.LENGTH_LONG).show();
                    Toast.makeText(AdminAddOrdersActivity.this, "Select product to add order", Toast.LENGTH_SHORT).show();


                    return;
                }

                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < arryOrders.size(); i++) {
                    if (!TextUtils.isEmpty(arryOrders.get(i).getQuantity())) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("product_id", arryOrders.get(i).getRouteId());
                        jsonObject.put("quantity", arryOrders.get(i).getQuantity());
                        jsonArray.put(jsonObject);
                    }
                }

                if (txtCustomer.getText().toString().equalsIgnoreCase("Select Customer")) {
                    Toast.makeText(AdminAddOrdersActivity.this, "Please select customer to add order", Toast.LENGTH_SHORT).show();
                    return;
                }


                if (ConnectivityReceiver.isConnected()) {
                    serviceAddOrder(jsonArray.toString());
                } else {
                    Toast.makeText(AdminAddOrdersActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (v.getId() == R.id.activity_admin_add_orders_btnCancel) {
            finish();

        } else if (v.getId() == R.id.activity_bottle_settlement_tab_imgBack) {
            finish();

        } else if (v.getId() == R.id.activity_admin_add_orders_deliverydate) {

            final Calendar calendar = Util.getCalDateFromString(txtDeliveryDate.getText().toString());
            DatePickerDialog datePickerDialog = new DatePickerDialog(AdminAddOrdersActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0, int year, int month, int day) {
                    String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                    String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);

                    txtDeliveryDate.setText(year + "-" + strMonth + "-" + strDay);
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        } else if (v.getId() == R.id.btnExpand) {
            if (arryOrders != null && arryOrders.size() > 0) {
                if (btnExpand.getVisibility() == View.VISIBLE) {
                    btnExpand.setVisibility(View.GONE);
                    btnColspan.setVisibility(View.VISIBLE);
                    recycleSelectProduct.setVisibility(View.VISIBLE);
                    selectAdminProductExpandAdapter.notifyDataSetChanged();
                } else {
                    btnExpand.setVisibility(View.VISIBLE);
                    btnColspan.setVisibility(View.GONE);
                    recycleSelectProduct.setVisibility(View.GONE);
                }
            } else {
                Toast.makeText(AdminAddOrdersActivity.this, "Please select product", Toast.LENGTH_SHORT).show();
            }
        } else if (v.getId() == R.id.btnColspan) {
            btnExpand.setVisibility(View.VISIBLE);
            btnColspan.setVisibility(View.GONE);
            recycleSelectProduct.setVisibility(View.GONE);
        } else if (v.getId() == R.id.ic_minus) {
            position = (int) v.getTag();
            spinnerModel = arryOrders.get(position);
            totalQty = Integer.parseInt(spinnerModel.getQuantity());
            if (Integer.parseInt(spinnerModel.getQuantity()) > 1) {
                totalQty = totalQty - 1;
                spinnerModel.setQuantity(totalQty + "");
                arryOrders.set(position, spinnerModel);
            }
            selectAdminProductExpandAdapter = (SelectAdminProductExpandAdapter) recycleSelectProduct.getAdapter();
            if (selectAdminProductExpandAdapter != null && selectAdminProductExpandAdapter.getItemCount() > 0) {
                selectAdminProductExpandAdapter.notifyAdapter(arryOrders);
            }
        } else if (v.getId() == R.id.ic_plus) {
            position = (int) v.getTag();
            spinnerModel = arryOrders.get(position);
            totalQty = totalQty + Integer.parseInt(spinnerModel.getQuantity());
            spinnerModel.setQuantity(totalQty + "");
            arryOrders.set(position, spinnerModel);
            selectAdminProductExpandAdapter = (SelectAdminProductExpandAdapter) recycleSelectProduct.getAdapter();
            if (selectAdminProductExpandAdapter != null && selectAdminProductExpandAdapter.getItemCount() > 0) {
                selectAdminProductExpandAdapter.notifyAdapter(arryOrders);
            }
        }
    }


    private void serviceGetReferanceNo() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.get_reference_no(
                "get_reference_no",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                "sales");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.optBoolean("status")) {
                            edtReference.setText(jsonResponse.optJSONObject("data").optString("reference_no"));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
