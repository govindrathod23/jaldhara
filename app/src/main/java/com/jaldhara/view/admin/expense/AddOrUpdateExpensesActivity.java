package com.jaldhara.view.admin.expense;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.ExpensesModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.invoice.CreateInvoiceActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddOrUpdateExpensesActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private ImageView imgBack;
    private Spinner spinnerRoute, spinnerWarehouse, spinnerCategory;
    private ArrayList<SpinnerModel> arrayRouteList = new ArrayList<>();
    private ArrayList<SpinnerModel> arrayWarehouseList = new ArrayList<>();
    private ArrayList<SpinnerModel> arrayCategoryList = new ArrayList<>();
    private boolean isCreateExpense = true;
    private TextInputEditText edtReference, edtAmount, edtNote;
    private TextView edtDate;
    private String expenseId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_or_update_expenses);

        context = this;

        imgBack = findViewById(R.id.activity_add_expenses_imgBack);
        spinnerRoute = findViewById(R.id.activity_add_expense_spinner_routes);
        spinnerWarehouse = findViewById(R.id.activity_add_expense_spinner_warehouse);
        spinnerCategory = findViewById(R.id.activity_add_expense_spinner_category);
        edtDate = findViewById(R.id.txtDate);
        edtReference = findViewById(R.id.activity_add_expense_edt_reference);
        edtAmount = findViewById(R.id.activity_add_expense_edt_amount);
        edtNote = findViewById(R.id.activity_add_expense_edt_note);
        TextView tvTitle = findViewById(R.id.activity_add_expense_txtTitle);
        Button btnSave = findViewById(R.id.activity_add_expense_btn_add);

        try {
            if (getIntent().getExtras() != null) {
                String activityType = getIntent().getExtras().getString("type");

                if (activityType.equals("edit")) {

                    tvTitle.setText("Edit Expense");
                    btnSave.setText("Update");

                    isCreateExpense = false;
                    ExpensesModel expenseModel = (ExpensesModel) getIntent().getSerializableExtra("expenseDetail");
                    if (expenseModel != null) {
                        setExpenseDetail(expenseModel);
                        expenseId = expenseModel.getId();

                    }
                } else if (activityType.equals("create")) {
                    isCreateExpense = true;
                }

            }else{

                if (ConnectivityReceiver.isConnected()) {
                    serviceGetReferanceNo();
                } else {
                    Snackbar.make(btnSave, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        serviceListOfRoutes();
        serviceListOfCategory();
        serviceListOfWarehouses();
        imgBack.setOnClickListener(this);
        edtDate.setOnClickListener(this);
        btnSave.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_add_expenses_imgBack) {
            onBackPressed();
        } else if (id == R.id.txtDate) {
            showDatePickerDialog();
        } else if (id == R.id.activity_add_expense_btn_add) {
            if (isCreateExpense) {
                serviceSaveExpense();
            } else {
                serviceUpdateExpense();
            }
        }
    }

    private void setExpenseDetail(ExpensesModel model) {
        edtDate.setText(model.getDate());
        edtReference.setText(model.getReference());
        edtAmount.setText(model.getAmount());
        edtNote.setText(Util.htmlToText(model.getNote()));
    }

    private void showDatePickerDialog() {

        new SingleDateAndTimePickerDialog.Builder(context)
                .bottomSheet()
                .setTimeZone(TimeZone.getDefault())
                .curved()
                .defaultDate(Calendar.getInstance().getTime())
                .mainColor(Color.BLACK)
                .titleTextColor(Color.BLACK)
                .displayAmPm(true)
                .title("Select date and time")
                .minutesStep(5)
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        edtDate.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US).format(date));
                    }
                }).display();
    }

    private void serviceListOfRoutes() {
        arrayRouteList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Routes", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_ROUTES, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            arrayRouteList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, arrayRouteList);
                        spinnerRoute.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfWarehouses() {
        arrayWarehouseList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Warehouses", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_WAREHOUSE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            arrayWarehouseList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, arrayWarehouseList);
                        spinnerWarehouse.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfCategory() {
        arrayCategoryList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Categories", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_CATEGORY, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).getString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).getString("id"));
                            arrayCategoryList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(context, R.layout.raw_spinner, R.id.raw_spinner_txt, arrayCategoryList);
                        spinnerCategory.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceSaveExpense() {

        arrayCategoryList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Saving expense", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String amount = edtAmount.getText().toString().trim();
        String userid = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID));
        String note = edtNote.getText().toString().trim();
        String route = arrayRouteList.get(spinnerCategory.getSelectedItemPosition()).getRouteId();
        String warehouse = arrayWarehouseList.get(spinnerWarehouse.getSelectedItemPosition()).getRouteId();
        String category = arrayCategoryList.get(spinnerCategory.getSelectedItemPosition()).getRouteId();

        Call<ResponseBody> call = apiService.saveExpense(Util.API_SAVE_EXPENSE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), amount, userid, note, category, warehouse, route);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                onBackPressed();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void serviceUpdateExpense() {

        arrayCategoryList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Updating expense", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        String amount = edtAmount.getText().toString().trim();
        String userid = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID));
        String note = edtNote.getText().toString().trim();
        String route = arrayRouteList.get(spinnerCategory.getSelectedItemPosition()).getRouteId();
        String warehouse = arrayWarehouseList.get(spinnerWarehouse.getSelectedItemPosition()).getRouteId();
        String category = arrayCategoryList.get(spinnerCategory.getSelectedItemPosition()).getRouteId();

        Call<ResponseBody> call = apiService.updateExpense(Util.API_UPDATE_EXPENSE, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), expenseId, amount, userid, note, category, warehouse, route);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();
                onBackPressed();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void serviceGetReferanceNo() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.get_reference_no(
                "get_reference_no",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                "expense");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.optBoolean("status")) {
                            edtReference.setText(jsonResponse.optJSONObject("data").optString("reference_no"));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AddOrUpdateExpensesActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AddOrUpdateExpensesActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }




}