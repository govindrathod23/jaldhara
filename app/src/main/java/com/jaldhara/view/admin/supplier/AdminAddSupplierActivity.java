package com.jaldhara.view.admin.supplier;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAddSupplierActivity extends AppCompatActivity implements View.OnClickListener {

    private Spinner spinnerStates;
    private TextInputEditText edtCompany, edtName, edtVATNumber, edtGSTNumber, edtEmail, edtPhone, edtAddress, edtCity, edtPostalCode, edtCountry;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_supplier);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_add_supplier_imgBack);
        imgBack.setOnClickListener(this);

        edtCompany = findViewById(R.id.activity_add_supplier_edt_company);
        edtName = findViewById(R.id.activity_add_supplier_edt_name);
        edtVATNumber = findViewById(R.id.activity_add_supplier_edt_vat_number);
        edtGSTNumber = findViewById(R.id.activity_add_supplier_edt_gst_number);
        edtEmail = findViewById(R.id.activity_add_supplier_edt_email);
        edtPhone = findViewById(R.id.activity_add_supplier_edt_phone);
        edtAddress = findViewById(R.id.activity_add_supplier_edt_address);
        edtCity = findViewById(R.id.activity_add_supplier_edt_city);
        edtPostalCode = findViewById(R.id.activity_add_supplier_edt_postal_code);
        edtCountry = findViewById(R.id.activity_add_supplier_edt_country);

        Button btnAdd = findViewById(R.id.activity_add_supplier_button_add);
        btnAdd.setOnClickListener(this);

        spinnerStates = findViewById(R.id.activity_add_purchase_spinner_state);
        ArrayAdapter adapterTax = new ArrayAdapter(this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.array_states));
        adapterTax.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStates.setAdapter(adapterTax);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_add_supplier_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_add_supplier_button_add) {
            addSupplier();
        }
    }

    private void addSupplier() {
        String company = edtCompany.getText().toString().trim();
        String name = edtName.getText().toString().trim();
        String VATNumber = edtVATNumber.getText().toString().trim();
        String GSTNumber = edtGSTNumber.getText().toString().trim();
        String email = edtEmail.getText().toString().trim();
        String phone = edtPhone.getText().toString().trim();
        String address = edtAddress.getText().toString().trim();
        String city = edtCity.getText().toString().trim();
        String state = spinnerStates.getSelectedItem().toString().trim();
        String postalCode = edtPostalCode.getText().toString().trim();
        String country = edtCountry.getText().toString().trim();

        if (company.isEmpty() || name.isEmpty() || email.isEmpty() || phone.isEmpty() || address.isEmpty() || city.isEmpty()) {
            Toast.makeText(context, "Please fill all the mandatory details.", Toast.LENGTH_SHORT).show();
            return;
        }

        final ProgressDialog dialog = ProgressDialog.show(context, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.addSupplier(Util.API_ADD_SUPPLIER, Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                name, email, company, address, VATNumber, city, state, postalCode, country, phone, GSTNumber);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                Toast.makeText(AdminAddSupplierActivity.this, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

}