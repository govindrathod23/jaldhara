package com.jaldhara.view.admin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.SearchCustomerListAdapter;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.PaginationScrollListener;
import com.jaldhara.utills.Util;
import com.jaldhara.utills.onCustomerClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchProductActivity extends Activity implements onCustomerClick {


    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerViewTxtSearch;
    private SearchCustomerListAdapter mAdapter;
    private ArrayList<SpinnerModel> arrCustomer;
    private ArrayList<SpinnerModel> arrCustomerTxtSearch;

    private String fromText = "";
    private String txtTO = "";
    private onCustomerClick customerClick;
    private EditText edtSearch;

    private TextView txtClear;
    //-----Pagination variables
    private static final int PER_PAGE_COUNT = 20;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int CURRENT_PAGE = 1;
    private int PAGE_START = 1;
    private int PAGE_LIMIT = 20;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_customer);

        mRecyclerView = (RecyclerView) findViewById(R.id.activity_search_cus_recycler_view);
        mRecyclerViewTxtSearch = (RecyclerView) findViewById(R.id.activity_search_cus_recycler_txtSerch);
        edtSearch = (EditText) findViewById(R.id.activity_search_cus_edt_search);
        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfCustomernext();
        } else {
            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        arrCustomer = new ArrayList<SpinnerModel>();
        arrCustomerTxtSearch = new ArrayList<SpinnerModel>();
        LinearLayoutManager  linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        mAdapter = new SearchCustomerListAdapter(SearchProductActivity.this, arrCustomer, SearchProductActivity.this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);


        mRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                CURRENT_PAGE += 1;
                PAGE_START += 20;

                Log.v("addOnScrollListener", "addOnScrollListener*************   " + CURRENT_PAGE);
                Log.v("addOnScrollListener", "Start*************   " + PAGE_START);
                Log.v("addOnScrollListener", "limit*************   " + PAGE_LIMIT);

                if (ConnectivityReceiver.isConnected()) {
                    serviceListOfCustomernext();
                } else {
                    Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (ConnectivityReceiver.isConnected()) {
                    txtClear.setVisibility(View.VISIBLE);
                    if ((!TextUtils.isEmpty(s.toString())) && s.toString().length() >= 3) {
                        serviceListOfSearchCus(s.toString());
                    } else {
                        Snackbar.make(mRecyclerView, "Search with more than 3 letter", Snackbar.LENGTH_LONG).show();
                    }


                } else {
                    Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void serviceListOfCustomernext() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllCustomerPage(
                "person",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                "customer",
                PAGE_START,
                PAGE_LIMIT);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        try {
                            int totalRecord = Integer.parseInt(jsonResponse.optString("total"));
                            TOTAL_PAGES = (int) Math.ceil(totalRecord / (float) PER_PAGE_COUNT);
                        } catch (Exception e) {

                        }


                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int route = 0; route < jsonArray.length(); route++) {
                            JSONObject jsonCust = jsonArray.optJSONObject(route);
                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonCust.optString("company"));
                            spinnerModel.setRouteId(jsonCust.optString("id"));

                            spinnerModel.setAddress(jsonCust.optString("address") + " "+jsonCust.optString("city") );
                            spinnerModel.setLatitude(jsonCust.optString("latitude"));
                            spinnerModel.setLongitude(jsonCust.optString("longitude"));
                            arrCustomer.add(spinnerModel);
                        }

                        mAdapter.notifyDataSetChanged();

                        if (!(CURRENT_PAGE <= TOTAL_PAGES)) {
                            isLastPage = true;
                        }
                        if (CURRENT_PAGE == TOTAL_PAGES) {
                            isLastPage = true;
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(SearchProductActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

                Util.hideKeyboard(SearchProductActivity.this);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(SearchProductActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void serviceListOfSearchCus(String searchText) {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getSearchTxtCustomer(
                "get_filtered_customer",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                searchText);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        arrCustomerTxtSearch.clear();
                        mRecyclerView.setVisibility(View.GONE);
                        mRecyclerViewTxtSearch.setVisibility(View.VISIBLE);
                        JSONArray jsonResponse = new JSONArray(response.body().string());


                        for (int route = 0; route < jsonResponse.length(); route++) {
                            JSONObject jsonCust = jsonResponse.optJSONObject(route);
                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonCust.optString("value"));
                            spinnerModel.setRouteId(jsonCust.optString("id"));
                            arrCustomerTxtSearch.add(spinnerModel);
                        }

                        mAdapter = new SearchCustomerListAdapter(SearchProductActivity.this, arrCustomerTxtSearch, SearchProductActivity.this);

                        mRecyclerViewTxtSearch.setAdapter(mAdapter);

                    }


                } catch (Exception e) {
                    Toast.makeText(SearchProductActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
//                    mRecyclerView.setVisibility(View.VISIBLE);
//                    mRecyclerViewTxtSearch.setVisibility(View.GONE);
//                    arrCustomerTxtSearch.clear();
//                    mAdapter.notifyDataSetChanged();
                }
                Util.hideKeyboard(SearchProductActivity.this);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(SearchProductActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
//                mRecyclerView.setVisibility(View.VISIBLE);
//                mRecyclerViewTxtSearch.setVisibility(View.GONE);
//                arrCustomerTxtSearch.clear();
//                mAdapter.notifyDataSetChanged();
            }
        });
    }


    public void onClickClearSearch(View v) {
        edtSearch.setText("");
        txtClear.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
        mRecyclerViewTxtSearch.setVisibility(View.GONE);
        arrCustomerTxtSearch.clear();
        mAdapter.notifyDataSetChanged();

    }

    public void onClickBackButton(View v) {
        finish();
    }

    @Override
    public void onCustomerRowClick(int position) {

        ArrayList<SpinnerModel> arr =  mRecyclerView.getVisibility() == View.VISIBLE ? arrCustomer : arrCustomerTxtSearch ;
        Intent intent = new Intent();
        intent.putExtra(getString(R.string.key_intent_customer_Name), arr.get(position).getRouteName());
        intent.putExtra(getString(R.string.key_intent_customer_id), arr.get(position).getRouteId());

        intent.putExtra(getString(R.string.key_intent_customer_Address), arr.get(position).getAddress());
        intent.putExtra(getString(R.string.key_intent_customer_Latitude), arr.get(position).getLatitude());
        intent.putExtra(getString(R.string.key_intent_customer_Longitude), arr.get(position).getLongitude());

        setResult(RESULT_OK, intent);
        finish();

    }
}
