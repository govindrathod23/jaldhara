package com.jaldhara.view.admin.purchase;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.ProductSelectionAdapter;
import com.jaldhara.models.ProductModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.PaginationScrollListener;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.products.AdminAddProductActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PER_PAGE_COUNT = 20;
    private int PAGE_START = 1;
    private int PAGE_LIMIT = 20;
    private int TOTAL_PAGES = 0;
    private int CURRENT_PAGE = 1;
    private boolean isLastPage = false;
    private RecyclerView recyclerViewProducts;
    private Context context;
    private ProductSelectionAdapter mAdapter;
    private ArrayList<ProductModel> selectedProductList = new ArrayList<>();
    private ArrayList<ProductModel> productListFromPurchase = new ArrayList<>();
    private ArrayList<ProductModel> productList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_products_imgBack);
        imgBack.setOnClickListener(this);

        FloatingActionButton btnAddProduct = findViewById(R.id.activity_products_button_add_product);
        btnAddProduct.setOnClickListener(this);

        Button btnOrder = findViewById(R.id.activity_products_button_order);
        btnOrder.setOnClickListener(this);

        recyclerViewProducts = findViewById(R.id.activity_products_recyclerview);
        mAdapter = new ProductSelectionAdapter(context, productList);
        recyclerViewProducts.setAdapter(mAdapter);

        try {
            if (getIntent().hasExtra(getString(R.string.key_intent_product_list))) {
//                productListFromPurchase = (ArrayList<ProductModel>) getIntent().getSerializableExtra(getString(R.string.key_intent_product_list));
                productListFromPurchase.addAll((ArrayList<ProductModel>) getIntent().getSerializableExtra(getString(R.string.key_intent_product_list)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        recyclerViewProducts.addOnScrollListener(new PaginationScrollListener(new LinearLayoutManager(context)) {
            @Override
            protected void loadMoreItems() {
                CURRENT_PAGE += 1;
                PAGE_START += 20;

                if (ConnectivityReceiver.isConnected()) {
                    serviceListOfProducts();
                } else {
                    Snackbar.make(recyclerViewProducts, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });

        serviceListOfProducts();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_add_product_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_products_button_add_product) {
            startActivity(new Intent(ProductsActivity.this, AdminAddProductActivity.class));
        } else if (id == R.id.activity_products_button_order) {
            finishWithResult();
        }
    }

    private void serviceListOfProducts() {
        productList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllProducts(
                Util.API_GET_PRODUCT,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                PAGE_START,
                PAGE_LIMIT);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        try {
                            int totalRecord = Integer.parseInt(jsonResponse.optString("total"));
                            TOTAL_PAGES = (int) Math.ceil(totalRecord / (float) PER_PAGE_COUNT);
                        } catch (Exception e) {

                        }

                        for (int header = 0; header < jsonArray.length(); header++) {

                            ProductModel productModel = new ProductModel();
                            productModel.setCode(jsonArray.optJSONObject(header).optString("code"));
                            productModel.setId(jsonArray.optJSONObject(header).optString("id"));
                            productModel.setImageUrl(jsonArray.optJSONObject(header).optString("image_url"));
                            productModel.setName(jsonArray.optJSONObject(header).optString("name"));
                            productModel.setNetPrice(jsonArray.optJSONObject(header).optString("net_price"));
                            productModel.setPrice(jsonArray.optJSONObject(header).optString("price"));
                            productModel.setSlug(jsonArray.optJSONObject(header).optString("slug"));
                            productModel.setTaxMethod(jsonArray.optJSONObject(header).optString("tax_method"));
                            productModel.setTaxRate(jsonArray.optJSONObject(header).optString("tax_rate"));
                            productModel.setType(jsonArray.optJSONObject(header).optString("type"));
                            productModel.setUnitPrice(jsonArray.optJSONObject(header).optString("unit_price"));


                            ProductModel.Unit unit = new ProductModel.Unit();
                            unit.setId(jsonArray.optJSONObject(header).optJSONObject("unit").optString("id"));
                            unit.setCompId(jsonArray.optJSONObject(header).optJSONObject("unit").optString("comp_id"));
                            unit.setCode(jsonArray.optJSONObject(header).optJSONObject("unit").optString("code"));
                            unit.setName(jsonArray.optJSONObject(header).optJSONObject("unit").optString("name"));
                            unit.setBaseUnit(jsonArray.optJSONObject(header).optJSONObject("unit").optString("base_unit"));
                            unit.setOperator(jsonArray.optJSONObject(header).optJSONObject("unit").optString("operator"));
                            unit.setUnitValue(jsonArray.optJSONObject(header).optJSONObject("unit").optString("unit_value"));
                            unit.setOperationValue(jsonArray.optJSONObject(header).optJSONObject("unit").optString("operation_value"));

                            productModel.setUnit(unit);
                            productList.add(productModel);
                        }

                        if (!productListFromPurchase.isEmpty() && productListFromPurchase.size() > 0) {
                            for (int i = 0; i < productListFromPurchase.size(); i++) {
                                for (int j = 0; j < productList.size(); j++) {
                                    if (productList.get(j).getId().equals(productListFromPurchase.get(i).getId())) {
                                        productList.get(j).setQuantity(productListFromPurchase.get(i).getQuantity());
                                        productList.get(j).setReturnQuantity(productListFromPurchase.get(i).getReturnQuantity());
                                    }
                                }
                            }
                        }

                        mAdapter.notifyDataSetChanged();

                        if (!(CURRENT_PAGE <= TOTAL_PAGES)) {
                            isLastPage = true;
                        }
                        if (CURRENT_PAGE == TOTAL_PAGES) {
                            isLastPage = true;
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void finishWithResult() {

        for (ProductModel product : productList) {
            if (product.getQuantity() != null && !TextUtils.isEmpty(product.getQuantity())) {
                if (Integer.parseInt(product.getQuantity()) > 0) {
                    selectedProductList.add(product);
                }
            }
        }

        Intent intent = new Intent();
        intent.putExtra("products", selectedProductList);
        setResult(RESULT_OK, intent);
        finish();
    }
}