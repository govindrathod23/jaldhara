package com.jaldhara.view.admin.pendingorder;


import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.R;
import com.jaldhara.utills.ConnectivityReceiver;

public class FilterActivity extends AppCompatActivity implements View.OnClickListener, ViewPager.OnPageChangeListener {

    private RadioButton btnCustomer;
    private RadioButton btnRouter;
    private RadioButton btnDriver;
    private RadioButton btnStatus;
    private Button btnApply;

    public static String CUSTOMER_ID = "";
    public static String ROUTE_ID = "";
    public static String DRIVER_ID = "";
    public static String SALE_STATUS = "";
    private ViewPager mViewPager;
    private SectionsPagerAdapter mSectionsPagerAdapter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);


        btnCustomer = (RadioButton) findViewById(R.id.activity_filter_btnCustomer);
        btnRouter = (RadioButton) findViewById(R.id.activity_filter_btnRoute);
        btnDriver = (RadioButton) findViewById(R.id.activity_filter_btnDriver);
        btnStatus = (RadioButton) findViewById(R.id.activity_filter_btnStatus);
        btnApply = (Button) findViewById(R.id.activity_filter_btnApply);
        btnCustomer.setOnClickListener(this);
        btnRouter.setOnClickListener(this);
        btnDriver.setOnClickListener(this);
        btnApply.setOnClickListener(this);
        btnStatus.setOnClickListener(this);


        mSectionsPagerAdapter = new FilterActivity.SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.)
        mViewPager = (ViewPager) findViewById(R.id.ll_container);


        if (ConnectivityReceiver.isConnected()) {
            mViewPager.setAdapter(mSectionsPagerAdapter);
            mViewPager.setOffscreenPageLimit(4);
            mViewPager.addOnPageChangeListener(this);
        } else {
            Snackbar.make(getCurrentFocus(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }


    }


    @Override
    public void onClick(View v) {
        if (v == btnCustomer) {
            mViewPager.setCurrentItem(0);
        } else if (v == btnRouter) {
            mViewPager.setCurrentItem(1);
        } else if (v == btnDriver) {
            mViewPager.setCurrentItem(2);
        } else if (v == btnStatus) {
            mViewPager.setCurrentItem(3);
        } else if (v == btnApply) {
//            Toast.makeText(FilterActivity.this,
//                        "Customer : " + CUSTOMER_ID
//                            + " \nROUTE : " + ROUTE_ID
//                            + " \nSTATUS : " + SALE_STATUS
//                            + " \nDRIVER : " + DRIVER_ID
//                            , Toast.LENGTH_LONG).show();

            Intent intent = new Intent();
            intent.putExtra(getString(R.string.key_intent_customer_id), CUSTOMER_ID);
            intent.putExtra(getString(R.string.key_intent_Route_ID), ROUTE_ID);
            intent.putExtra(getString(R.string.key_intent_status), SALE_STATUS);
            intent.putExtra(getString(R.string.key_intent_Driver_ID), DRIVER_ID);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
            btnCustomer.setChecked(true);
        } else if (position == 1) {
            btnRouter.setChecked(true);
        } else if (position == 2) {
            btnDriver.setChecked(true);
        }else if (position == 3) {
            btnStatus.setChecked(true);
        }

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * A {@link  } that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).

            if (position == 0) {
                FilterSearchCustomerFragment filterSearchCustomerFragment = new FilterSearchCustomerFragment();
                return filterSearchCustomerFragment;

            } else if (position == 1) {
                FilterRouteFragment filterRouteFragment = new FilterRouteFragment();
                return filterRouteFragment;

            } else if (position == 2) {
                FilterDriverFragment filterDriverFragment = new FilterDriverFragment();
                return filterDriverFragment;

            }else if (position == 3) {
                FilterDeliveryStatusFragment filterDeliveryStatusFragment = new FilterDeliveryStatusFragment();
                return filterDeliveryStatusFragment;
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }
    }
}
