package com.jaldhara.view.admin.pendingorder;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.SeeExpandOrderAdapter;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public   class PendingOrdersActivity extends AppCompatActivity {
    
    
        //    private RecyclerView mRecyclerView;
    //    private SeeOrderListAdapter mAdapter;
        private ArrayList arryOrders;
        private TextView txtLbl;
        private ArrayList<DeliveryModel> arryDeliveryItems;
        private ArrayList<SpinnerModel> arryCustomer;
        private ArrayList<DeliveryModel> arryBottleReturns;
        //    private Dialog dialogFilter;
    //    private String strDialogFromDate = "";
    //    private String strDialogTodate = "";
        private String customrID = "";
        private String RouteID = "";
        private String Status = "";
        private String DriverID = "";
    
        private TextView txtHeader;
        private static final int REQUEST_CODE_CUSTOMER = 2078;
        private String fromText = "";
        private String txtTO = "";
    
        private SeeExpandOrderAdapter listAdapter;
        private ExpandableListView expListView;
    
    
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_see_orders);
    
    
            txtHeader = (TextView) findViewById(R.id.activity_addRoute_txtTitle);
    //        mRecyclerView = (RecyclerView) findViewById(R.id.activity_see_order_recycler_view);
            txtLbl = (TextView) findViewById(R.id.lbl_nodelivery);
    
            arryCustomer = new ArrayList<SpinnerModel>();
            SpinnerModel spinnerModel = new SpinnerModel();
            spinnerModel.setRouteName("None");
            spinnerModel.setRouteId("0");
            arryCustomer.add(spinnerModel);
    
    
            arryDeliveryItems = new ArrayList<DeliveryModel>();
            arryOrders = new ArrayList<DeliveryModel>();
    //        mAdapter = new SeeOrderListAdapter(SeeOrdersActivity.this, arryOrders);
    //        mRecyclerView.setAdapter(mAdapter);
    
            expListView = (ExpandableListView) findViewById(R.id.activity_see_order_recycler_view);
            ImageView imgBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
    
            if (ConnectivityReceiver.isConnected()) {
    
                //pass From date and to date same for first time.
                serviceGetOrder(Util.getTodayDate(), Util.getTodayDate());
    
    
            } else {
                Snackbar.make(expListView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
            }
            setFromToDate();
        }
    
    
        private void setFromToDate() {
    
    
            Calendar calendar = Util.getCalDateFromString(Util.getTodayDate());
    
            View layoutFrom = (View) findViewById(R.id.ll_Date_from);
            final TextView txtDayFrom = (TextView) layoutFrom.findViewById(R.id.raw_date_day);
            final TextView txtMonthFrom = (TextView) layoutFrom.findViewById(R.id.raw_date_month);
            final TextView txtYearFrom = (TextView) layoutFrom.findViewById(R.id.raw_date_year);
    
            String strMonth = String.valueOf(calendar.get(Calendar.MONTH) + 1).length() == 2 ? String.valueOf(calendar.get(Calendar.MONTH) + 1) : ("0" + (calendar.get(Calendar.MONTH) + 1));
            String strDay = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)).length() == 2 ? String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)) : ("0" + calendar.get(Calendar.DAY_OF_MONTH));
    
    
            txtDayFrom.setText(strDay);
            txtMonthFrom.setText("" + Util.getMonth().get(calendar.get(Calendar.MONTH)));
            txtYearFrom.setText("" + calendar.get(Calendar.YEAR));
    
            fromText = txtTO = txtYearFrom.getText().toString() + "-" + strMonth + "-" + strDay;
    
            View layoutTo = (View) findViewById(R.id.ll_Date_to);
            final TextView txtDayTo = (TextView) layoutTo.findViewById(R.id.raw_date_day);
            final TextView txtMonthTo = (TextView) layoutTo.findViewById(R.id.raw_date_month);
            final TextView txtYearTo = (TextView) layoutTo.findViewById(R.id.raw_date_year);
    
            txtDayTo.setText(strDay);
            txtMonthTo.setText("" + Util.getMonth().get(calendar.get(Calendar.MONTH)));
            txtYearTo.setText("" + calendar.get(Calendar.YEAR));
    
    
            layoutFrom.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(PendingOrdersActivity.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker arg0, int year, int month, int day) {
                            String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                            String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);
    
                            fromText = year + "-" + strMonth + "-" + strDay;
    
                            txtDayFrom.setText(strDay);
                            txtMonthFrom.setText("" + Util.getMonth().get(month));
                            txtYearFrom.setText("" + year);
    
                            serviceGetOrder(fromText, txtTO);
    
                        }
                    }, Integer.valueOf(txtYearFrom.getText().toString()), Util.getMonthFromName().get(txtMonthFrom.getText().toString()), Integer.valueOf(txtDayFrom.getText().toString()));
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.show();
                }
            });
    
            layoutTo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
    
                    DatePickerDialog datePickerDialog = new DatePickerDialog(PendingOrdersActivity.this, new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker arg0, int year, int month, int day) {
    
                            String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                            String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);
    
                            txtTO = year + "-" + strMonth + "-" + strDay;
    
                            txtDayTo.setText(strDay);
                            txtMonthTo.setText("" + Util.getMonth().get(month));
                            txtYearTo.setText("" + year);
    
                            serviceGetOrder(fromText, txtTO);
    
                        }
                    }, Integer.valueOf(txtYearTo.getText().toString()), Util.getMonthFromName().get(txtMonthTo.getText().toString()), Integer.valueOf(txtDayTo.getText().toString()));
                    datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                    datePickerDialog.show();
                }
            });
        }
    
    
        private void serviceGetOrder(String StartDate, String EndDate) {
            final ProgressDialog dialog = ProgressDialog.show(PendingOrdersActivity.this, "", "Loading... ", false, false);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
    
            Call<ResponseBody> call = apiService.SeeOrdersPagination(
                    "get_order",
                    Util.API_KEY,
                    JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                    customrID,
                    "items",
                    Status,
                    RouteID,
                    DriverID,
                    StartDate,
                    EndDate,
                    1,
                    500);
    
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    dialog.dismiss();
    
                    try {
                        if (response.isSuccessful()) {
                            JSONObject jsonResponse = new JSONObject(response.body().string());
    
    
                            JSONArray jsonArrayDelivery = jsonResponse.optJSONArray("data");
                            fillDeliveryItems(jsonArrayDelivery);
    
    //                        JSONArray jsonArrayReturns = jsonResponse.optJSONArray("returns");
    //                        fillReturnItems(jsonArrayReturns);
    
                        }
    
    
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        Toast.makeText(PendingOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
    
    
                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    dialog.dismiss();
    
                    // Log error here since request failed
                    Log.e("", t.toString());
                    Toast.makeText(PendingOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    
        /**
         * @param v
         */
        public void onClickFilterApply(View v) {
    
    
    //        Intent intent = new Intent(SeeOrdersActivity.this, SearchCustomerActivity.class);
            Intent intent = new Intent(PendingOrdersActivity.this, FilterActivity.class);
            startActivityForResult(intent, REQUEST_CODE_CUSTOMER);
    
    
        }
    
        public void onClickRefresh(View v) {
    
            serviceGetOrder(fromText, txtTO);
        }
    
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
    
            if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_CUSTOMER) {
                customrID = data.getStringExtra(getString(R.string.key_intent_customer_id));
                RouteID = data.getStringExtra(getString(R.string.key_intent_Route_ID));
                Status = data.getStringExtra(getString(R.string.key_intent_status));
                DriverID = data.getStringExtra(getString(R.string.key_intent_Driver_ID));
    
                serviceGetOrder(fromText, txtTO);
            }
        }
    
    
        /**
         * set only delivery Fragment data data
         */
        private void fillDeliveryItems(JSONArray jsonArray) {
    
            List<DeliveryModel> listDataHeader = new ArrayList<DeliveryModel>();
            HashMap<DeliveryModel, List<DeliveryModel>> listDataChild = new HashMap<DeliveryModel, List<DeliveryModel>>();
    
            if (jsonArray == null || jsonArray.length() == 0) {
                Toast.makeText(PendingOrdersActivity.this, "No Delivery Items found!", Toast.LENGTH_SHORT).show();
                return;
            }
    
    
            for (int i = 0; i < jsonArray.length(); i++) {
    
                JSONObject jsonObject = jsonArray.optJSONObject(i);
    
                String orderID = jsonObject.optString("id");
                String orderDate = jsonObject.optString("date");
                String customerName = jsonObject.optString("customer");
    
                JSONArray arrayItems = jsonObject.optJSONArray("items");
                ////////////////////////////////////////////////
    
                DeliveryModel deliveryModelHeader = new DeliveryModel();
                deliveryModelHeader.setCustomerName(customerName);
                deliveryModelHeader.setOrderDate(orderDate);
                deliveryModelHeader.setOrderID(orderID);
                deliveryModelHeader.setIs_finish(jsonObject.optString("is_finish"));
                deliveryModelHeader.setSale_reference_no(jsonObject.optString("reference_no"));
                deliveryModelHeader.setBottleCount(arrayItems.length() + "");
                deliveryModelHeader.setSales_id(jsonObject.optString("id"));
                deliveryModelHeader.setOrder_tax_id(jsonObject.optString("order_tax_id"));
                deliveryModelHeader.setWarehouse_id(jsonObject.optString("warehouse_id"));
                deliveryModelHeader.setBiller_id(jsonObject.optString("biller_id"));
                deliveryModelHeader.setSale_status(jsonObject.optString("sale_status"));
    
                if (jsonObject.optString("sale_status").equalsIgnoreCase("delivering")){
                    deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_delivering));
                }else if (jsonObject.optString("sale_status").equalsIgnoreCase("partially")){
                    deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_partially));
                }else if (jsonObject.optString("sale_status").equalsIgnoreCase("over")){
                    deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_over));
                }else if (jsonObject.optString("sale_status").equalsIgnoreCase("completed")){
                    deliveryModelHeader.setSaleStatusColor(ContextCompat.getColor(this, R.color.color_status_completed));
                }
    
    
                listDataHeader.add(deliveryModelHeader);
                ///////////////////////////////////////////////
    
    
                final ArrayList<DeliveryModel> array = new ArrayList<>();
                for (int j = 0; j < arrayItems.length(); j++) {
    
                    DeliveryModel deliveryModel = new DeliveryModel();
    
                    JSONObject jobjItems = arrayItems.optJSONObject(j);
                    deliveryModel.setProduct_code(jobjItems.optString("product_code"));
                    deliveryModel.setProduct_id(jobjItems.optString("product_id"));
                    deliveryModel.setQuantity(jobjItems.optString("quantity"));
                    deliveryModel.setPendingQty(jobjItems.optString("pending_qty"));
                    deliveryModel.setUnit_price(jobjItems.optString("unit_price"));
                    deliveryModel.setProduct_name(jobjItems.optString("product_name"));
    
                    deliveryModel.setCgst(jobjItems.optString("cgst"));
                    deliveryModel.setComment(jobjItems.optString("comment"));
                    deliveryModel.setDiscount(jobjItems.optString("discount"));
                    deliveryModel.setGst(jobjItems.optString("gst"));
                    deliveryModel.setIgst(jobjItems.optString("igst"));
    
                    deliveryModel.setItem_discount(jobjItems.optString("item_discount"));
                    deliveryModel.setItem_tax(jobjItems.optString("item_tax"));
                    deliveryModel.setNet_unit_price(jobjItems.optString("net_unit_price"));
                    deliveryModel.setProduct_type(jobjItems.optString("product_type"));
                    deliveryModel.setProduct_unit_code(jobjItems.optString("product_unit_code"));
    
                    deliveryModel.setProduct_unit_id(jobjItems.optString("product_unit_id"));
                    deliveryModel.setProduct_unit_quantity(jobjItems.optString("product_unit_quantity"));
                    deliveryModel.setSerial_no(jobjItems.optString("serial_no"));
                    deliveryModel.setSgst(jobjItems.optString("sgst"));
                    deliveryModel.setSubtotal(jobjItems.optString("subtotal"));
    
                    deliveryModel.setTax(jobjItems.optString("tax"));
                    deliveryModel.setTax_rate_id(jobjItems.optString("tax_rate_id"));
    
                    try {
    
                        double pendingQty = Double.valueOf(deliveryModel.getPendingQty());
                        int pendingQtyint = ((int) pendingQty);
                        deliveryModel.setPendingQty("" + pendingQtyint);
                        deliveryModel.setDelivered("" + pendingQtyint);
    
                    } catch (NumberFormatException NumExe) {
                        deliveryModel.setPendingQty("0");
                        deliveryModel.setDelivered("0");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    array.add(deliveryModel);
                }
    
                listDataChild.put(listDataHeader.get(i), array);
    
            }
            listAdapter = new SeeExpandOrderAdapter(PendingOrdersActivity.this, listDataHeader, listDataChild);
            expListView.setAdapter(listAdapter);
        }
    
        /**
         * @param jsonArray
         */
    //    private void fillDeliveryItems(JSONArray jsonArray) {
    //
    //        arryDeliveryItems.clear();
    //
    //        for (int i = 0; jsonArray != null && i < jsonArray.length(); i++) {
    //
    //            JSONObject jsonObject = jsonArray.optJSONObject(i);
    //            JSONArray arrayItems = jsonObject.optJSONArray("items");
    //            String  strCname = jsonObject.optString("customer");
    //
    //            for (int j = 0; j < arrayItems.length(); j++) {
    //
    //                DeliveryModel deliveryModel = new DeliveryModel();
    //                JSONObject jobjItems = arrayItems.optJSONObject(j);
    //                deliveryModel.setQuantity(jobjItems.optString("quantity"));
    //                deliveryModel.setProduct_name(jobjItems.optString("product_name"));
    //                deliveryModel.setCustomerName(strCname);
    //
    //                try {
    //
    //                    double  Qty =  Double.valueOf(deliveryModel.getQuantity());
    ////                  deliveryModel.setPendingQty(""+  Qtyint);
    //                    deliveryModel.setDelivered(""+ ((int)Qty));
    //                } catch (NumberFormatException NumExe ){
    ////                    deliveryModel.setPendingQty("0");
    //                    deliveryModel.setDelivered("0");
    //                } catch (Exception e) {
    //                    e.printStackTrace();
    //                }
    //
    //
    //
    //                arryDeliveryItems.add(deliveryModel);
    //            }
    //
    //        }
    //
    //        if (arryDeliveryItems.isEmpty()) {
    //            txtLbl.setVisibility(View.VISIBLE);
    //            mRecyclerView.setVisibility(View.GONE);
    ////            txtHeader.setText("View Order");
    //        } else {
    //
    //            mAdapter = new SeeOrderListAdapter(SeeOrdersActivity.this, arryDeliveryItems);
    //            mRecyclerView.setAdapter(mAdapter);
    //            String hString = TextUtils.isEmpty(customrName)?"All customer`s order" : customrName+"`s Order";
    //            txtHeader.setText(hString);
    //            txtLbl.setVisibility(View.GONE);
    //            mRecyclerView.setVisibility(View.VISIBLE);
    //        }
    //
    //    }
    
    
    }