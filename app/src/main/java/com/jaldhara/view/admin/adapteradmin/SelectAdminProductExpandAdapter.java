package com.jaldhara.view.admin.adapteradmin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;

import java.util.ArrayList;

public class SelectAdminProductExpandAdapter extends RecyclerView.Adapter<SelectAdminProductExpandAdapter.MyViewHolder> {

    private ArrayList<SpinnerModel> productArray;
    private Context mContext;

    public SelectAdminProductExpandAdapter(Context context, ArrayList<SpinnerModel> productArray) {
        this.mContext = context;
        this.productArray = productArray;
    }


    // Create new views (invoked by the layout manager)
    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_admin_expand, parent, false);

        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final SpinnerModel bottleInfoModel = productArray.get(position);

        holder.txtProductName.setText(bottleInfoModel.getRouteName());
        holder.txtQty.setText(bottleInfoModel.getQuantity());
        holder.ic_minus.setTag(position);
        holder.ic_plus.setTag(position);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return productArray.size();
    }

    public void notifyAdapter(ArrayList<SpinnerModel> productArray) {
        this.productArray = productArray;
        notifyDataSetChanged();
    }

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class MyViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private TextView txtProductName;
        private TextView txtQty;
        private AppCompatImageView ic_minus, ic_plus;


        public MyViewHolder(View v) {
            super(v);
            txtProductName = (TextView) v.findViewById(R.id.txtProductName);
            txtQty = (TextView) v.findViewById(R.id.txtQty);
            ic_minus = v.findViewById(R.id.ic_minus);
            ic_plus = v.findViewById(R.id.ic_plus);
        }
    }
}