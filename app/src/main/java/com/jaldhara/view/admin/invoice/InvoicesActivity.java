package com.jaldhara.view.admin.invoice;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.InvoiceListAdminAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.SearchCustomerActivity;
import com.jaldhara.view.admin.order.AdminAddOrdersActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InvoicesActivity extends AppCompatActivity {

    private static final int REQUEST_CODE_CUSTOMER = 2078;
    private RecyclerView mRecyclerView;
    private ArrayList<InvoiceModel> arryInvoiceMainList;
    private ArrayList<InvoiceModel> arrInvoiceSearch;
    private ArrayList<SpinnerModel> arryRoutes;
    private Spinner spnRoutes;
    private InvoiceListAdminAdapter mAdapter;
    private FloatingActionButton btnCreateInvoice;
    private TextView txtCustomer;
    private TextView txt_clear;
    private String customerName = "";
    private String customerId = "";
    private String routeId = "";
    boolean isFirstTime = true;

    private EditText searchText;
    private TextView txtClear;

    private TextView txtNoRecord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoices);

        txtCustomer = (TextView) findViewById(R.id.activity_invoice_SpnCustomer);
        txt_clear = (TextView) findViewById(R.id.txt_clear);


        mRecyclerView = (RecyclerView) findViewById(R.id.activity_invoice_recyclerview);

        arryInvoiceMainList = new ArrayList<InvoiceModel>();
        arrInvoiceSearch = new ArrayList<InvoiceModel>();
        arryRoutes = new ArrayList<SpinnerModel>();
        spnRoutes = (Spinner) findViewById(R.id.activity_admin_invoice_SpnRoutes);


        btnCreateInvoice = findViewById(R.id.activity_invoice_btn_create);

        txtNoRecord = (TextView) findViewById(R.id.txtNoRecord);

        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);
        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);


        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        btnCreateInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(InvoicesActivity.this, CreateInvoiceActivity.class);
                startActivity(intent);
            }
        });

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfInvoice();
            serviceListOfRoutes();
        } else {
            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }


        spnRoutes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (isFirstTime) {
                    isFirstTime = false;
                    return;
                }

                routeId = arryRoutes.get(position).getRouteId();
                Log.e("setOn", "****position: " + arryRoutes.get(position).getRouteName() + routeId);
                filerInvoices();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    void filter(String text) {
        ArrayList<InvoiceModel> temp = new ArrayList();
        if (arryInvoiceMainList != null && arryInvoiceMainList.size() > 0) {
            for (InvoiceModel d : arryInvoiceMainList) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches
                if (d.getInv_reference_no().toLowerCase().contains(text) ||
                        d.getCustomer().toLowerCase().contains(text) ||
                        d.getPayment_status().toLowerCase().contains(text) ||
                        d.getRoute_name().toLowerCase().contains(text) ||
                        d.getCreated_at().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }

            if (temp != null && temp.size() > 0) {
                mRecyclerView.setVisibility(View.VISIBLE);
                txtNoRecord.setVisibility(View.GONE);
                //update recyclerview
                mAdapter.updateList(temp);
            } else {
                mRecyclerView.setVisibility(View.GONE);
                txtNoRecord.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_CUSTOMER) {
            customerName = data.getStringExtra(getString(R.string.key_intent_customer_Name));
            customerId = data.getStringExtra(getString(R.string.key_intent_customer_id));
            txtCustomer.setText(customerName);
            txt_clear.setVisibility(View.VISIBLE);
            filerInvoices();
        }
    }

    private void filerInvoices() {
//        arrInvoiceSearch.clear();

        ArrayList<InvoiceModel> arrInvoiceSearch = new ArrayList<InvoiceModel>();
//        arryInvoiceMainList.stream().allMatch(t -> t.toLowerCase().contains("test"));

        if (spnRoutes.getSelectedItemPosition() != 0 && !txtCustomer.getText().toString().equalsIgnoreCase(getString(R.string.lbl_select_customer))) {
            Log.e("setOn", "****1st: ");
            for (InvoiceModel model : arryInvoiceMainList) {
                if (model.getCustomer_id().equals(customerId) && model.getRoute_id().equals(routeId)) {
                    arrInvoiceSearch.add(model);
                    Log.e("setOn", "****1st: match " + customerId + ":" + routeId);
                }
            }


        } else if (spnRoutes.getSelectedItemPosition() != 0) {
            Log.e("setOn", "****2nd: ");
            for (InvoiceModel model : arryInvoiceMainList) {
                if (model.getRoute_id().equals(routeId)) {
                    arrInvoiceSearch.add(model);
                    Log.e("setOn", "****2: match " + routeId);
                }
            }

        } else if (!txtCustomer.getText().toString().equalsIgnoreCase(getString(R.string.lbl_select_customer))) {
            Log.e("setOn", "****3rd: ");
            for (InvoiceModel model : arryInvoiceMainList) {
                if (model.getCustomer_id().equals(customerId)) {
                    arrInvoiceSearch.add(model);
                    Log.e("setOn", "****3: match " + customerId);
                }
            }
        }

        if (spnRoutes.getSelectedItemPosition() == 0 && txtCustomer.getText().toString().equalsIgnoreCase(getString(R.string.lbl_select_customer))) {
            mAdapter.setList(arryInvoiceMainList);
        } else {
            mAdapter.setList(arrInvoiceSearch);
        }

        if (arryInvoiceMainList.isEmpty() || arrInvoiceSearch.isEmpty()) {
            Toast.makeText(InvoicesActivity.this, "No data found", Toast.LENGTH_LONG).show();
        }

        mAdapter.notifyDataSetChanged();

    }

    public void onClickFilterApply(View v) {
        Intent intent = new Intent(InvoicesActivity.this, SearchCustomerActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CUSTOMER);
    }

    public void onClear(View v) {
        txt_clear.setVisibility(View.GONE);
        txtCustomer.setText(getString(R.string.lbl_select_customer));
        filerInvoices();
    }

    public void onClickBackButton(View v) {
        onBackPressed();
    }


    private void serviceListOfInvoice() {
//        arryInvoice.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.getInvoiceList(
                Util.API_LIST_OF_INVOICE,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                ""
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("invoice")) {
                            JSONArray jsonArrayInvoice = jsonResponse.optJSONArray("invoice");

//                            Gson gson = new Gson();
//                            Type listType = new TypeToken<ArrayList<InvoiceModel>>() {
//                            }.getType();
//
//                            ArrayList<InvoiceModel>   arryInvoice = gson.fromJson(jsonArrayInvoice.toString(), listType);

                            if (jsonArrayInvoice != null && jsonArrayInvoice.length() > 0) {
                                for (int i = 0; i < jsonArrayInvoice.length(); i++) {
                                    JSONObject jobj = jsonArrayInvoice.getJSONObject(i);

                                    InvoiceModel model = new InvoiceModel();
                                    model.setInv_reference_no(jobj.optString("inv_reference_no"));
                                    model.setId(jobj.optString("id"));
                                    model.setComp_id(jobj.optString("comp_id"));
                                    model.setBiller_id(jobj.optString("biller_id"));
                                    model.setBiller(jobj.optString("biller"));
                                    model.setCustomer_id(jobj.optString("customer_id"));
                                    model.setCustomer(jobj.optString("customer"));
                                    model.setTotal_item(jobj.optString("total_item"));
                                    model.setTotal_amount(jobj.optString("total_amount"));
                                    model.setPaid(jobj.optString("paid"));
                                    model.setBalance(jobj.optString("balance"));
                                    model.setTax(jobj.optString("tax"));
                                    model.setPayment_status(jobj.optString("payment_status"));
                                    model.setPaid_by(jobj.optString("paid_by"));
                                    model.setInv_month(jobj.optString("inv_month"));
                                    model.setFrom_date(jobj.optString("from_date"));
                                    model.setTo_date(jobj.optString("to_date"));
                                    model.setNote(jobj.optString("note"));
                                    model.setStaff_note(jobj.optString("staff_note"));
                                    model.setCreated_at(jobj.optString("created_at"));
                                    model.setUpdated_at(jobj.optString("updated_at"));
                                    model.setName(jobj.optString("name"));
                                    model.setRoute_name(jobj.optString("route_name"));
                                    model.setRoute_id(jobj.optString("route_id"));

                                    arryInvoiceMainList.add(model);
                                }

                                mAdapter = new InvoiceListAdminAdapter(InvoicesActivity.this, arryInvoiceMainList);
                                mRecyclerView.setAdapter(mAdapter);
                                mRecyclerView.setVisibility(View.VISIBLE);
                                txtNoRecord.setVisibility(View.GONE);

                                mAdapter.setOnItemClick(new InvoiceListAdminAdapter.onItemClick<InvoiceModel>() {
                                    @Override
                                    public void onDetailClicked(InvoiceModel data) {
                                        Intent intent = new Intent(InvoicesActivity.this, ViewInvoiceListActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_purchase_detail), data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onAddPaymentClicked(InvoiceModel data) {
                                        Intent intent = new Intent(InvoicesActivity.this, AddInvoicePaymentActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onDeleteClicked(final InvoiceModel data) {


                                        new AlertDialog.Builder(InvoicesActivity.this)
                                                .setTitle("Delete Invoice")
                                                .setMessage("Are you sure you want to delete this invoice?")
                                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if (ConnectivityReceiver.isConnected()) {
                                                            serviceDeleteInvoice(data.getId());
                                                        } else {
                                                            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                                                        }
                                                    }
                                                })

                                                .setNegativeButton(android.R.string.no, null)
                                                .setIcon(android.R.drawable.ic_dialog_alert)
                                                .show();

                                    }

                                    @Override
                                    public void onEditClicked(InvoiceModel data) {
                                        Intent intent = new Intent(InvoicesActivity.this, CreateInvoiceActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onViewPaymentsClicked(InvoiceModel data) {
                                        Intent intent = new Intent(InvoicesActivity.this, ViewPaymentsActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onEmailInvoice(InvoiceModel data) {

                                        Intent intent = new Intent(Intent.ACTION_SEND);
                                        intent.setType("plain/text");
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "Your Invoice");
                                        intent.putExtra(Intent.EXTRA_TEXT, "invoice text HTML ma banava padse body mate");
                                        startActivity(Intent.createChooser(intent, "Mail invoice"));

                                    }
                                });
                            } else {
                                mRecyclerView.setVisibility(View.GONE);
                                txtNoRecord.setVisibility(View.VISIBLE);
                            }
                        } else {
                            mRecyclerView.setVisibility(View.GONE);
                            txtNoRecord.setVisibility(View.VISIBLE);
                        }
                    } else {
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoRecord.setVisibility(View.VISIBLE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();
                mRecyclerView.setVisibility(View.GONE);
                txtNoRecord.setVisibility(View.VISIBLE);
            }
        });
    }

    private void serviceDeleteInvoice(String invoiceID) {

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.delete_invoice(
                Util.API_DELETE_INVOICE,
                Util.API_KEY,
                invoiceID
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {
                    if (response.isSuccessful()) {
//                        {"message":"add purchases payment","status":true}
                        final String strResponse = response.body().string();
                        JSONObject jsonResponse = new JSONObject(strResponse);

                        Toast.makeText(InvoicesActivity.this, jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                        if (ConnectivityReceiver.isConnected()) {
                            serviceListOfInvoice();
                        } else {
                            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void serviceListOfRoutes() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Routes", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_ROUTES, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));

        SpinnerModel spinnerModel = new SpinnerModel();
        spinnerModel.setRouteName("Select Route");
        spinnerModel.setRouteId("-1");
        arryRoutes.add(spinnerModel);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryRoutes.add(spinnerModel);

                        }
                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(InvoicesActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryRoutes);
                        spnRoutes.setAdapter(arrayAdapter);


                    }


                } catch (Exception e) {
                    Toast.makeText(InvoicesActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(InvoicesActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}