package com.jaldhara.view.admin.customer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.RouteMapActivity;
import com.jaldhara.view.admin.SearchCustomerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateCustomerActivity extends Activity {


    private Spinner spnRoute;
    private TextInputEditText edtCustomerName;
    private TextInputEditText edtCustomerCompany;
    private TextInputEditText edtCustomerMobile;
    private TextInputEditText edtCustomerEmail;
    private TextInputEditText edtCustomerLat;
    private TextInputEditText edtCustomerLong;
    private EditText edtCustomerAddress;
    private ArrayList<SpinnerModel> arryRouteList;
    private ImageView btnEdit;
    private Button btnAddCustomer;
    private TextInputLayout edtCustomerNameInput;
    private TextInputLayout edtCustomerCompanyInput;
    private TextInputLayout edtCustomerMobileInput;
    private TextInputLayout edtCustomerEmailInput;
    private TextInputLayout edtCustomerAddressInput;
    private TextInputLayout edtCustomerLatInput;
    private TextInputLayout edtCustomerLongInput;
    private TextView lblRoute;
    private TextView txtActivityHeader;
    private String customerName;
    private String customerID;
    private static final int REQUEST_CODE_CUSTOMER = 2078;
    private static final int REQUEST_CODE_LOCATION = 1478;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createcustomer);


        spnRoute = (Spinner) findViewById(R.id.activity_createcustomer_spn_Routes);
        edtCustomerName = (TextInputEditText) findViewById(R.id.activity_createcustomer_edt_CustomerName);
        edtCustomerCompany = (TextInputEditText) findViewById(R.id.activity_createcustomer_edt_CustomerCompany);
        edtCustomerMobile = (TextInputEditText) findViewById(R.id.activity_createcustomer_edt_CustomerMobile);
        edtCustomerEmail = (TextInputEditText) findViewById(R.id.activity_createcustomer_edt_CustomerEmail);
        edtCustomerAddress = (EditText) findViewById(R.id.activity_createcustomer_edt_CustomerAddress);
        edtCustomerLat = (TextInputEditText) findViewById(R.id.activity_createcustomer_edt_CustomerLatitude);
        edtCustomerLong = (TextInputEditText) findViewById(R.id.activity_createcustomer_edt_CustomerLongitude);
        arryRouteList = new ArrayList<SpinnerModel>();
        btnEdit = (ImageView) findViewById(R.id.activity_addRoute_btnEdit);
        btnAddCustomer = (Button) findViewById(R.id.activity_createcustomer_btnAdd);
        lblRoute = (TextView) findViewById(R.id.lbl_route);
        txtActivityHeader = (TextView) findViewById(R.id.activity_addRoute_txtTitle);


        edtCustomerNameInput = (TextInputLayout) findViewById(R.id.activity_createcustomer_edtinput_CustomerName);
        edtCustomerCompanyInput = (TextInputLayout) findViewById(R.id.activity_createcustomer_edtinput_CustomerCompany);
        edtCustomerMobileInput = (TextInputLayout) findViewById(R.id.activity_createcustomer_edtinput_CustomerMobile);
        edtCustomerEmailInput = (TextInputLayout) findViewById(R.id.activity_createcustomer_edtinput_CustomerEmail);
        edtCustomerAddressInput = (TextInputLayout) findViewById(R.id.activity_createcustomer_edtinput_CustomerAddress);
        edtCustomerLatInput = (TextInputLayout) findViewById(R.id.activity_createcustomer_edtinput_CustomerLatitude);
        edtCustomerLongInput = (TextInputLayout) findViewById(R.id.activity_createcustomer_edtinput_CustomerLongitude);


        if (ConnectivityReceiver.isConnected()) {
            serviceListOfRoutes();
        } else {
            Snackbar.make(spnRoute, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

    }

    public void onClickCreateCustomer(View v) {

        if (v.getId() == R.id.activity_createcustomer_btnCancel || v.getId() == R.id.activity_bottle_settlement_tab_imgBack) {
            finish();

        } else if (btnAddCustomer.getText().toString().equalsIgnoreCase("Add")) {

            if (isValidationSuccess()) {
                if (ConnectivityReceiver.isConnected()) {
                    serviceAddCustomer();
                } else {
                    Snackbar.make(spnRoute, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }

        } else if (btnAddCustomer.getText().toString().equalsIgnoreCase("Edit")) {

            if (TextUtils.isEmpty(edtCustomerName.getText().toString().trim())) {
                edtCustomerName.setFocusable(true);
                edtCustomerNameInput.setError("should not be blank");
                return;

            } else if (TextUtils.isEmpty(edtCustomerAddress.getText().toString().trim())) {
                edtCustomerAddress.setFocusable(true);
                edtCustomerAddressInput.setError("should not be blank");
                return;

            } else if (TextUtils.isEmpty(edtCustomerLat.getText().toString().trim())) {
                edtCustomerLat.setFocusable(true);
                edtCustomerLatInput.setError("should not be blank");
                return;

            } else if (TextUtils.isEmpty(edtCustomerLong.getText().toString().trim())) {
                edtCustomerLong.setFocusable(true);
                edtCustomerLongInput.setError("should not be blank");
                return;
            }

            if (ConnectivityReceiver.isConnected()) {
                serviceUpdtCusLoc();
            } else {
                Snackbar.make(spnRoute, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
            }


        }
    }


    public void onClickEditCustomer(View v) {

        if (btnAddCustomer.getText().toString().equalsIgnoreCase("Add")) {

            edtCustomerCompanyInput.setVisibility(View.GONE);
            edtCustomerMobileInput.setVisibility(View.GONE);
            edtCustomerEmailInput.setVisibility(View.GONE);
            spnRoute.setVisibility(View.GONE);
            btnAddCustomer.setText("Edit");
            txtActivityHeader.setText("Edit Customer");
            Intent intent = new Intent(CreateCustomerActivity.this, SearchCustomerActivity.class);
            startActivityForResult(intent, REQUEST_CODE_CUSTOMER);
        } else {
            edtCustomerCompanyInput.setVisibility(View.VISIBLE);
            edtCustomerMobileInput.setVisibility(View.VISIBLE);
            edtCustomerEmailInput.setVisibility(View.VISIBLE);
            lblRoute.setVisibility(View.VISIBLE);
            spnRoute.setVisibility(View.VISIBLE);
            btnAddCustomer.setText("Add");
            txtActivityHeader.setText("Add Customer");
        }

    }

    private boolean isValidationSuccess() {


        edtCustomerNameInput.setError(null);
        edtCustomerMobileInput.setError(null);
        edtCustomerEmailInput.setError(null);
        edtCustomerAddressInput.setError(null);
        edtCustomerLatInput.setError(null);
        edtCustomerLongInput.setError(null);

        if (TextUtils.isEmpty(edtCustomerName.getText().toString().trim())) {
            edtCustomerName.setFocusable(true);
            edtCustomerNameInput.setError("should not be blank");
            return false;

        } else if (TextUtils.isEmpty(edtCustomerCompany.getText().toString().trim())) {
            edtCustomerCompany.setFocusable(true);
            edtCustomerCompanyInput.setError("should not be blank");
            return false;

        } else if (TextUtils.isEmpty(edtCustomerMobile.getText().toString().trim())) {
            edtCustomerMobile.setFocusable(true);
            edtCustomerMobileInput.setError("should not be blank");
            return false;

        } else if (TextUtils.isEmpty(edtCustomerEmail.getText().toString().trim())) {
            edtCustomerEmail.setFocusable(true);
            edtCustomerEmailInput.setError("should not be blank");
            return false;

        } else if (TextUtils.isEmpty(edtCustomerAddress.getText().toString().trim())) {
            edtCustomerAddress.setFocusable(true);
            edtCustomerAddressInput.setError("should not be blank");
            return false;

        } else if (TextUtils.isEmpty(edtCustomerLat.getText().toString().trim())) {
            edtCustomerLat.setFocusable(true);
            edtCustomerLatInput.setError("should not be blank");
            return false;

        } else if (TextUtils.isEmpty(edtCustomerLong.getText().toString().trim())) {
            edtCustomerLong.setFocusable(true);
            edtCustomerLongInput.setError("should not be blank");
            return false;

        }

        return true;
    }

    /**
     * @param v
     */
    public void onLatLongClick(View v) {
        Intent intent = new Intent(CreateCustomerActivity.this, RouteMapActivity.class);
        intent.putExtra(getString(R.string.key_intent_Show_On_Map_For), "getLatLong");
        startActivityForResult(intent, REQUEST_CODE_LOCATION);
    }

    /**
     *
     */
    private void serviceListOfRoutes() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Routes", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_ROUTES, Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryRouteList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(CreateCustomerActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryRouteList);
                        spnRoute.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CreateCustomerActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CreateCustomerActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceAddCustomer() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Adding Customer", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.addCustomer(
                "add_customer",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                edtCustomerName.getText().toString(),
                edtCustomerEmail.getText().toString(),
                edtCustomerCompany.getText().toString(),
                edtCustomerAddress.getText().toString(),
                arryRouteList.get(spnRoute.getSelectedItemPosition()).getRouteId(),
                edtCustomerLat.getText().toString(),
                edtCustomerLong.getText().toString(),
                edtCustomerMobile.getText().toString());


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        Toast.makeText(CreateCustomerActivity.this, "Customer Created Successfully!", Toast.LENGTH_SHORT).show();
                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CreateCustomerActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CreateCustomerActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_LOCATION) {
            edtCustomerLat.setText(data.getStringExtra("Lat"));
            edtCustomerLong.setText(data.getStringExtra("Long"));

        } else if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_CUSTOMER) {
            customerName = data.getStringExtra(getString(R.string.key_intent_customer_Name));
            customerID = data.getStringExtra(getString(R.string.key_intent_customer_id));

            edtCustomerName.setText(customerName);
            edtCustomerAddress.setText(data.getStringExtra(getString(R.string.key_intent_customer_Address)));

            edtCustomerLat.setText(data.getStringExtra(getString(R.string.key_intent_customer_Latitude)));
            edtCustomerLong.setText(data.getStringExtra(getString(R.string.key_intent_customer_Longitude)));

        }
    }

    /*****************************************************************************************************
     *
     *****************************************************************************************************
     */

    private void serviceUpdtCusLoc() {
        final ProgressDialog dialog = ProgressDialog.show(CreateCustomerActivity.this, "", "Updating... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.updateCusLoc(
                "update_customer_data",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                customerID,
                edtCustomerAddress.getText().toString(),
                edtCustomerLat.getText().toString(),
                edtCustomerLong.getText().toString());

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.optString("status").equalsIgnoreCase("true")) {
//                            showAlertdialog("Update", bottleInfoModel.getCompany() + "`s location updated successfully.");
                            Toast.makeText(CreateCustomerActivity.this,
                                    edtCustomerName.getText().toString() + "`s location updated successfully.",
                                    Toast.LENGTH_LONG).show();
                            finish();

                        }
                    }
                } catch (Exception e) {
                    Toast.makeText(CreateCustomerActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CreateCustomerActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
