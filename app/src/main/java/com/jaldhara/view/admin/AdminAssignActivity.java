package com.jaldhara.view.admin;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AdminAssignActivity extends Activity {

    private Spinner spnRoute;
    private Spinner spnDriver;
    private Spinner spnLoader;
    private ArrayList<SpinnerModel> arryRouteList;
    private ArrayList<SpinnerModel> arryDriverList;
    private ArrayList<SpinnerModel> arryLoaderList;
    private TextInputEditText edtDriverCommission;
    private TextInputEditText edtLoaderCommission;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_assign_delivery);


        spnRoute = (Spinner) findViewById(R.id.activity_assignDelivery_spn_Routes);
        spnDriver = (Spinner) findViewById(R.id.activity_assignDelivery_spn_Driver);
        spnLoader = (Spinner) findViewById(R.id.activity_assignDelivery_spn_Loader);

        edtDriverCommission = (TextInputEditText) findViewById(R.id.activity_assignDelivery_edt_DriverCommission);
        edtLoaderCommission = (TextInputEditText) findViewById(R.id.activity_assignDelivery_edt_LoaderCommission);


        arryRouteList = new ArrayList<>();
        arryDriverList = new ArrayList<>();
        arryLoaderList = new ArrayList<>();

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfRoutes();
            serviceListOfDriver();
            serviceListOfLoader();
        } else {
            Snackbar.make(spnRoute, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }


    }


    public void onClickAssign(View v) {

        if (v.getId() == R.id.activity_bottle_settlement_tab_imgBack || v.getId() == R.id.activity_assignDelivery_btnCancel) {

            finish();

        } else if (v.getId() == R.id.activity_assignDelivery_btnAdd) {

            if (ConnectivityReceiver.isConnected()) {
                serviceAssignDelivery();
            } else {
                Snackbar.make(v, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
            }
        }

    }


    private void serviceListOfRoutes() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Routes", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_ROUTES, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryRouteList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAssignActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryRouteList);
                        spnRoute.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAssignActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAssignActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfDriver() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Driver data", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfDrivers("list_of_drivers", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("first_name") + " " + jsonArray.optJSONObject(header).optString("last_name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryDriverList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAssignActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryDriverList);
                        spnDriver.setAdapter(arrayAdapter);

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAssignActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAssignActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfLoader() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Loader data", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfLoaders("list_of_loaders", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("first_name") + " " + jsonArray.optJSONObject(header).optString("last_name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryLoaderList.add(spinnerModel);

                        }

                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAssignActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryLoaderList);
                        spnLoader.setAdapter(arrayAdapter);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAssignActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAssignActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceAssignDelivery() {
        final ProgressDialog dialog = ProgressDialog.show(this, "", "Please wait", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.assignDelivery(
                "assign_driver_loader_on_route",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                arryRouteList.get(spnRoute.getSelectedItemPosition()).getRouteId(),
                arryDriverList.get(spnDriver.getSelectedItemPosition()).getRouteId(),
                arryLoaderList.get(spnLoader.getSelectedItemPosition()).getRouteId(),
                edtDriverCommission.getText().toString().trim(),
                edtLoaderCommission.getText().toString().trim()
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");


                        AlertDialog.Builder builder = new AlertDialog.Builder(AdminAssignActivity.this);
                        builder.setTitle("Assign delivery");
                        builder.setMessage("Assign delivery successfully!");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });

                        builder.show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AdminAssignActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AdminAssignActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
