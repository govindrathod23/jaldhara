package com.jaldhara.view.admin.purchase;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.PaymentModel;
import com.jaldhara.models.PurchaseModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private Context context;
    private LinearLayout linearDate, linearCreditCard, linearChequeNumber;
    private TextView txtDate;
    private EditText edtReference, edtAmount, edtCreditCardNumber, edtHolderName, edtMonth, edtYear, edtCVV, edtChequeNumber, edtNote;
    private Spinner spinnerPaymentMethod, spinnerCreditCard;
    private String[] paymentMethodList = {"Cash", "Gift Card", "Credit Card", "Cheque", "Other"};
    private String[] creditCardList = {"Visa", "Master Card", "Amex", "Discover"};
    private String purchaseId = "", paymentId = "";
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment);

        context = this;
        ImageView imgBack = findViewById(R.id.activity_add_payment_imgBack);
        Button btnAddPayment = findViewById(R.id.activity_add_payment_button_add);
        linearDate = findViewById(R.id.activity_add_payment_linear_date);
        txtDate = findViewById(R.id.activity_add_payment_tv_date);
        edtReference = findViewById(R.id.activity_add_payment_tv_reference);
        edtAmount = findViewById(R.id.activity_add_payment_edt_amount);
        spinnerPaymentMethod = findViewById(R.id.activity_add_payment_spinner_paying_by);
        linearCreditCard = findViewById(R.id.activity_add_payment_linear_credit_card);
        edtCreditCardNumber = findViewById(R.id.activity_add_payment_tv_credit_card_number);
        edtHolderName = findViewById(R.id.activity_add_payment_tv_holder_name);
        spinnerCreditCard = findViewById(R.id.activity_add_payment_spinner_credit_card);
        edtMonth = findViewById(R.id.activity_add_payment_edt_month);
        edtYear = findViewById(R.id.activity_add_payment_edt_year);
        edtCVV = findViewById(R.id.activity_add_payment_edt_cvv);
        linearChequeNumber = findViewById(R.id.activity_add_payment_linear_cheque_no);
        edtChequeNumber = findViewById(R.id.activity_add_payment_edt_cheque_no);
        edtNote = findViewById(R.id.activity_add_payment_edt_note);

        try {
            PurchaseModel purchaseModel = (PurchaseModel) getIntent().getSerializableExtra(getString(R.string.key_intent_purchase_detail));
            if (purchaseModel != null) {
                purchaseId = purchaseModel.getId();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        setSpinners();

        try {
            if (getIntent().hasExtra(getString(R.string.key_intent_payment_detail))) {
                PaymentModel paymentModel = (PaymentModel) getIntent().getSerializableExtra(getString(R.string.key_intent_payment_detail));
                btnAddPayment.setText("Update Payment");
                isEdit = true;
                if (paymentModel != null) {
                    setPaymentData(paymentModel);
                }
            }else{
                serviceGetReferanceNo();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        imgBack.setOnClickListener(this);
        txtDate.setOnClickListener(this);
        btnAddPayment.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_add_payment_imgBack) {
            onBackPressed();
        } else if (id == R.id.activity_add_payment_tv_date) {
            showDatePickerDialog();
        } else if (id == R.id.activity_add_payment_button_add) {
            checkValidation();
        }
    }

    private void setSpinners() {
        ArrayAdapter paymentAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, paymentMethodList);
        paymentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPaymentMethod.setAdapter(paymentAdapter);

        spinnerPaymentMethod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 2) {
                    linearCreditCard.setVisibility(View.VISIBLE);
                    linearChequeNumber.setVisibility(View.GONE);
                } else if (position == 3) {
                    linearCreditCard.setVisibility(View.GONE);
                    linearChequeNumber.setVisibility(View.VISIBLE);
                } else {
                    linearCreditCard.setVisibility(View.GONE);
                    linearChequeNumber.setVisibility(View.GONE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter creditCardAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, creditCardList);
        creditCardAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerCreditCard.setAdapter(creditCardAdapter);

    }

    private void showDatePickerDialog() {

        new SingleDateAndTimePickerDialog.Builder(context)
                .bottomSheet()
                .setTimeZone(TimeZone.getDefault())
                .curved()
                .defaultDate(Calendar.getInstance().getTime())
                .mainColor(Color.BLACK)
                .titleTextColor(Color.BLACK)
                .displayAmPm(true)
                .title("Select date and time")
                .minutesStep(1)
                .listener(new SingleDateAndTimePickerDialog.Listener() {
                    @Override
                    public void onDateSelected(Date date) {
                        txtDate.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.US).format(date));
                    }
                }).display();
    }

    private void checkValidation() {
        String date = txtDate.getText().toString().trim();
        String amount = edtAmount.getText().toString().trim();

        if (date.isEmpty()) {
            Toast.makeText(context, "Please select date.", Toast.LENGTH_SHORT).show();
        } else if (amount.isEmpty()) {
            Toast.makeText(context, "Please enter amount.", Toast.LENGTH_SHORT).show();
        } else {
            if (isEdit) {
                serviceEditPayment();
            } else {
                serviceAddPayment();
            }
        }
    }

    private void serviceAddPayment() {

        String date = txtDate.getText().toString().trim();
        String reference = edtReference.getText().toString().trim();
        String amount = edtAmount.getText().toString().trim();
        String payingBy = spinnerPaymentMethod.getSelectedItem().toString().trim();
        String creditCardNumber = edtCreditCardNumber.getText().toString().trim();
        String holderName = edtHolderName.getText().toString().trim();
        String creditCardName = spinnerCreditCard.getSelectedItem().toString().trim();
        String month = edtMonth.getText().toString().trim();
        String year = edtYear.getText().toString().trim();
        String cvv = edtCVV.getText().toString().trim();
        String checkNo = edtChequeNumber.getText().toString().trim();
        String note = edtNote.getText().toString().trim();

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.addPayment(Util.API_ADD_PAYMENT, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), amount, payingBy, purchaseId, reference, checkNo, cvv, holderName, month, year, creditCardName, note, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID)), "");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                Toast.makeText(context, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                                onBackPressed();
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void serviceEditPayment() {

        String date = txtDate.getText().toString().trim();
        String reference = edtReference.getText().toString().trim();
        String amount = edtAmount.getText().toString().trim();
        String payingBy = spinnerPaymentMethod.getSelectedItem().toString().trim();
        String creditCardNumber = edtCreditCardNumber.getText().toString().trim();
        String holderName = edtHolderName.getText().toString().trim();
        String creditCardName = spinnerCreditCard.getSelectedItem().toString().trim();
        String month = edtMonth.getText().toString().trim();
        String year = edtYear.getText().toString().trim();
        String cvv = edtCVV.getText().toString().trim();
        String checkNo = edtChequeNumber.getText().toString().trim();
        String note = edtNote.getText().toString().trim();

        final ProgressDialog dialog = ProgressDialog.show(this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.editPayment(Util.API_EDIT_PAYMENT, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), amount, payingBy, purchaseId, reference, checkNo, cvv, holderName, month, year, creditCardName, note, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID)), "", paymentId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.has("message")) {
                            Toast.makeText(context, jsonResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                        if (jsonResponse.has("status")) {
                            if (jsonResponse.getBoolean("status")) {
                                Intent intent = new Intent();
                                intent.putExtra("reload", true);
                                setResult(RESULT_OK, intent);
                                finish();
                            }
                        }

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void setPaymentData(PaymentModel paymentData) {

        try {
            paymentId = paymentData.getId();

            txtDate.setText(paymentData.getDate());
            edtReference.setText(paymentData.getReferenceNo());
            edtAmount.setText(paymentData.getAmount());
            edtNote.setText(paymentData.getNote());
            edtCreditCardNumber.setText(paymentData.getCcNo());
            edtHolderName.setText(paymentData.getCcHolder());
            edtMonth.setText(paymentData.getCcMonth());
            edtYear.setText(paymentData.getCcYear());

            if (paymentData.getCcType() != null) {

                int index = 0;
                for (int i = 0; i < creditCardList.length; i++) {
                    if (creditCardList[i].equals(paymentData.getCcType())) {
                        index = i;
                        break;
                    }
                }

                spinnerCreditCard.setSelection(index);
            }

            if (paymentData.getPaidBy() != null) {

                int index = 0;
                for (int i = 0; i < paymentMethodList.length; i++) {
                    if (paymentMethodList[i].equals(paymentData.getPaidBy())) {
                        index = i;
                        break;
                    }
                }

                spinnerPaymentMethod.setSelection(index);
            }

            edtChequeNumber.setText(paymentData.getChequeNo());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void serviceGetReferanceNo() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.get_reference_no(
                "get_reference_no",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                "payment");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.optBoolean("status")) {
                            edtReference.setText(jsonResponse.optJSONObject("data").optString("reference_no"));
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(AddPaymentActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(AddPaymentActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}