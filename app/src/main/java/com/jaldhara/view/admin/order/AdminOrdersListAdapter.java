package com.jaldhara.view.admin.order;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.jaldhara.R;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.view.customer.order.CustomerOrdersListActivity;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AdminOrdersListAdapter extends RecyclerView.Adapter<AdminOrdersListAdapter.MyViewHolder> {

    private Context context;
    private List<DeliveryModel> mainList;
    //    private List<DeliveryModel> searchList;
    private AdminOrdersListAdapter.onItemClick<DeliveryModel> onItemClick;
//    private UserFilter userFilter;
//    private ItemFilter mFilter = new ItemFilter();

    public AdminOrdersListAdapter(Context context, List<DeliveryModel> productList) {
        this.context = context;
        this.mainList = productList;
//        this.searchList = new ArrayList<>();

    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_orders_list, parent, false);
        MyViewHolder vh = new MyViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {

        final DeliveryModel productModel = mainList.get(position);


        holder.btnAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(context, holder.btnAction);
                //Inflating the Popup using xml file
                popup.getMenuInflater().inflate(R.menu.poupup_menu_raw_order_action, popup.getMenu());

                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_delete) {
                            if (onItemClick != null) {
                                onItemClick.onDeleteClicked(productModel);
                            }
                        } else if (item.getItemId() == R.id.action_edit) {
                            if (onItemClick != null) {
                                onItemClick.onEditPurchaseClicked(productModel);
                            }
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }

        });

        if (productModel.getSale_reference_no() != null && productModel.getSale_reference_no().length() > 0) {
            holder.txt_reference_no.setText(productModel.getSale_reference_no());
            holder.txt_reference_no.setVisibility(View.VISIBLE);
        } else {
            holder.txt_reference_no.setVisibility(View.GONE);
        }


        if (productModel.getOrderDate() != null && productModel.getOrderDate().length() > 0) {
            holder.txt_date.setText(productModel.getOrderDate());
            holder.txt_date.setVisibility(View.VISIBLE);
        } else {
            holder.txt_date.setVisibility(View.GONE);
        }

        holder.txt_Biller.setText(productModel.getBillerName());


        if (productModel.getCustomerName() != null && productModel.getCustomerName().length() > 0) {
            holder.txt_customer.setText(productModel.getCustomerName());
            holder.txt_customer.setVisibility(View.VISIBLE);
        } else {
            holder.txt_customer.setVisibility(View.GONE);
        }

        if (productModel.getSale_status() != null && productModel.getSale_status().length() > 0) {
            holder.txt_sale_status.setText(productModel.getSale_status());
            holder.txt_sale_status.setVisibility(View.VISIBLE);
        } else {
            holder.txt_sale_status.setVisibility(View.GONE);
        }


        holder.txt_grand_total.setText(productModel.getGrandTotal());
        holder.txt_total_amount.setText(productModel.getInvoices_total_amount());

        if (productModel.getInv_reference_no() != null && productModel.getInv_reference_no().length() > 0) {
            holder.txt_inv_ref_no.setText(productModel.getInv_reference_no());
            holder.txt_inv_ref_no.setVisibility(View.VISIBLE);
        } else {
            holder.txt_inv_ref_no.setVisibility(View.GONE);
        }

        if (productModel.getInvoices_payment_status() != null && productModel.getInvoices_payment_status().length() > 0) {
            holder.txt_inv_payment_status.setText(productModel.getInvoices_payment_status());
            holder.txt_inv_payment_status.setVisibility(View.VISIBLE);
        } else {
            holder.txt_inv_payment_status.setVisibility(View.GONE);
        }

        holder.vStatusColor.setBackgroundColor(productModel.getSaleStatusColor());

    }

    /*Notify Data*/
    public void setRefreshList(List<DeliveryModel> notificationlist) {
        this.mainList = notificationlist;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txt_reference_no, txt_date, txt_Biller, txt_customer, txt_grand_total,
                txt_total_amount, txt_sale_status, txt_inv_ref_no, txt_inv_payment_status;
        private Button btnAction;
        private View vStatusColor;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);


            txt_reference_no = itemView.findViewById(R.id.txt_reference_no);
            txt_date = itemView.findViewById(R.id.txt_date);
            txt_Biller = itemView.findViewById(R.id.txt_Biller);

            txt_customer = itemView.findViewById(R.id.txt_customer);
            txt_sale_status = itemView.findViewById(R.id.txt_sale_status);
            txt_grand_total = itemView.findViewById(R.id.txt_grand_total);

            txt_total_amount = itemView.findViewById(R.id.txt_total_amount);
            txt_inv_ref_no = itemView.findViewById(R.id.txt_inv_ref_no);
            txt_inv_payment_status = itemView.findViewById(R.id.txt_inv_payment_status);
            vStatusColor = itemView.findViewById(R.id.view_status_color);


            btnAction = itemView.findViewById(R.id.raw_purchase_button_action);
        }
    }

    public interface onItemClick<T> {
        void onDetailClicked(T data);

        void onViewPaymentClicked(T data);

        void onAddPaymentClicked(T data);

        void onDeleteClicked(T data);

        void onEditPurchaseClicked(T data);
    }

    public void setOnItemClick(AdminOrdersListAdapter.onItemClick<DeliveryModel> onItemClick) {
        this.onItemClick = onItemClick;
    }


    public void updateList(List<DeliveryModel> list) {
        mainList = list;
        notifyDataSetChanged();
    }


}