package com.jaldhara.view.admin;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.CustomerFromRouteAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.customer.CreateCustomerActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerFromRouteActivity extends Activity {


    private RecyclerView mRecyclerView;
    private CustomerFromRouteAdapter customerFromRouteAdapter;
    private ArrayList<BottleInfoModel> arrayListCustomerList;
    private TextView txtHeader;
    private TextView txtLbl;
    private Button btnAddCustomer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_from_route);

        mRecyclerView = (RecyclerView) findViewById(R.id.activity_customer_from_route_recycleView);
        txtHeader = (TextView) findViewById(R.id.activity_addRoute_txtTitle);
        txtLbl = (TextView) findViewById(R.id.lbl_noroute);
        btnAddCustomer = (Button) findViewById(R.id.btn_add_customer);

        if (getIntent().hasExtra(getString(R.string.key_intent_Route_Name))) {
            txtHeader.setText("Customer(s) of " + getIntent().getStringExtra(getString(R.string.key_intent_Route_Name)));
            txtLbl.setText("No customer found on this route!");
            btnAddCustomer.setText("Add customer on route");
        } else {
            txtHeader.setText("Customer(s) having '" + getIntent().getStringExtra(getString(R.string.key_intent_AssetsName)) + "'");
            txtLbl.setText("This assets is not found with any customer!");
            btnAddCustomer.setText("Add assets for customer");
        }

        ImageView imgBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfCustomer();
        } else {
            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }


        arrayListCustomerList = new ArrayList<>();
        customerFromRouteAdapter = new CustomerFromRouteAdapter(CustomerFromRouteActivity.this, arrayListCustomerList);
        mRecyclerView.setAdapter(customerFromRouteAdapter);


    }

    public void onCustomerAdd(View v) {
        if (getIntent().hasExtra(getString(R.string.key_intent_Route_Name))) {
            Intent intent = new Intent(CustomerFromRouteActivity.this, CreateCustomerActivity.class);
            intent.putExtra(getString(R.string.key_intent_Route_ID), getIntent().getStringExtra(getString(R.string.key_intent_Route_ID)));
            startActivity(intent);
        } else {
            Intent intent = new Intent(CustomerFromRouteActivity.this, AdminAddAssetsActivity.class);
//            intent.putExtra(getString(R.string.key_intent_Route_ID), getIntent().getStringExtra(getString(R.string.key_intent_Route_ID)));
            startActivity(intent);
        }
    }

    /**
     *
     */

    private void serviceListOfCustomer() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call;
        if (getIntent().hasExtra(getString(R.string.key_intent_Route_Name))) {
            call = apiService.listOfCustomersFromRoute("routes_wise_customer_by_routeID", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), getIntent().getStringExtra(getString(R.string.key_intent_Route_ID)));
        } else {
            call = apiService.listOfCustomersFromAssets("get_customer_by_assetID", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), getIntent().getStringExtra(getString(R.string.key_intent_AssetsID)));
        }


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data").optJSONObject(0).optJSONArray("customer");

                        arrayListCustomerList.clear();

                        if (jsonArray != null && jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                BottleInfoModel modelCutomer = new BottleInfoModel();

                                modelCutomer.setCustomerID(jsonObject.optString("id"));
                                modelCutomer.setCustomerName(jsonObject.optString("name"));
                                modelCutomer.setCompany(jsonObject.optString("company"));
                                modelCutomer.setAddress(
                                        jsonObject.optString("address") + ", " +
                                                jsonObject.optString("city") + ", " +
                                                jsonObject.optString("state") + ", " +
                                                jsonObject.optString("country") + ", " +
                                                jsonObject.optString("postal_code")
                                );
                                modelCutomer.setLatitude(jsonObject.optString("latitude"));
                                modelCutomer.setLongitude(jsonObject.optString("longitude"));
                                modelCutomer.setPhone(jsonObject.optString("phone"));
                                modelCutomer.setEmail(jsonObject.optString("email"));

                                arrayListCustomerList.add(modelCutomer);

                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CustomerFromRouteActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }


                if (arrayListCustomerList.isEmpty()) {

                    txtLbl.setVisibility(View.VISIBLE);
                    btnAddCustomer.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);
                } else {
                    customerFromRouteAdapter = new CustomerFromRouteAdapter(CustomerFromRouteActivity.this, arrayListCustomerList);
                    mRecyclerView.setAdapter(customerFromRouteAdapter);
                }


            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CustomerFromRouteActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();

                txtLbl.setVisibility(View.VISIBLE);
                btnAddCustomer.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);

            }
        });
    }

}
