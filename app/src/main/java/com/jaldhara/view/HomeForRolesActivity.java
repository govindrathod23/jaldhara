package com.jaldhara.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.AdminDashBoardFragment;
import com.jaldhara.view.admin.AdminCustomerCountersActivity;
import com.jaldhara.view.admin.commission.AdminCommissionListActivity;
import com.jaldhara.view.customer.CustomerBoardFragment;
import com.jaldhara.view.customer.CustomerCountersActivity;
import com.jaldhara.view.deliveryboy.DeliveryRouteListFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeForRolesActivity extends BestLocationProvider implements NavigationView.OnNavigationItemSelectedListener {

    private AppCompatImageView imgMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark, this.getTheme()));
        }

        imgMenu = (AppCompatImageView) findViewById(R.id.appbar_main_imgmenu);

        JalDharaApplication.getInstance().getFirebaseCrashlytics().log("HomeForRolesActivity");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        TextView txtUserName = (TextView) navigationView.getHeaderView(0).findViewById(R.id.slide_bar_txt_userName);
        TextView txtUserRole = (TextView) navigationView.getHeaderView(0).findViewById(R.id.slide_bar_txt_userRole);
        txtUserName.setText(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserName)).toUpperCase());

        TextView txtVersion = (TextView) navigationView.getHeaderView(0).findViewById(R.id.slide_bar_txt_version);
        txtVersion.setText("Version: " + Util.getAppVersionName(HomeForRolesActivity.this));

        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true);

        //This menu is only for driver role so its showing and workin only in drive login.

        if (ConnectivityReceiver.isConnected()) {
            serviceGetPermmission();
            serviceIsLatestVersion();

            //throw new RuntimeException("Test Crash"); // Force a crash

        } else {
            Toast.makeText(HomeForRolesActivity.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
        }


        FragmentManager fragmentManager = getSupportFragmentManager();
        String userGroup = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID));

        if (JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID)).equalsIgnoreCase("5")) {
            showMenuForDrivers();
            imgMenu.setVisibility(View.VISIBLE);
            DeliveryRouteListFragment deliveryListFragment = new DeliveryRouteListFragment();
            fragmentManager.beginTransaction().replace(R.id.container_fragment, deliveryListFragment).commit();
            txtUserRole.setText("Role : Delivery Man");

        } else if (JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID)).equalsIgnoreCase("3")) {

            txtUserRole.setText("Role : Customer");

            if (getIntent() != null && getIntent().hasExtra(getString(R.string.key_intent_comes_from)) && getIntent().getStringExtra(getString(R.string.key_intent_comes_from)).equalsIgnoreCase("customer")) {
                CustomerBoardFragment customerBoardFragment = new CustomerBoardFragment();
                fragmentManager.beginTransaction().replace(R.id.container_fragment, customerBoardFragment).commit();
            } else {
                Intent intent = new Intent(HomeForRolesActivity.this, CustomerCountersActivity.class);
                intent.putExtra(getString(R.string.key_intent_comes_from), "customer");
                startActivity(intent);
                finish();
            }
        } else {
            //Admin Login
            txtUserRole.setText(userGroup.equals("1") ? "Role : Admin" : "Role : Payment Collector");
            if (getIntent() != null
                    && getIntent().hasExtra(getString(R.string.key_intent_comes_from))
                    && getIntent().getStringExtra(getString(R.string.key_intent_comes_from)).equalsIgnoreCase("admin")) {

                AdminDashBoardFragment adminDashBoardFragment = new AdminDashBoardFragment();
                getSupportFragmentManager().beginTransaction().replace(R.id.container_fragment, adminDashBoardFragment).commit();
            } else {
                Intent intent = new Intent(HomeForRolesActivity.this, AdminCustomerCountersActivity.class);
                intent.putExtra(getString(R.string.key_intent_comes_from), "admin");
                startActivity(intent);
                finish();
            }
        }
    }

    private void showMenuForDrivers() {
        imgMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popup = new PopupMenu(HomeForRolesActivity.this, imgMenu);
                popup.getMenuInflater().inflate(R.menu.menu_driver, popup.getMenu());
                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getItemId() == R.id.action_update_attandance) {
                            OpenDialogForAttandance();
                        } else if (item.getItemId() == R.id.action_my_commission) {
                            Intent intent = new Intent(HomeForRolesActivity.this, AdminCommissionListActivity.class);
                            intent.putExtra(getString(R.string.key_intent_is_from_driver), true);
                            startActivity(intent);
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
    }

    private void OpenDialogForAttandance() {

        final Dialog dialog = new Dialog(HomeForRolesActivity.this);
        dialog.setContentView(R.layout.dialog_attendance);
        dialog.findViewById(R.id.btn_inTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.v("comp_lat",""+JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_latitude)));
//                Log.v("comp_long",""+JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_longitude)));
//                Log.v("lat",""+mCurrentLocation.getLatitude());
//                Log.v("long",""+mCurrentLocation.getLongitude());

                double distance = Util.getDistanceFromLatLonInMeter(
                        mCurrentLocation.getLatitude(),
                        mCurrentLocation.getLongitude(),
                        Double.valueOf(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_latitude))),
                        Double.valueOf(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_longitude)))
                );


                if (distance > 300) {
                    Util.showDialog(HomeForRolesActivity.this, "Distance not match", "Your distance should be around 300 meter to your company when attendance in or out");
                } else {
                    serviceUpdtAttedance("0");
                }

                Log.v("time: ", "" + Util.getCurrentDateWithTime());

                dialog.dismiss();
            }
        });
        dialog.findViewById(R.id.btn_outTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                double distance = Util.getDistanceFromLatLonInMeter(
                        mCurrentLocation.getLatitude(),
                        mCurrentLocation.getLongitude(),
                        Double.valueOf(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_latitude))),
                        Double.valueOf(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_longitude)))
                );


                if (distance > 300) {
                    Util.showDialog(HomeForRolesActivity.this, "Distance not match", "Your distance should be around 300 meter to your company when attendance in or out");
                } else {
                    serviceUpdtAttedance("1");
                }
                dialog.dismiss();
            }
        });
        dialog.show();


    }

    private void serviceUpdtAttedance(String attendanceType) {
        final ProgressDialog dialog = ProgressDialog.show(HomeForRolesActivity.this, "", "Updating... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.add_attendance(
                "add_attendance",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                attendanceType,
                Util.getCurrentDateWithTime(),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserID)),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_email)));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.optString("status").equalsIgnoreCase("true")) {
                            Util.showDialog(HomeForRolesActivity.this, "Success", "" + jsonResponse.optString("message"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(HomeForRolesActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(HomeForRolesActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);

        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            askForLogout();

        } else if (id == R.id.nav_help) {
            String url = "http://myjaldhara.com/assets/uploads/document/help_document.pdf";
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void askForLogout() {

        AlertDialog.Builder builder = new AlertDialog.Builder(HomeForRolesActivity.this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure, you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (ConnectivityReceiver.isConnected()) {
                    serviceLogout();
                } else {
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }

    /**
     *
     */
    private void serviceIsLatestVersion() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.get_version(
                "app_version",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id))
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonObject = jsonResponse.optJSONObject("data");
                        String description = jsonObject.optString("description");

                        int versionFromService = Integer.valueOf(jsonObject.optString("version_number"));


                        if (versionFromService > Util.getAppVersion(HomeForRolesActivity.this)) {
                            AlertDialog.Builder dialog = new AlertDialog.Builder(HomeForRolesActivity.this);
                            dialog.setTitle("Update Available");
                            dialog.setMessage("Please update your application from playstore\n" + description);
                            dialog.setNegativeButton("Update " + getString(R.string.app_name), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    try {
                                        PackageManager manager = getPackageManager();
                                        PackageInfo info = manager.getPackageInfo(getApplicationContext().getPackageName(), 0);
                                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + info.packageName)));
                                    } catch (PackageManager.NameNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                            dialog.setCancelable(false);
                            dialog.show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    JalDharaApplication.getInstance().getFirebaseCrashlytics().recordException(e);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private void serviceGetPermmission() {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.get_group_permissions(
                "get_group_permissions",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID))
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.optBoolean("status")) {
                            JSONObject jsonObject = jsonResponse.optJSONObject("data");
                            JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_group_permission), jsonObject.toString());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(HomeForRolesActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceLogout() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Signing off", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.logoutAuth("logout", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
//                List<Movie> movies = response.body().getResults();
//                    Log.d("", "Number of movies received: ");
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.optJSONObject("data");

                        //start----  Clear shared preference of permission on logout
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_group_permission), "");
                        //end----    Clear shared preference of permission on logout

                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_isLogin), "");
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_intent_LoginUser_email), "");
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID), "");
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_comp_id), "");
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_company_id), "");

                        Intent intent = new Intent(HomeForRolesActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(HomeForRolesActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(HomeForRolesActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
