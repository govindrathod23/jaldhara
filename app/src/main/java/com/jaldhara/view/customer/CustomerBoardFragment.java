package com.jaldhara.view.customer;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.view.RouteMapActivity;
import com.jaldhara.view.admin.AdminCustomerCountersActivity;
import com.jaldhara.view.admin.invoice.InvoicesActivity;
import com.jaldhara.view.customer.invoice.CustomerInvoicesActivity;
import com.jaldhara.view.customer.order.CustomerOrdersListActivity;

public class CustomerBoardFragment extends Fragment implements View.OnClickListener {

    private View vLayout;

    private CardView cardViewDelivery;
    private CardView cardViewReturns;
    private CardView cardViewTrackLocation;
    private CardView cardAddOrders;
    private CardView cardViewBottleAssets;
    private CardView cardViewBottleBilling;
    private ImageView imgDashBoard;
    private CardView cardInvoices;
    private CardView cardWishList;
    private CardView cardCalender;
    private CardView  cardRedeem;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        vLayout = inflater.inflate(R.layout.activity_customer_dashboard, container, false);


//
        cardViewDelivery = (CardView) vLayout.findViewById(R.id.activity_customer_dashBoard_ViewDelivery);
        cardViewDelivery.setOnClickListener(this);


        cardViewReturns = (CardView) vLayout.findViewById(R.id.activity_customer_dashBoard_ViewReturns);
        cardViewReturns.setOnClickListener(this);

        cardViewTrackLocation = (CardView) vLayout.findViewById(R.id.activity_customer_dashBoard_trackRoutes);
        cardViewTrackLocation.setOnClickListener(this);

        cardAddOrders = (CardView) vLayout.findViewById(R.id.activity_customer_dashBoard_AddOrders);
        cardAddOrders.setOnClickListener(this);

        cardViewBottleAssets = (CardView) vLayout.findViewById(R.id.activity_customer_dashBoard_ViewAssets);
        cardViewBottleAssets.setOnClickListener(this);

        cardViewBottleBilling = (CardView) vLayout.findViewById(R.id.activity_customer_dashBoard_viewBilling);
        cardViewBottleBilling.setOnClickListener(this);

        imgDashBoard = (ImageView) vLayout.findViewById(R.id.activity_img_dashboard);
        imgDashBoard.setOnClickListener(this);

        cardInvoices = (CardView) vLayout.findViewById(R.id.activity_customer_dashboard_invoices);
        cardInvoices.setOnClickListener(this);

        cardWishList = (CardView) vLayout.findViewById(R.id.activity_customer_dashboard_wishList);
        cardWishList.setOnClickListener(this);

        cardCalender = (CardView) vLayout.findViewById(R.id.activity_customer_dashboard_Calender);
        cardCalender.setOnClickListener(this);

        cardRedeem = (CardView) vLayout.findViewById(R.id.activity_customer_dashboard_redeem);
        cardRedeem.setOnClickListener(this);



        return vLayout;
    }


    @Override
    public void onClick(View v) {


        if (v.getId() == R.id.activity_customer_dashBoard_ViewDelivery) {
            BottleInfoModel bottleInfoModel = new BottleInfoModel();
            bottleInfoModel.setCompany(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id)));
            Intent intent = new Intent(getActivity(), ViewCustomerDeliveryActivity.class);
            intent.putExtra(getString(R.string.key_intent_customer_id), bottleInfoModel);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_customer_dashBoard_ViewReturns) {
//            BottleInfoModel bottleInfoModel = new BottleInfoModel();
//            bottleInfoModel.setCompany(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id)));
//            Intent intent = new Intent(getActivity(), ViewCustomerReturnActivity.class);
//            intent.putExtra(getString(R.string.key_intent_customer_id), bottleInfoModel);
//            startActivity(intent);

            Intent intent = new Intent(getActivity(), PromotionOffersActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_customer_dashBoard_trackRoutes) {
            Intent intent = new Intent(getActivity(), RouteMapActivity.class);
            intent.putExtra(getString(R.string.key_intent_Show_On_Map_For), "Customer");
            startActivity(intent);

        } else if (v.getId() == R.id.activity_customer_dashBoard_AddOrders) {
//            Intent intent = new Intent(getActivity(), CustomerAddOrdersActivity.class);
            Intent intent = new Intent(getActivity(), CustomerOrdersListActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_customer_dashBoard_ViewAssets) {
            Intent intent = new Intent(getActivity(), CustomerBottleAssetsActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_customer_dashBoard_viewBilling) {
            Intent intent = new Intent(getActivity(), CustomerBottleBillingActivity.class);
            startActivity(intent);

        }else if (v.getId() == R.id.activity_customer_dashboard_invoices) {
            Intent intent = new Intent(getActivity(), CustomerInvoicesActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_customer_dashboard_wishList) {
            Intent intent = new Intent(getActivity(), CustomerWishListActivity.class);
            startActivity(intent);

        }else if (v.getId() == R.id.activity_customer_dashboard_Calender) {
            Intent intent = new Intent(getActivity(), CalenderActivity.class);
            startActivity(intent);

        }else if (v.getId() == R.id.activity_customer_dashboard_redeem) {
            Intent intent = new Intent(getActivity(), RedeemCouponActivity.class);
            startActivity(intent);

        } else if (v.getId() == R.id.activity_img_dashboard || v.getId() == R.id.activity_admin_dashboard_Counters) {
            Intent intent = new Intent(getActivity(), CustomerCountersActivity.class);
            intent.putExtra(getString(R.string.key_intent_comes_from),"customer");
            startActivity(intent);
            getActivity().finish();

        }
    }
}
