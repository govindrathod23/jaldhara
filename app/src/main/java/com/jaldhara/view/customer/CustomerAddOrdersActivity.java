package com.jaldhara.view.customer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.AddOrderSelectionAdapter;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.PaginationScrollListener;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerAddOrdersActivity extends Activity {


    //-----Pagination variables
    private static final int PER_PAGE_COUNT = 20;
    private ArrayList<SpinnerModel> arryOrders;
    private ArrayList<SpinnerModel> arryBiller;
    private Spinner spnBiller;
    private TextView txtDeliveryDate;
    private RecyclerView mRecyclerView;
    private AddOrderSelectionAdapter selectionAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int CURRENT_PAGE = 1;
    private int PAGE_START = 1;
    private int PAGE_LIMIT = 20;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_add_orders);


        mRecyclerView = (RecyclerView) findViewById(R.id.fragment_admin_add_orders_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        arryOrders = new ArrayList<SpinnerModel>();
        arryBiller = new ArrayList<SpinnerModel>();

        txtDeliveryDate = (TextView) findViewById(R.id.activity_admin_add_orders_deliverydate);
        txtDeliveryDate.setText(Util.getTodayDate());

        spnBiller = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnBiller);
        Button btnAdd = (Button) findViewById(R.id.activity_admin_add_orders_btnAdd);
        Button btnCancle = (Button) findViewById(R.id.activity_admin_add_orders_btnCancel);
        ImageView imgBtnBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);

        selectionAdapter = new AddOrderSelectionAdapter(CustomerAddOrdersActivity.this, arryOrders);
        mRecyclerView.setAdapter(selectionAdapter);


        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONArray jsonArray = new JSONArray();
                    for (int i = 0; i < arryOrders.size(); i++) {
                        if (!TextUtils.isEmpty(arryOrders.get(i).getQuantity())) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("product_id", arryOrders.get(i).getRouteId());
                            jsonObject.put("quantity", arryOrders.get(i).getQuantity());
                            jsonArray.put(jsonObject);
                        }
                    }
                    if (jsonArray.length() == 0) {
                        Snackbar.make(getWindow().getDecorView().getRootView(), "Please add quantity to add order", Snackbar.LENGTH_LONG).show();
                        return;
                    }
                    if (ConnectivityReceiver.isConnected()) {
                        serviceAddOrder(jsonArray.toString());
                    } else {
                        Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btnCancle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Calendar calendar = Util.getCalDateFromString(txtDeliveryDate.getText().toString());
        txtDeliveryDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(CustomerAddOrdersActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day) {
                        String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                        String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);


                        txtDeliveryDate.setText(year + "-" + strMonth + "-" + strDay);


                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });


        if (ConnectivityReceiver.isConnected()) {
            serviceListOfProducts();
            serviceListOfRoutes();
        } else {
            Snackbar.make(btnAdd, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        mRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                CURRENT_PAGE += 1;
                PAGE_START += 20;

                Log.v("addOnScrollListener", "addOnScrollListener*************   " + CURRENT_PAGE);
                Log.v("addOnScrollListener", "Start*************   " + PAGE_START);
                Log.v("addOnScrollListener", "limit*************   " + PAGE_LIMIT);

                if (ConnectivityReceiver.isConnected()) {
                    serviceListOfProducts();
                } else {
                    Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return false;
            }
        });


    }


    /***************************************************************************************
     * List of Products -- List of Products  -- List of Products -- List of Products  -- List of Products
     ***************************************************************************************/

    private void serviceListOfProducts() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllProducts(
                Util.API_GET_PRODUCT,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                PAGE_START,
                PAGE_LIMIT);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        try {
                            int totalRecord = Integer.parseInt(jsonResponse.optString("total"));
                            TOTAL_PAGES = (int) Math.ceil(totalRecord / (float) PER_PAGE_COUNT);
                        } catch (Exception e) {

                        }

                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            spinnerModel.setProductPrice(jsonArray.optJSONObject(header).optString("unit_price"));
                            arryOrders.add(spinnerModel);

                        }

                        selectionAdapter.notifyDataSetChanged();

                        if (!(CURRENT_PAGE <= TOTAL_PAGES)) {
                            isLastPage = true;
                        }
                        if (CURRENT_PAGE == TOTAL_PAGES) {
                            isLastPage = true;
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CustomerAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CustomerAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void serviceListOfProducts() {
//        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
//        ApiInterface apiService =
//                ApiClient.getClient().create(ApiInterface.class);
//
//        Call<ResponseBody> call = apiService.getAllProducts("get_product", Util.API_KEY,1,20);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                dialog.dismiss();
//                try {
//
//                    if (response.isSuccessful()) {
//                        JSONObject jsonResponse = new JSONObject(response.body().string());
//                        JSONArray jsonArray = jsonResponse.optJSONArray("data");
//
//
//                        for (int header = 0; header < jsonArray.length(); header++) {
//
//                            SpinnerModel spinnerModel = new SpinnerModel();
//                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
//                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
//                            spinnerModel.setProductPrice(jsonArray.optJSONObject(header).optString("unit_price"));
//                            arryOrders.add(spinnerModel);
//
//                        }
//
////                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddAssetsActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryOrders);
////                        spnAssets.setAdapter(arrayAdapter);
//                        AddOrderSelectionAdapter selectionAdapter = new AddOrderSelectionAdapter(CustomerAddOrdersActivity.this, arryOrders);
//                        mRecyclerView.setAdapter(selectionAdapter);
//
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (Exception e) {
//                    Toast.makeText(CustomerAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                dialog.dismiss();
//                // Log error here since request failed
//                Log.e("", t.toString());
//                Toast.makeText(CustomerAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }


    /***************************************************************************************
     *
     ***************************************************************************************/
    private void serviceAddOrder(String jsonData) {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Adding Assets", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ResponseBody> call = apiService.addOrderNew(
                Util.API_ADD_ORDER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id))  ,
                arryBiller.get(spnBiller.getSelectedItemPosition()).getRouteId().toString(),
                txtDeliveryDate.getText().toString(),
                jsonData,"","","","","","","customer");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    if (response.isSuccessful() && jsonResponse.optBoolean("status")) {

//                        {"message":"Assets update sucessfully","status":true}
                        AlertDialog.Builder builder = new AlertDialog.Builder(CustomerAddOrdersActivity.this);
                        builder.setTitle("Order added");
                        builder.setMessage("Order added successfully!");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();

                            }
                        });

                        builder.show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CustomerAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CustomerAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfRoutes() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Billers", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

//        Call<ResponseBody> call = apiService.getAllBillers( "person", Util.API_KEY, "biller",1,100);
        Call<ResponseBody> call = apiService.listOfRoutes(Util.API_LIST_OF_ROUTES, Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

//                        for (int route = 0; route < jsonArray.length(); route++) {
//                            JSONObject jsonCust = jsonArray.optJSONObject(route);
//                            SpinnerModel spinnerModel = new SpinnerModel();
//                            spinnerModel.setRouteName(jsonCust.optString("company"));
//                            spinnerModel.setRouteId(jsonCust.optString("id"));
//                            arryBiller.add(spinnerModel);
//                        }
//
//                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(AdminAddOrdersActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryBiller);
//                        spnBiller.setAdapter(arrayAdapter);

                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            arryBiller.add(spinnerModel);

                        }
                        ArrayAdapter<SpinnerModel> arrayAdapter = new ArrayAdapter<SpinnerModel>(CustomerAddOrdersActivity.this, R.layout.raw_spinner, R.id.raw_spinner_txt, arryBiller);
                        spnBiller.setAdapter(arrayAdapter);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CustomerAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CustomerAddOrdersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


}
