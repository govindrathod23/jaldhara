package com.jaldhara.view.customer;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.AddOrderSelectionAdapter;
import com.jaldhara.adapters.PromotionOffersAdapter;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.PaginationScrollListener;
import com.jaldhara.utills.Util;
import com.smarteist.autoimageslider.SliderView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromotionOffersActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int PER_PAGE_COUNT = 20;
    private int PAGE_START = 1;
    private int PAGE_LIMIT = 20;
    private int TOTAL_PAGES = 0;
    private int CURRENT_PAGE = 1;
    private boolean isLastPage = false;
//    private RecyclerView recyclerViewProducts;
    private Context context;
    private PromotionOffersAdapter mAdapter;
    private ArrayList<SpinnerModel> selectedProductList = new ArrayList<>();
    private ArrayList<SpinnerModel> productListFromOrders = new ArrayList<>();
    private ArrayList<SpinnerModel> arryProductList = new ArrayList<>();

    private SliderView sliderView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);

        context = this;

        ImageView imgBack = findViewById(R.id.activity_products_imgBack);
        imgBack.setOnClickListener(this);
        TextView textView = findViewById(R.id.activity_products_txtTitle);
        textView.setText("Promotions");




//        recyclerViewProducts = findViewById(R.id.activity_products_recyclerview);



        //recyclerViewProducts.setAdapter(mAdapter);

        sliderView =  findViewById(R.id.slider);
        // below method is used to set auto cycle direction in left to




//        try {
//            if (getIntent().hasExtra(getString(R.string.key_intent_product_list))) {
//                //arryProductList = (ArrayList<SpinnerModel>) getIntent().getSerializableExtra(getString(R.string.key_intent_product_list));
//                productListFromOrders.addAll((ArrayList<SpinnerModel>) getIntent().getSerializableExtra(getString(R.string.key_intent_product_list)));
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


//        recyclerViewProducts.addOnScrollListener(new PaginationScrollListener(new LinearLayoutManager(context)) {
//            @Override
//            protected void loadMoreItems() {
//                CURRENT_PAGE += 1;
//                PAGE_START += 20;
//
//                if (ConnectivityReceiver.isConnected()) {
//                    serviceListOfProducts();
//                } else {
//                    Snackbar.make(recyclerViewProducts, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public boolean isLastPage() {
//                return isLastPage;
//            }
//
//            @Override
//            public boolean isLoading() {
//                return false;
//            }
//        });

        serviceListOfProducts();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.activity_add_product_imgBack) {
            onBackPressed();
        } else if (id == R.id.btn_select) {
            finishWithResult();
        }
    }

    private void serviceListOfProducts() {
        arryProductList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllPromotions(
                Util.API_GET_PRODUCT,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                "1",
                PAGE_START,
                PAGE_LIMIT);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        try {
                            int totalRecord = Integer.parseInt(jsonResponse.optString("total"));
                            TOTAL_PAGES = (int) Math.ceil(totalRecord / (float) PER_PAGE_COUNT);
                        } catch (Exception e) {

                        }

                        for (int header = 0; header < jsonArray.length(); header++) {

                            SpinnerModel spinnerModel = new SpinnerModel();
                            spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                            spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                            spinnerModel.setProductPrice(jsonArray.optJSONObject(header).optString("unit_price"));
                            spinnerModel.setPromo_price(jsonArray.optJSONObject(header).optString("promo_price"));
                            spinnerModel.setImage_url(jsonArray.optJSONObject(header).optString("image_url"));

                            arryProductList.add(spinnerModel);

                        }

                        mAdapter = new PromotionOffersAdapter(PromotionOffersActivity.this, arryProductList);
                        // right direction you can change according to requirement.
                        sliderView.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LTR);
                        // below method is used to
                        // setadapter to sliderview.
                        sliderView.setSliderAdapter(mAdapter);

                        // below method is use to set
                        // scroll time in seconds.
                        sliderView.setScrollTimeInSec(3);

                        // to set it scrollable automatically
                        // we use below method.
                        sliderView.setAutoCycle(true);

                        // to start autocycle below method is used.
                        sliderView.startAutoCycle();

//                        if (!productListFromOrders.isEmpty() && productListFromOrders.size() > 0) {
//                            for (int i = 0; i < productListFromOrders.size(); i++) {
//                                for (int j = 0; j < arryProductList.size(); j++) {
//                                    if (arryProductList.get(j).getRouteId().equals(productListFromOrders.get(i).getRouteId())) {
//                                        arryProductList.get(j).setQuantity(productListFromOrders.get(i).getQuantity());
//                                    }
//                                }
//                            }
//                        }

//                        mAdapter.notifyDataSetChanged();

//                        if (!(CURRENT_PAGE <= TOTAL_PAGES)) {
//                            isLastPage = true;
//                        }
//                        if (CURRENT_PAGE == TOTAL_PAGES) {
//                            isLastPage = true;
//                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(context, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void finishWithResult() {

        for (SpinnerModel product : arryProductList) {
            if (product.getQuantity() != null && !TextUtils.isEmpty(product.getQuantity())) {
                if (Integer.parseInt(product.getQuantity()) > 0) {
                    selectedProductList.add(product);
                }
            }
        }

        Intent intent = new Intent();
        intent.putExtra("products", selectedProductList);
        setResult(RESULT_OK, intent);
        finish();
    }
}