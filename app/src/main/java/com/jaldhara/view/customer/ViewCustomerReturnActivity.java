package com.jaldhara.view.customer;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.BottleReturnListAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.utills.onRemoveClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCustomerReturnActivity extends Activity implements onRemoveClick {


    //    private FloatingActionButton btnAddBottle;
    private RecyclerView mRecyclerView;
    private BottleReturnListAdapter mAdapter;
    private onRemoveClick removeClickListener;

    private ArrayList<DeliveryModel> arryBottleReturns;

    private TextView txtLblReturn;
    private RelativeLayout rlCalander;

    private Dialog dialogFilter;
    private String strDialogFromDate = "";
    private String strDialogTodate = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_bottles_return);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setVisibility(View.VISIBLE);
        ImageView imgBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txtLblReturn = (TextView) findViewById(R.id.lbl_nodelivery);


        mRecyclerView = (RecyclerView) findViewById(R.id.fragment_bottles_return_recycler_view);

        arryBottleReturns = new ArrayList<DeliveryModel>();
        mAdapter = new BottleReturnListAdapter(ViewCustomerReturnActivity.this, arryBottleReturns);
        mAdapter.setRemoveClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        arryBottleReturns = new ArrayList<DeliveryModel>();


//        rlCalander = (RelativeLayout) findViewById(R.id.ll_calender);
//        final TextView txtMonth = (TextView) findViewById(R.id.cal_month);
//        final TextView txtYear = (TextView) findViewById(R.id.cal_year);
//        final TextView txtDate = (TextView) findViewById(R.id.cal_date);


        Util.hideKeyboard(ViewCustomerReturnActivity.this);

        if (ConnectivityReceiver.isConnected()) {
//            serviceReturnOrder(Util.getTodayDate(), Util.getTodayDate());

            serviceReturnOrder(  Util.getTodayDate());
        } else {
            Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }


    }

    /**
     * @param v
     */
    public void onClickFilterApply(View v) {
        dialogFilter = new Dialog(ViewCustomerReturnActivity.this);
        dialogFilter.setContentView(R.layout.dialog_order_filter);

        final TextView txtTO = dialogFilter.findViewById(R.id.dialog_filter_txtTO);
//        final TextView txtFrom = dialogFilter.findViewById(R.id.dialog_filter_txtFrom);
        (dialogFilter.findViewById(R.id.dialog_filter_SpnCustomer)).setVisibility(View.GONE);
        (dialogFilter.findViewById(R.id.lblSpn)).setVisibility(View.GONE);
        Button btnApply = dialogFilter.findViewById(R.id.dialog_filter_btnApply);


//        txtFrom.setText(TextUtils.isEmpty(strDialogFromDate) ? Util.getTodayDate() : strDialogFromDate);
        txtTO.setText(TextUtils.isEmpty(strDialogTodate) ? Util.getTodayDate() : strDialogTodate);

        txtTO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                openDialogForDate(txtTO);

                Calendar calendar = Util.getCalDateFromString(txtTO.getText().toString());

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewCustomerReturnActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day) {

                        String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                        String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);

                        txtTO.setText(year + "-" + strMonth + "-" + strDay);
                    }
                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

//        txtFrom.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Calendar calendar = Util.getCalDateFromString(txtFrom.getText().toString());
//
//                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewCustomerReturnActivity.this, new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker arg0, int year, int month, int day) {
//                        String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
//                        String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);
//
//                        txtFrom.setText(year + "-" + strMonth + "-" + strDay);
//                    }
//                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//                datePickerDialog.show();
//            }
//        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectivityReceiver.isConnected()) {


//                    serviceReturnOrder(txtFrom.getText().toString(), txtTO.getText().toString());
                    serviceReturnOrder(  txtTO.getText().toString());

//                    strDialogFromDate = txtFrom.getText().toString();
                    strDialogTodate = txtTO.getText().toString();

                } else {
                    Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        dialogFilter.show();

    }

    @Override
    public void onClick(int position) {
        Log.v("TAG", "click working on : " + position);
//        arrayListBottleReturn.remove(position);
//        arrayListBottleReturn.trimToSize();
//        mAdapter = new BottleReturnListAdapter(getActivity(), arrayListBottleReturn);
//        mAdapter.setRemoveClickListener(this);
//        mRecyclerView.setAdapter(mAdapter);

    }


    private void serviceReturnOrder(  String EndDate) {

        final ProgressDialog dialog = ProgressDialog.show(ViewCustomerReturnActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        BottleInfoModel bottleInfoModel = getIntent().getParcelableExtra(getString(R.string.key_intent_customer_id));


        //oldcode
//        Call<ResponseBody> call = apiService.getOrderCustomer(
//                "get_order",
//                Util.API_KEY,
//                bottleInfoModel.getCustomerID(),
//                "items",
//                "delivering",
//                date);

        Call<ResponseBody> call = apiService.SeeOrders(
                "get_order",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                bottleInfoModel.getCustomerID(),
                "items",
                "delivering",
//                StartDate,
                EndDate);


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                if (dialogFilter != null && dialogFilter.isShowing()) {
                    dialogFilter.dismiss();
                }
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        arryBottleReturns.clear();

                        JSONArray jsonArray = jsonResponse.optJSONArray("returns");

                        if (jsonArray != null && jsonArray.length() > 0) {

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject = jsonArray.optJSONObject(i);
                                DeliveryModel deliveryModel = new DeliveryModel();
                                deliveryModel.setSales_id(jsonObject.optString("id"));
                                deliveryModel.setProduct_name(jsonObject.optString("name"));
                                deliveryModel.setQuantity(jsonObject.optString("quantity"));
                                arryBottleReturns.add(deliveryModel);

                            }
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(ViewCustomerReturnActivity.this, "Please try after sometime!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }


                if (arryBottleReturns.isEmpty()) {
                    txtLblReturn.setVisibility(View.VISIBLE);
                    mRecyclerView.setVisibility(View.GONE);

                } else {
                    txtLblReturn.setVisibility(View.GONE);
                    mRecyclerView.setVisibility(View.VISIBLE);
                    mAdapter = new BottleReturnListAdapter(ViewCustomerReturnActivity.this, arryBottleReturns);
                    mRecyclerView.setAdapter(mAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                if (dialogFilter != null && dialogFilter.isShowing()) {
                    dialogFilter.dismiss();
                }
                Log.e("", t.toString());

                txtLblReturn.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        });
    }


    /*******************************************************
     *
     *******************************************************/
    public void onDoneClick() {

        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arryBottleReturns.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("product_id", arryBottleReturns.get(i).getSales_id());
                jsonObject.put("quantity", arryBottleReturns.get(i).getQuantity());

                jsonArray.put(jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (ConnectivityReceiver.isConnected()) {
            serviceAddReturnOrder(jsonArray.toString());
        } else {
            Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
    }


    private void serviceAddReturnOrder(String jsonArray) {
        final ProgressDialog dialog = ProgressDialog.show(ViewCustomerReturnActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);


        BottleInfoModel bottleInfoModel = getIntent().getParcelableExtra(getString(R.string.key_intent_customer_id));

        Call<ResponseBody> call = apiService.addReturn("add_return", Util.API_KEY,JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)), bottleInfoModel.getCustomerID(), jsonArray,"");

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        if (jsonResponse.optBoolean("status")) {

                            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ViewCustomerReturnActivity.this);
                            builder.setMessage(jsonResponse.optString("message"));
                            builder.setTitle("Return");
                            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    finish();
                                }
                            });
                            builder.setCancelable(false);
                            builder.show();


                        }


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(ViewCustomerReturnActivity.this, "Please try after sometime!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(ViewCustomerReturnActivity.this, "Please try after sometime!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void setServiceData(ArrayList<DeliveryModel> arryBottleReturns) {

        this.arryBottleReturns = arryBottleReturns;
        mAdapter = new BottleReturnListAdapter(ViewCustomerReturnActivity.this, arryBottleReturns);
        mRecyclerView.setAdapter(mAdapter);
    }

}
