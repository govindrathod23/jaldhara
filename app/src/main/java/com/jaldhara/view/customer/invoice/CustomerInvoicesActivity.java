package com.jaldhara.view.customer.invoice;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.InvoiceListDriverAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.InvoiceModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.invoice.ViewInvoiceListActivity;
import com.jaldhara.view.deliveryboy.invoice.DAddInvoicePaymentActivity;
import com.jaldhara.view.deliveryboy.invoice.DViewPaymentsActivity;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerInvoicesActivity extends Activity {

    private RecyclerView mRecyclerView;
    private ArrayList<InvoiceModel> arryInvoice;
    private InvoiceListDriverAdapter mAdapter;
    private ImageView btnCreateInvoice;
    public static String KEY_CUSTOMER_INFO = "CUSTOMER_INFO";

    private TextView txtNoRecord;

    private EditText searchText;
    private TextView txtClear;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invoices);


        mRecyclerView = (RecyclerView) findViewById(R.id.activity_invoice_recyclerview);
        arryInvoice = new ArrayList<InvoiceModel>();

        //LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mAdapter = new InvoiceListDriverAdapter(CustomerInvoicesActivity.this, arryInvoice);
        //mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        btnCreateInvoice = findViewById(R.id.activity_invoice_btn_create);
        btnCreateInvoice.setVisibility(View.GONE);

        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);
        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);


        if (ConnectivityReceiver.isConnected()) {
            serviceListOfInvoice();
        } else {
            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        txtNoRecord = findViewById(R.id.txtNoRecord);

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        ((TextView) findViewById(R.id.lbl_customer)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.activity_invoice_SpnCustomer)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.lblRoute)).setVisibility(View.GONE);
        ((Spinner) findViewById(R.id.activity_admin_invoice_SpnRoutes)).setVisibility(View.GONE);

    }

    void filter(String text) {
        ArrayList<InvoiceModel> temp = new ArrayList();

        if (arryInvoice != null && arryInvoice.size() > 0) {
            for (InvoiceModel d : arryInvoice) {
                if (d.getInv_reference_no().toLowerCase().contains(text) ||
                        d.getCreated_at().toLowerCase().contains(text) ||
                        d.getCustomer().toLowerCase().contains(text) ||
                        d.getPayment_status().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }
            if (temp != null && temp.size() > 0) {
                //update recyclerview
                txtNoRecord.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                mAdapter.setRefreshList(temp);
            } else {
                txtNoRecord.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }


    private void serviceListOfInvoice() {
        arryInvoice.clear();

        final ProgressDialog dialog = ProgressDialog.show(CustomerInvoicesActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getInvoiceList(
                Util.API_LIST_OF_INVOICE,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id)));

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());


                        if (jsonResponse.has("invoice")) {
                            JSONArray jsonArrayInvoice = jsonResponse.optJSONArray("invoice");
                            if (jsonArrayInvoice != null && jsonArrayInvoice.length() > 0) {
                                for (int i = 0; i < jsonArrayInvoice.length(); i++) {
                                    JSONObject jobj = jsonArrayInvoice.getJSONObject(i);


                                    InvoiceModel model = new InvoiceModel();
                                    model.setInv_reference_no(jobj.optString("inv_reference_no"));
                                    model.setId(jobj.optString("id"));
                                    model.setComp_id(jobj.optString("comp_id"));
                                    model.setBiller_id(jobj.optString("biller_id"));
                                    model.setBiller(jobj.optString("biller"));
                                    model.setCustomer_id(jobj.optString("customer_id"));
                                    model.setCustomer(jobj.optString("customer"));
                                    model.setTotal_item(jobj.optString("total_item"));
                                    model.setTotal_amount(jobj.optString("total_amount"));
                                    model.setPaid(jobj.optString("paid"));
                                    model.setBalance(jobj.optString("balance"));
                                    model.setTax(jobj.optString("tax"));
                                    model.setPayment_status(jobj.optString("payment_status"));
                                    model.setPaid_by(jobj.optString("paid_by"));
                                    model.setInv_month(jobj.optString("inv_month"));
                                    model.setFrom_date(jobj.optString("from_date"));
                                    model.setTo_date(jobj.optString("to_date"));
                                    model.setNote(jobj.optString("note"));
                                    model.setStaff_note(jobj.optString("staff_note"));
                                    model.setCreated_at(jobj.optString("created_at"));
                                    model.setUpdated_at(jobj.optString("updated_at"));
                                    model.setName(jobj.optString("name"));
                                    model.setRoute_name(jobj.optString("route_name"));
                                    model.setRoute_id(jobj.optString("route_id"));

                                    arryInvoice.add(model);
                                }

                                if (arryInvoice != null && arryInvoice.size() > 0) {
                                    mAdapter = new InvoiceListDriverAdapter(CustomerInvoicesActivity.this, arryInvoice);
                                    mRecyclerView.setAdapter(mAdapter);
                                    txtNoRecord.setVisibility(View.GONE);
                                    mRecyclerView.setVisibility(View.VISIBLE);
                                } else {
                                    txtNoRecord.setVisibility(View.VISIBLE);
                                    mRecyclerView.setVisibility(View.GONE);
                                }
                                mAdapter.setOnItemClick(new InvoiceListDriverAdapter.onItemClick<InvoiceModel>() {
                                    @Override
                                    public void onDetailClicked(InvoiceModel data) {
                                        Intent intent = new Intent(CustomerInvoicesActivity.this, ViewInvoiceListActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
                                        startActivity(intent);

                                    }

                                    @Override
                                    public void onAddPaymentClicked(InvoiceModel data) {
                                        Intent intent = new Intent(CustomerInvoicesActivity.this, DAddInvoicePaymentActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onDeleteClicked(final InvoiceModel data) {
                                        Util.showDialog(CustomerInvoicesActivity.this, "Delete Invoice", "Please contact your admin.\nOnly admin can delete the invoice.");
                                    }

                                    @Override
                                    public void onEditClicked(InvoiceModel data) {
                                        Util.showDialog(CustomerInvoicesActivity.this, "Edit Invoice", "Please contact your admin.\nOnly admin can edit the invoice.");
                                    }

                                    @Override
                                    public void onViewPaymentsClicked(InvoiceModel data) {
                                        Intent intent = new Intent(CustomerInvoicesActivity.this, DViewPaymentsActivity.class);
                                        intent.putExtra(getString(R.string.key_intent_invoice_detail), data);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onEmailInvoice(InvoiceModel data) {

                                        Intent intent = new Intent(Intent.ACTION_SEND);
                                        intent.setType("plain/text");
                                        intent.putExtra(Intent.EXTRA_SUBJECT, "Your Invoice");
                                        intent.putExtra(Intent.EXTRA_TEXT, "Coming soon HTML invoice text, needs to create with web developer");
                                        startActivity(Intent.createChooser(intent, "Mail invoice"));

                                    }

                                });
                            } else {
                                txtNoRecord.setVisibility(View.VISIBLE);
                                mRecyclerView.setVisibility(View.GONE);
                            }
                        }
                    } else {
                        txtNoRecord.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();
                txtNoRecord.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        });
    }

    private void serviceDeleteInvoice(String invoiceID) {

        final ProgressDialog dialog = ProgressDialog.show(CustomerInvoicesActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.delete_invoice(
                Util.API_DELETE_INVOICE,
                Util.API_KEY,
                invoiceID
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {
                    if (response.isSuccessful()) {
//                        {"message":"add purchases payment","status":true}
                        final String strResponse = response.body().string();
                        JSONObject jsonResponse = new JSONObject(strResponse);

                        Toast.makeText(CustomerInvoicesActivity.this, jsonResponse.optString("message"), Toast.LENGTH_SHORT).show();
                        if (ConnectivityReceiver.isConnected()) {
                            serviceListOfInvoice();
                        } else {
                            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }


    public void onClickBackButton(View v) {
        onBackPressed();
    }
}