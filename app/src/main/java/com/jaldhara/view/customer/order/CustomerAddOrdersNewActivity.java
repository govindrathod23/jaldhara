package com.jaldhara.view.customer.order;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.admin.SearchCustomerActivity;
import com.jaldhara.view.admin.order.ProductsListForOrderActivity;
import com.jaldhara.view.customer.SelectProductExpandAdapter;


import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerAddOrdersNewActivity extends Activity implements View.OnClickListener {


    private static final int REQUEST_CODE_CUSTOMER = 2078;
    private static final int INTENT_PRODUCTS_LIST = 101;

    //    private ArrayList<SpinnerModel> arryRoutes;
//    private ArrayList<SpinnerModel> arryBiller;
//    private ArrayList<SpinnerModel> arryDriver;
    private ArrayList<SpinnerModel> arryOrders;
    //    private ArrayList<SpinnerModel> arrayWarehouseList = new ArrayList<>();
    private TextView txtShowProductCount;
    private TextView txtDeliveryDate;
    //    private TextView txtCustomer;
//    private Spinner spnWarehouse;
    private Spinner spnRoutes;
    private Spinner spnBillers;
    //    private Spinner spnSaleStatus;
//    private Spinner spnPaymentStatus;
//    private Spinner spnDriver;
//    private String customerName = "";
//    private String customerId = "";
//    private RadioButton rbPeriodic;
    private EditText edtReference;
    private Button btnExpand, btnColspan;
    private LinearLayout laySelectProduct;
    private RecyclerView recycleSelectProduct;
    private LinearLayoutManager linearLayoutManager;
    private SelectProductExpandAdapter selectProductExpandAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_add_orders_new);


//        spnWarehouse = findViewById(R.id.activity_admin_add_orders_SpnWarehouse);
        spnRoutes = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnRoutes);
        spnBillers = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnBiller);
//        spnDriver = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnDriver);

//        spnSaleStatus = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnSaleStatus);
//        spnPaymentStatus = (Spinner) findViewById(R.id.activity_admin_add_orders_SpnPaymentStatus);
//        txtCustomer = (TextView) findViewById(R.id.activity_admin_add_orders_SpnCustomer);
        txtShowProductCount = (TextView) findViewById(R.id.txt_showProductCount);
//        rbPeriodic = (RadioButton) findViewById(R.id.rb_Periodic);
        txtDeliveryDate = (TextView) findViewById(R.id.activity_admin_add_orders_deliverydate);
        edtReference = (EditText) findViewById(R.id.activity_admin_add_orders_edtReferance);

        txtDeliveryDate.setText(Util.getTodayDate());
        txtDeliveryDate.setOnClickListener(this);

        btnExpand = findViewById(R.id.btnExpand);
        btnColspan = findViewById(R.id.btnColspan);
        laySelectProduct = findViewById(R.id.laySelectProduct);
        recycleSelectProduct = findViewById(R.id.recycleSelectProduct);
        linearLayoutManager = new LinearLayoutManager(CustomerAddOrdersNewActivity.this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        recycleSelectProduct.setLayoutManager(linearLayoutManager);

//        arryRoutes = new ArrayList<SpinnerModel>();
//        arryBiller = new ArrayList<SpinnerModel>();
//        arryDriver = new ArrayList<SpinnerModel>();
        arryOrders = new ArrayList<SpinnerModel>();

        selectProductExpandAdapter = new SelectProductExpandAdapter(CustomerAddOrdersNewActivity.this, arryOrders);
        recycleSelectProduct.setAdapter(selectProductExpandAdapter);

        Button btnAdd = (Button) findViewById(R.id.activity_admin_add_orders_btnAdd);
        btnAdd.setOnClickListener(this);


        Button btnCancle = (Button) findViewById(R.id.activity_admin_add_orders_btnCancel);
        btnCancle.setOnClickListener(this);
        ImageView imgBtnBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBtnBack.setOnClickListener(this);

        if (ConnectivityReceiver.isConnected()) {
            //serviceListOfRoutes();
            //serviceListOfBiller();
            //serviceListOfDriver();
            //serviceListOfWarehouses();
            serviceGetReferanceNo();
        } else {
            Snackbar.make(getCurrentFocus(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
    }


    /***************************************************************************************
     *
     *
     **/
    private void serviceAddOrder(String jsonData) {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Adding Assets", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


//        Util.API_ADD_ORDER,
//                Util.API_KEY,
//                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
//                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id))

        Call<ResponseBody> call = apiService.addOrderNew(
                Util.API_ADD_ORDER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id)),
                "",
                txtDeliveryDate.getText().toString(),
                jsonData,
                "",
                "0",
                "Ordered",
                "",
                "",
                "",
                "customer"
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    if (response.isSuccessful() && jsonResponse.optBoolean("status")) {

//                        {"message":"Assets update sucessfully","status":true}
                        AlertDialog.Builder builder = new AlertDialog.Builder(CustomerAddOrdersNewActivity.this);
                        builder.setTitle("Order added");
                        builder.setMessage("Order added successfully!");
                        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();

                            }
                        });
                        builder.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CustomerAddOrdersNewActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CustomerAddOrdersNewActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//
        if (requestCode == INTENT_PRODUCTS_LIST && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra("products")) {
                Log.e("ADDPurchase", " data " + data.getSerializableExtra("products"));
           /*     ArrayList<ProductModel> selectedProducts = (ArrayList<ProductModel>) data.getSerializableExtra("products");
                selectedProductList.addAll(selectedProducts);*/
                arryOrders.clear();
                arryOrders.addAll(data.getParcelableArrayListExtra("products"));
                txtShowProductCount.setText(arryOrders.size() + " Product(s) selected");
                selectProductExpandAdapter.notifyDataSetChanged();
            }
        }
    }

    public void onClickFilterApply(View v) {
        Intent intent = new Intent(CustomerAddOrdersNewActivity.this, SearchCustomerActivity.class);
        startActivityForResult(intent, REQUEST_CODE_CUSTOMER);
    }

    public void onClickSelectProduct(View v) {
        startActivityForResult(new Intent(CustomerAddOrdersNewActivity.this, ProductsListForOrderActivity.class).putExtra(getString(R.string.key_intent_product_list), arryOrders), INTENT_PRODUCTS_LIST);
    }

    @Override
    public void onClick(View v) {
        int position = 0;
        SpinnerModel spinnerModel;
        int totalQty = 1;
        if (v.getId() == R.id.activity_admin_add_orders_btnAdd) {
            try {

                if (arryOrders.isEmpty()) {
                    Snackbar.make(getWindow().getDecorView().getRootView(), "Please add quantity to add order", Snackbar.LENGTH_LONG).show();
                    Toast.makeText(CustomerAddOrdersNewActivity.this, "Select product to add order", Toast.LENGTH_SHORT).show();
                    return;
                }

                JSONArray jsonArray = new JSONArray();
                for (int i = 0; i < arryOrders.size(); i++) {
                    if (!TextUtils.isEmpty(arryOrders.get(i).getQuantity())) {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("product_id", arryOrders.get(i).getRouteId());
                        jsonObject.put("quantity", arryOrders.get(i).getQuantity());
                        jsonArray.put(jsonObject);
                    }
                }


                if (ConnectivityReceiver.isConnected()) {
                    serviceAddOrder(jsonArray.toString());
                } else {
                    Toast.makeText(CustomerAddOrdersNewActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (v.getId() == R.id.activity_admin_add_orders_btnCancel) {
            finish();

        } else if (v.getId() == R.id.activity_bottle_settlement_tab_imgBack) {
            finish();

        } else if (v.getId() == R.id.activity_admin_add_orders_deliverydate) {

            final Calendar calendar = Util.getCalDateFromString(txtDeliveryDate.getText().toString());
            DatePickerDialog datePickerDialog = new DatePickerDialog(CustomerAddOrdersNewActivity.this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker arg0, int year, int month, int day) {
                    String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                    String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);

                    txtDeliveryDate.setText(year + "-" + strMonth + "-" + strDay);
                }
            }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.show();
        } else if (v.getId() == R.id.btnExpand) {
            if (arryOrders != null && arryOrders.size() > 0) {
                if (btnExpand.getVisibility() == View.VISIBLE) {
                    btnExpand.setVisibility(View.GONE);
                    btnColspan.setVisibility(View.VISIBLE);
                    recycleSelectProduct.setVisibility(View.VISIBLE);
                    selectProductExpandAdapter.notifyDataSetChanged();
                } else {
                    btnExpand.setVisibility(View.VISIBLE);
                    btnColspan.setVisibility(View.GONE);
                    recycleSelectProduct.setVisibility(View.GONE);
                }
            } else {
                Toast.makeText(CustomerAddOrdersNewActivity.this, "Please select product", Toast.LENGTH_SHORT).show();
            }
        } else if (v.getId() == R.id.btnColspan) {
            btnExpand.setVisibility(View.VISIBLE);
            btnColspan.setVisibility(View.GONE);
            recycleSelectProduct.setVisibility(View.GONE);
        } else if (v.getId() == R.id.ic_minus) {
            position = (int) v.getTag();
            spinnerModel = arryOrders.get(position);
            totalQty = Integer.parseInt(spinnerModel.getQuantity());
            if (Integer.parseInt(spinnerModel.getQuantity()) > 1) {
                totalQty = totalQty - 1;
                spinnerModel.setQuantity(totalQty + "");
                arryOrders.set(position, spinnerModel);
            }
            selectProductExpandAdapter = (SelectProductExpandAdapter) recycleSelectProduct.getAdapter();
            if (selectProductExpandAdapter != null && selectProductExpandAdapter.getItemCount() > 0) {
                selectProductExpandAdapter.notifyAdapter(arryOrders);
            }
        } else if (v.getId() == R.id.ic_plus) {
            position = (int) v.getTag();
            spinnerModel = arryOrders.get(position);
            totalQty = totalQty + Integer.parseInt(spinnerModel.getQuantity());
            spinnerModel.setQuantity(totalQty + "");
            arryOrders.set(position, spinnerModel);
            selectProductExpandAdapter = (SelectProductExpandAdapter) recycleSelectProduct.getAdapter();
            if (selectProductExpandAdapter != null && selectProductExpandAdapter.getItemCount() > 0) {
                selectProductExpandAdapter.notifyAdapter(arryOrders);
            }
        }
    }


    private void serviceGetReferanceNo() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customers", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.get_reference_no(
                "get_reference_no",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                "sales");
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        if (jsonResponse.optBoolean("status")) {
                            edtReference.setText(jsonResponse.optJSONObject("data").optString("reference_no"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CustomerAddOrdersNewActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CustomerAddOrdersNewActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
