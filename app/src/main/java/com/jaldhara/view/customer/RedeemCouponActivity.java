package com.jaldhara.view.customer;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;

public class RedeemCouponActivity extends Activity {

    private ImageView activity_bottle_settlement_tab_imgBack;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_redeem);


        activity_bottle_settlement_tab_imgBack = findViewById(R.id.activity_bottle_settlement_tab_imgBack);


        activity_bottle_settlement_tab_imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String companyId = "";
        if (!TextUtils.isEmpty(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id)))) {
            companyId = JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id));
        }

        String URL = "http://myjaldhara.com/api/v1/rewards/view_gift_card_api/" + companyId
//                +JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id))
                ;

        WebView myWebView = (WebView) findViewById(R.id.webview);
        myWebView.loadUrl(URL);
    }
}
