package com.jaldhara.view.customer;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.AbsoluteSizeSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.BrandCustomerAdapter;
import com.jaldhara.adapters.CategoryCustomerAdapter;
import com.jaldhara.adapters.FeatureOffersAdapter;
import com.jaldhara.adapters.PromotionOffersAdapter;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.view.HomeForRolesActivity;
import com.jaldhara.view.admin.order.AdminOrdersListActivity;
import com.jaldhara.view.customer.order.CustomerOrdersListActivity;
import com.smarteist.autoimageslider.SliderView;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.text.Spanned.SPAN_INCLUSIVE_INCLUSIVE;

public class CustomerCountersActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgHome;

    private TextView txt_user;
    private TextView txt_customer;
    private TextView txt_driver;
    private TextView txt_order;
    private TextView txt_purchaces;
    private TextView txt_todays_payments;
    private TextView txt_deliverys;
    private TextView txt_payments;
    private TextView txt_today_sales;
    private TextView txt_today_pending_deliveries;
    private TextView txt_daily_deliveries;

    private TextView txt_routes;
    private TextView txt_warehouses;
    private TextView txt_products;
    private TextView txt_sales;
    private TextView txt_owner_count;
    private TextView txt_SalesValue;
    private TextView txt_today_return_bottle;
    private TextView txt_total_return_bot;
    private TextView txt_total_pending_empty_bott;
    private TextView txt_total_expenses;
    private TextView txt_today_expenses;
    private TextView txt_total_purchases;
    private TextView txt_total_sales;
    private TextView txt_invoices_amount;
    private SliderView sliderView;
    private ArrayList<SpinnerModel> arryProductList = new ArrayList<>();

    private static final int PER_PAGE_COUNT = 20;
    private int PAGE_START = 1;
    private int PAGE_LIMIT = 20;
    private int TOTAL_PAGES = 0;
    private int CURRENT_PAGE = 1;
    private boolean isLastPage = false;

    private FeatureOffersAdapter mAdapter;
    private RecyclerView recycleBrand;
    private RecyclerView recycleCategory;
    private BrandCustomerAdapter brandAdapter;
    private CategoryCustomerAdapter categoryAdapter;

    private ArrayList<SpinnerModel> arryBrand = new ArrayList<SpinnerModel>();
    ;
    private ArrayList<SpinnerModel> arryCategory = new ArrayList<SpinnerModel>();

    private TextView txtNoBrand, txtNoCategory, txtFeatureProduct;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_dashboard_counter);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorAccent));

//            this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                    WindowManager.LayoutParams.FLAG_FULLSCREEN); //show the activity in full screen
        }


        TextView txt_user_name = (TextView) findViewById(R.id.txt_user_name);
        txt_user_name.setText(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_intent_LoginUser_UserName)));

        imgHome = (ImageView) findViewById(R.id.activity_img_dashboard);
        imgHome.setOnClickListener(this);

        if (getIntent().getStringExtra("comes_from").equals("admin")) {
            txt_customer = (TextView) findViewById(R.id.txt_customer);

            txt_user = (TextView) findViewById(R.id.txt_user);
            txt_driver = (TextView) findViewById(R.id.txt_driver);

            txt_purchaces = (TextView) findViewById(R.id.txt_purchaces);
            txt_todays_payments = (TextView) findViewById(R.id.txt_todays_payments);


            txt_payments = (TextView) findViewById(R.id.txt_payments);
            txt_today_sales = (TextView) findViewById(R.id.txt_today_sale);
            txt_today_pending_deliveries = (TextView) findViewById(R.id.txt_todays_pending_deliverys);
            txt_daily_deliveries = (TextView) findViewById(R.id.txt_daily_deliverys);

            txt_routes = (TextView) findViewById(R.id.txt_routes);
            txt_warehouses = (TextView) findViewById(R.id.txt_warehoues);
            txt_products = (TextView) findViewById(R.id.txt_products);
            txt_sales = (TextView) findViewById(R.id.txt_sales);
            txt_owner_count = (TextView) findViewById(R.id.txt_owner_count);
            txt_SalesValue = (TextView) findViewById(R.id.txt_SalesValue);
            txt_today_return_bottle = (TextView) findViewById(R.id.txt_today_return_bottle);
            txt_total_return_bot = (TextView) findViewById(R.id.txt_total_return_bottle);
            txt_total_pending_empty_bott = (TextView) findViewById(R.id.txt_total_pending_empty_bottle);
            txt_total_expenses = (TextView) findViewById(R.id.txt_total_expenses);
            txt_today_expenses = (TextView) findViewById(R.id.txt_total_expenses);
            txt_total_purchases = (TextView) findViewById(R.id.txt_total_purchases);
            txt_total_sales = (TextView) findViewById(R.id.txt_total_sales);

        }
        ImageView imgBtnBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);

        txt_order = (TextView) findViewById(R.id.txt_orders);
        txt_deliverys = (TextView) findViewById(R.id.txt_deliverys);
        txt_invoices_amount = (TextView) findViewById(R.id.txt_invoices_amount);

        recycleBrand = (RecyclerView) findViewById(R.id.activity_customer_dashboard_count_recycle_brand);
        recycleBrand.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        brandAdapter = new BrandCustomerAdapter(CustomerCountersActivity.this, arryBrand);
        recycleBrand.setAdapter(brandAdapter);

        recycleCategory = (RecyclerView) findViewById(R.id.activity_customer_dashboard_count_recycle_category);
        recycleCategory.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        categoryAdapter = new CategoryCustomerAdapter(CustomerCountersActivity.this, arryCategory);
        recycleCategory.setAdapter(categoryAdapter);

        txtNoBrand = findViewById(R.id.txtNoBrand);
        txtNoCategory = findViewById(R.id.txtNoCategory);
        txtFeatureProduct = findViewById(R.id.txtNoCategory);

        sliderView = findViewById(R.id.slider);


//        if (getIntent().getStringExtra("comes_from").equals("admin")) {
//            serviceGetAdminCounters();

//        }else{
//            serviceGetCustomerCounters();
//            serviceListOfFeatureProducts();
//        }

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfBrands();
            serviceListOfCategory();
//            serviceGetAdminCounters();
            serviceGetCustomerCounters();
            serviceListOfFeatureProducts();
        } else {
            Snackbar.make(getCurrentFocus(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {

        if (v == imgHome) {

            if (getIntent().getStringExtra("comes_from").equals("admin")) {
                Intent intent = new Intent(CustomerCountersActivity.this, HomeForRolesActivity.class);
                intent.putExtra(getString(R.string.key_intent_comes_from), "admin");
                startActivity(intent);
                finish();
            } else if (getIntent().getStringExtra("comes_from").equals("customer")) {
                Intent intent = new Intent(CustomerCountersActivity.this, HomeForRolesActivity.class);
                intent.putExtra(getString(R.string.key_intent_comes_from), "customer");
                startActivity(intent);
                finish();
            }

        } else if (v == txt_order) {
            if (getIntent().getStringExtra("comes_from").equals("admin")) {
                Intent intent = new Intent(CustomerCountersActivity.this, AdminOrdersListActivity.class);
                startActivity(intent);
//            finish();
            } else {
                Intent intent = new Intent(CustomerCountersActivity.this, CustomerOrdersListActivity.class);
                startActivity(intent);
//            finish();
            }

        }

    }

    private void serviceGetAdminCounters() {

        final ProgressDialog dialog = ProgressDialog.show(CustomerCountersActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.admin_counter(
                Util.API_ADMIN_COUNTER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id))
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.optJSONObject("data");

                        txt_customer.setText(jsonData.optString("customer"));

                        convertValue(jsonData.optString("users"), txt_user);
                        convertValue(jsonData.optString("driver"), txt_driver);
                        convertValue(jsonData.optString("orders"), txt_order);
                        convertValue(jsonData.optString("purchases"), txt_purchaces);

                        convertValue(jsonData.optString("today_payments"), txt_todays_payments);
                        convertValue(jsonData.optString("deliveries"), txt_deliverys);

                        convertValue(jsonData.optString("payments"), txt_payments);
                        convertValue(jsonData.optString("today_sales"), txt_today_sales);

                        convertValue(jsonData.optString("today_pending_deliveries"), txt_today_pending_deliveries);

                        convertValue(jsonData.optString("daily_deliveries"), txt_daily_deliveries);
                        convertValue(jsonData.optString("sales"), txt_sales);
                        convertValue(jsonData.optString("today_return_bottle"), txt_today_return_bottle);
                        convertValue(jsonData.optString("total_return_bottle"), txt_total_return_bot);
                        convertValue(jsonData.optString("total_pending_empty_bottle"), txt_total_pending_empty_bott);

                        convertValue(jsonData.optString("total_purchases"), txt_total_purchases);
                        convertValue(jsonData.optString("total_sales"), txt_total_sales);
                        convertValue(jsonData.optString("invoices_amount"), txt_invoices_amount);

                        convertValue(jsonData.optString("routes"), txt_routes);
                        convertValue(jsonData.optString("warehouses"), txt_warehouses);
                        convertValue(jsonData.optString("products"), txt_products);

                        convertValue(jsonData.optString("owner_count"), txt_owner_count);
                        convertValue(jsonData.optString("SalesValue"), txt_SalesValue);
                        convertValue(jsonData.optString("total_expenses"), txt_total_expenses);

                        convertValue(jsonData.optString("today_expenses"), txt_today_expenses);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();
            }
        });
    }

    private void serviceGetCustomerCounters() {

        final ProgressDialog dialog = ProgressDialog.show(CustomerCountersActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseBody> call = apiService.customer_counter(
                Util.API_CUSTOMER_COUNTER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id))
        );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NotNull Call<ResponseBody> call, @NotNull Response<ResponseBody> response) {
                dialog.dismiss();

                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.optJSONObject("data");

                        //txt_customer.setText(jsonData.optString("customer"));

                        // {"data":{"orders":5,"deliveries":5,"invoices_amount_paid":0,"invoices_amount_pending":0},"status":true}
// convertValue(jsonData.optString("users"), txt_user);
//                        convertValue(jsonData.optString("driver"), txt_driver);
                        convertValue(jsonData.optString("orders"), txt_order);
//                        convertValue(jsonData.optString("purchases"), txt_purchaces);
//
//                        convertValue(jsonData.optString("today_payments"), txt_todays_payments);
                        convertValue(jsonData.optString("deliveries"), txt_deliverys);
//
//                        convertValue(jsonData.optString("payments"), txt_payments);
//                        convertValue(jsonData.optString("today_sales"), txt_today_sales);
//
//                        convertValue(jsonData.optString("today_pending_deliveries"), txt_today_pending_deliveries);
//
//                        convertValue(jsonData.optString("daily_deliveries"), txt_daily_deliveries);
//                        convertValue(jsonData.optString("sales"), txt_sales);
//                        convertValue(jsonData.optString("today_return_bottle"), txt_today_return_bottle);
//                        convertValue(jsonData.optString("total_return_bottle"), txt_total_return_bot);
//                        convertValue(jsonData.optString("total_pending_empty_bottle"), txt_total_pending_empty_bott);
//
//                        convertValue(jsonData.optString("total_purchases"), txt_total_purchases);
//                        convertValue(jsonData.optString("total_sales"), txt_total_sales);
                        convertValue(jsonData.optString("invoices_amount_pending"), txt_invoices_amount);
//
//
//                        convertValue(jsonData.optString("routes"), txt_routes);
//                        convertValue(jsonData.optString("warehouses"), txt_warehouses);
//                        convertValue(jsonData.optString("products"), txt_products);
//
//                        convertValue(jsonData.optString("owner_count"), txt_owner_count);
//                        convertValue(jsonData.optString("SalesValue"), txt_SalesValue);
//                        convertValue(jsonData.optString("total_expenses"), txt_total_expenses);
//
//                        convertValue(jsonData.optString("today_expenses"), txt_today_expenses);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NotNull Call<ResponseBody> call, @NotNull Throwable t) {
                dialog.dismiss();

            }
        });
    }

    private void convertValue(String value, TextView txtView) {

        int text_size_counter = getResources().getDimensionPixelSize(R.dimen.text_size_counter);

        if (value.contains(",")) {
            value = value.replaceAll(",", "");
        }
        if (value.contains(".")) {
            value = value.substring(0, value.lastIndexOf("."));
        }

        if (value.length() <= 3) {
            SpannableString span1 = new SpannableString(value);
            span1.setSpan(new AbsoluteSizeSpan(text_size_counter), 0, value.length(), SPAN_INCLUSIVE_INCLUSIVE);
            txtView.setText(span1);
            return;
        }

        long valL = Long.valueOf(value);
        final String[] units = new String[]{"", "K", "M", "B", "P"};
        int digitGroups = (int) (Math.log10(valL) / Math.log10(1000));

        //------------------------------------------------------------------------------------------------------------

        String digitFormat = new DecimalFormat("#,##0.#").format(valL / Math.pow(1000, digitGroups));
        String unitFormat = units[digitGroups];


        int text_size_counterK = getResources().getDimensionPixelSize(R.dimen.text_size_counterK);

        SpannableString span1 = new SpannableString(digitFormat);
        span1.setSpan(new AbsoluteSizeSpan(text_size_counter), 0, digitFormat.length(), SPAN_INCLUSIVE_INCLUSIVE);

        SpannableString span2 = new SpannableString(unitFormat);
        span2.setSpan(new AbsoluteSizeSpan(text_size_counterK), 0, unitFormat.length(), SPAN_INCLUSIVE_INCLUSIVE);

        // let's put both spans together with a separator and all
        CharSequence finalText = TextUtils.concat(span1, span2);

        txtView.setText(finalText);

    }


    private void serviceListOfFeatureProducts() {
        arryProductList.clear();
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllProducts(
                Util.API_GET_PRODUCT,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
//                "1",
                PAGE_START,
                PAGE_LIMIT);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        try {
                            int totalRecord = Integer.parseInt(jsonResponse.optString("total"));
                            TOTAL_PAGES = (int) Math.ceil(totalRecord / (float) PER_PAGE_COUNT);
                        } catch (Exception e) {
                            e.printStackTrace();

                        }

                        if (jsonArray != null && jsonArray.length() > 0) {

                            for (int header = 0; header < jsonArray.length(); header++) {
                                SpinnerModel spinnerModel = new SpinnerModel();
                                spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                                spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                                spinnerModel.setProductPrice(jsonArray.optJSONObject(header).optString("unit_price"));
                                spinnerModel.setImage_url(jsonArray.optJSONObject(header).optString("image_url"));
                                arryProductList.add(spinnerModel);

                            }

                            mAdapter = new FeatureOffersAdapter(CustomerCountersActivity.this, arryProductList);
                            // right direction you can change according to requirement.
                            sliderView.setAutoCycleDirection(SliderView.LAYOUT_DIRECTION_LTR);
                            // below method is used to
                            // setadapter to sliderview.
                            sliderView.setSliderAdapter(mAdapter);

                            // below method is use to set
                            // scroll time in seconds.
                            sliderView.setScrollTimeInSec(3);

                            // to set it scrollable automatically
                            // we use below method.
                            sliderView.setAutoCycle(true);

                            // to start autocycle below method is used.
                            sliderView.startAutoCycle();
                            txtFeatureProduct.setVisibility(View.GONE);
                            sliderView.setVisibility(View.VISIBLE);


//                        if (!productListFromOrders.isEmpty() && productListFromOrders.size() > 0) {
//                            for (int i = 0; i < productListFromOrders.size(); i++) {
//                                for (int j = 0; j < arryProductList.size(); j++) {
//                                    if (arryProductList.get(j).getRouteId().equals(productListFromOrders.get(i).getRouteId())) {
//                                        arryProductList.get(j).setQuantity(productListFromOrders.get(i).getQuantity());
//                                    }
//                                }
//                            }
//                        }

//                        mAdapter.notifyDataSetChanged();
                        } else {
                            txtFeatureProduct.setVisibility(View.VISIBLE);
                            sliderView.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    txtFeatureProduct.setVisibility(View.VISIBLE);
                    sliderView.setVisibility(View.GONE);
                    Toast.makeText(CustomerCountersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                txtFeatureProduct.setVisibility(View.VISIBLE);
                sliderView.setVisibility(View.GONE);
                Toast.makeText(CustomerCountersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfBrands() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getBrandsList(
                Util.API_BRAND_LIST,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id))
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

//                        try {
//                            int totalRecord = Integer.parseInt(jsonResponse.optString("total"));
//                            TOTAL_PAGES = (int) Math.ceil(totalRecord / (float) PER_PAGE_COUNT);
//                        } catch (Exception e) {
//
//                        }

                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int header = 0; header < jsonArray.length(); header++) {
                                SpinnerModel spinnerModel = new SpinnerModel();
                                spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                                spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                                if (jsonArray.optJSONObject(header).optString("image_url").length() > 0) {
                                    spinnerModel.setImage_url(jsonArray.optJSONObject(header).optString("image_url"));
                                }
                                //spinnerModel.setProductPrice(jsonArray.optJSONObject(header).optString("unit_price"));
                                arryBrand.add(spinnerModel);
                            }
                            brandAdapter = new BrandCustomerAdapter(CustomerCountersActivity.this, arryBrand);
                            recycleBrand.setAdapter(brandAdapter);
                            recycleBrand.setVisibility(View.VISIBLE);
                            txtNoBrand.setVisibility(View.GONE);
                        } else {
                            recycleBrand.setVisibility(View.GONE);
                            txtNoBrand.setVisibility(View.VISIBLE);
                        }
                    }

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    recycleBrand.setVisibility(View.GONE);
                    txtNoBrand.setVisibility(View.VISIBLE);
                    Toast.makeText(CustomerCountersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                recycleBrand.setVisibility(View.GONE);
                txtNoBrand.setVisibility(View.VISIBLE);
                Toast.makeText(CustomerCountersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfCategory() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getBrandsList(
                Util.API_CATEGORY_LIST,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id))
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

//                        try {
//                            int totalRecord = Integer.parseInt(jsonResponse.optString("total"));
//                            TOTAL_PAGES = (int) Math.ceil(totalRecord / (float) PER_PAGE_COUNT);
//                        } catch (Exception e) {
//
//                        }

                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int header = 0; header < jsonArray.length(); header++) {
                                SpinnerModel spinnerModel = new SpinnerModel();
                                spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                                spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                                jsonArray.optJSONObject(header).optString("image_url");
                                if (jsonArray.optJSONObject(header).optString("image_url").length() > 0) {
                                    spinnerModel.setImage_url(jsonArray.optJSONObject(header).optString("image_url"));
                                }
                                //spinnerModel.setProductPrice(jsonArray.optJSONObject(header).optString("unit_price"));
                                arryCategory.add(spinnerModel);
                            }

                            categoryAdapter = new CategoryCustomerAdapter(CustomerCountersActivity.this, arryCategory);
                            recycleCategory.setAdapter(categoryAdapter);
                            txtNoCategory.setVisibility(View.GONE);
                            recycleCategory.setVisibility(View.VISIBLE);
                        } else {
                            txtNoCategory.setVisibility(View.VISIBLE);
                            recycleCategory.setVisibility(View.GONE);
                        }
                    }

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    txtNoCategory.setVisibility(View.VISIBLE);
                    recycleCategory.setVisibility(View.GONE);
                    Toast.makeText(CustomerCountersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                txtNoCategory.setVisibility(View.VISIBLE);
                recycleCategory.setVisibility(View.GONE);
                Toast.makeText(CustomerCountersActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}