package com.jaldhara.view.customer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.AddOrderSelectionAdapter;
import com.jaldhara.models.DriverCommissionModel;
import com.jaldhara.models.SpinnerModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.PaginationScrollListener;
import com.jaldhara.utills.Util;

import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerWishListActivity extends Activity {

    //-----Pagination variables
    private static final int PER_PAGE_COUNT = 20;
    private ArrayList<SpinnerModel> arryOrders;
    private RecyclerView mRecyclerView;
    private WishlistCustomerAdapter selectionAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private boolean isLastPage = false;
    private int TOTAL_PAGES = 0;
    private int CURRENT_PAGE = 1;
    private int PAGE_START = 1;
    private int PAGE_LIMIT = 20;
    private TextView txtNoRecord;

    private EditText searchText;
    private TextView txtClear;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_wishlist);


        mRecyclerView = (RecyclerView) findViewById(R.id.fragment_admin_add_orders_recycler_view);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(linearLayoutManager);

        arryOrders = new ArrayList<SpinnerModel>();

        ImageView imgBtnBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);

        selectionAdapter = new WishlistCustomerAdapter(CustomerWishListActivity.this, arryOrders);
        mRecyclerView.setAdapter(selectionAdapter);

        txtNoRecord = findViewById(R.id.txtNoRecord);


        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);
        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);


        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);
                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        if (ConnectivityReceiver.isConnected()) {
            serviceListOfProducts();
        } else {
            Snackbar.make(getCurrentFocus(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }


//        mRecyclerView.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
//            @Override
//            protected void loadMoreItems() {
//                CURRENT_PAGE += 1;
//                PAGE_START += 20;
//
//                Log.v("addOnScrollListener", "addOnScrollListener*************   " + CURRENT_PAGE);
//                Log.v("addOnScrollListener", "Start*************   " + PAGE_START);
//                Log.v("addOnScrollListener", "limit*************   " + PAGE_LIMIT);
//
//                if (ConnectivityReceiver.isConnected()) {
//                    serviceListOfProducts();
//                } else {
//                    Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
//                }
//            }
//
//            @Override
//            public boolean isLastPage() {
//                return isLastPage;
//            }
//
//            @Override
//            public boolean isLoading() {
//                return false;
//            }
//        });


    }


    void filter(String text) {
        ArrayList<SpinnerModel> temp = new ArrayList();

        if (arryOrders != null && arryOrders.size() > 0) {
            for (SpinnerModel d : arryOrders) {
                if (d.getRouteName().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }
            if (temp != null && temp.size() > 0) {
                //update recyclerview
                txtNoRecord.setVisibility(View.GONE);
                mRecyclerView.setVisibility(View.VISIBLE);
                selectionAdapter.setRefreshList(temp);
            } else {
                txtNoRecord.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }

    /***************************************************************************************
     * List of Products -- List of Products  -- List of Products -- List of Products  -- List of Products
     ***************************************************************************************/

    private void serviceListOfProducts() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Products", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getWishList(
                Util.API_GET_WISHLIST,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id))
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");
                        if (jsonArray != null && jsonArray.length() > 0) {
                            for (int header = 0; header < jsonArray.length(); header++) {
                                SpinnerModel spinnerModel = new SpinnerModel();
                                spinnerModel.setRouteName(jsonArray.optJSONObject(header).optString("name"));
                                spinnerModel.setRouteId(jsonArray.optJSONObject(header).optString("id"));
                                spinnerModel.setProductPrice(jsonArray.optJSONObject(header).optString("unit_price"));
                                arryOrders.add(spinnerModel);
                            }
                        }

                        if (arryOrders != null && arryOrders.size() > 0) {
                            selectionAdapter = new WishlistCustomerAdapter(CustomerWishListActivity.this, arryOrders);
                            mRecyclerView.setAdapter(selectionAdapter);
                            mRecyclerView.setVisibility(View.VISIBLE);
                            txtNoRecord.setVisibility(View.GONE);
                        } else {
                            mRecyclerView.setVisibility(View.GONE);
                            txtNoRecord.setVisibility(View.VISIBLE);
                        }


//                        selectionAdapter.notifyDataSetChanged();

//                        if (!(CURRENT_PAGE <= TOTAL_PAGES)) {
//                            isLastPage = true;
//                        }
//                        if (CURRENT_PAGE == TOTAL_PAGES) {
//                            isLastPage = true;
//                        }

                    } else {
                        mRecyclerView.setVisibility(View.GONE);
                        txtNoRecord.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CustomerWishListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                mRecyclerView.setVisibility(View.GONE);
                txtNoRecord.setVisibility(View.VISIBLE);
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CustomerWishListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
