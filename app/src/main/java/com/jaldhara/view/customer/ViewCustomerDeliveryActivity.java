package com.jaldhara.view.customer;


import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.SeeExpandOrderAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;
import com.jaldhara.utills.onRemoveClick;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewCustomerDeliveryActivity extends Activity implements onRemoveClick {

    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 1;
    private String[] storage_permissions =
            {
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };


    //    private RecyclerView mRecyclerView;
//    private BottleDeliveryListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    // private ArrayList<DeliveryModel> arrayListBottleReturn;
    private onRemoveClick removeClickListener;
    //    public static String KEY_CUSTOMER_INFO = "CUSTOMER_INFO";
    ArrayList<DeliveryModel> arryBottleBrands;
    private RelativeLayout rlCalander;

    private TextView txtLbldelivery;
    private Dialog dialogFilter;
    private String strDialogFromDate = "";
    private String strDialogTodate = "";

    private SeeExpandOrderAdapter listAdapter;
    private ExpandableListView expListView;
    private String customerID = "";
    private String fromText = "";
    private String txtTO = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewcustomerdelivery);


//        mRecyclerView = (RecyclerView) findViewById(R.id.fragment_bottles_return_recycler_view);
        expListView = (ExpandableListView) findViewById(R.id.fragment_bottles_return_recycler_view);

        txtLbldelivery = (TextView) findViewById(R.id.lbl_nodelivery);

        arryBottleBrands = new ArrayList<DeliveryModel>();

//        mRecyclerView.setAdapter(mAdapter);
        ImageView imgBtnBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        Util.hideKeyboard(ViewCustomerDeliveryActivity.this);


        BottleInfoModel bottleInfoModel = getIntent().getParcelableExtra(getString(R.string.key_intent_customer_id));
        customerID = bottleInfoModel.getCustomerID();


        if (ConnectivityReceiver.isConnected()) {

            //pass From date and to date same for first time.
            serviceGetOrder(customerID, Util.getTodayDate(), Util.getTodayDate());

        } else {
            Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
        setFromToDate();
    }


    private void setFromToDate() {


        Calendar calendar = Util.getCalDateFromString(Util.getTodayDate());

        View layoutFrom = (View) findViewById(R.id.ll_Date_from);
        final TextView txtDayFrom = (TextView) layoutFrom.findViewById(R.id.raw_date_day);
        final TextView txtMonthFrom = (TextView) layoutFrom.findViewById(R.id.raw_date_month);
        final TextView txtYearFrom = (TextView) layoutFrom.findViewById(R.id.raw_date_year);

        String strMonth = String.valueOf(calendar.get(Calendar.MONTH) + 1).length() == 2 ? String.valueOf(calendar.get(Calendar.MONTH) + 1) : ("0" + (calendar.get(Calendar.MONTH) + 1));
        String strDay = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)).length() == 2 ? String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)) : ("0" + calendar.get(Calendar.DAY_OF_MONTH));


        txtDayFrom.setText(strDay);
        txtMonthFrom.setText("" + Util.getMonth().get(calendar.get(Calendar.MONTH)));
        txtYearFrom.setText("" + calendar.get(Calendar.YEAR));

        fromText = txtTO = txtYearFrom.getText().toString() + "-" + strMonth + "-" + strDay;

        View layoutTo = (View) findViewById(R.id.ll_Date_to);
        final TextView txtDayTo = (TextView) layoutTo.findViewById(R.id.raw_date_day);
        final TextView txtMonthTo = (TextView) layoutTo.findViewById(R.id.raw_date_month);
        final TextView txtYearTo = (TextView) layoutTo.findViewById(R.id.raw_date_year);

        txtDayTo.setText(strDay);
        txtMonthTo.setText("" + Util.getMonth().get(calendar.get(Calendar.MONTH)));
        txtYearTo.setText("" + calendar.get(Calendar.YEAR));


        layoutFrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewCustomerDeliveryActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day) {
                        String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                        String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);

                        fromText = year + "-" + strMonth + "-" + strDay;

                        txtDayFrom.setText(strDay);
                        txtMonthFrom.setText("" + Util.getMonth().get(month));
                        txtYearFrom.setText("" + year);

                        serviceGetOrder(customerID, fromText, txtTO);

                    }
                }, Integer.valueOf(txtYearFrom.getText().toString()), Util.getMonthFromName().get(txtMonthFrom.getText().toString()), Integer.valueOf(txtDayFrom.getText().toString()));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });

        layoutTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewCustomerDeliveryActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker arg0, int year, int month, int day) {

                        String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
                        String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);

                        txtTO = year + "-" + strMonth + "-" + strDay;

                        txtDayTo.setText(strDay);
                        txtMonthTo.setText("" + Util.getMonth().get(month));
                        txtYearTo.setText("" + year);

                        serviceGetOrder(customerID, fromText, txtTO);

                    }
                }, Integer.valueOf(txtYearTo.getText().toString()), Util.getMonthFromName().get(txtMonthTo.getText().toString()), Integer.valueOf(txtDayTo.getText().toString()));
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();
            }
        });
    }


//    /**
//     * @param v
//     */
//    public void onClickFilterApply(View v) {
//        dialogFilter = new Dialog(ViewCustomerDeliveryActivity.this);
//        dialogFilter.setContentView(R.layout.dialog_order_filter);
//
//        final TextView txtTO = dialogFilter.findViewById(R.id.dialog_filter_txtTO);
////        final TextView txtFrom = dialogFilter.findViewById(R.id.dialog_filter_txtFrom);
//        (dialogFilter.findViewById(R.id.dialog_filter_SpnCustomer)).setVisibility(View.GONE);
//        (dialogFilter.findViewById(R.id.lblSpn)).setVisibility(View.GONE);
//        Button btnApply = dialogFilter.findViewById(R.id.dialog_filter_btnApply);
//
//
////        txtFrom.setText(TextUtils.isEmpty(strDialogFromDate) ? Util.getTodayDate() : strDialogFromDate);
//        txtTO.setText(TextUtils.isEmpty(strDialogTodate) ? Util.getTodayDate() : strDialogTodate);
//
//        txtTO.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                openDialogForDate(txtTO);
//
//                Calendar calendar = Util.getCalDateFromString(txtTO.getText().toString());
//
//                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewCustomerDeliveryActivity.this, new DatePickerDialog.OnDateSetListener() {
//                    @Override
//                    public void onDateSet(DatePicker arg0, int year, int month, int day) {
//
//                        String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
//                        String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);
//
//                        txtTO.setText(year + "-" + strMonth + "-" + strDay);
//                    }
//                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
//                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
//                datePickerDialog.show();
//            }
//        });
//
////        txtFrom.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////
////                Calendar calendar = Util.getCalDateFromString(txtFrom.getText().toString());
////
////                DatePickerDialog datePickerDialog = new DatePickerDialog(ViewCustomerDeliveryActivity.this, new DatePickerDialog.OnDateSetListener() {
////                    @Override
////                    public void onDateSet(DatePicker arg0, int year, int month, int day) {
////                        String strMonth = String.valueOf(month + 1).length() == 2 ? String.valueOf(month + 1) : ("0" + (month + 1));
////                        String strDay = String.valueOf(day).length() == 2 ? String.valueOf(day) : ("0" + day);
////
////                        txtFrom.setText(year + "-" + strMonth + "-" + strDay);
////                    }
////                }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
////                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
////                datePickerDialog.show();
////            }
////        });
//
//        btnApply.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (ConnectivityReceiver.isConnected()) {
//
////                    serviceGetOrder(txtFrom.getText().toString(), txtTO.getText().toString());
//                    serviceGetOrder( txtTO.getText().toString());
//
////                    strDialogFromDate = txtFrom.getText().toString();
//                    strDialogTodate = txtTO.getText().toString();
//
//                } else {
//                    Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
//                }
//            }
//        });
//
//        dialogFilter.show();
//
//    }

    @Override
    public void onClick(int position) {
        Log.v("TAG", "click working on : " + position);
        //arrayListBottleReturn.remove(position);
        //arrayListBottleReturn.trimToSize();
        //mAdapter = new BottleDeliveryListAdapter(ViewCustomerDeliveryActivity.this, arrayListBottleReturn);
        // mAdapter.setRemoveClickListener(this);
        // mRecyclerView.setAdapter(mAdapter);

    }


    private void serviceGetOrder(String CustomerID, String StartDate, String EndDate) {
        final ProgressDialog dialog = ProgressDialog.show(ViewCustomerDeliveryActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.SeeOrdersPagination(
                "get_order",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                CustomerID,
                "items",
                "",
                "",
                "",
                StartDate,
                EndDate,
                1,
                500);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();

                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArrayDelivery = jsonResponse.optJSONArray("data");
                        fillDeliveryItems(jsonArrayDelivery);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(ViewCustomerDeliveryActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();

                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(ViewCustomerDeliveryActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fillDeliveryItems(JSONArray jsonArray) {

        List<DeliveryModel> listDataHeader = new ArrayList<DeliveryModel>();
        HashMap<DeliveryModel, List<DeliveryModel>> listDataChild = new HashMap<DeliveryModel, List<DeliveryModel>>();

        if (jsonArray == null || jsonArray.length() == 0) {
            Toast.makeText(ViewCustomerDeliveryActivity.this, "No Delivery Items found!", Toast.LENGTH_SHORT).show();

//            if (arryBottleBrands.isEmpty()) {
            txtLbldelivery.setVisibility(View.VISIBLE);
            expListView.setVisibility(View.GONE);
//            } else {
//                txtLbldelivery.setVisibility(View.GONE);
//                expListView.setVisibility(View.VISIBLE);
//           }
            return;
        }


        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.optJSONObject(i);

            String orderID = jsonObject.optString("id");
            String orderDate = jsonObject.optString("delivery_date");

            JSONArray arrayItems = jsonObject.optJSONArray("items");
            ////////////////////////////////////////////////

            DeliveryModel deliveryModelHeader = new DeliveryModel();
            deliveryModelHeader.setOrderDate(orderDate);
            deliveryModelHeader.setOrderID(orderID);
            deliveryModelHeader.setIs_finish(jsonObject.optString("is_finish"));
            deliveryModelHeader.setSale_reference_no(jsonObject.optString("reference_no"));
            deliveryModelHeader.setBottleCount(arrayItems.length() + "");
            deliveryModelHeader.setSales_id(jsonObject.optString("id"));
            deliveryModelHeader.setOrder_tax_id(jsonObject.optString("order_tax_id"));
            deliveryModelHeader.setWarehouse_id(jsonObject.optString("warehouse_id"));
            deliveryModelHeader.setBiller_id(jsonObject.optString("biller_id"));
            deliveryModelHeader.setSale_status(jsonObject.optString("sale_status"));


            listDataHeader.add(deliveryModelHeader);
            ///////////////////////////////////////////////


            final ArrayList<DeliveryModel> array = new ArrayList<>();
            for (int j = 0; j < arrayItems.length(); j++) {

                DeliveryModel deliveryModel = new DeliveryModel();

                JSONObject jobjItems = arrayItems.optJSONObject(j);
                deliveryModel.setProduct_code(jobjItems.optString("product_code"));
                deliveryModel.setProduct_id(jobjItems.optString("product_id"));
                deliveryModel.setQuantity(jobjItems.optString("quantity"));
                deliveryModel.setPendingQty(jobjItems.optString("pending_qty"));
                deliveryModel.setUnit_price(jobjItems.optString("unit_price"));
                deliveryModel.setProduct_name(jobjItems.optString("product_name"));


                deliveryModel.setCgst(jobjItems.optString("cgst"));
                deliveryModel.setComment(jobjItems.optString("comment"));
                deliveryModel.setDiscount(jobjItems.optString("discount"));
                deliveryModel.setGst(jobjItems.optString("gst"));
                deliveryModel.setIgst(jobjItems.optString("igst"));

                deliveryModel.setItem_discount(jobjItems.optString("item_discount"));
                deliveryModel.setItem_tax(jobjItems.optString("item_tax"));
                deliveryModel.setNet_unit_price(jobjItems.optString("net_unit_price"));
                deliveryModel.setProduct_type(jobjItems.optString("product_type"));
                deliveryModel.setProduct_unit_code(jobjItems.optString("product_unit_code"));

                deliveryModel.setProduct_unit_id(jobjItems.optString("product_unit_id"));
                deliveryModel.setProduct_unit_quantity(jobjItems.optString("product_unit_quantity"));
                deliveryModel.setSerial_no(jobjItems.optString("serial_no"));
                deliveryModel.setSgst(jobjItems.optString("sgst"));
                deliveryModel.setSubtotal(jobjItems.optString("subtotal"));

                deliveryModel.setTax(jobjItems.optString("tax"));
                deliveryModel.setTax_rate_id(jobjItems.optString("tax_rate_id"));

                try {

                    double pendingQty = Double.valueOf(deliveryModel.getPendingQty());
                    int pendingQtyint = ((int) pendingQty);
                    deliveryModel.setPendingQty("" + pendingQtyint);
                    deliveryModel.setDelivered("" + pendingQtyint);

                } catch (NumberFormatException NumExe) {
                    deliveryModel.setPendingQty("0");
                    deliveryModel.setDelivered("0");
                } catch (Exception e) {
                    e.printStackTrace();
                }
                array.add(deliveryModel);
            }

            listDataChild.put(listDataHeader.get(i), array);

        }
        listAdapter = new SeeExpandOrderAdapter(ViewCustomerDeliveryActivity.this, listDataHeader, listDataChild);
        expListView.setAdapter(listAdapter);
        txtLbldelivery.setVisibility(View.GONE);
        expListView.setVisibility(View.VISIBLE);
    }
}
