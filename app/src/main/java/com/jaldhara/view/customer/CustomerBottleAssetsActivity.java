package com.jaldhara.view.customer;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.AssetsListAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerBottleAssetsActivity extends Activity {

    private RecyclerView mRecyclerView;
    private AssetsListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    public static String KEY_CUSTOMER_INFO = "CUSTOMER_INFO";
    private ArrayList<DeliveryModel> arryBottleAssets;
    private TextView txtLbl;

    private EditText searchText;
    private TextView txtClear;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_bottle_assets);

        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        txtLbl = (TextView) findViewById(R.id.lbl_nodelivery);
        arryBottleAssets = new ArrayList<DeliveryModel>();

        searchText = (EditText) findViewById(R.id.activity_search_cus_edt_search);

        txtClear = (TextView) findViewById(R.id.activity_search_cus_img_clear);

        ImageView imgBtnBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String str = s.toString().toLowerCase().trim();
                filter(str);

                txtClear.setVisibility(str.length() > 0 ? View.VISIBLE : View.GONE);

            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });

        if (ConnectivityReceiver.isConnected()) {
            serviceReturnOrder();
        } else {
            Snackbar.make(mRecyclerView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
    }

    void filter(String text) {
        ArrayList<DeliveryModel> temp = new ArrayList();
        if (arryBottleAssets != null && arryBottleAssets.size() > 0) {
            for (DeliveryModel d : arryBottleAssets) {
                //or use .equal(text) with you want equal match
                //use .toLowerCase() for better matches

                if (d.getProduct_name().toLowerCase().contains(text)) {
                    temp.add(d);
                }
            }
            if (temp != null && temp.size() > 0) {
                mRecyclerView.setVisibility(View.VISIBLE);
                txtLbl.setVisibility(View.GONE);
                //update recyclerview
                mAdapter.updateList(temp);
            } else {
                mRecyclerView.setVisibility(View.GONE);
                txtLbl.setVisibility(View.VISIBLE);
            }
        }
    }

    public void onClickClearSearch(View v) {
        searchText.setText("");
        txtClear.setVisibility(View.GONE);
    }

    /**
     * =============================================================================================
     * Service for get assets
     * =============================================================================================
     */
    private void serviceReturnOrder() {
        final ProgressDialog dialog = ProgressDialog.show(CustomerBottleAssetsActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getOrder(
                Util.API_GET_ORDER,
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                Util.getTodayDate(),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id)),
                "items"
        );

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());

                        arryBottleAssets.clear();
                        if (jsonResponse.has("assets")) {
                            JSONArray jsonArray = jsonResponse.optJSONArray("assets");
                            if (jsonArray != null && jsonArray.length() > 0) {
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.optJSONObject(i);

                                    DeliveryModel deliveryModel = new DeliveryModel();
                                    deliveryModel.setSales_id(jsonObject.optString("id"));
                                    deliveryModel.setProduct_name(jsonObject.optString("name"));

                                    arryBottleAssets.add(deliveryModel);
                                }
                            }
                        }
                    }

                    if (arryBottleAssets.isEmpty()) {
                        txtLbl.setVisibility(View.VISIBLE);
                        mRecyclerView.setVisibility(View.GONE);
                    } else {
                        mAdapter = new AssetsListAdapter(CustomerBottleAssetsActivity.this, arryBottleAssets);
                        mRecyclerView.setAdapter(mAdapter);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(CustomerBottleAssetsActivity.this, "Please try after sometime!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(CustomerBottleAssetsActivity.this, "Please try after sometime!", Toast.LENGTH_SHORT).show();

                txtLbl.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);

            }
        });
    }


}
