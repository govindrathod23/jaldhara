package com.jaldhara.view;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.job.JobParameters;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.models.DeliveryModel;
import com.jaldhara.models.Locations;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.services.LocationJobService;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.DirectionsJSONParser;
import com.jaldhara.utills.Util;
import com.jaldhara.view.customer.order.CustomerOrdersListActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteMapActivity extends BestLocationProvider implements OnMapReadyCallback,
        GoogleMap.OnMapClickListener, ValueEventListener, GoogleMap.OnInfoWindowClickListener {

    final Handler handler = new Handler();
    ArrayList markerPoints = new ArrayList();
    ArrayList<BottleInfoModel> arryCustomer = new ArrayList<>();
    Runnable runnable;
    HashSet<String> arryDriverIDs = new HashSet<>();

    @Override
    public void onConnected(Bundle arg0) {
        super.onConnected(arg0);
    }

    /**
     * *****************************************************************************************
     * Method call for ADMIN role -- getl.
     * *****************************************************************************************
     */
    ProgressDialog dialogForCustomerLocation;
    /**
     * *****************************************************************************************
     * Method call for ADMIN role -- getl .
     * *****************************************************************************************
     */
    ProgressDialog dialogForDrivers;
    private GoogleMap mMap;
    private JobParameters jobParams;
    private LocationJobService jobService;
    private LatLng latLongClicked;
    private HashMap<String, Marker> arryMarkers = new HashMap<String, Marker>();
    private Button btnViewCustomer;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routemap);

        ImageView imgBack = (ImageView) findViewById(R.id.activity_bottle_settlement_tab_imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnViewCustomer = (Button) findViewById(R.id.activity_routeMap_btn_visibleCustomer);


    }

    @Override
    public void onStart() {
        super.onStart();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                connectGoogleClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            connectGoogleClient();
            mMap.setMyLocationEnabled(true);
        }


        mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                String methodMessage = isGPSsettingDone();
                if (!methodMessage.equalsIgnoreCase("AllDone")) {
                    showAlertdialog("Alert!", methodMessage);
                }

                return false;
            }
        });

        mMap.clear();


        if (ConnectivityReceiver.isConnected()) {

            if (getIntent().getStringExtra(getString(R.string.key_intent_Show_On_Map_For)).equalsIgnoreCase("Admin")) {
                //serviceListOfCustomer();
                //serviceListOfDriversLocation(true);
                //above methods are commented, instead firebase live data is implemented for get driver location

                DatabaseReference databaseReferenceLocations = FirebaseDatabase.getInstance().getReference("Locations");
                databaseReferenceLocations.addValueEventListener(RouteMapActivity.this);
                databaseReferenceLocations.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                        for (DataSnapshot locationData : dataSnapshot.getChildren()) {

//                            Locations locationsModel = locationData.getValue(Locations.class);
//                            Toast.makeText(RouteMapActivity.this,"Chiled Count is single  "+dataSnapshot.getChildrenCount(),Toast.LENGTH_LONG).show();
//                                Log.v("get live database",
//                                        "**************************** "+locationsModel.getDriverID()
//                                        + "  : " + locationsModel.getLatitude()
//                                        + "  :  " + locationsModel.getLongitude());

                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });


            } else if (getIntent().getStringExtra(getString(R.string.key_intent_Show_On_Map_For)).equalsIgnoreCase("Customer")) {


                //With using this service we will get all orders of client and get the drivers into that order for showing on map
                serviceGetCustomerOrder();
                // this will set the customer location it self when in case of customer only
                BottleInfoModel infoModel = new BottleInfoModel();
                infoModel.setLatitude(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_latitude)));
                infoModel.setLongitude(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_longitude)));
                infoModel.setCompany("I am Here!");
                arryCustomer.add(infoModel);
                showCustomersOnMap(arryCustomer, R.drawable.ic_home_map_marker);
                //THis approch is not in use
//                serviceLocationForCustomer(true);

            } else if (getIntent().getStringExtra(getString(R.string.key_intent_Show_On_Map_For)).equalsIgnoreCase("FromCustomer")) {
                arryCustomer = getIntent().getParcelableArrayListExtra("CustomerArray");
                showCustomersOnMap(arryCustomer, R.drawable.ic_home_map_marker);

                //Below intent is comes from --CustomerFromRouteAdapter--
            } else if (getIntent().getStringExtra(getString(R.string.key_intent_Show_On_Map_For)).equalsIgnoreCase("FromCustomerChild")) {
                BottleInfoModel bottleInfoModel = getIntent().getParcelableExtra("CustomerArray");
                arryCustomer.add(bottleInfoModel);
                showCustomersOnMap(arryCustomer, R.drawable.ic_home_map_marker);

            } else if (getIntent().getStringExtra(getString(R.string.key_intent_Show_On_Map_For)).equalsIgnoreCase("getLatLong")) {
                Button btnLatLong = (Button) findViewById(R.id.activity_routeMap_btn_latlong);
                btnLatLong.setVisibility(View.VISIBLE);
                mMap.setOnMapClickListener(RouteMapActivity.this);
            }

        } else {
            Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }

        mMap.setOnInfoWindowClickListener(this);


//        }

        /*******************************************************************************************
         *
         ******************************************************************************************/
//        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
//            @Override
//            public void onMapClick(LatLng latLng) {
//
//                if (markerPoints.size() > 1) {
//                    markerPoints.clear();
//                    mMap.clear();
//                }
//
//                // Adding new item to the ArrayList
//                markerPoints.add(latLng);
//
//                // Creating MarkerOptions
//                MarkerOptions options = new MarkerOptions();
//
//                // Setting the position of the marker
//                options.position(latLng);
//
//                if (markerPoints.size() == 1) {
//                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
//                } else if (markerPoints.size() == 2) {
//                    options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//                }
//
//                // Add new marker to the Google Map Android API V2
//                mMap.addMarker(options);
//
//                // Checks, whether start and end locations are captured
//                if (markerPoints.size() >= 2) {
//                    LatLng origin = (LatLng) markerPoints.get(0);
//                    LatLng dest = (LatLng) markerPoints.get(1);
//
//                    // Getting URL to the Google Directions API
//                    String url = getDirectionsUrl(origin, dest);
//
//                    DownloadTask downloadTask = new DownloadTask();
//
//                    // Start downloading json data from Google Directions API
//                    downloadTask.execute(url);
//                }
//
//            }
//        });
    }

    private BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (runnable != null) {
            handler.removeCallbacks(runnable);
        }
    }

    private void showCustomersOnMap(ArrayList<BottleInfoModel> arryCustomer, int drawable) {

//        R.drawable.ic_map_truck_fast
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        mMap.clear();
        boolean hasMarkerData = false;
        for (int i = 0; i < arryCustomer.size(); i++) {

            if (arryCustomer.get(i).getLatitude() != null && !arryCustomer.get(i).getLatitude().equalsIgnoreCase("null") && !TextUtils.isEmpty(arryCustomer.get(i).getLatitude())) {

                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.valueOf(arryCustomer.get(i).getLatitude()), Double.valueOf(arryCustomer.get(i).getLongitude())))
                        .anchor(0.5f, 0.5f)
                        .title(arryCustomer.get(i).getCompany())
                        .snippet(arryCustomer.get(i).getAlias_name())
                        .icon(BitmapDescriptorFactory.fromResource(drawable)));

                builder.include(marker.getPosition());
                hasMarkerData = true;

            } else {

                Snackbar.make(getWindow().getDecorView().getRootView(), "Location not updated for " + arryCustomer.get(i).getCompany(), Snackbar.LENGTH_LONG).show();

            }
        }

        if (hasMarkerData) {
            LatLngBounds bounds = builder.build();
            int width = getResources().getDisplayMetrics().widthPixels;
            int height = getResources().getDisplayMetrics().heightPixels;
            int padding = (int) (width * 0.30); // offset from edges of the map 10% of screen

            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

            mMap.animateCamera(cu);
        }

        // Below method is commented because Firebase live location is useing instedof
//        if (getIntent().getStringExtra(getString(R.string.key_intent_Show_On_Map_For)).equalsIgnoreCase("Admin")) {
//            runnable = new Runnable() {
//                @Override
//                public void run() {
//                    if (ConnectivityReceiver.isConnected()) {
//                        serviceListOfDriversLocation(false);
//                    } else {
//                        Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
//                    }
//                }
//            };
//            handler.postDelayed(runnable, Util.JOB_SCHEDULE_TIME);
//        } else
        // Below method is commented because Firebase live location is useing instedof
//        if (getIntent().getStringExtra(getString(R.string.key_intent_Show_On_Map_For)).equalsIgnoreCase("Customer")) {
//
//            runnable = new Runnable() {
//                @Override
//                public void run() {
//                    if (ConnectivityReceiver.isConnected()) {
//                        serviceLocationForCustomer(false);
//                    } else {
//                        Snackbar.make(getWindow().getDecorView().getRootView(), "Please check your internet connection", Snackbar.LENGTH_LONG).show();
//                    }
//
//                }
//            };
//            handler.postDelayed(runnable, Util.JOB_SCHEDULE_TIME);
//
//        }
    }

    //With using this service we will get all orders of client and get the drivers into that order for showing on map
    private void serviceGetCustomerOrder() {
//        final ProgressDialog dialog = ProgressDialog.show(CustomerOrdersListActivity.this, "", "Loading... ", false, false);
        ApiInterface apiService = ApiClient.getClient( ).create(ApiInterface.class);

        Call<ResponseBody> call = apiService.get_all_order(
                "get_all_order",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id)),
                1,
                500);



        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                dialog.dismiss();

                try {
                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArrayDelivery = jsonResponse.optJSONArray("data");
//
                        for (int i = 0; i < jsonArrayDelivery.length(); i++) {
                            JSONObject jsonObject = jsonArrayDelivery.optJSONObject(i);
                            arryDriverIDs.add(jsonObject.optString("driver")) ;
                        }

                        DatabaseReference databaseReferenceLocations = FirebaseDatabase.getInstance().getReference("Locations");
                        databaseReferenceLocations.addValueEventListener(RouteMapActivity.this);

                    }


                } catch (Exception e) {
//                    Toast.makeText(CustomerOrdersListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                dialog.dismiss();

                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(RouteMapActivity.this, "Unable to get drivers ", Toast.LENGTH_SHORT).show();
            }
        });
    }

//    public void serviceLocationForCustomer(final boolean isFirstTime) {
//
//
//        if (isFirstTime) {
//            dialogForCustomerLocation = ProgressDialog.show(this, "Please wait", "Getting Drivers", false, false);
//        }
//        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
//
//        Call<ResponseBody> call = apiService.getLocationForCustomer("get_lat_long_by_customer_id", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
//                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_company_id)));
//
//        Log.e("Service call :: ", "service call   : " + isFirstTime);
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (isFirstTime) {
//                    dialogForCustomerLocation.dismiss();
//                }
//                try {
//
//                    Log.e("onResponse :: ", "is success : " + response.isSuccessful());
//
//                    if (response.isSuccessful()) {
//                        final String strResponse = response.body().string();
//                        JSONObject jsonResponse = new JSONObject(strResponse);
//                        Log.e("onResponse :: ", "Response  : " + strResponse);
//                        JSONObject jsonObject = jsonResponse.optJSONObject("data");
//
////                        for (int route = 0; route < jsonArray.length(); route++) {
////                            JSONObject jsonCust = jsonArray.optJSONObject(route);
//
//                        if (TextUtils.isEmpty(jsonObject.optString("latitude")) ||
//                                jsonObject.optString("latitude").equalsIgnoreCase("null") ||
//                                TextUtils.isEmpty(jsonObject.optString("longitude")) ||
//                                jsonObject.optString("longitude").equalsIgnoreCase("null")) {
//                            return;
//                        }
//
//
//                        BottleInfoModel infoModel = new BottleInfoModel();
//                        infoModel.setCompany(jsonObject.optString("first_name") + " " + jsonObject.optString("last_name"));
//                        infoModel.setAlias_name(jsonObject.optString("phone"));
//                        infoModel.setLatitude(jsonObject.optString("latitude"));
//                        infoModel.setLongitude(jsonObject.optString("longitude"));
//
//                        arryCustomer.add(infoModel);
//
////                        }
//
//                        showCustomersOnMap(arryCustomer, R.drawable.ic_map_truck_fast);
//
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                } catch (Exception e) {
//                    Toast.makeText(RouteMapActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                if (isFirstTime) {
//                    dialogForCustomerLocation.dismiss();
//                }
//                // Log error here since request failed
//                Log.e("onFailure :: ", t.toString());
//                Toast.makeText(RouteMapActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    public void serviceListOfDriversLocation(final boolean isFirstTime) {


        if (isFirstTime) {
            dialogForDrivers = ProgressDialog.show(this, "Please wait", "Getting Drivers", false, false);
        }
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.getAllDriversLatLong("get_all_lat_long_of_drivers", Util.API_KEY, JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)));

        Log.e("Service call :: ", "service call   : " + isFirstTime);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (isFirstTime) {
                    dialogForDrivers.dismiss();
                }
                try {

                    Log.e("onResponse :: ", "is success : " + response.isSuccessful());

                    if (response.isSuccessful()) {
                        final String strResponse = response.body().string();
                        JSONObject jsonResponse = new JSONObject(strResponse);
                        Log.e("onResponse :: ", "Response  : " + strResponse);
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int route = 0; route < jsonArray.length(); route++) {
                            JSONObject jsonCust = jsonArray.optJSONObject(route);

                            if (TextUtils.isEmpty(jsonCust.optString("latitude")) ||
                                    jsonCust.optString("latitude").equalsIgnoreCase("null") ||
                                    TextUtils.isEmpty(jsonCust.optString("longitude")) ||
                                    jsonCust.optString("longitude").equalsIgnoreCase("null")) {
                                continue;
                            }


                            BottleInfoModel infoModel = new BottleInfoModel();
                            infoModel.setCompany(jsonCust.optString("first_name") + " " + jsonCust.optString("last_name"));
                            infoModel.setAlias_name(jsonCust.optString("phone"));
                            infoModel.setLatitude(jsonCust.optString("latitude"));
                            infoModel.setLongitude(jsonCust.optString("longitude"));

                            arryCustomer.add(infoModel);

                        }

                        showCustomersOnMap(arryCustomer, R.drawable.ic_map_truck_fast);


                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(RouteMapActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                if (isFirstTime) {
                    dialogForDrivers.dismiss();
                }
                // Log error here since request failed
                Log.e("onFailure :: ", t.toString());
                Toast.makeText(RouteMapActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onMapClick(LatLng latLng) {


        Log.e("onMapClick", "markerPoints.size() " + markerPoints.size());
        if (markerPoints.size() >= 1) {
            markerPoints.clear();

            mMap.clear();
        }

        // Adding new item to the ArrayList
        markerPoints.add(latLng);

        // Creating MarkerOptions
        MarkerOptions options = new MarkerOptions();

        // Setting the position of the marker
        options.position(latLng);

        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }
//        else if (markerPoints.size() == 2) {
//            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
//        }

        // Add new marker to the Google Map Android API V2
        mMap.addMarker(options);
        latLongClicked = latLng;

        // Checks, whether start and end locations are captured
//        if (markerPoints.size() >= 2) {
//            LatLng origin = (LatLng) markerPoints.get(0);
//            LatLng dest = (LatLng) markerPoints.get(1);
//
//            // Getting URL to the Google Directions API
//            String url = getDirectionsUrl(origin, dest);
//
//            DownloadTask downloadTask = new DownloadTask();
//
//            // Start downloading json data from Google Directions API
//            downloadTask.execute(url);
//        }


    }


    public void onLatLongCollected(View v) {

        if (latLongClicked != null) {
            Intent intent = new Intent();
            intent.putExtra("Lat", "" + latLongClicked.latitude);
            intent.putExtra("Long", "" + latLongClicked.longitude);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else {
            Snackbar.make(getWindow().getDecorView().getRootView(), "Pick Lat Long to click on map", Snackbar.LENGTH_LONG).show();
        }
    }

    public void onCustomerRemove(View v) {

        for (Iterator<Map.Entry<String, Marker>> it = arryMarkers.entrySet().iterator(); it.hasNext(); ) {
//        Iterator<Map.Entry<String,Marker>> it = arryMarkers.entrySet().iterator();
//            while(it.hasNext()){
            Map.Entry<String, Marker> entry = it.next();
            if (entry.getKey().contains("C")) {
                entry.getValue().remove();
                it.remove();

            }
        }
        btnViewCustomer.setVisibility(View.GONE);
    }


    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        mMap.clear();
//        Toast.makeText(RouteMapActivity.this,"Chiled Count is all  "+dataSnapshot.getChildrenCount(),Toast.LENGTH_LONG).show();

        //first loop will get the data based on company
        for (DataSnapshot companyData : dataSnapshot.getChildren()) {

            //This check will only receive and process own company data.
            String companyIDKey = "CompID:" + JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id));
            if (!companyData.getKey().equalsIgnoreCase(companyIDKey)) {
                continue;
            }

            for (DataSnapshot locationData : companyData.getChildren()) {
//                    Locations locationsModel = CompanyData.getValue(Locations.class);
//                    Log.v("locationData : ","Name:"+ locationsModel.getDriverName());


                Locations locationsModel = locationData.getValue(Locations.class);

                if (locationsModel == null || locationsModel.getLatitude() == null) continue;

//            Log.v("get live database data ", locationsModel.getDriverID()
//                    + "  : " + locationsModel.getLatitude()
//                    + "  :  " + locationsModel.getLongitude());

                if (arryMarkers.get("D:" + locationsModel.getDriverID()) != null) {
                    arryMarkers.get("D:" + locationsModel.getDriverID()).remove();
                    arryMarkers.remove("D:" + locationsModel.getDriverID());

                }


                try {
                    if (!TextUtils.isEmpty(locationsModel.getTime())) {
                        String conDate = Util.formatDateFromDateString(Util.DATE_FORMATE_1, Util.DATE_FORMATE_2, locationsModel.getTime());
                        if (!conDate.equalsIgnoreCase(Util.getTodayDate())) {
                            continue;
                        }
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                    continue;
                }

//                If block is only for get the driver while customer login
                 if (getIntent().hasExtra(getString(R.string.key_intent_Show_On_Map_For))){
                     if (getIntent().getStringExtra(getString(R.string.key_intent_Show_On_Map_For)).equalsIgnoreCase("Customer") && (!arryCustomer.isEmpty())) {
                         if (!arryCustomer.contains(locationsModel.getDriverID())){
                             continue;
                         }
                     }
                 }



//            || locationsModel.getRole().equals(BestLocationProvider.ROLE_PAYMENT_COLLECTOR))
                int bitmapResource = locationsModel.getRole() == null || locationsModel.getRole().equals(BestLocationProvider.ROLE_DRIVER) ? R.drawable.ic_map_truck_fast : R.drawable.ic_payment_collector;
                Marker marker = mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.valueOf(locationsModel.getLatitude()), Double.valueOf(locationsModel.getLongitude())))
                        .anchor(0.5f, 0.5f)
                        .title(locationsModel.getDriverName())
                        .snippet(locationsModel.getTime())
                        .icon(BitmapDescriptorFactory.fromResource(bitmapResource)));


                marker.setTag("D:" + locationsModel.getDriverID());
                arryMarkers.put("D:" + locationsModel.getDriverID(), marker);
                builder.include(marker.getPosition());

            }
        }

//        Log.e("live database count ", "live database count size :"+arryMarkers.size());
        if (arryMarkers.size() == 0) return;

        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.30); // offset from edges of the map 10% of screen

        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

        mMap.animateCamera(cu);

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }


    @Override
    public void onInfoWindowClick(Marker marker) {
        Log.v("onMarkerClick", "onMarkerClick : " + marker);
        String identifier[] = marker.getTag().toString().split(":");
        if (identifier[0].toString().equalsIgnoreCase("D")) {
            serviceListOfCustomerFrom(identifier[1].toString(), marker.getTitle().toString());

        }
    }


    private void serviceListOfCustomerFrom(String driverID, final String driverName) {

        final ProgressDialog dialog = ProgressDialog.show(RouteMapActivity.this, "Please wait", "Getting Customer", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);


        Call<ResponseBody> call = apiService.listOfCustomer(
                "routes_wise_customer",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                driverID);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {
                    if (response.isSuccessful()) {

                        for (Iterator<Map.Entry<String, Marker>> it = arryMarkers.entrySet().iterator(); it.hasNext(); ) {
                            Map.Entry<String, Marker> entry = it.next();
                            if (entry.getKey().contains("C")) {
                                entry.getValue().remove();
                                it.remove();
                            }
                        }


                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");
                        for (int header = 0; header < jsonArray.length(); header++) {
                            JSONArray jsonArrayChild = jsonArray.optJSONObject(header).optJSONArray("customer");
                            LatLngBounds.Builder builder = new LatLngBounds.Builder();

                            for (int child = 0; child < jsonArrayChild.length(); child++) {
                                JSONObject jsonObjectChild = jsonArrayChild.optJSONObject(child);

                                if (TextUtils.isEmpty(jsonObjectChild.optString("latitude"))) {
                                    continue;
                                }

                                Marker marker = mMap.addMarker(new MarkerOptions()
                                        .position(new LatLng(
                                                Double.valueOf(jsonObjectChild.optString("latitude")),
                                                Double.valueOf(jsonObjectChild.optString("longitude"))))
//                                                23.5555,72.0022))
                                        .anchor(0.5f, 0.5f)
                                        .title(jsonObjectChild.optString("company"))
                                        .snippet(driverName + "`s Route")
                                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_home_customer)));


                                marker.setTag("C:" + jsonObjectChild.optString("c_id"));
                                arryMarkers.put("C:" + jsonObjectChild.optString("c_id"), marker);


                                builder.include(marker.getPosition());

                            }
                            btnViewCustomer.setVisibility(View.VISIBLE);

                            LatLngBounds bounds = builder.build();
                            int width = getResources().getDisplayMetrics().widthPixels;
                            int height = getResources().getDisplayMetrics().heightPixels;
                            int padding = (int) (width * 0.30); // offset from edges of the map 10% of screen

                            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);

                            mMap.animateCamera(cu);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
//                    Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
//                Toast.makeText(getActivity(), "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * *****************************************************************************************
     *
     * @param origin
     * @param dest
     * @return *****************************************************************************************
     */
    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode=driving";
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    /**
     * *****************************************************************************************
     * A method to download json data from url
     * *****************************************************************************************
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    /**
     * --Below entire code is for Draw route path which is not currently fully implemented
     * *****************************************************************************************
     * Use for draw route path.
     * *****************************************************************************************
     */
    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();


            parserTask.execute(result);

        }
    }

    /**
     * *****************************************************************************************
     * A class to parse the Google Places in JSON format
     * *****************************************************************************************
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.RED);
                lineOptions.geodesic(true);

            }

// Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }


}
