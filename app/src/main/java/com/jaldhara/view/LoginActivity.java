package com.jaldhara.view;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.AnimationUtils;
import com.jaldhara.utills.ConnectivityReceiver;
import com.jaldhara.utills.Util;

import org.json.JSONObject;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener, View.OnClickListener {


    private EditText edtUser;
    private Button btnLogin;
    private EditText edtPass;
    private ImageView logo;
    private TextView txtSlogan;
    private TextView txtForgotPass;
    private TextView txtDataConnection;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_isLogin)).equals("true")) {
            Intent intent = new Intent(LoginActivity.this, HomeForRolesActivity.class);
            startActivity(intent);
            finish();
        } else {
            initComponet();
        }

    }

    @Override
    protected void onStart() {
        super.onStart();


    }

    @Override
    protected void onResume() {
        super.onResume();

        JalDharaApplication.getInstance().setConnectivityListener((ConnectivityReceiver.ConnectivityReceiverListener) LoginActivity.this);

    }

    public void onClickForgotPass(View v) {

        final Dialog dialog = new Dialog(LoginActivity.this);
        dialog.setContentView(R.layout.dialog_forgotpasswprd);
        final EditText editEmail = (EditText) dialog.findViewById(R.id.dialogforgotpassword_edt_user);
        Button btnSumit = (Button) dialog.findViewById(R.id.dialogforgotpassword_btn_submit);

        btnSumit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(editEmail.getText().toString().trim())) {
                    Toast.makeText(LoginActivity.this, "Please enter Email id", Toast.LENGTH_LONG).show();
                } else {
                    if (ConnectivityReceiver.isConnected()) {
                        callForgotPass(editEmail.getText().toString().trim());
                        dialog.dismiss();
                    } else {
                        Snackbar.make(edtUser, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });


        dialog.show();

    }

    /**
     * Login button Click
     */
    @Override
    public void onClick(View v) {

        if (TextUtils.isEmpty(edtUser.getText().toString().trim())) {
            AnimationUtils.landMe(edtUser, 200);
            Snackbar.make(edtUser, "Please enter UserName.", Snackbar.LENGTH_LONG).show();
            edtUser.requestFocus();
            return;
        } else if (TextUtils.isEmpty(edtPass.getText().toString().trim())) {
            AnimationUtils.landMe(edtPass, 200);
            Snackbar.make(edtUser, "Please enter Password.", Snackbar.LENGTH_LONG).show();
            edtPass.requestFocus();
            return;
        }

        if (ConnectivityReceiver.isConnected()) {
            callLogin();
        } else {
            Snackbar.make(edtUser, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
        }
    }

    private void callLogin() {

        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Login Authedication", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.loginAuth(edtUser.getText().toString(), edtPass.getText().toString(), "login", Util.API_KEY);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();

                if (!response.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "Username or password invalid", Toast.LENGTH_SHORT).show();
                    return;

//                    JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID), "");
//                    JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_user_name), edtUser.getText().toString());
//                    Intent intent = new Intent(LoginActivity.this, HomeForRolesActivity.class);
//                    startActivity(intent);
//                    finish();
                }

                try {
                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    JSONObject jsonData = jsonResponse.optJSONObject("data");
                    //group id : 5 saleas person
                    // 1 : super admin
                    // 2 admin
                    // 3 customer

                    if (jsonResponse.optBoolean("status")) {
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_isLogin), "true");
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_user_name), edtUser.getText().toString());
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_intent_LoginUser_email), jsonData.optString("email"));

                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_intent_LoginUser_UserID), jsonData.optString("user_id"));
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_intent_LoginUser_GroupID), jsonData.optString("group_id"));
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_intent_LoginUser_RouteID), jsonData.optString("route"));

                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_intent_LoginUser_UserName), jsonData.optString("username"));
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_company_id), jsonData.optString("company_id"));

                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_comp_id), jsonData.optString("comp_id"));
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_comp_latitude), jsonData.optString("comp_latitude"));
                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_comp_longitude), jsonData.optString("comp_longitude"));


                        FirebaseCrashlytics.getInstance().setUserId(jsonData.optString("user_id"));

                        Intent intent = new Intent(LoginActivity.this, HomeForRolesActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (Exception e) {
                    Toast.makeText(LoginActivity.this, "Username or password invalid", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();

                //Toast.makeText(LoginActivity.this, "Username or password invalid", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void callForgotPass(String email) {

        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "", false, false);
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.forgotPass("forgot_password",
                Util.API_KEY,
                JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_comp_id)),
                email);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();

                if (!response.isSuccessful()) {
                    Toast.makeText(LoginActivity.this, "Username or password invalid", Toast.LENGTH_SHORT).show();
                    return;
                }

                try {
                    JSONObject jsonResponse = new JSONObject(response.body().string());
                    JSONObject jsonData = jsonResponse.optJSONObject("message");
                    Toast.makeText(LoginActivity.this, "" + jsonData, Toast.LENGTH_SHORT).show();
                } catch (Exception e) {
                    Toast.makeText(LoginActivity.this, "Username or password invalid", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                Log.e("", t.toString());
                Toast.makeText(LoginActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void initComponet() {
        edtUser = (EditText) findViewById(R.id.activity_login_edt_user);
        edtPass = (EditText) findViewById(R.id.activity_login_edt_pass);
        btnLogin = (Button) findViewById(R.id.activity_login_btn_login);

        btnLogin.setOnClickListener(this);
        logo = (ImageView) findViewById(R.id.img_logo);
        txtSlogan = (TextView) findViewById(R.id.txt_slogan);
        txtForgotPass = (TextView) findViewById(R.id.activity_login_txt_forgotPass);
        LinearLayout llRnDLogo = (LinearLayout) findViewById(R.id.llLogo);
        txtDataConnection = (TextView) findViewById(R.id.activity_login_txt_dataConnection);

        AnimationUtils.showMe(logo, 300);
        AnimationUtils.enterTop(txtSlogan, 300);
        AnimationUtils.popOut(txtSlogan, 300);
        AnimationUtils.enterBottom(edtUser, 500);
        AnimationUtils.enterBottom(edtPass, 600);
        AnimationUtils.enterBottom(btnLogin, 800);
        AnimationUtils.enterBottom(txtForgotPass, 900);
        AnimationUtils.enterBottom(llRnDLogo, 1000);
        AnimationUtils.rotateY(logo, 1150);


        ((TextView) findViewById(R.id.app_version)).setText("Version: " + Util.getAppVersionName(LoginActivity.this));

    }


//    private void callLogin() {
//
//        WebServiceCall webServiceCall = new WebServiceCall(LoginActivity.this);
//        final ProgressDialog progressDialog = ProgressDialog.show(LoginActivity.this, "", "Logging in...", true, false);
//        webServiceCall.callLogin(edtUser.getText().toString(), edtPass.getText().toString()).enqueue(new Callback() {
//            @Override
//            public void onFailure(Call call, IOException e) {
//                Log.v("onFailure", "onFailure*************");
//                Log.v("onFailure", "*****  " + e.getMessage().toString());
//                if (progressDialog != null && progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//            }
//
//            @Override
//            public void onResponse(Call call, Response response) throws IOException {
//
//                final String strResponse = response.body().string();
//                Log.v("onResponse", "Response*************" + strResponse);
//                if (response.isSuccessful() && response.code() == 200) {
//                    try {
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//
//                                JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_u), edtUser.getText().toString());
//                                JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_p), edtPass.getText().toString());
//
//                                Intent intent = new Intent(LoginActivity.this, HomeForRolesActivity.class);
//                                startActivity(intent);
//                                finish();
//
//                            }
//                        });
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                }
//                if (progressDialog != null && progressDialog.isShowing()) {
//                    progressDialog.dismiss();
//                }
//            }
//        });
//    }


    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {
            txtDataConnection.setVisibility(View.GONE);
        } else {
            txtDataConnection.setVisibility(View.VISIBLE);
        }

    }
}