package com.jaldhara.models;

public class DeliveryModel {


    private String sales_id = "";
    private String sale_reference_no = "";
    private String product_code = "";
    private String product_id = "";
    private String quantity = "";
    private String unit_price = "";
    private String adjustmentQuantity = "";

    public String getInvoice_id() {
        return invoice_id;
    }

    public void setInvoice_id(String invoice_id) {
        this.invoice_id = invoice_id;
    }

    private String invoice_id = "";


    private String orderID = "";
    private String orderDate = "";

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    private String order_tax_id = "";
    private String warehouse_id = "";
    private String biller_id = "";
    private String sale_status = "";
    private String product_name = "";

    public String getBillerName() {
        return billerName;
    }

    public void setBillerName(String billerName) {
        this.billerName = billerName;
    }

    private String billerName = "";

    private String cgst = "";
    private String comment = "";
    private String discount = "";
    private String gst = "";
    private String igst = "";



    private String item_discount = "";
    private String item_tax = "";
    private String net_unit_price = "";
    private boolean isSwitchOn = false;

    public boolean isSwitchOn() {
        return isSwitchOn;
    }

    public void setSwitchOn(boolean switchOn) {
        isSwitchOn = switchOn;
    }

    public String getIs_finish() {
        return is_finish;
    }

    public void setIs_finish(String is_finish) {
        this.is_finish = is_finish;
    }

    private String is_finish = "0";

    public String getBottleCount() {
        return bottleCount;
    }

    public void setBottleCount(String bottleCount) {
        this.bottleCount = bottleCount;
    }

    private String bottleCount = "";

    private String product_type = "";
    private String product_unit_code = "";
    private String product_unit_id = "";
    private String product_unit_quantity = "";
    private String serial_no = "";
    private String sgst = "";
    private String subtotal = "";
    private String tax = "";
    private String tax_rate_id = "";

    private String pendingQty = "";
    private String delivered = "";

    private int saleStatusColor = 0;

    public int getPaymentStatusColor() {
        return paymentStatusColor;
    }

    public void setPaymentStatusColor(int paymentStatusColor) {
        this.paymentStatusColor = paymentStatusColor;
    }

    private int paymentStatusColor = 0;

    public int getSaleStatusColor() {
        return saleStatusColor;
    }

    public void setSaleStatusColor(int saleStatusColor) {
        this.saleStatusColor = saleStatusColor;
    }

    public String getPendingQty() {
        return pendingQty;
    }

    public void setPendingQty(String pendingQty) {
        this.pendingQty = pendingQty;
    }

    public String getDelivered() {
        return delivered;
    }

    public void setDelivered(String delivered) {
        this.delivered = delivered;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    private String customerName = "";

    private String month = "";

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    private String total = "";
    private String total_amount = "";
    private String balance = "";

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String inv_reference_no = "";
    public String invoices_total_amount = "";
    public String invoices_payment_status = "";


    public String getInv_reference_no() {
        return inv_reference_no;
    }

    public void setInv_reference_no(String inv_reference_no) {
        this.inv_reference_no = inv_reference_no;
    }

    public String getInvoices_total_amount() {
        return invoices_total_amount;
    }

    public void setInvoices_total_amount(String invoices_total_amount) {
        this.invoices_total_amount = invoices_total_amount;
    }

    public String getInvoices_payment_status() {
        return invoices_payment_status;
    }

    public void setInvoices_payment_status(String invoices_payment_status) {
        this.invoices_payment_status = invoices_payment_status;
    }



    private String grandTotal = "";


    public String getOrder_tax_id() {
        return order_tax_id;
    }

    public void setOrder_tax_id(String order_tax_id) {
        this.order_tax_id = order_tax_id;
    }

    public String getWarehouse_id() {
        return warehouse_id;
    }

    public void setWarehouse_id(String warehouse_id) {
        this.warehouse_id = warehouse_id;
    }

    public String getBiller_id() {
        return biller_id;
    }

    public void setBiller_id(String biller_id) {
        this.biller_id = biller_id;
    }

    public String getSale_status() {
        return sale_status;
    }

    public void setSale_status(String sale_status) {
        this.sale_status = sale_status;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getGst() {
        return gst;
    }

    public void setGst(String gst) {
        this.gst = gst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getItem_discount() {
        return item_discount;
    }

    public void setItem_discount(String item_discount) {
        this.item_discount = item_discount;
    }

    public String getItem_tax() {
        return item_tax;
    }

    public void setItem_tax(String item_tax) {
        this.item_tax = item_tax;
    }

    public String getNet_unit_price() {
        return net_unit_price;
    }

    public void setNet_unit_price(String net_unit_price) {
        this.net_unit_price = net_unit_price;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getProduct_unit_code() {
        return product_unit_code;
    }

    public void setProduct_unit_code(String product_unit_code) {
        this.product_unit_code = product_unit_code;
    }

    public String getProduct_unit_id() {
        return product_unit_id;
    }

    public void setProduct_unit_id(String product_unit_id) {
        this.product_unit_id = product_unit_id;
    }

    public String getProduct_unit_quantity() {
        return product_unit_quantity;
    }

    public void setProduct_unit_quantity(String product_unit_quantity) {
        this.product_unit_quantity = product_unit_quantity;
    }

    public String getSerial_no() {
        return serial_no;
    }

    public void setSerial_no(String serial_no) {
        this.serial_no = serial_no;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTax_rate_id() {
        return tax_rate_id;
    }

    public void setTax_rate_id(String tax_rate_id) {
        this.tax_rate_id = tax_rate_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }


    public String getSales_id() {
        return sales_id;
    }

    public void setSales_id(String sales_id) {
        this.sales_id = sales_id;
    }

    public String getSale_reference_no() {
        return sale_reference_no;
    }

    public void setSale_reference_no(String sale_reference_no) {
        this.sale_reference_no = sale_reference_no;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getAdjustmentQuantity() {
        return adjustmentQuantity;
    }

    public void setAdjustmentQuantity(String adjustmentQuantity) {
        this.adjustmentQuantity = adjustmentQuantity;
    }


}
