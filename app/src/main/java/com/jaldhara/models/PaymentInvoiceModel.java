package com.jaldhara.models;

import android.os.Parcel;
import android.os.Parcelable;

public class PaymentInvoiceModel implements Parcelable {

    private String id = "";
    private String compId= "";
    private String date= "";
    private String saleId= "";
    private String returnId= "";
    private String purchaseId= "";
    private String deliveriesReportId= "";
    private String customerInvoicesId= "";
    private String referenceNo= "";
    private String transactionId= "";
    private String paidBy= "";
    private String chequeNo= "";
    private String ccNo= "";
    private String ccHolder= "";
    private String ccMonth= "";
    private String ccYear= "";
    private String ccType= "";
    private String amount= "";
    private String currency= "";
    private String createdBy= "";
    private String attachment= "";
    private String type= "";
    private String note= "";
    private String posPaid= "";
    private String posBalance= "";
    private String approvalCode= "";

    public PaymentInvoiceModel(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSaleId() {
        return saleId;
    }

    public void setSaleId(String saleId) {
        this.saleId = saleId;
    }

    public String getReturnId() {
        return returnId;
    }

    public void setReturnId(String returnId) {
        this.returnId = returnId;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getDeliveriesReportId() {
        return deliveriesReportId;
    }

    public void setDeliveriesReportId(String deliveriesReportId) {
        this.deliveriesReportId = deliveriesReportId;
    }

    public String getCustomerInvoicesId() {
        return customerInvoicesId;
    }

    public void setCustomerInvoicesId(String customerInvoicesId) {
        this.customerInvoicesId = customerInvoicesId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getCcNo() {
        return ccNo;
    }

    public void setCcNo(String ccNo) {
        this.ccNo = ccNo;
    }

    public String getCcHolder() {
        return ccHolder;
    }

    public void setCcHolder(String ccHolder) {
        this.ccHolder = ccHolder;
    }

    public String getCcMonth() {
        return ccMonth;
    }

    public void setCcMonth(String ccMonth) {
        this.ccMonth = ccMonth;
    }

    public String getCcYear() {
        return ccYear;
    }

    public void setCcYear(String ccYear) {
        this.ccYear = ccYear;
    }

    public String getCcType() {
        return ccType;
    }

    public void setCcType(String ccType) {
        this.ccType = ccType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPosPaid() {
        return posPaid;
    }

    public void setPosPaid(String posPaid) {
        this.posPaid = posPaid;
    }

    public String getPosBalance() {
        return posBalance;
    }

    public void setPosBalance(String posBalance) {
        this.posBalance = posBalance;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }


    public PaymentInvoiceModel(Parcel in) {
        id = in.readString();
        compId = in.readString();
        date = in.readString();
        saleId = in.readString();
        returnId = in.readString();
        purchaseId = in.readString();
        deliveriesReportId = in.readString();
        customerInvoicesId = in.readString();
        referenceNo = in.readString();
        transactionId = in.readString();
        paidBy = in.readString();
        chequeNo = in.readString();
        ccNo = in.readString();
        ccHolder = in.readString();
        ccMonth = in.readString();
        ccYear = in.readString();
        ccType = in.readString();
        amount = in.readString();
        currency = in.readString();
        createdBy = in.readString();
        attachment = in.readString();
        type = in.readString();
        note = in.readString();
        posPaid = in.readString();
        posBalance = in.readString();
        approvalCode = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(compId);
        dest.writeString(date);
        dest.writeString(saleId);
        dest.writeString(returnId);
        dest.writeString(purchaseId);
        dest.writeString(deliveriesReportId);
        dest.writeString(customerInvoicesId);
        dest.writeString(referenceNo);
        dest.writeString(transactionId);
        dest.writeString(paidBy);
        dest.writeString(chequeNo);
        dest.writeString(ccNo);
        dest.writeString(ccHolder);
        dest.writeString(ccMonth);
        dest.writeString(ccYear);
        dest.writeString(ccType);
        dest.writeString(amount);
        dest.writeString(currency);
        dest.writeString(createdBy);
        dest.writeString(attachment);
        dest.writeString(type);
        dest.writeString(note);
        dest.writeString(posPaid);
        dest.writeString(posBalance);
        dest.writeString(approvalCode);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PaymentInvoiceModel> CREATOR = new Parcelable.Creator<PaymentInvoiceModel>() {
        @Override
        public PaymentInvoiceModel createFromParcel(Parcel in) {
            return new PaymentInvoiceModel(in);
        }

        @Override
        public PaymentInvoiceModel[] newArray(int size) {
            return new PaymentInvoiceModel[size];
        }
    };
}