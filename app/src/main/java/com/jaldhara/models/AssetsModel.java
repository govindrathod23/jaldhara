package com.jaldhara.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AssetsModel implements Serializable {

    private String assets_paid_status;
    private String assets_status;
    private String biller;
    private int biller_id;
    private int cgst;
    private int comp_id;
    private int created_by;
    private int customer_id;
    private String date;
    private String delivery_date;
    private int driver;
    private int grand_total;
    private String hash;
    private int id;
    private int igst;
    private int order_discount;
    private int order_discount_id;
    private int order_tax;
    private int order_tax_id;
    private int paid;
    private String payment_method;
    private String payment_status;
    private int product_discount;
    private int product_tax;
    private String reference_no;
    private int return_sale_total;
    private int total;
    private int total_discount;
    private int total_tax;

    public String getAssets_paid_status() {
        return assets_paid_status;
    }

    public void setAssets_paid_status(String assets_paid_status) {
        this.assets_paid_status = assets_paid_status;
    }

    public String getAssets_status() {
        return assets_status;
    }

    public void setAssets_status(String assets_status) {
        this.assets_status = assets_status;
    }

    public String getBiller() {
        return biller;
    }

    public void setBiller(String biller) {
        this.biller = biller;
    }

    public int getBiller_id() {
        return biller_id;
    }

    public void setBiller_id(int biller_id) {
        this.biller_id = biller_id;
    }

    public int getCgst() {
        return cgst;
    }

    public void setCgst(int cgst) {
        this.cgst = cgst;
    }

    public int getComp_id() {
        return comp_id;
    }

    public void setComp_id(int comp_id) {
        this.comp_id = comp_id;
    }

    public int getCreated_by() {
        return created_by;
    }

    public void setCreated_by(int created_by) {
        this.created_by = created_by;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDelivery_date() {
        return delivery_date;
    }

    public void setDelivery_date(String delivery_date) {
        this.delivery_date = delivery_date;
    }

    public int getDriver() {
        return driver;
    }

    public void setDriver(int driver) {
        this.driver = driver;
    }

    public int getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(int grand_total) {
        this.grand_total = grand_total;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIgst() {
        return igst;
    }

    public void setIgst(int igst) {
        this.igst = igst;
    }

    public int getOrder_discount() {
        return order_discount;
    }

    public void setOrder_discount(int order_discount) {
        this.order_discount = order_discount;
    }

    public int getOrder_discount_id() {
        return order_discount_id;
    }

    public void setOrder_discount_id(int order_discount_id) {
        this.order_discount_id = order_discount_id;
    }

    public int getOrder_tax() {
        return order_tax;
    }

    public void setOrder_tax(int order_tax) {
        this.order_tax = order_tax;
    }

    public int getOrder_tax_id() {
        return order_tax_id;
    }

    public void setOrder_tax_id(int order_tax_id) {
        this.order_tax_id = order_tax_id;
    }

    public int getPaid() {
        return paid;
    }

    public void setPaid(int paid) {
        this.paid = paid;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public int getProduct_discount() {
        return product_discount;
    }

    public void setProduct_discount(int product_discount) {
        this.product_discount = product_discount;
    }

    public int getProduct_tax() {
        return product_tax;
    }

    public void setProduct_tax(int product_tax) {
        this.product_tax = product_tax;
    }

    public String getReference_no() {
        return reference_no;
    }

    public void setReference_no(String reference_no) {
        this.reference_no = reference_no;
    }

    public int getReturn_sale_total() {
        return return_sale_total;
    }

    public void setReturn_sale_total(int return_sale_total) {
        this.return_sale_total = return_sale_total;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getTotal_discount() {
        return total_discount;
    }

    public void setTotal_discount(int total_discount) {
        this.total_discount = total_discount;
    }

    public int getTotal_tax() {
        return total_tax;
    }

    public void setTotal_tax(int total_tax) {
        this.total_tax = total_tax;
    }
}
