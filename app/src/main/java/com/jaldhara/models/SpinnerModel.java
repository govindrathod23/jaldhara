package com.jaldhara.models;


import android.os.Parcel;
import android.os.Parcelable;

public class SpinnerModel implements Parcelable {

    String routeName = "";

    public SpinnerModel() {

    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    String quantity = "";

    private boolean isSelected = false;

    public String getProductPrice() {
        return ProductPrice;
    }

    public void setProductPrice(String productPrice) {
        ProductPrice = productPrice;
    }

    String latitude = "";

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public boolean isWishItem() {
        return isWishItem;
    }

    public void setWishItem(boolean wishItem) {
        isWishItem = wishItem;
    }

    public boolean isWishItem = false;


    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    String longitude = "";

    String ProductPrice = "";


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    String address = "";

    public String getPromo_price() {
        return promo_price;
    }

    public void setPromo_price(String promo_price) {
        this.promo_price = promo_price;
    }

    String promo_price = "";


    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }


    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    String routeId = "";

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    String image_url = "";

    @Override
    public String toString() {
        return routeName;
    }


    public SpinnerModel(Parcel in) {
        routeName = in.readString();
        quantity = in.readString();
        isSelected = in.readByte() != 0x00;
        latitude = in.readString();
        longitude = in.readString();
        ProductPrice = in.readString();
        address = in.readString();
        routeId = in.readString();
        image_url = in.readString();
        promo_price = in.readString();
        isWishItem = in.readByte() != 0x00;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(routeName);
        dest.writeString(quantity);
        dest.writeByte((byte) (isSelected ? 0x01 : 0x00));
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(ProductPrice);
        dest.writeString(address);
        dest.writeString(routeId);
        dest.writeString(image_url);
        dest.writeString(promo_price);
        dest.writeByte((byte) (isWishItem ? 0x01 : 0x00));
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<SpinnerModel> CREATOR = new Parcelable.Creator<SpinnerModel>() {
        @Override
        public SpinnerModel createFromParcel(Parcel in) {
            return new SpinnerModel(in);
        }

        @Override
        public SpinnerModel[] newArray(int size) {
            return new SpinnerModel[size];
        }
    };
}