package com.jaldhara.models;

public class CustomerCardModel {

    private String date = "";
    private String driver_name = "";
    private String products_name = "";
    private String attachment = "";
    private String return_qty = "";
    private String delivered_qty = "";
    private String balance_qty = "";

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDriver_name() {
        return driver_name;
    }

    public void setDriver_name(String driver_name) {
        this.driver_name = driver_name;
    }

    public String getProducts_name() {
        return products_name;
    }

    public void setProducts_name(String products_name) {
        this.products_name = products_name;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getReturn_qty() {
        return return_qty;
    }

    public void setReturn_qty(String return_qty) {
        this.return_qty = return_qty;
    }

    public String getDelivered_qty() {
        return delivered_qty;
    }

    public void setDelivered_qty(String delivered_qty) {
        this.delivered_qty = delivered_qty;
    }

    public String getBalance_qty() {
        return balance_qty;
    }

    public void setBalance_qty(String balance_qty) {
        this.balance_qty = balance_qty;
    }


}
