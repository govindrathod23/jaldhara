package com.jaldhara.models;

import androidx.annotation.Keep;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.PropertyName;

@IgnoreExtraProperties
public  class Locations {


    public String Latitude;
    public String Longitude;
    public String DriverID;
    public String DriverName;
    public String Time;
    public String Role;


    @PropertyName("Latitude")
    public String getLatitude() {
        return Latitude;
    }

    @PropertyName("Longitude")
    public String getLongitude() {
        return Longitude;
    }

    @PropertyName("DriverID")
    public String getDriverID() {
        return DriverID;
    }

    @PropertyName("DriverName")
    public String getDriverName() {
        return DriverName;
    }

    @PropertyName("Time")
    public String getTime() {
        return Time;
    }

    @PropertyName("Role")
    public String getRole() {
        return Role;
    }

    public Locations() {

    }

    public Locations(String latitude, String longitude, String driverID,String driverName,String time,String role) {
        this.Latitude = latitude;
        this.Longitude = longitude;
        this.DriverID = driverID;
        this.DriverName = driverName;
        this.Time = time;
        this.Role = role;
    }


}
