package com.jaldhara.models;

public class GroupPermissionModel {

    public String id = "";
    public String comp_id = "";
    public String group_id = "";
    public String products_index = "";
    public String products_add = "";
    public String products_edit = "";
    public String products_delete = "";
    public String products_cost = "";
    public String products_price = "";
    public String quotes_index = "";
    public String quotes_add = "";
    public String quotes_edit = "";
    public String quotes_pdf = "";
    public String quotes_email = "";
    public String quotes_delete = "";
    public String sales_index = "";
    public String sales_add = "";
    public String sales_edit = "";
    public String sales_pdf = "";
    public String sales_email = "";
    public String sales_delete = "";
    public String purchases_index = "";
    public String purchases_add = "";
    public String purchases_edit = "";
    public String purchases_pdf = "";
    public String purchases_email = "";
    public String purchases_delete = "";
    public String transfers_index = "";
    public String transfers_add = "";
    public String transfers_edit = "";
    public String transfers_pdf = "";
    public String transfers_email = "";
    public String transfers_delete = "";
    public String customers_index = "";
    public String customers_add = "";
    public String customers_edit = "";
    public String customers_delete = "";
    public String suppliers_index = "";
    public String suppliers_add = "";
    public String suppliers_edit = "";
    public String suppliers_delete = "";
    public String sales_deliveries = "";
    public String sales_add_delivery = "";
    public String sales_edit_delivery = "";
    public String sales_delete_delivery = "";
    public String sales_email_delivery = "";
    public String sales_pdf_delivery = "";
    public String sales_gift_cards = "";
    public String sales_add_gift_card = "";
    public String sales_edit_gift_card = "";
    public String sales_delete_gift_card = "";
    public String pos_index = "";
    public String sales_return_sales = "";
    public String reports_index = "";
    public String reports_warehouse_stock = "";
    public String reports_quantity_alerts = "";
    public String reports_expiry_alerts = "";
    public String reports_products = "";
    public String reports_daily_sales = "";
    public String reports_monthly_sales = "";
    public String reports_sales = "";
    public String reports_payments = "";
    public String reports_purchases = "";
    public String reports_profit_loss = "";
    public String reports_customers = "";
    public String reports_suppliers = "";
    public String reports_staff = "";
    public String reports_register = "";
    public String sales_payments = "";
    public String purchases_payments = "";
    public String purchases_expenses = "";
    public String products_adjustments = "";
    public String bulk_actions = "";
    public String customers_deposits = "";
    public String customers_delete_deposit = "";
    public String products_barcode = "";
    public String purchases_return_purchases = "";
    public String reports_expenses = "";
    public String reports_daily_purchases = "";
    public String reports_monthly_purchases = "";
    public String products_stock_count = "";
    public String edit_price = "";
    public String returns_index = "";
    public String returns_add = "";
    public String returns_edit = "";
    public String returns_delete = "";
    public String returns_email = "";
    public String returns_pdf = "";
    public String reports_tax = "";
    public String sales_invoices_add = "";
    public String sales_invoices_edit = "";
    public String sales_invoices_delete = "";
    public String sales_invoices_pdf = "";
    public String sales_invoices_email = "";
    public String sales_invoices_payments = "";
    public String sales_invoices_index = "";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComp_id() {
        return comp_id;
    }

    public void setComp_id(String comp_id) {
        this.comp_id = comp_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getProducts_index() {
        return products_index;
    }

    public void setProducts_index(String products_index) {
        this.products_index = products_index;
    }

    public String getProducts_add() {
        return products_add;
    }

    public void setProducts_add(String products_add) {
        this.products_add = products_add;
    }

    public String getProducts_edit() {
        return products_edit;
    }

    public void setProducts_edit(String products_edit) {
        this.products_edit = products_edit;
    }

    public String getProducts_delete() {
        return products_delete;
    }

    public void setProducts_delete(String products_delete) {
        this.products_delete = products_delete;
    }

    public String getProducts_cost() {
        return products_cost;
    }

    public void setProducts_cost(String products_cost) {
        this.products_cost = products_cost;
    }

    public String getProducts_price() {
        return products_price;
    }

    public void setProducts_price(String products_price) {
        this.products_price = products_price;
    }

    public String getQuotes_index() {
        return quotes_index;
    }

    public void setQuotes_index(String quotes_index) {
        this.quotes_index = quotes_index;
    }

    public String getQuotes_add() {
        return quotes_add;
    }

    public void setQuotes_add(String quotes_add) {
        this.quotes_add = quotes_add;
    }

    public String getQuotes_edit() {
        return quotes_edit;
    }

    public void setQuotes_edit(String quotes_edit) {
        this.quotes_edit = quotes_edit;
    }

    public String getQuotes_pdf() {
        return quotes_pdf;
    }

    public void setQuotes_pdf(String quotes_pdf) {
        this.quotes_pdf = quotes_pdf;
    }

    public String getQuotes_email() {
        return quotes_email;
    }

    public void setQuotes_email(String quotes_email) {
        this.quotes_email = quotes_email;
    }

    public String getQuotes_delete() {
        return quotes_delete;
    }

    public void setQuotes_delete(String quotes_delete) {
        this.quotes_delete = quotes_delete;
    }

    public String getSales_index() {
        return sales_index;
    }

    public void setSales_index(String sales_index) {
        this.sales_index = sales_index;
    }

    public String getSales_add() {
        return sales_add;
    }

    public void setSales_add(String sales_add) {
        this.sales_add = sales_add;
    }

    public String getSales_edit() {
        return sales_edit;
    }

    public void setSales_edit(String sales_edit) {
        this.sales_edit = sales_edit;
    }

    public String getSales_pdf() {
        return sales_pdf;
    }

    public void setSales_pdf(String sales_pdf) {
        this.sales_pdf = sales_pdf;
    }

    public String getSales_email() {
        return sales_email;
    }

    public void setSales_email(String sales_email) {
        this.sales_email = sales_email;
    }

    public String getSales_delete() {
        return sales_delete;
    }

    public void setSales_delete(String sales_delete) {
        this.sales_delete = sales_delete;
    }

    public String getPurchases_index() {
        return purchases_index;
    }

    public void setPurchases_index(String purchases_index) {
        this.purchases_index = purchases_index;
    }

    public String getPurchases_add() {
        return purchases_add;
    }

    public void setPurchases_add(String purchases_add) {
        this.purchases_add = purchases_add;
    }

    public String getPurchases_edit() {
        return purchases_edit;
    }

    public void setPurchases_edit(String purchases_edit) {
        this.purchases_edit = purchases_edit;
    }

    public String getPurchases_pdf() {
        return purchases_pdf;
    }

    public void setPurchases_pdf(String purchases_pdf) {
        this.purchases_pdf = purchases_pdf;
    }

    public String getPurchases_email() {
        return purchases_email;
    }

    public void setPurchases_email(String purchases_email) {
        this.purchases_email = purchases_email;
    }

    public String getPurchases_delete() {
        return purchases_delete;
    }

    public void setPurchases_delete(String purchases_delete) {
        this.purchases_delete = purchases_delete;
    }

    public String getTransfers_index() {
        return transfers_index;
    }

    public void setTransfers_index(String transfers_index) {
        this.transfers_index = transfers_index;
    }

    public String getTransfers_add() {
        return transfers_add;
    }

    public void setTransfers_add(String transfers_add) {
        this.transfers_add = transfers_add;
    }

    public String getTransfers_edit() {
        return transfers_edit;
    }

    public void setTransfers_edit(String transfers_edit) {
        this.transfers_edit = transfers_edit;
    }

    public String getTransfers_pdf() {
        return transfers_pdf;
    }

    public void setTransfers_pdf(String transfers_pdf) {
        this.transfers_pdf = transfers_pdf;
    }

    public String getTransfers_email() {
        return transfers_email;
    }

    public void setTransfers_email(String transfers_email) {
        this.transfers_email = transfers_email;
    }

    public String getTransfers_delete() {
        return transfers_delete;
    }

    public void setTransfers_delete(String transfers_delete) {
        this.transfers_delete = transfers_delete;
    }

    public String getCustomers_index() {
        return customers_index;
    }

    public void setCustomers_index(String customers_index) {
        this.customers_index = customers_index;
    }

    public String getCustomers_add() {
        return customers_add;
    }

    public void setCustomers_add(String customers_add) {
        this.customers_add = customers_add;
    }

    public String getCustomers_edit() {
        return customers_edit;
    }

    public void setCustomers_edit(String customers_edit) {
        this.customers_edit = customers_edit;
    }

    public String getCustomers_delete() {
        return customers_delete;
    }

    public void setCustomers_delete(String customers_delete) {
        this.customers_delete = customers_delete;
    }

    public String getSuppliers_index() {
        return suppliers_index;
    }

    public void setSuppliers_index(String suppliers_index) {
        this.suppliers_index = suppliers_index;
    }

    public String getSuppliers_add() {
        return suppliers_add;
    }

    public void setSuppliers_add(String suppliers_add) {
        this.suppliers_add = suppliers_add;
    }

    public String getSuppliers_edit() {
        return suppliers_edit;
    }

    public void setSuppliers_edit(String suppliers_edit) {
        this.suppliers_edit = suppliers_edit;
    }

    public String getSuppliers_delete() {
        return suppliers_delete;
    }

    public void setSuppliers_delete(String suppliers_delete) {
        this.suppliers_delete = suppliers_delete;
    }

    public String getSales_deliveries() {
        return sales_deliveries;
    }

    public void setSales_deliveries(String sales_deliveries) {
        this.sales_deliveries = sales_deliveries;
    }

    public String getSales_add_delivery() {
        return sales_add_delivery;
    }

    public void setSales_add_delivery(String sales_add_delivery) {
        this.sales_add_delivery = sales_add_delivery;
    }

    public String getSales_edit_delivery() {
        return sales_edit_delivery;
    }

    public void setSales_edit_delivery(String sales_edit_delivery) {
        this.sales_edit_delivery = sales_edit_delivery;
    }

    public String getSales_delete_delivery() {
        return sales_delete_delivery;
    }

    public void setSales_delete_delivery(String sales_delete_delivery) {
        this.sales_delete_delivery = sales_delete_delivery;
    }

    public String getSales_email_delivery() {
        return sales_email_delivery;
    }

    public void setSales_email_delivery(String sales_email_delivery) {
        this.sales_email_delivery = sales_email_delivery;
    }

    public String getSales_pdf_delivery() {
        return sales_pdf_delivery;
    }

    public void setSales_pdf_delivery(String sales_pdf_delivery) {
        this.sales_pdf_delivery = sales_pdf_delivery;
    }

    public String getSales_gift_cards() {
        return sales_gift_cards;
    }

    public void setSales_gift_cards(String sales_gift_cards) {
        this.sales_gift_cards = sales_gift_cards;
    }

    public String getSales_add_gift_card() {
        return sales_add_gift_card;
    }

    public void setSales_add_gift_card(String sales_add_gift_card) {
        this.sales_add_gift_card = sales_add_gift_card;
    }

    public String getSales_edit_gift_card() {
        return sales_edit_gift_card;
    }

    public void setSales_edit_gift_card(String sales_edit_gift_card) {
        this.sales_edit_gift_card = sales_edit_gift_card;
    }

    public String getSales_delete_gift_card() {
        return sales_delete_gift_card;
    }

    public void setSales_delete_gift_card(String sales_delete_gift_card) {
        this.sales_delete_gift_card = sales_delete_gift_card;
    }

    public String getPos_index() {
        return pos_index;
    }

    public void setPos_index(String pos_index) {
        this.pos_index = pos_index;
    }

    public String getSales_return_sales() {
        return sales_return_sales;
    }

    public void setSales_return_sales(String sales_return_sales) {
        this.sales_return_sales = sales_return_sales;
    }

    public String getReports_index() {
        return reports_index;
    }

    public void setReports_index(String reports_index) {
        this.reports_index = reports_index;
    }

    public String getReports_warehouse_stock() {
        return reports_warehouse_stock;
    }

    public void setReports_warehouse_stock(String reports_warehouse_stock) {
        this.reports_warehouse_stock = reports_warehouse_stock;
    }

    public String getReports_quantity_alerts() {
        return reports_quantity_alerts;
    }

    public void setReports_quantity_alerts(String reports_quantity_alerts) {
        this.reports_quantity_alerts = reports_quantity_alerts;
    }

    public String getReports_expiry_alerts() {
        return reports_expiry_alerts;
    }

    public void setReports_expiry_alerts(String reports_expiry_alerts) {
        this.reports_expiry_alerts = reports_expiry_alerts;
    }

    public String getReports_products() {
        return reports_products;
    }

    public void setReports_products(String reports_products) {
        this.reports_products = reports_products;
    }

    public String getReports_daily_sales() {
        return reports_daily_sales;
    }

    public void setReports_daily_sales(String reports_daily_sales) {
        this.reports_daily_sales = reports_daily_sales;
    }

    public String getReports_monthly_sales() {
        return reports_monthly_sales;
    }

    public void setReports_monthly_sales(String reports_monthly_sales) {
        this.reports_monthly_sales = reports_monthly_sales;
    }

    public String getReports_sales() {
        return reports_sales;
    }

    public void setReports_sales(String reports_sales) {
        this.reports_sales = reports_sales;
    }

    public String getReports_payments() {
        return reports_payments;
    }

    public void setReports_payments(String reports_payments) {
        this.reports_payments = reports_payments;
    }

    public String getReports_purchases() {
        return reports_purchases;
    }

    public void setReports_purchases(String reports_purchases) {
        this.reports_purchases = reports_purchases;
    }

    public String getReports_profit_loss() {
        return reports_profit_loss;
    }

    public void setReports_profit_loss(String reports_profit_loss) {
        this.reports_profit_loss = reports_profit_loss;
    }

    public String getReports_customers() {
        return reports_customers;
    }

    public void setReports_customers(String reports_customers) {
        this.reports_customers = reports_customers;
    }

    public String getReports_suppliers() {
        return reports_suppliers;
    }

    public void setReports_suppliers(String reports_suppliers) {
        this.reports_suppliers = reports_suppliers;
    }

    public String getReports_staff() {
        return reports_staff;
    }

    public void setReports_staff(String reports_staff) {
        this.reports_staff = reports_staff;
    }

    public String getReports_register() {
        return reports_register;
    }

    public void setReports_register(String reports_register) {
        this.reports_register = reports_register;
    }

    public String getSales_payments() {
        return sales_payments;
    }

    public void setSales_payments(String sales_payments) {
        this.sales_payments = sales_payments;
    }

    public String getPurchases_payments() {
        return purchases_payments;
    }

    public void setPurchases_payments(String purchases_payments) {
        this.purchases_payments = purchases_payments;
    }

    public String getPurchases_expenses() {
        return purchases_expenses;
    }

    public void setPurchases_expenses(String purchases_expenses) {
        this.purchases_expenses = purchases_expenses;
    }

    public String getProducts_adjustments() {
        return products_adjustments;
    }

    public void setProducts_adjustments(String products_adjustments) {
        this.products_adjustments = products_adjustments;
    }

    public String getBulk_actions() {
        return bulk_actions;
    }

    public void setBulk_actions(String bulk_actions) {
        this.bulk_actions = bulk_actions;
    }

    public String getCustomers_deposits() {
        return customers_deposits;
    }

    public void setCustomers_deposits(String customers_deposits) {
        this.customers_deposits = customers_deposits;
    }

    public String getCustomers_delete_deposit() {
        return customers_delete_deposit;
    }

    public void setCustomers_delete_deposit(String customers_delete_deposit) {
        this.customers_delete_deposit = customers_delete_deposit;
    }

    public String getProducts_barcode() {
        return products_barcode;
    }

    public void setProducts_barcode(String products_barcode) {
        this.products_barcode = products_barcode;
    }

    public String getPurchases_return_purchases() {
        return purchases_return_purchases;
    }

    public void setPurchases_return_purchases(String purchases_return_purchases) {
        this.purchases_return_purchases = purchases_return_purchases;
    }

    public String getReports_expenses() {
        return reports_expenses;
    }

    public void setReports_expenses(String reports_expenses) {
        this.reports_expenses = reports_expenses;
    }

    public String getReports_daily_purchases() {
        return reports_daily_purchases;
    }

    public void setReports_daily_purchases(String reports_daily_purchases) {
        this.reports_daily_purchases = reports_daily_purchases;
    }

    public String getReports_monthly_purchases() {
        return reports_monthly_purchases;
    }

    public void setReports_monthly_purchases(String reports_monthly_purchases) {
        this.reports_monthly_purchases = reports_monthly_purchases;
    }

    public String getProducts_stock_count() {
        return products_stock_count;
    }

    public void setProducts_stock_count(String products_stock_count) {
        this.products_stock_count = products_stock_count;
    }

    public String getEdit_price() {
        return edit_price;
    }

    public void setEdit_price(String edit_price) {
        this.edit_price = edit_price;
    }

    public String getReturns_index() {
        return returns_index;
    }

    public void setReturns_index(String returns_index) {
        this.returns_index = returns_index;
    }

    public String getReturns_add() {
        return returns_add;
    }

    public void setReturns_add(String returns_add) {
        this.returns_add = returns_add;
    }

    public String getReturns_edit() {
        return returns_edit;
    }

    public void setReturns_edit(String returns_edit) {
        this.returns_edit = returns_edit;
    }

    public String getReturns_delete() {
        return returns_delete;
    }

    public void setReturns_delete(String returns_delete) {
        this.returns_delete = returns_delete;
    }

    public String getReturns_email() {
        return returns_email;
    }

    public void setReturns_email(String returns_email) {
        this.returns_email = returns_email;
    }

    public String getReturns_pdf() {
        return returns_pdf;
    }

    public void setReturns_pdf(String returns_pdf) {
        this.returns_pdf = returns_pdf;
    }

    public String getReports_tax() {
        return reports_tax;
    }

    public void setReports_tax(String reports_tax) {
        this.reports_tax = reports_tax;
    }

    public String getSales_invoices_add() {
        return sales_invoices_add;
    }

    public void setSales_invoices_add(String sales_invoices_add) {
        this.sales_invoices_add = sales_invoices_add;
    }

    public String getSales_invoices_edit() {
        return sales_invoices_edit;
    }

    public void setSales_invoices_edit(String sales_invoices_edit) {
        this.sales_invoices_edit = sales_invoices_edit;
    }

    public String getSales_invoices_delete() {
        return sales_invoices_delete;
    }

    public void setSales_invoices_delete(String sales_invoices_delete) {
        this.sales_invoices_delete = sales_invoices_delete;
    }

    public String getSales_invoices_pdf() {
        return sales_invoices_pdf;
    }

    public void setSales_invoices_pdf(String sales_invoices_pdf) {
        this.sales_invoices_pdf = sales_invoices_pdf;
    }

    public String getSales_invoices_email() {
        return sales_invoices_email;
    }

    public void setSales_invoices_email(String sales_invoices_email) {
        this.sales_invoices_email = sales_invoices_email;
    }

    public String getSales_invoices_payments() {
        return sales_invoices_payments;
    }

    public void setSales_invoices_payments(String sales_invoices_payments) {
        this.sales_invoices_payments = sales_invoices_payments;
    }

    public String getSales_invoices_index() {
        return sales_invoices_index;
    }

    public void setSales_invoices_index(String sales_invoices_index) {
        this.sales_invoices_index = sales_invoices_index;
    }


}
