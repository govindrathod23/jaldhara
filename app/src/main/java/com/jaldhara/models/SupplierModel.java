package com.jaldhara.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SupplierModel {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("comp_id")
    @Expose
    private String compId;
    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("group_name")
    @Expose
    private String groupName;
    @SerializedName("customer_type_id")
    @Expose
    private Object customerTypeId;
    @SerializedName("customer_type_name")
    @Expose
    private Object customerTypeName;
    @SerializedName("customer_group_id")
    @Expose
    private Object customerGroupId;
    @SerializedName("customer_group_name")
    @Expose
    private Object customerGroupName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("alias_name")
    @Expose
    private Object aliasName;
    @SerializedName("company")
    @Expose
    private String company;
    @SerializedName("vat_no")
    @Expose
    private String vatNo;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("postal_code")
    @Expose
    private String postalCode;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("route_id")
    @Expose
    private Object routeId;
    @SerializedName("latitude")
    @Expose
    private Object latitude;
    @SerializedName("longitude")
    @Expose
    private Object longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("cf1")
    @Expose
    private String cf1;
    @SerializedName("cf2")
    @Expose
    private String cf2;
    @SerializedName("cf3")
    @Expose
    private String cf3;
    @SerializedName("cf4")
    @Expose
    private String cf4;
    @SerializedName("cf5")
    @Expose
    private String cf5;
    @SerializedName("cf6")
    @Expose
    private String cf6;
    @SerializedName("days")
    @Expose
    private Object days;
    @SerializedName("products")
    @Expose
    private Object products;
    @SerializedName("invoice_footer")
    @Expose
    private Object invoiceFooter;
    @SerializedName("payment_term")
    @Expose
    private String paymentTerm;
    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("award_points")
    @Expose
    private String awardPoints;
    @SerializedName("deposit_amount")
    @Expose
    private Object depositAmount;
    @SerializedName("price_group_id")
    @Expose
    private Object priceGroupId;
    @SerializedName("price_group_name")
    @Expose
    private Object priceGroupName;
    @SerializedName("gst_no")
    @Expose
    private String gstNo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Object getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(Object customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public Object getCustomerTypeName() {
        return customerTypeName;
    }

    public void setCustomerTypeName(Object customerTypeName) {
        this.customerTypeName = customerTypeName;
    }

    public Object getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(Object customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public Object getCustomerGroupName() {
        return customerGroupName;
    }

    public void setCustomerGroupName(Object customerGroupName) {
        this.customerGroupName = customerGroupName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getAliasName() {
        return aliasName;
    }

    public void setAliasName(Object aliasName) {
        this.aliasName = aliasName;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getVatNo() {
        return vatNo;
    }

    public void setVatNo(String vatNo) {
        this.vatNo = vatNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Object getRouteId() {
        return routeId;
    }

    public void setRouteId(Object routeId) {
        this.routeId = routeId;
    }

    public Object getLatitude() {
        return latitude;
    }

    public void setLatitude(Object latitude) {
        this.latitude = latitude;
    }

    public Object getLongitude() {
        return longitude;
    }

    public void setLongitude(Object longitude) {
        this.longitude = longitude;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCf1() {
        return cf1;
    }

    public void setCf1(String cf1) {
        this.cf1 = cf1;
    }

    public String getCf2() {
        return cf2;
    }

    public void setCf2(String cf2) {
        this.cf2 = cf2;
    }

    public String getCf3() {
        return cf3;
    }

    public void setCf3(String cf3) {
        this.cf3 = cf3;
    }

    public String getCf4() {
        return cf4;
    }

    public void setCf4(String cf4) {
        this.cf4 = cf4;
    }

    public String getCf5() {
        return cf5;
    }

    public void setCf5(String cf5) {
        this.cf5 = cf5;
    }

    public String getCf6() {
        return cf6;
    }

    public void setCf6(String cf6) {
        this.cf6 = cf6;
    }

    public Object getDays() {
        return days;
    }

    public void setDays(Object days) {
        this.days = days;
    }

    public Object getProducts() {
        return products;
    }

    public void setProducts(Object products) {
        this.products = products;
    }

    public Object getInvoiceFooter() {
        return invoiceFooter;
    }

    public void setInvoiceFooter(Object invoiceFooter) {
        this.invoiceFooter = invoiceFooter;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getAwardPoints() {
        return awardPoints;
    }

    public void setAwardPoints(String awardPoints) {
        this.awardPoints = awardPoints;
    }

    public Object getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(Object depositAmount) {
        this.depositAmount = depositAmount;
    }

    public Object getPriceGroupId() {
        return priceGroupId;
    }

    public void setPriceGroupId(Object priceGroupId) {
        this.priceGroupId = priceGroupId;
    }

    public Object getPriceGroupName() {
        return priceGroupName;
    }

    public void setPriceGroupName(Object priceGroupName) {
        this.priceGroupName = priceGroupName;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }


}
