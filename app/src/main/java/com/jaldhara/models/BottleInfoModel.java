package com.jaldhara.models;

import android.os.Parcel;
import android.os.Parcelable;

public class BottleInfoModel implements Parcelable {

    private String bottleIsWith = "";
    private String bottleTotalCount = "";
    private String routeName = "";


    private String routeID = "";
    private String bottleBrand = "";
    private Boolean isHeader = false;
    private String customrsOnRoute = "";
    private String customerID = "";
    private String customerName = "";
    private String alias_name = "";
    private String company = "";
    private String latitude = "";
    private String longitude = "";
    private String phone = "";
    private String email = "";
    private String address = "";
    private String driverName = "";
    private String loaderName = "";

    public String getCustomer_status() {
        return customer_status;
    }

    public void setCustomer_status(String customer_status) {
        this.customer_status = customer_status;
    }

    private String customer_status = "";


    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getLoaderName() {
        return loaderName;
    }

    public void setLoaderName(String loaderName) {
        this.loaderName = loaderName;
    }


    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    public String getRouteID() {
        return routeID;
    }

    public void setRouteID(String routeID) {
        this.routeID = routeID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getAlias_name() {
        return alias_name;
    }

    public void setAlias_name(String alias_name) {
        this.alias_name = alias_name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }


    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }


    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    private String routeCode = "";


    public String getCustomrsOnRoute() {
        return customrsOnRoute;
    }

    public void setCustomrsOnRoute(String customrsOnRoute) {
        this.customrsOnRoute = customrsOnRoute;
    }


    public Boolean isHeader() {
        return isHeader;
    }

    public void setHeader(Boolean header) {
        this.isHeader = header;
    }

    public BottleInfoModel() {

    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getBottleIsWith() {
        return bottleIsWith;
    }

    public void setBottleIsWith(String bottleIsWith) {
        this.bottleIsWith = bottleIsWith;
    }

    public String getBottleTotalCount() {
        return bottleTotalCount;
    }

    public String getBottleBrand() {
        return bottleBrand;
    }

    public void setBottleBrand(String bottleBrand) {
        this.bottleBrand = bottleBrand;
    }


    public void setBottleTotalCount(String bottleTotalCount) {
        this.bottleTotalCount = bottleTotalCount;
    }


    protected BottleInfoModel(Parcel in) {
        bottleIsWith = in.readString();
        bottleTotalCount = in.readString();
        routeName = in.readString();
        bottleBrand = in.readString();
        routeCode = in.readString();
        customrsOnRoute = in.readString();
        customerID = in.readString();

        alias_name = in.readString();
        company = in.readString();
        latitude = in.readString();
        longitude = in.readString();
        customerName = in.readString();
        routeID = in.readString();
        address = in.readString();
        phone = in.readString();
        email = in.readString();
        driverName = in.readString();
        loaderName = in.readString();
        customer_status =   in.readString();

        byte isHeaderVal = in.readByte();
        isHeader = isHeaderVal == 0x02 ? null : isHeaderVal != 0x00;


    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(bottleIsWith);
        dest.writeString(bottleTotalCount);
        dest.writeString(routeName);
        dest.writeString(bottleBrand);
        dest.writeString(routeCode);
        dest.writeString(customrsOnRoute);
        dest.writeString(customerID);

        dest.writeString(alias_name);
        dest.writeString(company);
        dest.writeString(latitude);
        dest.writeString(longitude);
        dest.writeString(customerName);
        dest.writeString(routeID);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(driverName);
        dest.writeString(loaderName);
        dest.writeString(customer_status);

        if (isHeader == null) {
            dest.writeByte((byte) (0x02));
        } else {
            dest.writeByte((byte) (isHeader ? 0x01 : 0x00));
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<BottleInfoModel> CREATOR = new Parcelable.Creator<BottleInfoModel>() {
        @Override
        public BottleInfoModel createFromParcel(Parcel in) {
            return new BottleInfoModel(in);
        }

        @Override
        public BottleInfoModel[] newArray(int size) {
            return new BottleInfoModel[size];
        }
    };
}