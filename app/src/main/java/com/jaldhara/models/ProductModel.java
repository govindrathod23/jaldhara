package com.jaldhara.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ProductModel implements Serializable {

    private String code;
    private String id;
    private String imageUrl;
    private String name;
    private String netPrice;
    private String price;
    private String slug;
    private Object taxMethod;
    private Object taxRate;
    private String type;
    private Unit unit;
    private String unitPrice;
    private String quantity;
    private String returnQuantity;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(String netPrice) {
        this.netPrice = netPrice;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public Object getTaxMethod() {
        return taxMethod;
    }

    public void setTaxMethod(Object taxMethod) {
        this.taxMethod = taxMethod;
    }

    public Object getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Object taxRate) {
        this.taxRate = taxRate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getReturnQuantity() {
        return returnQuantity;
    }

    public void setReturnQuantity(String returnQuantity) {
        this.returnQuantity = returnQuantity;
    }

    public static class Unit implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("comp_id")
        @Expose
        private String compId;
        @SerializedName("code")
        @Expose
        private String code;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("base_unit")
        @Expose
        private Object baseUnit;
        @SerializedName("operator")
        @Expose
        private Object operator;
        @SerializedName("unit_value")
        @Expose
        private Object unitValue;
        @SerializedName("operation_value")
        @Expose
        private Object operationValue;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getCompId() {
            return compId;
        }

        public void setCompId(String compId) {
            this.compId = compId;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Object getBaseUnit() {
            return baseUnit;
        }

        public void setBaseUnit(Object baseUnit) {
            this.baseUnit = baseUnit;
        }

        public Object getOperator() {
            return operator;
        }

        public void setOperator(Object operator) {
            this.operator = operator;
        }

        public Object getUnitValue() {
            return unitValue;
        }

        public void setUnitValue(Object unitValue) {
            this.unitValue = unitValue;
        }

        public Object getOperationValue() {
            return operationValue;
        }

        public void setOperationValue(Object operationValue) {
            this.operationValue = operationValue;
        }

    }

}
