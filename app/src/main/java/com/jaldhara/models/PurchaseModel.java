package com.jaldhara.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class PurchaseModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("comp_id")
    @Expose
    private String compId;
    @SerializedName("reference_no")
    @Expose
    private String referenceNo;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("supplier_id")
    @Expose
    private String supplierId;
    @SerializedName("supplier")
    @Expose
    private String supplier;
    @SerializedName("warehouse_id")
    @Expose
    private String warehouseId;
    @SerializedName("route")
    @Expose
    private String route;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("product_discount")
    @Expose
    private String productDiscount;
    @SerializedName("order_discount_id")
    @Expose
    private String orderDiscountId;
    @SerializedName("order_discount")
    @Expose
    private String orderDiscount;
    @SerializedName("total_discount")
    @Expose
    private String totalDiscount;
    @SerializedName("product_tax")
    @Expose
    private String productTax;
    @SerializedName("order_tax_id")
    @Expose
    private String orderTaxId;
    @SerializedName("order_tax")
    @Expose
    private String orderTax;
    @SerializedName("total_tax")
    @Expose
    private String totalTax;
    @SerializedName("shipping")
    @Expose
    private String shipping;
    @SerializedName("grand_total")
    @Expose
    private String grandTotal;
    @SerializedName("paid")
    @Expose
    private String paid;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("payment_status")
    @Expose
    private String paymentStatus;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("updated_by")
    @Expose
    private String updatedBy;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("attachment")
    @Expose
    private Object attachment;
    @SerializedName("payment_term")
    @Expose
    private String paymentTerm;
    @SerializedName("due_date")
    @Expose
    private Object dueDate;
    @SerializedName("return_id")
    @Expose
    private Object returnId;
    @SerializedName("surcharge")
    @Expose
    private String surcharge;
    @SerializedName("return_purchase_ref")
    @Expose
    private Object returnPurchaseRef;
    @SerializedName("purchase_id")
    @Expose
    private Object purchaseId;
    @SerializedName("return_purchase_total")
    @Expose
    private String returnPurchaseTotal;
    @SerializedName("cgst")
    @Expose
    private String cgst;
    @SerializedName("sgst")
    @Expose
    private String sgst;
    @SerializedName("igst")
    @Expose
    private String igst;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(String supplierId) {
        this.supplierId = supplierId;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public String getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(String warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }

    public String getOrderDiscountId() {
        return orderDiscountId;
    }

    public void setOrderDiscountId(String orderDiscountId) {
        this.orderDiscountId = orderDiscountId;
    }

    public String getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(String orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public String getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(String totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public String getProductTax() {
        return productTax;
    }

    public void setProductTax(String productTax) {
        this.productTax = productTax;
    }

    public String getOrderTaxId() {
        return orderTaxId;
    }

    public void setOrderTaxId(String orderTaxId) {
        this.orderTaxId = orderTaxId;
    }

    public String getOrderTax() {
        return orderTax;
    }

    public void setOrderTax(String orderTax) {
        this.orderTax = orderTax;
    }

    public String getTotalTax() {
        return totalTax;
    }

    public void setTotalTax(String totalTax) {
        this.totalTax = totalTax;
    }

    public String getShipping() {
        return shipping;
    }

    public void setShipping(String shipping) {
        this.shipping = shipping;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Object getAttachment() {
        return attachment;
    }

    public void setAttachment(Object attachment) {
        this.attachment = attachment;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public Object getDueDate() {
        return dueDate;
    }

    public void setDueDate(Object dueDate) {
        this.dueDate = dueDate;
    }

    public Object getReturnId() {
        return returnId;
    }

    public void setReturnId(Object returnId) {
        this.returnId = returnId;
    }

    public String getSurcharge() {
        return surcharge;
    }

    public void setSurcharge(String surcharge) {
        this.surcharge = surcharge;
    }

    public Object getReturnPurchaseRef() {
        return returnPurchaseRef;
    }

    public void setReturnPurchaseRef(Object returnPurchaseRef) {
        this.returnPurchaseRef = returnPurchaseRef;
    }

    public Object getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Object purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getReturnPurchaseTotal() {
        return returnPurchaseTotal;
    }

    public void setReturnPurchaseTotal(String returnPurchaseTotal) {
        this.returnPurchaseTotal = returnPurchaseTotal;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }
}
