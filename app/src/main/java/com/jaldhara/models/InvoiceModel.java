package com.jaldhara.models;

import android.os.Parcel;
import android.os.Parcelable;

public class InvoiceModel implements Parcelable {


    public InvoiceModel() {
    }

    public String id = "";
    public String comp_id = "";
    public String inv_reference_no = "";
    public String biller_id = "";
    public String biller = "";
    public String customer_id = "";
    public String customer = "";
    public String total_item = "";
    public String total_amount = "";
    public String paid = "";
    public String balance = "";
    public String tax = "";
    public String payment_status = "";
    public String paid_by = "";
    public String inv_month = "";
    public String from_date = "";
    public String to_date = "";
    public String note = "";
    public String staff_note = "";
    public String created_at = "";
    public String updated_at = "";
    public String name = "";
    public String route_name = "";

    public String getRoute_name() {
        return route_name;
    }

    public void setRoute_name(String route_name) {
        this.route_name = route_name;
    }

    public String getRoute_id() {
        return route_id;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    public String route_id = "";


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getComp_id() {
        return comp_id;
    }

    public void setComp_id(String comp_id) {
        this.comp_id = comp_id;
    }

    public String getInv_reference_no() {
        return inv_reference_no;
    }

    public void setInv_reference_no(String inv_reference_no) {
        this.inv_reference_no = inv_reference_no;
    }

    public String getBiller_id() {
        return biller_id;
    }

    public void setBiller_id(String biller_id) {
        this.biller_id = biller_id;
    }

    public String getBiller() {
        return biller;
    }

    public void setBiller(String biller) {
        this.biller = biller;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getTotal_item() {
        return total_item;
    }

    public void setTotal_item(String total_item) {
        this.total_item = total_item;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getPaid() {
        return paid;
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getPaid_by() {
        return paid_by;
    }

    public void setPaid_by(String paid_by) {
        this.paid_by = paid_by;
    }

    public String getInv_month() {
        return inv_month;
    }

    public void setInv_month(String inv_month) {
        this.inv_month = inv_month;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getStaff_note() {
        return staff_note;
    }

    public void setStaff_note(String staff_note) {
        this.staff_note = staff_note;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }


    public InvoiceModel(Parcel in) {
        id = in.readString();
        comp_id = in.readString();
        inv_reference_no = in.readString();
        biller_id = in.readString();
        biller = in.readString();
        customer_id = in.readString();
        customer = in.readString();
        total_item = in.readString();
        total_amount = in.readString();
        paid = in.readString();
        balance = in.readString();
        tax = in.readString();
        payment_status = in.readString();
        paid_by = in.readString();
        inv_month = in.readString();
        from_date = in.readString();
        to_date = in.readString();
        note = in.readString();
        staff_note = in.readString();
        created_at = in.readString();
        updated_at = in.readString();
        name = in.readString();
        route_name = in.readString();
        route_id = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(comp_id);
        dest.writeString(inv_reference_no);
        dest.writeString(biller_id);
        dest.writeString(biller);
        dest.writeString(customer_id);
        dest.writeString(customer);
        dest.writeString(total_item);
        dest.writeString(total_amount);
        dest.writeString(paid);
        dest.writeString(balance);
        dest.writeString(tax);
        dest.writeString(payment_status);
        dest.writeString(paid_by);
        dest.writeString(inv_month);
        dest.writeString(from_date);
        dest.writeString(to_date);
        dest.writeString(note);
        dest.writeString(staff_note);
        dest.writeString(created_at);
        dest.writeString(updated_at);
        dest.writeString(name);
        dest.writeString(route_name);
        dest.writeString(route_id);

    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<InvoiceModel> CREATOR = new Parcelable.Creator<InvoiceModel>() {
        @Override
        public InvoiceModel createFromParcel(Parcel in) {
            return new InvoiceModel(in);
        }

        @Override
        public InvoiceModel[] newArray(int size) {
            return new InvoiceModel[size];
        }
    };
}