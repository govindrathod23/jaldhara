package com.jaldhara.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class PaymentModel implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("comp_id")
    @Expose
    private String compId;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("sale_id")
    @Expose
    private Object saleId;
    @SerializedName("return_id")
    @Expose
    private Object returnId;
    @SerializedName("purchase_id")
    @Expose
    private String purchaseId;
    @SerializedName("deliveries_report_id")
    @Expose
    private String deliveriesReportId;
    @SerializedName("customer_invoices_id")
    @Expose
    private String customerInvoicesId;
    @SerializedName("reference_no")
    @Expose
    private String referenceNo;
    @SerializedName("transaction_id")
    @Expose
    private Object transactionId;
    @SerializedName("paid_by")
    @Expose
    private String paidBy;
    @SerializedName("cheque_no")
    @Expose
    private String chequeNo;
    @SerializedName("cc_no")
    @Expose
    private String ccNo;
    @SerializedName("cc_holder")
    @Expose
    private String ccHolder;
    @SerializedName("cc_month")
    @Expose
    private String ccMonth;
    @SerializedName("cc_year")
    @Expose
    private String ccYear;
    @SerializedName("cc_type")
    @Expose
    private String ccType;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("currency")
    @Expose
    private Object currency;
    @SerializedName("created_by")
    @Expose
    private String createdBy;
    @SerializedName("attachment")
    @Expose
    private Object attachment;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("pos_paid")
    @Expose
    private String posPaid;
    @SerializedName("pos_balance")
    @Expose
    private String posBalance;
    @SerializedName("approval_code")
    @Expose
    private Object approvalCode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Object getSaleId() {
        return saleId;
    }

    public void setSaleId(Object saleId) {
        this.saleId = saleId;
    }

    public Object getReturnId() {
        return returnId;
    }

    public void setReturnId(Object returnId) {
        this.returnId = returnId;
    }

    public String getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(String purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getDeliveriesReportId() {
        return deliveriesReportId;
    }

    public void setDeliveriesReportId(String deliveriesReportId) {
        this.deliveriesReportId = deliveriesReportId;
    }

    public String getCustomerInvoicesId() {
        return customerInvoicesId;
    }

    public void setCustomerInvoicesId(String customerInvoicesId) {
        this.customerInvoicesId = customerInvoicesId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public Object getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(Object transactionId) {
        this.transactionId = transactionId;
    }

    public String getPaidBy() {
        return paidBy;
    }

    public void setPaidBy(String paidBy) {
        this.paidBy = paidBy;
    }

    public String getChequeNo() {
        return chequeNo;
    }

    public void setChequeNo(String chequeNo) {
        this.chequeNo = chequeNo;
    }

    public String getCcNo() {
        return ccNo;
    }

    public void setCcNo(String ccNo) {
        this.ccNo = ccNo;
    }

    public String getCcHolder() {
        return ccHolder;
    }

    public void setCcHolder(String ccHolder) {
        this.ccHolder = ccHolder;
    }

    public String getCcMonth() {
        return ccMonth;
    }

    public void setCcMonth(String ccMonth) {
        this.ccMonth = ccMonth;
    }

    public String getCcYear() {
        return ccYear;
    }

    public void setCcYear(String ccYear) {
        this.ccYear = ccYear;
    }

    public String getCcType() {
        return ccType;
    }

    public void setCcType(String ccType) {
        this.ccType = ccType;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Object getCurrency() {
        return currency;
    }

    public void setCurrency(Object currency) {
        this.currency = currency;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Object getAttachment() {
        return attachment;
    }

    public void setAttachment(Object attachment) {
        this.attachment = attachment;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPosPaid() {
        return posPaid;
    }

    public void setPosPaid(String posPaid) {
        this.posPaid = posPaid;
    }

    public String getPosBalance() {
        return posBalance;
    }

    public void setPosBalance(String posBalance) {
        this.posBalance = posBalance;
    }

    public Object getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(Object approvalCode) {
        this.approvalCode = approvalCode;
    }

}

