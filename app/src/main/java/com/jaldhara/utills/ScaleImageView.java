package com.jaldhara.utills;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.jaldhara.R;

/**
 * create by ts_xiaoa
 * create Time 2018/9/11
   * Description: You can set the aspect ratio of ImageView
 */
public class ScaleImageView extends AppCompatImageView {
 
         // height: width = heightScale 0f not treated
    private float heightScale = 0f;
         // width: height = widthScale 0f not treated
    private float widthScale = 0f;
 
    public ScaleImageView(Context context) {
        super(context);
    }
 
    /**
           * Constructor two parameters when using the controls in the xml file in a callback
           * We use here is the xml directly so we have to do here write initialization, such as initializing the aspect ratio
     *
     * @param context
     * @param attrs
     */
    public ScaleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
                 // by context.obtainStyledAttributes, obtained TypedArray object can be obtained a value corresponding to the custom attribute
                 //context.obtainStyledAttributes The first parameter is the constructor of attrs
                 //context.obtainStyledAttributes The first parameter is your custom attributes
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ScaleImageView);
                 // get the value widthScale attribute xml file
        widthScale = typedArray.getFloat(R.styleable.ScaleImageView_widthScale, 0f);
                 // get the value heightScale attribute xml file
        heightScale = typedArray.getFloat(R.styleable.ScaleImageView_heightScale, 0f);
                 // objects recovered typedArray
        typedArray.recycle();
    }
    
    public ScaleImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ScaleImageView);
        widthScale = typedArray.getFloat(R.styleable.ScaleImageView_widthScale, 0f);
        heightScale = typedArray.getFloat(R.styleable.ScaleImageView_heightScale, 0f);
        typedArray.recycle();
    }
 
    /**
           * Measurement control, measurement rules which implement controls,
           * And finally call setMeasuredDimension in the method (widthSize, heightSize)
           * Or super.onMeasure (widthMeasureSpec, heightMeasureSpec)
           * Save the final measurement of the size of the controls
     *
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
                 // default measurement rules
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
                 // only restriction is provided a developer ratio (ratio must have a reference to the actual size, such as width 200, heightScale = 1f, then the height value = 200)
        if (widthScale != 0f && heightScale != 0f) {
                         throw new IllegalArgumentException ( "aspect ratio can only set a");
        }
                 // default obtained measured width measurement rule
        int measuredWidth = getMeasuredWidth();
                 // get measured default height measuring rule
        int measuredHeight = getMeasuredHeight();
                 // determine the aspect ratio set by the developer
        if (widthScale != 0) {
                         // set by the developer is the ratio of the width, the actual width is calculated by the ratio of the actual height and
            measuredWidth = (int) (widthScale * measuredHeight);
                         // Save the measurement of final results setMeasuredDimension () or super.onMeasure ()
            setMeasuredDimension(measuredWidth, measuredHeight);
//            super.onMeasure(MeasureSpec.makeMeasureSpec(measuredWidth, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(measuredHeight, MeasureSpec.EXACTLY));
        } else if (heightScale != 0) {
                         // set by the developer is the ratio of the height, calculated by the ratio of the actual height and width of the actual
            measuredHeight = (int) (heightScale * measuredWidth);
                         // Save the measurement of final results setMeasuredDimension () or super.onMeasure ()
            setMeasuredDimension(measuredWidth, measuredHeight);
//            super.onMeasure(MeasureSpec.makeMeasureSpec(measuredWidth, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(measuredHeight, MeasureSpec.EXACTLY));
        }
    }
}