package com.jaldhara.utills;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.core.content.pm.PackageInfoCompat;
import androidx.fragment.app.Fragment;

import com.jaldhara.JalDharaApplication;

import org.jsoup.Jsoup;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class Util {


    public static String API_KEY = "s4sk44gw4480owk08gko0w0okkswggk0sgkwk44k";


    public static float GEOFENCE_RADIUS = 100.0f; // in meters
    public static String KEY_INTENT_CUSTOMER_MODEL = "CUSTOMER_MODEL";
    public static int JOB_ID_UPDATE_DRIVER_LOCATION = 1;
    public static int JOB_ID_GET_DRIVERS_LOCATION_FOR_ADMIN = 2;
    public static int JOB_ID_GET_DRIVER_LOCATION_FOR_CUSTOMER = 3;
    public static final long JOB_SCHEDULE_TIME = 15 * 1000;
    public static String IMAGE_PATH = "/storage/emulated/0/Pictures/JalDhara_Signature.png";
    public static String DATE_FORMATE_1 = "dd MMM yyyy / hh:mm:aa";
    public static String DATE_FORMATE_2 = "yyyy-MM-dd";


    public static String API_GET_ORDER = "get_order";
    public static String API_GET_PRODUCT = "get_product";

    //By Pratik
    public static String API_GET_ASSET = "get_all_assets";

    public static String API_ADD_ORDER = "add_order";
    public static String API_LIST_OF_ROUTES = "list_of_routes";
    public static final String API_VIEW_EXPENSES = "view";
    public static final String API_DELETE_EXPENSE = "delete";
    public static final String API_SAVE_EXPENSE = "save";
    public static final String API_UPDATE_EXPENSE = "update";
    public static final String API_LIST_OF_WAREHOUSE = "get_all_warehouses";
    public static final String API_LIST_OF_CATEGORY = "get_all_category";
    public static final String API_LIST_OF_PURCHASES = "get";
    public static final String API_DELETE_PURCHASE = "delete";
    public static final String API_ADD_PAYMENT = "add_payment";
    public static final String API_EDIT_PAYMENT = "edit_payment";
    public static final String API_LIST_OF_PAYMENT = "payment";
    public static final String API_DELETE_PAYMENT = "delete_payment";
    public static final String API_LIST_OF_SUPPLIER = "get_all_supplier";
    public static final String API_ADD_SUPPLIER = "add_suppliers";
    public static final String API_ADD_PRODUCT = "add_product";
    public static final String API_ADD_PURCHASE = "add";
    public static final String API_VIEW_PURCHASE = "view";
    public static final String API_LIST_PRODUCT_TYPE = "product_type";
    public static final String API_LIST_OF_BRANDS = "list_of_brands";
    public static final String API_LIST_OF_SUBCATEGORY = "get_sub_category";

    public static final String API_GET_SALES_BY_ID = "get_sales_by_id";

    public static final String API_ADD_RETURN = "add_return";

    public static final String API_LIST_OF_INVOICE = "get";
    public static final String API_INVOICE_DETAILS = "invoice_detail";
    public static final String API_ADD_INVOICE = "add_invoice";
    public static final String API_DELETE_INVOICE = "delete";
    public static final String API_EDIT_INVOICE = "edit_invoice";

    public static final String API_ADMIN_COUNTER = "admin_counter";
    public static final String API_CUSTOMER_COUNTER = "customer_counter";

    public static String API_CUSTOMER_CARD = "customer_card";

    public static String API_GET_WISHLIST = "get_wishlists";
    public static String API_ADD_WISHLIST = "add_wishlist";
    public static String API_REMOVE_WISHLIST = "remove_wishlist";

    public static String API_BRAND_LIST = "get_brands";
    public static String API_CATEGORY_LIST = "get_categories";


//    public static String API_ = "";
//    public static String API_ = "";
//    public static String API_ = "";


    /**
     * API NAMES
     */

    public static final String METHOD_PROFIT_LOSS = "profit_loss";

    public static boolean IS_TEST = true;

    public static boolean checkConnection(Context mContext) {
        NetworkInfo info = ((ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE))
                .getActiveNetworkInfo();
        if (info == null || !info.isConnected()) {
            return false;
        }
        if (info.isRoaming()) {
            return true;
        }
        return true;
    }


    public static boolean isEmailValid(String email) {
        return (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches());
    }


    public static String getBase64FromImage(String image) {
        try {
            Bitmap bm = BitmapFactory.decodeFile(image);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 40, baos); //bm is the bitmap object
            byte[] b = baos.toByteArray();
            return Base64.encodeToString(b, Base64.DEFAULT);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static void hideSoftKeyboardFragment(Fragment mFragment) {
        try {
            if (mFragment == null || mFragment.getActivity() == null) {
                return;
            }
            View view = mFragment.getActivity().getCurrentFocus();
            if (view != null) {
                InputMethodManager inputManager = (InputMethodManager) mFragment.getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static HashMap<Integer, String> getMonth() {

        HashMap<Integer, String> hashMap = new HashMap<>();
        hashMap.put(0, "JAN");
        hashMap.put(1, "FEB");
        hashMap.put(2, "MAR");
        hashMap.put(3, "APR");
        hashMap.put(4, "MAY");
        hashMap.put(5, "JUN");
        hashMap.put(6, "JUL");
        hashMap.put(7, "AUG");
        hashMap.put(8, "SEP");
        hashMap.put(9, "OCT");
        hashMap.put(10, "NOV");
        hashMap.put(11, "DEC");
        return hashMap;
    }

    public static HashMap<String, Integer> getMonthFromName() {

        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("JAN", 0);
        hashMap.put("FEB", 1);
        hashMap.put("MAR", 2);
        hashMap.put("APR", 3);
        hashMap.put("MAY", 4);
        hashMap.put("JUN", 5);
        hashMap.put("JUL", 6);
        hashMap.put("AUG", 7);
        hashMap.put("SEP", 8);
        hashMap.put("OCT", 9);
        hashMap.put("NOV", 10);
        hashMap.put("DEC", 11);
        return hashMap;
    }


    public static String getTodayDate() {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(today);
    }

    public static String getCurrentDateWithTime() {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(today);
    }


    public static Calendar getCalDateFromString(String strDate) {

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date = sdf.parse(strDate);
            calendar.setTime(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return calendar;
    }

    public static String formatDateFromDateString(String inputDateFormat, String outputDateFormat,
                                                  String inputDate) throws ParseException {
        Date mParsedDate;
        String mOutputDateString;
        SimpleDateFormat mInputDateFormat = new SimpleDateFormat(inputDateFormat, java.util.Locale.getDefault());
        SimpleDateFormat mOutputDateFormat = new SimpleDateFormat(outputDateFormat, java.util.Locale.getDefault());
        mParsedDate = mInputDateFormat.parse(inputDate);
        mOutputDateString = mOutputDateFormat.format(mParsedDate);
        return mOutputDateString;
    }

    public static String htmlToText(String html) {
        return Jsoup.parse(html).text();
    }


    public static int getAppVersion(Context c) {
        int versionFromApp = 0;

        Context context = c.getApplicationContext();
        PackageManager manager = context.getPackageManager();
        String myversionName = "0";

        try {
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            myversionName = info.versionName;
            versionFromApp = (int) PackageInfoCompat.getLongVersionCode(info);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return versionFromApp;
    }

    public static String getAppVersionName(Context c) {
        String myversionName = "1.0";
        Context context = c.getApplicationContext();
        PackageManager manager = context.getPackageManager();
        try {
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            myversionName = info.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return myversionName;
    }

    public static void showDialog(Context c, String title, String message) {
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }


    public static double getDistanceFromLatLonInMeter(double lat1, double lon1, double lat2, double lon2) {
//        double R = 6371; // Radius of the earth in km
//        double dLat = deg2rad(lat2 - lat1);  // deg2rad below
//        double dLon = deg2rad(lon2 - lon1);
//        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
//                   Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
//                   Math.sin(dLon / 2) * Math.sin(dLon / 2);
//        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//        double d = (R * c) * 1000; // Distance in km to meter
//        return d;
        double theta = lon1 - lon2;
        double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
        dist = Math.acos(dist);
        dist = Math.toDegrees(dist);
        dist = dist * 60 * 1.1515;
        dist = dist * 1.609344;

        return (dist * 1000);//1000 multiply is for convert km to meter

    }

    private static double deg2rad(double deg) {
        return deg * (Math.PI / 180);
    }

    @SuppressLint("InvalidAnalyticsName")
    public static void analyticsLog(String activityName, String exceptionMsg) {
        Bundle params = new Bundle();
        params.putString("Activity Name", activityName);
        params.putString("Exception Msg", exceptionMsg);
        JalDharaApplication.getInstance().getFirebaseAnalytics().logEvent("Crash", params);
    }

}
