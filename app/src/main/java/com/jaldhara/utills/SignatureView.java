package com.jaldhara.utills;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.media.MediaScannerConnection;
import android.os.Build;
import android.os.Environment;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.jaldhara.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class SignatureView extends View {

    // set the stroke width
    private static final float STROKE_WIDTH = 5f;
    private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;

    private Paint paint = new Paint();
    private Path path = new Path();

    private float lastTouchX;
    private float lastTouchY;
    private final RectF dirtyRect = new RectF();
    private boolean isSignatureOk = false;

    public SignatureView(Context context) {
        super(context);
        initParams();
    }


    public SignatureView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initParams();

    }

    public SignatureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initParams();
    }

    private void initParams() {
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(STROKE_WIDTH);

        // set the bg color as white
        this.setBackgroundColor(Color.WHITE);

        // width and height should cover the screen
        this.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
    }

    /**
     * Get signature
     *
     * @return
     */
    public Bitmap getSignature() {

        Bitmap signatureBitmap = null;

        // set the signature bitmap
        if (signatureBitmap == null) {
            signatureBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.RGB_565);
        }

        // important for saving signature
        final Canvas canvas = new Canvas(signatureBitmap);
        this.draw(canvas);

        return signatureBitmap;
    }

    public static File getSignatireFile(Context context) {
//        File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String root = Environment.getExternalStorageDirectory().toString();

        // the directory where the signature will be saved
        File directory = new File(root + File.separator+context.getResources().getString(R.string.folder_name));

        // make the directory if it does not exist yet
        if (!directory.exists()) {
            directory.mkdirs();
        }

        return new File(directory, "Signature.png");
    }

    @SuppressLint("WrongThread")
    public void saveSignature(Context context) {

        File file = getSignatireFile(context);

        FileOutputStream out = null;
        Bitmap bitmap = getSignature();
        try {
            out = new FileOutputStream(file);
            if (bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);
            } else {
                throw new FileNotFoundException();
            }
        } catch (Exception e) {
            e.printStackTrace();
            //Util.showDialog(context, "Driver error", "saveSignature msg : " + e.getMessage() + "\n\n\n****only e" + e);
        } finally {
            try {
                if (out != null) {
                    out.flush();
                    out.close();

                    if (bitmap != null) {
                        //Toast.makeText(context, "Image saved successfully at " + file.getPath(), Toast.LENGTH_LONG).show();
//                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
//                            // new MyMediaScanner(this, file);
//                        } else {
                        ArrayList<String> toBeScanned = new ArrayList<String>();
                        toBeScanned.add(file.getAbsolutePath());
                        String[] toBeScannedStr = new String[toBeScanned.size()];
                        toBeScannedStr = toBeScanned.toArray(toBeScannedStr);
                        MediaScannerConnection.scanFile(context, toBeScannedStr, null,
                                null);
//                        }
                    }
                }
            }   catch (Exception e) {

                //Util.showDialog(context, "Driver error", "saveSignature msg : " + e.getMessage() + "\n\n\n****only e" + e);
                e.printStackTrace();
            }
        }


    }

    final void saveImage(Bitmap signature) {

        String root = Environment.getExternalStorageDirectory().toString();

        // the directory where the signature will be saved
        File myDir = new File(root + "/saved_signature");

        // make the directory if it does not exist yet
        if (!myDir.exists()) {
            myDir.mkdirs();
        }

        // set the file name of your choice
        String fname = "signature.png";

        // in our case, we delete the previous file, you can remove this
        File file = new File(myDir, fname);
        if (file.exists()) {
            file.delete();
        }

        try {

            // save the signature
            FileOutputStream out = new FileOutputStream(file);
            signature.compress(Bitmap.CompressFormat.PNG, 90, out);
            out.flush();
            out.close();

            Toast.makeText(this.getContext(), "Signature saved.", Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * clear signature canvas
     */
    public void clearSignature() {
        isSignatureOk = false;
        path.reset();
        this.invalidate();
    }

    public boolean isSignatureOk() {

        return isSignatureOk;

    }

    // all touch events during the drawing
    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(this.path, this.paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();
        isSignatureOk = true;

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                path.moveTo(eventX, eventY);

                lastTouchX = eventX;
                lastTouchY = eventY;
                return true;

            case MotionEvent.ACTION_MOVE:

            case MotionEvent.ACTION_UP:

                resetDirtyRect(eventX, eventY);
                int historySize = event.getHistorySize();
                for (int i = 0; i < historySize; i++) {
                    float historicalX = event.getHistoricalX(i);
                    float historicalY = event.getHistoricalY(i);

                    expandDirtyRect(historicalX, historicalY);
                    path.lineTo(historicalX, historicalY);
                }
                path.lineTo(eventX, eventY);
                break;

            default:

                return false;
        }

        invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

        lastTouchX = eventX;
        lastTouchY = eventY;

        return true;
    }

    private void expandDirtyRect(float historicalX, float historicalY) {
        if (historicalX < dirtyRect.left) {
            dirtyRect.left = historicalX;
        } else if (historicalX > dirtyRect.right) {
            dirtyRect.right = historicalX;
        }

        if (historicalY < dirtyRect.top) {
            dirtyRect.top = historicalY;
        } else if (historicalY > dirtyRect.bottom) {
            dirtyRect.bottom = historicalY;
        }
    }

    private void resetDirtyRect(float eventX, float eventY) {
        dirtyRect.left = Math.min(lastTouchX, eventX);
        dirtyRect.right = Math.max(lastTouchX, eventX);
        dirtyRect.top = Math.min(lastTouchY, eventY);
        dirtyRect.bottom = Math.max(lastTouchY, eventY);
    }

}