package com.jaldhara.view;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.jaldhara.JalDharaApplication;
import com.jaldhara.R;
import com.jaldhara.adapters.ExpandableListAdapter;
import com.jaldhara.adapters.RouteBottleListAdapter;
import com.jaldhara.models.BottleInfoModel;
import com.jaldhara.services.ApiClient;
import com.jaldhara.services.ApiInterface;
import com.jaldhara.utills.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteListActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ArrayList<BottleInfoModel> arrayList;
    private FrameLayout containerFragment;

    //----------------------------------------------------------------------------------
//    ExpandableListAdapter listAdapter;
//    ExpandableListView expListView;
//    List<String> listDataHeader;
//    HashMap<String, List<String>> listDataChild;
    //----------------------------------------------------------------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


//        containerFragment = (FrameLayout) findViewById(R.id.container_fragment);

//        ((TextView)findViewById(R.id.slide_bar_txt_userName)).setText(JalDharaApplication.getInstance().getSharedPreferance(getString(R.string.key_prefrence_user_name)));
        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        mLayoutManager = new LinearLayoutManager(this);
//        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.setLayoutManager(mLayoutManager);

        arrayList = new ArrayList<>();
//        prepareArrayList("ICICI Bank", "10", "Gota");
//        prepareArrayList("Reliance Mall", "50", "Satalite");
//        prepareArrayList("Adani House", "40", "Satadhar");
//        prepareArrayList("Mangalam Store", "15", "Bapunagar");
//        prepareArrayList("Royal Villa", "9", "Maninagar");

//        prepareArrayList();


//        mAdapter = new RouteBottleListAdapter(RouteListActivity.this, arrayList);
//        mRecyclerView.setAdapter(mAdapter);

//        init();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        serviceListOfCustomer();
    }

//    private void init() {
//
//        // get the listview
//        expListView = (ExpandableListView) findViewById(R.id.lvExp);
//
//        // preparing list data
////        prepareListData();
//        listDataHeader = new ArrayList<String>();
//        listDataChild = new HashMap<String, List<String>>();
//
//
//
//        // Listview Group click listener
//        expListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
//
//            @Override
//            public boolean onGroupClick(ExpandableListView parent, View v,
//                                        int groupPosition, long id) {
//                 Toast.makeText(getApplicationContext(),
//                 "Group Clicked " + listDataHeader.get(groupPosition),
//                 Toast.LENGTH_SHORT).show();
//                return false;
//            }
//        });
//
//        // Listview Group expanded listener
//        expListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
//
//            @Override
//            public void onGroupExpand(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Expanded",
//                        Toast.LENGTH_SHORT).show();
//            }
//        });
//
//        // Listview Group collasped listener
//        expListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
//
//            @Override
//            public void onGroupCollapse(int groupPosition) {
//                Toast.makeText(getApplicationContext(),
//                        listDataHeader.get(groupPosition) + " Collapsed",
//                        Toast.LENGTH_SHORT).show();
//
//            }
//        });
//
//        // Listview on child click listener
//        expListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
//
//            @Override
//            public boolean onChildClick(ExpandableListView parent, View v,
//                                        int groupPosition, int childPosition, long id) {
//                // TODO Auto-generated method stub
//                Toast.makeText(
//                        getApplicationContext(),
//                        listDataHeader.get(groupPosition)
//                                + " : "
//                                + listDataChild.get(
//                                listDataHeader.get(groupPosition)).get(
//                                childPosition), Toast.LENGTH_SHORT)
//                        .show();
//                return false;
//            }
//        });
//
//

//
//
//    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            askForLogout();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void askForLogout() {

        AlertDialog.Builder builder = new AlertDialog.Builder(RouteListActivity.this);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure, you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                serviceLogout();
//                serviceListOfRoutes();


//                serviceGetBrandsForShow();
            }
        });
        builder.show();

    }

    private void serviceLogout() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Signing off", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.logoutAuth("logout", Util.API_KEY);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
//                List<Movie> movies = response.body().getResults();
//                    Log.d("", "Number of movies received: ");
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.optJSONObject("data");

                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_isLogin), "");
                        Intent intent = new Intent(RouteListActivity.this, LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(RouteListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(RouteListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfRoutes() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Routes", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfRoutes("list_of_routes", Util.API_KEY);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
//                List<Movie> movies = response.body().getResults();
//                    Log.d("", "Number of movies received: ");
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.optJSONObject("data");

                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_isLogin), "");
//                        Intent intent = new Intent(RouteListActivity.this, LoginActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(RouteListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(RouteListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void serviceGetBrandsForShow() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "list_of_brands  ", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.showBrands("list_of_brands", Util.API_KEY);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
//                List<Movie> movies = response.body().getResults();
//                    Log.d("", "Number of movies received: ");
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONObject jsonData = jsonResponse.optJSONObject("data");

                        JalDharaApplication.getInstance().setSharedPreferance(getString(R.string.key_prefrence_isLogin), "");
//                        Intent intent = new Intent(RouteListActivity.this, LoginActivity.class);
//                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        startActivity(intent);
//                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(RouteListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(RouteListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void serviceListOfCustomer() {
        final ProgressDialog dialog = ProgressDialog.show(this, "Please wait", "Getting Customer", false, false);
        ApiInterface apiService =
                ApiClient.getClient().create(ApiInterface.class);

        Call<ResponseBody> call = apiService.listOfCustomer("routes_wise_customer", Util.API_KEY);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                dialog.dismiss();
                try {

                    if (response.isSuccessful()) {
                        JSONObject jsonResponse = new JSONObject(response.body().string());
                        JSONArray jsonArray = jsonResponse.optJSONArray("data");

                        for (int header = 0; header < jsonArray.length(); header++) {
                            BottleInfoModel modelHeader = new BottleInfoModel();
                            modelHeader.setRouteName(jsonArray.optJSONObject(header).optString("name"));
//                             listDataHeader.add(modelHeader);
                            modelHeader.setHeader(true);


                            JSONArray jsonArrayChild = jsonArray.optJSONObject(header).optJSONArray("customer");
                            modelHeader.setCustomrsOnRoute(jsonArrayChild.length() + "");
                            arrayList.add(modelHeader);


//                            ArrayList<BottleInfoModel> array =new ArrayList<>();
                            for (int child = 0; child < jsonArrayChild.length(); child++) {
                                BottleInfoModel modelChild = new BottleInfoModel();
//
                                JSONObject jsonObjectChild = jsonArrayChild.optJSONObject(child);
                                modelChild.setBottleIsWith(jsonObjectChild.optString("company"));
                                modelChild.setBottleTotalCount(jsonObjectChild.optString("id"));
//                                 array.add(modelChild);


//                             listDataHeader.add(modelHeader);
                                modelChild.setHeader(false);
                                arrayList.add(modelChild);


                            }


//                             listDataChild.put(listDataHeader.get(header).getRouteName(), array);
                        }

                        mAdapter = new RouteBottleListAdapter(RouteListActivity.this, arrayList);
                        mRecyclerView.setAdapter(mAdapter);

//                        listAdapter = new ExpandableListAdapter(RouteListActivity.this, listDataHeader, listDataChild);
//                        expListView.setAdapter(listAdapter);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    Toast.makeText(RouteListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                dialog.dismiss();
                // Log error here since request failed
                Log.e("", t.toString());
                Toast.makeText(RouteListActivity.this, "Please try after some time!", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
